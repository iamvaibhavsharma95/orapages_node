FROM node:14

WORKDIR /app

COPY package.json /app

RUN mkdir banners post_images banners bulk_attacments company_images csvs post_imagespost_videos sitemaps

RUN npm install

RUN npm install -g nodemon

COPY . /app

ENV DOCKERIZE_VERSION v0.6.0
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz


EXPOSE 9000

CMD dockerize -wait tcp://mysql:3306 -timeout 60m npm run dev