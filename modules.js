const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const xss = require("xss-clean");
const csvtojson = require("csvtojson");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const request = require("request");
const cookieParser = require("cookie-parser");
const routes = require("./routes/admin_server");
// const appRouter = require("./routes/app_index");
const ownerRoutes = require("./routes/owner_server");
// const mailer = require('nodemailer');
const fs = require("fs");
const fse = require("fs-extra");
// const request_ip = require('request-ip');
const expressip = require("express-ip");
const session = require("express-session");
// const premiumRoutes = require("./routes/premium_profile_subsciption");
const googleAnalytics = require("./routes/google_analytics");
const premiumRoutes = require("./routes/premium_profile");
const sitemaps = require("./routes/sitemap");
const postRoutes = require("./business_network/bn_routers/post_router");
const notificationRoutes = require("./business_network/bn_routers/notification_router");
const timeLineRoutes = require("./business_network/bn_routers/timeline_router");
const friendRequestRoutes = require("./business_network/bn_routers/friend_request_router");
const friendRoutes = require("./business_network/bn_routers/friend_router");
const commentsRoutes = require("./business_network/bn_routers/comment_router");
const likesRoutes = require("./business_network/bn_routers/likes_router");
const shareRouter = require("./business_network/bn_routers/share_router");
const suggestionRouter = require("./business_network/bn_routers/suggestion_router");
const followRouter = require("./business_network/bn_routers/follow_router");
const onBoardingRouter = require("./business_network/bn_routers/user_onboarding_router");
const ReportRouter = require("./business_network/bn_routers/report_router");
// require('./routes/databasesql');
// require('./routes/database_mirgration');

app.use(cookieParser());

app.use(
  session({
    secret: "orapages",
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 2 * 30 * 24 * 60 * 60 * 1000,
    },
  })
);

app.use(
  bodyParser.json({
    limit: "200mb",
  })
);

app.use(
  bodyParser.urlencoded({
    limit: "200mb",
    extended: true,
    parameterLimit: 10000000,
  })
);

app.use(cors());
app.use(expressip().getIpInfoMiddleware);
app.use(express.static(__dirname + "/views"));
app.use("/company_images", express.static(__dirname + "/company_images"));
app.use("/post_images", express.static(__dirname + "/post_images"));
app.use("/post_videos", express.static(__dirname + "/post_videos"));
app.use("/banners", express.static(__dirname + "/banners"));
app.use("/enclosed", express.static(__dirname + "/enclosed"));
app.use("/emailer", express.static(`${__dirname}/emailer`));
app.use("/videos", express.static(`${__dirname}/videos`));
app.use("/sitemaps", express.static(`${__dirname}/sitemaps`));
app.use("/bulk_attachments", express.static(`${__dirname}/bulk_attachments`));
// app.set('trust proxy', true);

app.set("views", [
  __dirname + "/views",
  __dirname + "/business_network/bn_views",
]);
app.set("view engine", "ejs");
// app.get('*',(req,res)=>{
//     res.render('frontend_new/maintinance');
// });

app.use(routes);
app.use(ownerRoutes);
app.use(sitemaps);

app.use("/index", googleAnalytics);
app.use("/premium", premiumRoutes);
app.use("/posts", postRoutes);
app.use("/notifications", notificationRoutes);
app.use("/", timeLineRoutes);
app.use("/friend_request", friendRequestRoutes);
app.use("/friends", friendRoutes);
app.use("/comments", commentsRoutes);
app.use("/likes", likesRoutes);
app.use("/share", shareRouter);
app.use("/suggestions", suggestionRouter);
app.use("/follow", followRouter);
app.use("/on_boarding", onBoardingRouter);
app.use("/report_post", ReportRouter);

module.exports = {
  express,
  app,
  cors,
  bodyParser,
  csvtojson,
  createCsvWriter,
  request,
  fs,
  fse,
};
