const express = require("express");
const router = express.Router();
const csurf = require("csurf");
const xss = require("xss-clean");
const PremiumOrder = require("../database/premium_order");
const Paypal = require("paypal-rest-sdk");
const invoice = require("../database/email-invoice");
const Moment = require("moment");
const paypal = {};

require("dotenv").config();
const { queryPromise } = require("../database/database");

if (process.env.PAYMENT_MODE == "sandbox") {
  Paypal.configure({
    mode: process.env.PAYMENT_MODE,
    client_id:
      "AQUymqqV40mLxjfW3L_Dz3z6a0-qLIMtVJfzQGpSuHHoWt-DN7rcRJeocthc1ekomZCnIri8pM_Vr2DN",
    client_secret:
      "EPX6vdHxuQAVBnVKaGnB1SCyefVy3cFKNWmcPye6yvCcX-rm3qDiYdHVxMhG7WSamrsPP9O9nX7cvSAS",
  });
} else {
  Paypal.configure({
    mode: process.env.PAYMENT_MODE,
    client_id: process.env.CLIENT_ID,
    client_secret: process.env.SECRET,
  });
}

const csrfProtection = csurf({
  cookie: true,
});

router.use(csrfProtection);

router.use(xss());

const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "enclosed/");
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    cb(
      null,
      file.fieldname +
        "-" +
        uniqueSuffix +
        "." +
        file.mimetype.replace("image/", "")
    );
  },
});
const upload = multer({
  storage: storage,
});

router.get("/", (req, res) => {
  res.render("frontend_new/premium_profile", {
    ip: req.ipInfo.ip,
    csrfToken: req.csrfToken(),
  });
});

router.post("/", upload.array("pics_enclosed", 10), async (req, res) => {
  try {
    let billingPlanAttributes = {
      description: "Every Year subscription plan for premium profiles",
      merchant_preferences: {
        auto_bill_amount: "yes",
        cancel_url: "http://localhost:7070/premium/cancel",
        initial_fail_amount_action: "continue",
        max_fail_attempts: "3",
        return_url: "http://localhost:7070/premium/success",
        setup_fee: {
          currency: "USD",
          value: "1",
        },
      },
      name: "Paypal Agreement",
      payment_definitions: [
        {
          amount: {
            currency: "USD",
            value: "100",
          },
          charge_models: [],
          cycles: "10",
          frequency: "DAY",
          frequency_interval: 1,
          name: "Premium Profile",
          type: "REGULAR",
        },
      ],
      type: "INFINITE",
    };

    //Once a billing plan is created it must be updated with the following attributes.
    let billingPlanUpdateAttributes = [
      {
        op: "replace",
        path: "/",
        value: {
          state: "ACTIVE",
        },
      },
    ];

    //Attributes for creating the billing agreement.
    //Start Date should be greater than current time and date.
    let startDate =
      Moment(new Date()).add(1, "minute").format("gggg-MM-DDTHH:mm:ss") + "Z";
    console.log(startDate + "\n\n\n\n\n\n\n\n");
    let billingAgreementAttributes = {
      name: "Premium Profile",
      description: "Every Year subscription plan for premium profiles",
      start_date: startDate,
      plan: {
        id: "",
      },
      payer: {
        payment_method: "paypal",
      },
    };
    let profile = req.body;
    if (!profile.pics_website) {
      profile.pics_website = null;
    }
    if (req.files.length > 0) {
      profile.pics_enclosed = JSON.stringify(req.files);
    } else {
      profile.pics_later = 1;
    }
    if (profile.pics_later == "on") {
      profile.pics_later = 1;
    } else {
      profile.pics_later = 0;
    }
    if (!profile.details) {
      profile.details = null;
    }
    Paypal.billingPlan.create(billingPlanAttributes, (error, billingPlan) => {
      if (error) {
        console.log(error);
      } else {
        console.log("billing", billingPlan);
        Paypal.billingPlan.update(
          billingPlan.id,
          billingPlanUpdateAttributes,
          (error, response) => {
            if (error) {
              console.log(error);
            } else {
              // update the billing agreement attributes before creating it.
              billingAgreementAttributes.plan.id = billingPlan.id;

              //Step 8:
              Paypal.billingAgreement.create(
                billingAgreementAttributes,
                async (error, billingAgreement) => {
                  if (error) {
                    console.log(error);
                  } else {
                    let links = {};
                    console.log("billing ", billingAgreement);
                    billingAgreement.links.forEach((agreement) => {
                      links[agreement.rel] = {
                        href: agreement.href,
                        method: agreement.method,
                      };
                    });
                    if (links.hasOwnProperty("approval_url")) {
                      const order = new PremiumOrder(profile);
                      await order.setOrder();
                      res.redirect(links["approval_url"].href);
                    } else {
                      res.redirect("/cancel");
                    }
                  }
                }
              );
            }
          }
        );
      }
    });
  } catch (error) {
    console.log(error);
    res.status(400).send("Failed to place the order");
  }
});

router.get("/success", async (req, res) => {
  try {
    const { token } = req.query;
    let paymentId = token;
    // const query = `select * from premium_orders where payment_id = ?`;
    // const orderDB = await queryPromise(query, [paymentId]);
    // console.log(orderDB);
    // const order = new PremiumOrder(orderDB[0]);
    // if (order.payment_status == 'approved') {
    //     return res.render('frontend_new/payment_done', {
    //         status: true,
    //         ip: req.ipInfo.ip,
    //         csrfToken: req.csrfToken(),
    //         name: order.name,
    //         owner: false
    //     });
    // }
    Paypal.billingAgreement.execute(
      token,
      {},
      async (error, billingAgreement) => {
        if (error) {
          console.log(error);
          throw error;
        } else {
          console.log(billingAgreement);
          if (billingAgreement.state == "Active") {
            // await order.updateOrder(payment.state, payment.update_time, payment.id);
            // await invoice(order.name, order.email, order.payment_update_time, payment.id);
            res.send({
              status: true,
              ip: req.ipInfo.ip,
              csrfToken: req.csrfToken(),
              // name: order.name,
              owner: false,
            });
          } else {
            res.redirect("/cancel");
          }
        }
      }
    );
  } catch (error) {
    console.log(error);
    res.redirect("/cancel");
  }
});

router.get("/cancel", (req, res) => {
  res.render("frontend_new/payment_done", {
    status: false,
    ip: req.ipInfo.ip,
    csrfToken: req.csrfToken(),
    owner: false,
    name: "Buyer",
  });
});

module.exports = router;
