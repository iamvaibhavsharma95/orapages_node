const csurf = require("csurf");
const express = require("express");
const multer = require("multer");
const router = express.Router();

const { getCode } = require("country-list");
const AdminOperations = require("./../database/admin_operations");
const GoogleAnalytics = require("./../analytics/gaapi");

const { getAdmin, logoutAdmin } = require("./../database/admin");
const Post = require("../business_network/bn_models/post");

const {
  admin_auth,
  admin_log_auth,
  admin_auth_ajax,
} = require("./../middlewares/admin_auth");

const { passport, insertSocialMedia } = require("../fb_sitemap/fb_sitemap");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "csvs/");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});

const storageVideo = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `videos/`);
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    if (file.mimetype !== "video/mp4") {
      cb(
        null,
        file.fieldname +
          "-" +
          uniqueSuffix +
          "." +
          file.originalname.split(".")[1]
      );
    } else {
      cb(
        null,
        file.fieldname +
          "-" +
          uniqueSuffix +
          "." +
          file.mimetype.replace("video/", "")
      );
    }
  },
});

const upload = multer({
  storage: storage,
});

const uploadVideo = multer({
  storage: storageVideo,
});

const csrfProtection = csurf({
  cookie: true,
});

let adminOpen = async function (req, res, next) {
  try {
    let result = await AdminOperations.getCountryCompanyCount();
    let x = await AdminOperations.catSubCatNonCatTagTotal();
    res.render("admin-dashboard", {
      page: {
        name: "dashboard",
        company_count: result,
        total: x,
      },
    });
  } catch (error) {
    next(error);
  }
};

router.get("/admin", csrfProtection, admin_log_auth, (req, res, next) => {
  return res.render("admin-login", {
    csrfToken: req.csrfToken(),
  });
});

router.get("/admin/get_countries", admin_auth_ajax, async (req, res, next) => {
  try {
    const countries = await AdminOperations.getCountriesByterm(req.query.term);
    res.send(countries);
  } catch (error) {
    next(error);
  }
});

router.get("/admin/get_states", admin_auth_ajax, async (req, res, next) => {
  try {
    const states = await AdminOperations.getStatesByterm(
      req.query.country_id,
      req.query.term
    );
    res.send(states);
  } catch (error) {
    next(error);
  }
});

router.get("/admin/get_cities", admin_auth_ajax, async (req, res, next) => {
  try {
    const { country_id, state_id, term } = req.query;
    const cities = await AdminOperations.getCitiesByterm(
      state_id,
      country_id,
      term
    );
    res.send(cities);
  } catch (error) {
    next(error);
  }
});

router.get("/admin/get_categories", admin_auth_ajax, async (req, res, next) => {
  try {
    const { term } = req.query;
    const categories = await AdminOperations.getCategoriesByTerm(term);
    res.send(categories);
  } catch (error) {
    next(error);
  }
});

router.get(
  "/admin/get_subcategories",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const { term } = req.query;
      const subcategories = await AdminOperations.getSubcategoriesByTerm(term);
      res.send(subcategories);
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/admin/get_non_dis_subcategories",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const { term } = req.query;
      const subcategories = await AdminOperations.getNonDisSubcategoriesByTerm(
        term
      );
      res.send(subcategories);
    } catch (error) {
      next(error);
    }
  }
);

router.get("/admin/get_subcat_cat", admin_auth_ajax, async (req, res, next) => {
  try {
    const { subcategory_id } = req.query;
    const categories = await AdminOperations.getSubcatCategories(
      subcategory_id
    );
    res.send(categories);
  } catch (error) {
    next(error);
  }
});

router.get(
  "/admin/get_non_dis_subcat_cat",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const { subcategory_id } = req.query;
      const categories = await AdminOperations.getNonDisSubcatCategories(
        subcategory_id
      );
      res.send(categories);
    } catch (error) {
      next(error);
    }
  }
);

router.get("/admin_panel", admin_auth, async (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "home",
    },
  });
});

router.get("/admin_profile", admin_auth, async (req, res, next) => {
  try {
    await adminOpen(req, res, next);
  } catch (e) {
    res.render("admin-dashboard", {
      page: {
        name: "dashboard",
        company_count: [],
      },
    });
  }
});

router.get("/admin_analytics", admin_auth, async (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "dashboard-count",
    },
  });
});

router.get(
  "/admin/admin_analytics_results",
  admin_auth,
  async (req, res, next) => {
    let { start_timestamp, end_timestamp, start_date, end_date } = req.query;
    try {
      const results = await AdminOperations.getReportsData(
        start_timestamp,
        end_timestamp,
        start_date,
        end_date
      );
      res.render("dashboard/report_results", { results });
    } catch (error) {
      next(error);
    }
  }
);

router.get("/admin_country_add", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "add-country",
    },
  });
});

router.get("/admin_country_delete", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "del-country",
    },
  });
});

router.get("/admin_country_edit", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "edit-country",
    },
  });
});

router.get("/admin_state_add", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "add-state",
    },
  });
});

router.get("/admin_state_edit", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "edit-state",
    },
  });
});

router.get("/admin_state_delete", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "delete-state",
    },
  });
});

router.get("/admin_city_add", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "add-city",
    },
  });
});

router.get("/admin_city_edit", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "edit-city",
    },
  });
});

router.get("/admin_city_delete", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "delete-city",
    },
  });
});

router.get("/admin_tagline_add", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "add-tagline",
    },
  });
});

router.get("/admin_tagline_delete", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "delete-tagline",
    },
  });
});

router.get("/admin_tagline_edit", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "edit-tagline",
    },
  });
});

router.get("/admin_brands_add", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "add-brands",
    },
  });
});

router.get("/admin_brands_delete", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "delete-brands",
    },
  });
});

router.get("/admin_brands_edit", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "edit-brands",
    },
  });
});

router.get("/admin/get_banners", admin_auth_ajax, async (req, res, next) => {
  try {
    const banners = await AdminOperations.getBanners();
    res.send(banners);
  } catch (error) {
    next(error);
  }
});

router.get("/admin_banners_add", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "add-banners",
    },
  });
});

router.get("/admin_banners_update", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "update-banners",
    },
  });
});

router.get("/csv_edit", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "edit-csv-main",
      csvId: req.query.csv_id,
      ip: req.ipInfo.ip,
      type: req.query.type,
    },
  });
});

router.get(
  "/admin/get_temp_companies",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const companies = await AdminOperations.getTempCompanies(
        req.query.csvId,
        req.query.type
      );
      res.send(companies[0]);
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/admin/validate_name_url",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const checkName = await AdminOperations.checkNameUrl(req.query.url);
      res.send(checkName);
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/admin/get_category_id",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const category = await AdminOperations.getCategoryById(
        req.query.category
      );
      res.send(category);
    } catch (error) {
      next(error);
    }
  }
);

router.get("/csv_list", admin_auth, async (req, res, next) => {
  try {
    const csvs = await AdminOperations.getCSVList(req.query.type);
    res.render("admin-dashboard", {
      page: {
        name: "list-csv-main",
        csv_list: csvs,
        type: req.query.type,
      },
    });
  } catch (e) {
    res.render("admin-dashboard", {
      page: {
        name: "list-csv-main",
        csv_list: [],
        type: req.query.type,
      },
    });
  }
});

// city-category-id
router.get("/city_category_id", admin_auth, async (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "city-category-id",
    },
  });
});

router.get("/companies_list", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "edit-company-main",
    },
  });
});

router.get("/reported_posts", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "reported-posts",
    },
  });
});

router.get("/getAllPosts", admin_auth, async function (req, res, next) {
  try {
    const {
      start,
      length,
      draw,
      order = [
        {
          dir: "desc",
        },
      ],
    } = req.query;
    let [{ dir }] = order;
    const { posts, count } = await AdminOperations.allPosts(length, start, dir);

    res.send({
      draw,
      recordsTotal: count,
      recordsFiltered: count,
      data: posts,
    });
  } catch (error) {
    next(error);
  }
});

router.get("/getReportedPosts", admin_auth, async function (req, res, next) {
  try {
    const {
      start,
      length,
      draw,
      order = [
        {
          dir: "desc",
        },
      ],
    } = req.query;
    let [{ dir }] = order;
    const { posts, count } = await AdminOperations.reportedPosts(
      length,
      start,
      dir
    );

    res.send({
      draw,
      recordsTotal: count,
      recordsFiltered: count,
      data: posts,
    });
  } catch (error) {
    next(error);
  }
});

router.get("/admin/delete_post", admin_auth, async (req, res, next) => {
  try {
    let { post_id } = req.query;
    const post = new Post({ post_id });
    await post.deletePost(post_id);
    res.send("Deleted");
  } catch (error) {
    next(error);
  }
});

router.get("/admin/delete_report", admin_auth, async (req, res, next) => {
  try {
    let { post_id, report_id } = req.query;
    const post = new Post({ post_id });
    await post.deleteReport(post_id, report_id);
    res.send("Deleted");
  } catch (error) {
    next(error);
  }
});

router.get("/all_posts", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "all-posts",
    },
  });
});

router.get("/companies", admin_auth, async (req, res, next) => {
  try {
    const s = await AdminOperations.getCompanies(
      req.query.limit,
      req.query.offset
    );
    res.send(s);
  } catch (error) {
    next(error);
  }
});

router.get("/company_count", admin_auth, async (req, res, next) => {
  try {
    const count = await AdminOperations.getTotalCompanies();
    return res.status(200).send(count);
  } catch (error) {
    next(error);
  }
});

router.get("/admin/companiesName", admin_auth_ajax, async (req, res, next) => {
  try {
    const names = await AdminOperations.getCompaniesName(req.query.term);
    res.send(names);
  } catch (e) {
    res.send({
      label: "no results found",
    });
  }
});

router.get("/admin/companiesEmail", admin_auth_ajax, async (req, res, next) => {
  try {
    const emails = await AdminOperations.getCompanyEmail(req.query.term);
    res.send(emails);
  } catch (e) {
    res.send({
      label: "no results found",
    });
  }
});

router.get(
  "/admin/category_company_list",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const key = req.query.category_id;
      const companies = await AdminOperations.getCompaniesByCategory(key);
      res.send(companies);
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/admin/brand_company_list",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const key = req.query.brand_id;
      const companies = await AdminOperations.getCompaniesByBrand(key);
      res.send(companies);
    } catch (error) {
      next(error);
    }
  }
);

router.get("/admin/company_by_id", admin_auth_ajax, async (req, res, next) => {
  try {
    const company = await AdminOperations.getCompanyById(req.query.company_id);
    return res.send(company);
  } catch (error) {
    next(error);
  }
});

router.get("/admin_csv_add", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "add-csv",
    },
  });
});

router.get("/admin_auto_csv_add", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "add-csv-auto",
    },
  });
});

router.get("/admin_large_csv_add", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "add-csv-large",
    },
  });
});

router.get("/admin_category_add", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "add-category",
    },
  });
});

// router.get("/admin_category_delete", validateSession, (req, res, next) => {
//     res.render("admin-dashboard", {
//         page: {
//             name: "delete-category"
//         }
//     });
// });

router.get("/admin_category_edit", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "edit-category",
    },
  });
});

router.get("/admin_subcategory_add", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "add-subcategory",
    },
  });
});

router.get("/admin_non_dis_subcategory_add", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "add-non-dis-subcategory",
    },
  });
});

router.get("/admin_subcategory_edit", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "edit-subcategory",
    },
  });
});

router.get("/admin_non_dis_subcategory_edit", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "edit-non-dis-subcategory",
    },
  });
});

router.get("/admin_category_transfer", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "transfer-categories",
    },
  });
});

router.get("/admin_city_transfer", admin_auth, (req, res, next) => {
  res.render("admin-dashboard", {
    page: {
      name: "transfer-cities",
    },
  });
});

router.get(
  "/admin/get_transfer_companies_city",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const companies = await AdminOperations.getTranferCityCompanies(
        req.query.city_id
      );
      res.send(companies);
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/admin/get_transfer_companies_category",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const companies = await AdminOperations.getTranferCategoryCompanies(
        req.query.category_id
      );
      res.send(companies);
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/admin/transfer_companies_by_city",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      await AdminOperations.transferCompaniesByCity(
        req.query.first_city,
        req.query.second_city
      );
      res.send("Companies has been tranfered");
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/admin/transfer_companies_by_category",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      await AdminOperations.transferCompaniesByCategory(
        req.query.first_category,
        req.query.second_category
      );
      res.send("Companies has been tranfered");
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/edit_company_admin",
  csrfProtection,
  admin_auth,
  async (req, res, next) => {
    try {
      const company = await AdminOperations.getCompanyByIdFull(
        req.query.companyKey
      );
      company.csrfToken = req.csrfToken();
      company.last_update_ip = req.ipInfo.ip;
      // res.send(company);
      res.render("frontend_new/registration_form_edit_admin", company);
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/edit_company_admin/analytics",
  csrfProtection,
  admin_auth,
  async (req, res, next) => {
    try {
      const company = await AdminOperations.getCompanyByIdFull(
        req.query.companyKey
      );
      res.render(
        "frontend_new/registration_form_edit_admin_analytics",
        company
      );
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/admin/getPageViewsGA",
  csrfProtection,
  admin_auth,
  async (req, res, next) => {
    try {
      let gaPageViewsResults;
      if (!req.query.days) {
        gaPageViewsResults = await GoogleAnalytics.gaPageViews(
          req.query.company_url
        );
      } else {
        gaPageViewsResults = await GoogleAnalytics.gaPageViews(
          req.query.company_url,
          (startDate = req.query.days)
        );
      }
      let chart = {};
      chart.labels = [];
      chart.data = [];
      gaPageViewsResults.rows.forEach((row) => {
        chart.labels.push(GoogleAnalytics.convertDate(row[0]));
        chart.data.push(row[1]);
      });
      res.json(chart);
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/admin/getUsersGA",
  csrfProtection,
  admin_auth,
  async (req, res, next) => {
    try {
      let gaUsersResults;
      if (!req.query.days) {
        gaUsersResults = await GoogleAnalytics.gaUsers(req.query.company_url);
      } else {
        gaUsersResults = await GoogleAnalytics.gaUsers(
          req.query.company_url,
          (startDate = req.query.days)
        );
      }
      let chart = {};
      chart.labels = [];
      chart.data = [];
      gaUsersResults.rows.forEach((row) => {
        chart.labels.push(GoogleAnalytics.convertDate(row[0]));
        chart.data.push(row[1]);
      });
      res.json(chart);
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/admin/getCountryCityGA",
  csrfProtection,
  admin_auth,
  async (req, res, next) => {
    try {
      let gaCityResults;
      if (!req.query.days) {
        gaCityResults = await GoogleAnalytics.gaCity(req.query.company_url);
      } else {
        gaCityResults = await GoogleAnalytics.gaCity(
          req.query.company_url,
          (startDate = req.query.days)
        );
      }
      let chart = {};
      chart.labels = [];
      chart.data = [];
      chart.backgroundColor = [];
      gaCityResults.rows.forEach((row) => {
        chart.labels.push(`${row[0]}, ${row[1]}`);
        chart.data.push(row[2]);
        chart.backgroundColor.push(GoogleAnalytics.getRandomColor());
      });
      res.json(chart);
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/admin/getCountryGA",
  csrfProtection,
  admin_auth,
  async (req, res, next) => {
    try {
      let gaCountryResults;
      if (!req.query.days) {
        gaCountryResults = await GoogleAnalytics.gaCountry(
          req.query.company_url
        );
      } else {
        gaCountryResults = await GoogleAnalytics.gaCountry(
          req.query.company_url,
          (startDate = req.query.days)
        );
      }
      let chart = {};
      chart.labels = [];
      chart.data = [];
      chart.backgroundColor = [];
      chart.map = {};
      gaCountryResults.rows.forEach((row) => {
        chart.labels.push(row[0]);
        chart.data.push(row[1]);
        chart.backgroundColor.push(GoogleAnalytics.getRandomColor());
        chart.map[getCode(row[0])] = row[1];
      });
      res.json(chart);
    } catch (error) {
      next(error);
    }
  }
);

router.get("/check_email_admin", admin_auth, async (req, res, next) => {
  try {
    const r = await AdminOperations.checkEmail(
      req.query.company_email,
      req.query.company_id
    );

    if (r.length > 0) {
      res.send('""');
    } else {
      res.send('"true"');
    }
  } catch (e) {
    res.send('""');
  }
});

router.get("/check_name_admin", admin_auth, async (req, res, next) => {
  try {
    const r = await AdminOperations.checkName(
      req.query.company_business_name,
      req.query.company_id
    );

    if (r.length > 0) {
      res.send('""');
    } else {
      res.send('"true"');
    }
  } catch (e) {
    res.send('""');
  }
});

router.get(
  "/add_premium_company_admin",
  csrfProtection,
  admin_auth,
  async (req, res, next) => {
    try {
      const { email, company_name } = req.query;
      const csrfToken = req.csrfToken();
      const ip = req.ipInfo.ip;
      res.render("frontend_new/registration_pr", {
        csrfToken,
        ip,
        email,
        company_name,
      });
    } catch (error) {
      next(error);
    }
  }
);

router.get("/admin_delete_popup", admin_auth, async function (req, res, next) {
  return res.send(`<div class="jumbotron text-center orapages-url" style="padding: 32px;" id="delete_popup">
                <h1 class="text-danger" style="color: #0070c0;">
                <strong>WARNING! You are about to delete your listing permanently.</strong>
                <br/></h1>
                <h4 class="text-danger" style="color: #0070c0;">
                <strong>Deletion of listings is a permanent operation and cannot be undone.</strong>
                <br/></h4>
                <h4 style="color: #0070c0;">
                Are you sure you wish to delete your listing ?
                <br/></h4>
                <div class="form-row" style="margin-top: 21px;">
                    <div class="col text-right">
                    <a href="javascript:void(0)" onclick="$('#delete_popup').remove()"
                    class="btn btn-secondary text-white" role="button">Cancel</a></div>
                    <div class="col text-left">
                    <a href="/admin_delete_fe?company_id=${req.query.company_id}"
                    class="btn btn-danger text-white" role="button">Yes. Delete my Listing Now
                    <br /></a></div>
                </div>
            </div>`);
});

router.get("/admin_delete_fe", admin_auth, async function (req, res, next) {
  try {
    const company_id = req.query.company_id;
    await AdminOperations.deleteCompany(company_id);
    return res.send(
      `<h1>This profile has been deleted </h1><a href="/">Go to Home Page<a>`
    );
  } catch (error) {
    return res.send(
      `<h2>This profile is not deleted yet due to some problem Please try again </h2>` +
        `<a href="/admin_delete_fe?company_id=${company_id}">Try Again<a>`
    );
  }
});

router.get("/premium_orders", admin_auth, async (req, res, next) => {
  try {
    const orders = await AdminOperations.getPremiumOrders();
    res.render("admin-dashboard", {
      page: {
        name: "premium-orders",
        orders,
      },
    });
  } catch (error) {
    next(error);
  }
});

router.get(
  "/admin/updatePremiumUrlCompleted",
  admin_auth,
  async (req, res, next) => {
    try {
      const order = await AdminOperations.updatePremiumOrderCompleted(
        req.query.premium_url,
        req.query.payment_id
      );
      res.send(order);
    } catch (e) {
      next(new Error(`Could not updated the premium url ${e.message}`));
    }
  }
);

router.get(
  "/facebook",
  admin_auth,
  passport.authenticate("facebook", {
    scope: ["pages_manage_posts", "pages_show_list"],
  })
);

router.get("/facebook_post", async (req, res, next) => {
  try {
    const s = await insertSocialMedia(
      "New_post_222",
      "Temp Content",
      "www.orapages.com",
      ["hashtag"]
    );
    res.json(s);
  } catch (error) {
    next(error);
  }
});

router.get(
  "/facebook_auth",
  admin_auth,
  passport.initialize(),
  passport.authenticate("facebook"),
  async function (req, res, next) {
    res.redirect("/admin");
  }
);

router.get("/logout", admin_auth, async (req, res, next) => {
  try {
    await logoutAdmin(req.session.admin_token);
    req.session.destroy();
    res.redirect("/admin");
  } catch (e) {
    req.session.destroy();
    res.redirect("/admin");
  }
});

router.post(
  "/admin/uploadVideo",
  csrfProtection,
  uploadVideo.array("video"),
  async (req, res, next) => {
    try {
      let company = await AdminOperations.getCompanyByIdFull(
        req.body.company_key
      );
      let video = {};
      if (!req.body.video_uploaded) {
        video = req.files[0];
        video.video_description = req.body.video_description;
        await company.setVideo(video);
        res.status(200).send("You Video has been uploaded");
      } else {
        video.video_id = req.body.video_id;
        video.video_description = req.body.video_description;
        await company.updateVideo(video);
        res.status(200).send("You Video has been uploaded");
      }
    } catch (e) {
      next(new Error("Your Video is not uploaded"));
    }
  }
);

router.post("/admin/deleteVideo", csrfProtection, async (req, res, next) => {
  try {
    let company = await AdminOperations.getCompanyById(req.body.company_key);
    await company.deleteExtraVideo();
    res.status(200).send("Video has been removed");
  } catch (e) {
    next(new Error("Your Video is not removed"));
  }
});

router.post(
  "/admin_panel",
  csrfProtection,
  admin_log_auth,
  async (req, res, next) => {
    try {
      const token = await getAdmin(req.body.email, req.body.password);
      req.session.admin_token = token;
      req.session.admin = true;
      res.status(200).send(token);
    } catch (error) {
      next(error);
    }
  }
);

router.post("/admin/add_country", admin_auth_ajax, async (req, res, next) => {
  try {
    await AdminOperations.addCountry(req.body.country);
    res.status(200).send("Country has been added");
  } catch (error) {
    next(error);
  }
});

router.post(
  "/admin/update_country",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      await AdminOperations.updateCountry(
        req.body.country,
        req.body.country_id
      );
      res.status(200).send("country has been changed");
    } catch (error) {
      next(error);
    }
  }
);

router.post(
  "/admin/delete_country",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      await AdminOperations.deleteCountry(req.body.country_id);
      res.status(200).send("Country has been deleted");
    } catch (error) {
      next(error);
    }
  }
);

router.post("/admin/add_states", admin_auth_ajax, async (req, res, next) => {
  try {
    let states = req.body.states;
    states = states.split(",");
    let error = "";
    for (const state of states) {
      try {
        await AdminOperations.addState(state.trim(), req.body.country_id);
        error += `${state} has been added \n`;
      } catch (e) {
        error += `${state} ${e.message} \n`;
      }
    }
    res.status(200).send(error);
  } catch (error) {
    next(error);
  }
});

router.post("/admin/update_state", admin_auth_ajax, async (req, res, next) => {
  try {
    const { country_id, state_id, state } = req.body;
    await AdminOperations.updateState(state, state_id, country_id);
    res.send("State has been updated");
  } catch (error) {
    next(error);
  }
});

router.post("/admin/delete_state", admin_auth_ajax, async (req, res, next) => {
  try {
    const { country_id, state_id } = req.body;
    await AdminOperations.deleteState(state_id, country_id);
    res.send("State has been deleted");
  } catch (error) {
    next(error);
  }
});

router.post("/admin/add_cities", admin_auth_ajax, async (req, res, next) => {
  try {
    let { cities, country_id, state_id } = req.body;
    cities = cities.split(",");
    let error = "";
    for (const city of cities) {
      try {
        await AdminOperations.addCity(city.trim(), state_id, country_id);
        error += `${city} has been added \n`;
      } catch (e) {
        error += `${city}, ${e.message} \n`;
      }
    }
    res.status(200).send(error);
  } catch (error) {
    next(error);
  }
});

router.post("/admin/update_city", admin_auth_ajax, async (req, res, next) => {
  try {
    const { country_id, state_id, city_id, city } = req.body;
    await AdminOperations.updateCity(city, city_id, state_id, country_id);
    res.send("City has been updated");
  } catch (error) {
    next(error);
  }
});

router.post("/admin/delete_city", admin_auth_ajax, async (req, res, next) => {
  try {
    const { country_id, city_id, state_id } = req.body;
    await AdminOperations.deleteCity(city_id, state_id, country_id);
    res.send("City has been deleted");
  } catch (error) {
    next(error);
  }
});

router.post(
  "/admin/upload_banners",
  admin_auth_ajax,
  async (req, res, next) => {
    const { banner_arr } = req.body;
    let message = "";
    for (const banner of banner_arr) {
      try {
        await AdminOperations.uploadBanner(banner);
        message += "Banner has been uploaded\n";
      } catch (e) {
        message += e.message + "\n";
      }
    }
    next(new Error(message));
  }
);

router.post(
  "/admin/update_banners",
  admin_auth_ajax,
  async (req, res, next) => {
    const { banner_arr } = req.body;
    let message = ``;
    for (const banner of banner_arr) {
      try {
        await AdminOperations.updateBanner(banner);
        message += `Banner has been updated\n`;
      } catch (e) {
        message += e.message + "\n";
      }
    }

    next(new Error(message));
  }
);

router.post("/admin/delete_banner", admin_auth_ajax, async (req, res, next) => {
  try {
    await AdminOperations.deleteBanner(req.body.banner_id);
    res.send("Banner has been removed");
  } catch (e) {
    next(new Error(e.message));
  }
});

router.post(
  "/admin/upload_csv",
  admin_auth_ajax,
  upload.single("csv"),
  async (req, res, next) => {
    try {
      const result = await AdminOperations.uploadCSV(req.file);
      res.status(200).send(result);
    } catch (e) {
      next(new Error("File is not uploaded"));
    }
  }
);

router.post(
  "/admin/auto_upload_csv",
  admin_auth_ajax,
  upload.single("csv"),
  async (req, res, next) => {
    try {
      const result = await AdminOperations.uploadAutoCSV(req.file);
      res.status(200).send(result);
    } catch (error) {
      next(error);
    }
  }
);

router.post(
  "/admin/upload_large_csv",
  admin_auth_ajax,
  upload.single("csv"),
  async (req, res, next) => {
    try {
      const result = await AdminOperations.uploadLargeCSV(
        req.file,
        req.ipInfo.ip
      );
      res.status(200).send(result);
    } catch (error) {
      next(error);
    }
  }
);

router.post("/admin/update_csv", admin_auth_ajax, async (req, res, next) => {
  try {
    const { csv, company_data } = req.body;
    await AdminOperations.updateCSV(csv, company_data);
    res.send("CSV has been updated");
  } catch (e) {
    next(new Error("CSV is not updated"));
  }
});

router.post("/admin/delete_csv", admin_auth_ajax, async (req, res, next) => {
  try {
    const { csv, type } = req.body;
    await AdminOperations.deleteCSV(csv, type);
    res.send("Csv has been deleted");
  } catch (error) {
    next(error);
  }
});

router.post(
  "/admin/transfer_companies",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const { csv, ip, uploaded_by } = req.body;
      await AdminOperations.transferCSVToDatabase(csv, ip, uploaded_by);
      res.send("companies has been tranfered");
    } catch (error) {
      next(new Error("Companies has not been transfered"));
    }
  }
);

router.post("/admin/add_category", admin_auth_ajax, async (req, res, next) => {
  try {
    await AdminOperations.addCategory(req.body.category);
    res.status(200).send("Category has been added");
  } catch (error) {
    next(error);
  }
});

router.post(
  "/admin/update_category",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const { category_id, category } = req.body;
      await AdminOperations.updateCategory(category_id, category);
      res.status(200).send("Category has been updated");
    } catch (error) {
      next(error);
    }
  }
);

router.post(
  "/admin/delete_category",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      await AdminOperations.deleteCategory(req.body.category_id);
      res.status(200).send("Category has been deleted");
    } catch (error) {
      next(error);
    }
  }
);

router.post(
  "/admin/add_subcategory",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      let id = "";
      const { category_arr, subcategory } = req.body;
      for (const category of category_arr) {
        id = await AdminOperations.addSubcategory(category, id, subcategory);
      }
      res.send("Subcategory has been added");
    } catch (error) {
      next(new Error("Subcategory is not added"));
    }
  }
);

router.post(
  "/admin/add_non_dis_subcategory",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      let id = "";
      const { category_arr, subcategory } = req.body;
      for (const category of category_arr) {
        id = await AdminOperations.addNonDisSubcategory(
          category,
          id,
          subcategory
        );
      }
      res.send("Non Displayable Subcategory has been added");
    } catch (error) {
      next(new Error("Non Displayable Subcategory is not added !"));
    }
  }
);

router.post(
  "/admin/delete_subcat_cat",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const { category_id, subcategory_id } = req.body;
      await AdminOperations.deleteSubcatCat(category_id, subcategory_id);
      res.send("Category of this subcategory has been removed");
    } catch (error) {
      next(error);
    }
  }
);

router.post(
  "/admin/delete_non_dis_subcat_cat",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const { category_id, subcategory_id } = req.body;
      await AdminOperations.deleteNonDisSubcatCat(category_id, subcategory_id);
      res.send("Category of this subcategory has been removed");
    } catch (error) {
      next(error);
    }
  }
);

router.post(
  "/admin/delete_subcategory",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const { subcategory_id } = req.body;
      await AdminOperations.deleteSubcategory(subcategory_id);
      res.send("Subcategory has been removed");
    } catch (e) {
      next(new Error("Subcategory is not removed !"));
    }
  }
);

router.post(
  "/admin/delete_non_dis_subcategory",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const { subcategory_id } = req.body;
      await AdminOperations.deleteNonDisSubcategory(subcategory_id);
      res.send("Non Displayable Subcategory has been removed");
    } catch (e) {
      next(new Error("Non Displayable Subcategory is not removed !"));
    }
  }
);

router.post(
  "/admin/update_subcategory",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const { category_arr, subcategory, subcategory_id } = req.body;

      await AdminOperations.updateSubcategory(
        category_arr,
        subcategory_id,
        subcategory
      );
      res.send("Subcategory has been updated");
    } catch (e) {
      next(new Error("Subcategory is not updated !"));
    }
  }
);

router.post(
  "/admin/update_non_dis_subcategory",
  admin_auth_ajax,
  async (req, res, next) => {
    try {
      const { category_arr, subcategory, subcategory_id } = req.body;

      await AdminOperations.updateNonDisSubcategory(
        category_arr,
        subcategory_id,
        subcategory
      );
      res.send("Non Displayable Subcategory has been updated");
    } catch (e) {
      next(new Error("Non Displayable Subcategory is not updated !"));
    }
  }
);

router.post(
  "/add_premium_company_admin",
  csrfProtection,
  admin_auth,
  async (req, res, next) => {
    try {
      const { email, company_name } = req.query;
      const csrfToken = req.csrfToken();
      const ip = req.ipInfo.ip;
      res.render("frontend_new/registration_pr", {
        csrfToken,
        ip,
        email,
        company_name,
      });
    } catch (error) {
      next(error);
    }
  }
);

router.post(
  "/register_premium_business",
  csrfProtection,
  admin_auth,
  async (req, res, next) => {
    try {
      const company = await AdminOperations.insertCompanyPremium(req.body);
      res
        .status(200)
        .send(
          `Premimum Profile is completed ${company.company_url}` +
            `Please copy the url and press done in Premium orders`
        );
    } catch (error) {
      next(error);
    }
  }
);

router.post(
  "/update_business_admin",
  csrfProtection,
  admin_auth,
  async (req, res, next) => {
    try {
      const company = await AdminOperations.updateAdminCompany(req.body);

      res.status(200).json({
        url: company.company_url,
      });
    } catch (error) {
      next(error);
    }
  }
);

// router.get

router.post(
  "/admin_delete_company",
  admin_auth,
  async function (req, res, next) {
    try {
      let company_id = req.body.company_id;
      await AdminOperations.deleteCompany(company_id);
      res.status("200").send("Deleted");
    } catch (error) {
      next(error);
    }
  }
);

module.exports = router;
