const mssql = require("mssql");
const bcrypt = require("bcryptjs");
require("dotenv").config();
const orapages_dir = require("./orapagesdir.json");
const { queryPromise, selectPromise } = require("./../database/database");
const random = require("randomstring");

const setUser = async (user_id, email, password, company_id, name = null) => {
  let hashed_password = await bcrypt.hash(password, 8);

  await queryPromise(
    `insert into users(user_id,user_name,user_email,password) values('${user_id}',${JSON.stringify(
      name
    )},'${email}', '${hashed_password}')`
  );
  await queryPromise(
    `insert into company_user(user_id,company_id) values('${user_id}','${company_id}')`
  );
};

async function insertCategories() {
  let categories = orapages_dir["categories"];
  Object.values(categories).forEach(async function (category) {
    try {
      let query = `insert into category (category_id,category_name,category_name_short) values("${category.category_id}","${category.category_name}","${category.category_name_short}")`;
      await queryPromise(query);
    } catch (error) {
      console.log(error);
    }
  });
}

async function insertSubcategories() {
  let subcategories = orapages_dir["subcategories"];
  let subcatArray = [];
  for (subcategory in subcategories) {
    subcatArray.push(...Object.values(subcategories[subcategory]));
  }
  subcatArray.forEach(async function (subcat) {
    try {
      let query = `insert into subcategory (subcategory_id,subcategory_name,subcategory_name_short) values("${subcat.subcategory_id}","${subcat.subcategory_name}","${subcat.subcategory_name_short}")`;
      await queryPromise(query);
    } catch (error) {
      console.log(error);
    }
    try {
      let query2 = `insert into category_subcategory (subcat_id,category_id) values ("${subcat.subcategory_id}","${subcat.category_id}")`;
      await queryPromise(query2);
    } catch (err) {
      console.log("2\t" + err);
    }
  });
}

async function insertCountries() {
  let countries = orapages_dir["countries"];
  for (country in countries) {
    try {
      let r = countries[country];
      let query = `insert into countries (country_id,country_name,country_name_short) values("${r.country_id}","${r.country_name}","${r.country_name_short}")`;
      await queryPromise(query);
    } catch (err) {
      console.log(err);
    }
  }
}

async function insertStates() {
  let states = orapages_dir["states"];
  let stateArr = [];
  for (state in states) {
    stateArr.push(...Object.values(states[state]));
  }
  stateArr.forEach(async function (state) {
    try {
      let stateQuery = `insert into states(country_id,state_id,state_name,state_name_short) values("${state.country_id}","${state.state_id}","${state.state_name}","${state.state_name_short}")`;
      await queryPromise(stateQuery);
    } catch (err) {
      console.log(err);
    }
  });
}

async function insertCities() {
  let cities = orapages_dir["cities"];
  let cityArr = [];
  for (city in cities) {
    cityArr.push(...Object.values(cities[city]));
  }
  cityArr.forEach(async function (city) {
    try {
      let cityQuery = `insert into cities(country_id,state_id,city_id,city_name,city_name_short) values("${city.country_id}","${city.state_id}","${city.city_id}","${city.city_name}","${city.city_name_short}")`;
      await queryPromise(cityQuery);
    } catch (err) {
      console.log(err);
    }
  });
}

async function insertBrand() {
  let brands = orapages_dir["brands"];
  Object.values(brands).forEach(async function (brand) {
    try {
      let query = `insert into brands (brand_id,brand_name,brand_name_short) values("${
        brand.brand_id
      }","${brand.brand_name.replace(/\"/g, "")}","${brand.brand_name_short}")`;
      await queryPromise(query);
    } catch (error) {
      console.log(error);
    }
  });
}

async function insertUsers() {
  try {
    var config = {
      user: "sa",
      password: "vaibhav",
      server: "localhost",
      database: "orapages_dir",
      options: {
        enableArithAbort: true,
      },
    };
    await mssql.connect(config);

    let users = orapages_dir["owners"];

    for (const user in users) {
      if (users.hasOwnProperty(user)) {
        const r = users[user];
        try {
          const s = Object.keys(r.companies)[0];

          if (s.includes("business-") && !!orapages_dir["companies"][s]) {
            let member_id = await mssql.query(
              `select id_member from business where id_business = '${s.replace(
                "business-",
                ""
              )}'`
            );

            if (!!member_id.recordset[0]) {
              let member = await mssql.query(
                `select * from member_main where id_member=${member_id.recordset[0].id_member}`
              );

              let { tx_email, tx_password, nm_member } = member.recordset[0];

              await setUser(user, tx_email, tx_password, s, nm_member);
            } else {
              console.log("Undefined", s);
            }
          } else if (!s.includes("business-") && !!r.password) {
            await setUser(user, r.email, r.password, s);
          } else if (!s.includes("business-") && !r.password) {
            let password = random.generate({
              length: 7,
              charset: "hex",
            });
            await setUser(user, r.email, password, s);
            console.log("self defined", s);
          }
        } catch (err) {
          console.log(err);
        }
      }
    }
  } catch (e) {
    console.log(e);
  }
  let users = orapages_dir["owners"];
}

async function insertCompanies() {
  let companies = orapages_dir["companies"];
  // ["business-15392"]["locations"]["-LzO3q0m5agz9bzBVSj7"]["fax"];
  // return console.log(JSON.stringify(companies));

  for (const company in companies) {
    if (companies.hasOwnProperty(company)) {
      const {
        categories = [],
        images = [],
        brands = [],
        company_business_name,
        company_business_details = "\n",
        company_email,
        company_name,
        company_name_short,
        company_url,
        company_website,
        locations = [],
        created_at = null,
        last_update = null,
        last_update_ip = "0.0.0.0",
        uploaded_by,
        uploaded_ip,
        logo,
      } = companies[company];
      let companyQuery = `INSERT INTO company(company_id,company_business_name,company_name,company_name_short,company_url, company_email, company_website,created_at, last_update, uploaded_by, uploaded_ip, company_business_details,last_update_ip) VALUES ("${company}","${company_business_name}","${company_name}","${company_name_short}","${company_url}","${company_email}","${company_website}",${new Date(
        created_at
      ).getTime()},${new Date(
        last_update
      ).getTime()},"${uploaded_by}","${uploaded_ip}",${JSON.stringify(
        company_business_details
      )},"${last_update_ip}")`;

      try {
        await queryPromise(companyQuery);

        for (const location in locations) {
          if (locations.hasOwnProperty(location)) {
            const {
              complete_address,
              email = "",
              fax = "",
              location_id,
              mobile = "",
              website = "",
            } = locations[location];
            const { city_id, state_id, country_id } = location_id;
            let companyLocationQuery = `INSERT INTO company_location(company_id, city_id, state_id, country_id, complete_address, phone, fax, email,website) VALUES ("${company}","${city_id}","${state_id}","${country_id}",${JSON.stringify(
              complete_address
            )},${JSON.stringify(mobile)},${JSON.stringify(
              fax
            )},"${email}","${website}")`;

            try {
              await queryPromise(companyLocationQuery);
            } catch (error) {
              console.log(error);
            }
          }
        }

        for (const category in categories) {
          if (categories.hasOwnProperty(category)) {
            const categoryr = categories[category];
            let { category_id, category_des } = categoryr;
            let companyCategoryQuery = `INSERT INTO company_category(company_id, category_id, category_desc) VALUES ("${company}","${category_id}",${JSON.stringify(
              category_des
            )})`;
            try {
              await queryPromise(companyCategoryQuery);
            } catch (error) {
              console.log(error);
            }
          }
        }

        for (const brand in brands) {
          if (brands.hasOwnProperty(brand)) {
            const brandr = brands[brand];
            let { brand_id, brand_des } = brandr;
            let companybrandQuery = `INSERT INTO company_brand(company_id, brand_id, brand_desc) VALUES ("${company}","${brand_id}",${JSON.stringify(
              brand_des
            )})`;
            try {
              await queryPromise(companybrandQuery);
            } catch (error) {
              console.log(error);
            }
          }
        }

        let i = 0;
        for (const image in images) {
          if (images.hasOwnProperty(image)) {
            const imager = images[image];
            let { description = "", name, url } = imager;

            try {
              if (url.length <= 500) {
                let companyimageQuery = `INSERT INTO company_image(company_id, image_id, image_url, image_original_name, image_des, image_order, image_stored_name) VALUES ("${company}","${image}","${url}","${name}",${JSON.stringify(
                  description
                )},${i},"${url.split("/")[2]}")`;
                await queryPromise(companyimageQuery);
                i++;
              }
            } catch (error) {
              console.log(error);
            }
          }
        }

        if (!!logo) {
          let { logo_caption, logo_name, url } = logo;

          try {
            let logo_id = random.generate({
              length: 30,
              charset: "hex",
            });
            const companyLogoQuery = `INSERT INTO company_logo(company_id, logo_id, logo_url, logo_original_name, logo_des, logo_stored_name) VALUES ("${company}","${logo_id}","${url}",${JSON.stringify(
              logo_name
            )},${JSON.stringify(logo_caption)},"${url.split("/")[3]}")`;
            await queryPromise(companyLogoQuery);
          } catch (e) {
            console.log(e);
          }
        }
      } catch (error) {
        console.log("Company no inserted: ", company);

        // console.log(error);
      }
    }
  }
}

async function insertBanner() {
  try {
    const banners = orapages_dir["banners"];
    for (const banner in banners) {
      if (banners.hasOwnProperty(banner)) {
        const b = banners[banner];
        const query = `insert into banner (banner_id,name,flag,url) values('${banner}',${JSON.stringify(
          b.name
        )},'${b.status}','${b.url}')`;
        await queryPromise(query);
      }
    }
  } catch (e) {
    console.log(e);
  }
}

async function getCompany(id) {
  try {
    let r = `select * from company where company_id = '$id'`;
    let n = await selectPromise(r);
    console.log(n);
  } catch (error) {
    console.log(error);
  }
}

//change ut8mb4_bin
setTimeout(() => {
  // insert into order one by one
  // insertCategories();
  // insertSubcategories();
  // insertBanner();
  // insertCountries();
  // insertStates();
  // insertCities();
  // insertBrand();
  // insertCompanies();
  // getCompany("xbh")
  // insertUsers();
}, 3000);
