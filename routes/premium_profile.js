const express = require("express");
const router = express.Router();
const csurf = require("csurf");
const xss = require("xss-clean");
const PremiumOrder = require("../database/premium_order");
const paypal = require("paypal-rest-sdk");
const invoice = require("../database/email-invoice");
require("dotenv").config();
const { queryPromise } = require("../database/database");

if (process.env.PAYMENT_MODE == "sandbox") {
  paypal.configure({
    mode: process.env.PAYMENT_MODE,
    client_id:
      "AQUymqqV40mLxjfW3L_Dz3z6a0-qLIMtVJfzQGpSuHHoWt-DN7rcRJeocthc1ekomZCnIri8pM_Vr2DN",
    client_secret:
      "EPX6vdHxuQAVBnVKaGnB1SCyefVy3cFKNWmcPye6yvCcX-rm3qDiYdHVxMhG7WSamrsPP9O9nX7cvSAS",
  });
} else {
  paypal.configure({
    mode: process.env.PAYMENT_MODE,
    client_id: process.env.CLIENT_ID,
    client_secret: process.env.SECRET,
  });
}

const csrfProtection = csurf({
  cookie: true,
});

router.use(csrfProtection);

router.use(xss());

const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "enclosed/");
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    cb(
      null,
      file.fieldname +
        "-" +
        uniqueSuffix +
        "." +
        file.mimetype.replace("image/", "")
    );
  },
});
const upload = multer({
  storage: storage,
});

router.get("/", (req, res) => {
  res.render("frontend_new/premium_profile", {
    ip: req.ipInfo.ip,
    csrfToken: req.csrfToken(),
  });
});

router.post("/", upload.array("pics_enclosed", 10), async (req, res) => {
  try {
    let redirect = {};
    if (process.env.PAYMENT_MODE == "sandbox") {
      redirect = {
        return_url: "http://localhost:7070/premium/success",
        cancel_url: "http://localhost:7070/premium/cancel",
      };
    } else {
      redirect = {
        return_url: "http://www.orapages.com/premium/success",
        cancel_url: "http://www.orapages.com/premium/cancel",
      };
    }
    let create_payment_json = {
      intent: "sale",
      payer: {
        payment_method: "paypal",
      },
      redirect_urls: redirect,
      transactions: [
        {
          item_list: {
            items: [
              {
                name: "Premium profile",
                sku: "001",
                price: "100.00",
                currency: "USD",
                quantity: 1,
              },
            ],
          },
          amount: {
            currency: "USD",
            total: "100.00",
          },
          description: "Premium Profile in orapages.com",
        },
      ],
    };
    let profile = req.body;
    if (!profile.pics_website) {
      profile.pics_website = null;
    }
    if (req.files.length > 0) {
      profile.pics_enclosed = JSON.stringify(req.files);
    } else {
      profile.pics_later = 1;
    }
    if (profile.pics_later == "on") {
      profile.pics_later = 1;
    } else {
      profile.pics_later = 0;
    }
    if (!profile.details) {
      profile.details = null;
    }
    paypal.payment.create(create_payment_json, async function (error, payment) {
      if (error) {
        throw error;
      } else {
        let links = {};
        profile.payment_create_time = payment.create_time;
        profile.payment_id = payment.id;
        payment.links.forEach(function (linkObj) {
          links[linkObj.rel] = {
            href: linkObj.href,
            method: linkObj.method,
          };
        });

        if (links.hasOwnProperty("approval_url")) {
          const order = new PremiumOrder(profile);
          await order.setOrder();
          res.redirect(links["approval_url"].href);
        } else {
          res.redirect("/cancel");
        }
      }
    });
  } catch (error) {
    res.status(400).send("Failed to place the order");
  }
});

router.get("/success", async (req, res) => {
  try {
    const { PayerID, paymentId } = req.query;
    let execute_payment_json = {
      payer_id: PayerID,
      transactions: [
        {
          amount: {
            currency: "USD",
            total: "100.00",
          },
        },
      ],
    };
    const query = `select * from premium_orders where payment_id = ?`;
    const orderDB = await queryPromise(query, [paymentId]);
    const order = new PremiumOrder(orderDB[0]);
    if (order.payment_status == "approved") {
      return res.render("frontend_new/payment_done", {
        status: true,
        ip: req.ipInfo.ip,
        csrfToken: req.csrfToken(),
        name: order.name,
        owner: false,
      });
    }
    paypal.payment.execute(
      paymentId,
      execute_payment_json,
      async function (error, payment) {
        if (error) {
          throw error;
        } else {
          if (payment.state == "approved") {
            await order.updateOrder(
              payment.state,
              payment.update_time,
              payment.id
            );
            await invoice(
              order.name,
              order.email,
              order.payment_update_time,
              payment.id
            );
            res.render("frontend_new/payment_done", {
              status: true,
              ip: req.ipInfo.ip,
              csrfToken: req.csrfToken(),
              name: order.name,
              owner: false,
            });
          } else {
            res.redirect("/cancel");
          }
        }
      }
    );
  } catch (error) {
    res.redirect("/cancel");
  }
});

router.get("/cancel", (req, res) => {
  res.render("frontend_new/payment_done", {
    status: false,
    ip: req.ipInfo.ip,
    csrfToken: req.csrfToken(),
    owner: false,
    name: "Buyer",
  });
});

module.exports = router;
