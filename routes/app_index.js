// const {
//     auth,
//     database,
//     storage
// } = require('../firebase_init_admin');

// var express = require('express');
// var router = express.Router();



// var isOwner = function (req, res, next) {
//     var idToken = req.body.idToken;
//     auth.verifyIdToken(idToken).then((claims) => {
//         if (claims.owner === true) {
//             req.uid = claims.uid;
//             next();
//         } else {
//             res.send('You do not own any company');
//         }
//     }).catch(function (err) {
//         res.send(err);
//     });
// }


// router.get("/getip", (req, res) => {
//     res.json(req.headers['fastly-client-ip']);
// });

// var validateOwner = (req, res, task) => {
//     // console.log(req.cookies.session);
//     const sessionCookie = req.cookies.__session || '';
//     // Verify the session cookie. In this case an additional check is added to detect
//     // if the user's Firebase session was revoked, user deleted/disabled, etc.
//     if (sessionCookie == '') {
//         req.body.owner = false;
//         return task(req);
//     }
//     auth.verifySessionCookie(sessionCookie)
//         .then((decodedClaims) => {
//             if (decodedClaims.owner) {
//                 req.body.owner = true;
//                 task(req);
//             } else {
//                 req.body.owner = false;
//                 task(req);
//             }
//         })
//         .catch(error => {
//             res.json({
//                 owner: false
//             })
//         });
// };


// // router.get("/app/", (req, res) => {
// //     validateOwner(req, res, function (req) {
// //         res.json({
// //             owner: req.body.owner
// //         });
// //     });
// //     return;
// // });

// /*
//     params {
//         filter_type : [company,services,brands],
//         location_key:key of location,
//         keyword:like abc 
//     }
// */
// router.get("/app/search_results", async function (req, res) {
//     if (req.query.filter_type == 'company') {
//         let companiesData = {};
//         let company_locations = await database.ref("company_locations/" + req.query.location_key).once("value");
//         if (!company_locations.exists()) {
//             validateOwner(req, res, function (req) {
//                 res.json( {
//                     companies: {
//                         companydetails: [],
//                         type: 'company',
//                         location_name: req.query.location_name,
//                         keyword: req.query.keyword,
//                         location_key: req.query.location_key,
//                         owner: req.body.owner
//                     }
//                 })
//             });
//             return;
//         }

//         let companies = await database.ref("company_names").orderByChild("short-name").startAt(rewrite(req.query.keyword)).endAt(rewrite(req.query.keyword) + "\uf8ff").once("value");
//         if (!companies.exists()) {
//             validateOwner(req, res, function (req) {
//                 res.json( {
//                     companies: {
//                         companydetails: [],
//                         type: 'company',
//                         location_name: req.query.location_name,
//                         keyword: req.query.keyword,
//                         location_key: req.query.location_key,
//                         owner: req.body.owner
//                     }
//                 });
//             });
//             return;
//         }
//         company_locations = Object.keys(company_locations.val());
//         companies = Object.keys(companies.val());

//         intersectCompanies = company_locations.filter(function (x) {
//             if (companies.indexOf(x) != -1)
//                 return true;
//             else
//                 return false;
//         });


//         if (intersectCompanies.length <= 0) {
//             validateOwner(req, res, function (req) {
//                 res.json( {
//                     companies: {
//                         companydetails: [],
//                         type: 'company',
//                         location_name: req.query.location_name,
//                         keyword: req.query.keyword,
//                         location_key: req.query.location_key,
//                         owner: req.body.owner
//                     }
//                 })
//             });
//             return;
//         }

//         for (const company of intersectCompanies) {
//             let data = await database.ref(`companies/${company}`).once("value");
//             var v = data.val();
//             v["company_key"] = data.key
//             companiesData[data.key] = v;
//         };

//         var c = Object.values(companiesData);
//         c.sort((a, b) => {
//             return a.company_name_short.localeCompare(b.company_name_short)
//         });


//         validateOwner(req, res, function (req) {
//             res.json({
//                 companies: {
//                     companydetails: c,
//                     type: 'company',
//                     location_name: req.query.location_name,
//                     keyword: req.query.keyword,
//                     location_key: req.query.location_key,
//                     owner: req.body.owner
//                 }
//             })
//         });
//         return;

//     } else if (req.query.filter_type == 'services') {
//         var companiesData = {};
//         var company_location_wise = {};
//         var company_category_wise = [];
//         var category_wise = [];
//         var category_wise_value = [];
//         var intersectCompanies = {};
//         let ref_company_location_wise = await database.ref("company_locations/" + req.query.location_key).once("value");
//         if (!ref_company_location_wise.exists()) {
//             validateOwner(req, res, function (req) {
//                 res.json({
//                     companies: {
//                         companydetails: [],
//                         type: 'services',
//                         location_name: req.query.location_name,
//                         keyword: req.query.keyword,
//                         location_key: req.query.location_key,
//                         owner: req.body.owner
//                     }
//                 })
//             });
//             return;
//         }
//         company_location_wise = Object.keys(ref_company_location_wise.val());
//         let ref_category = await database.ref("categories").orderByChild("category_name_short").startAt(rewrite(req.query.keyword)).endAt(rewrite(req.query.keyword) + "\uf8ff").once("value");
//         if (!ref_category.exists()) {
//             validateOwner(req, res, function (req) {
//                 res.json({
//                     companies: {
//                         companydetails: [],
//                         type: 'services',
//                         location_name: req.query.location_name,
//                         keyword: req.query.keyword,
//                         location_key: req.query.location_key,
//                         owner: req.body.owner
//                     }
//                 })
//             });
//             return;
//         }
//         category_wise = Object.keys(ref_category.val());
//         category_wise_value = ref_category.val();

//         for (let index = 0; index < category_wise.length; index++) {
//             let companies_categories = await database.ref(`company_categories/${category_wise[index]}`).once("value");
//             if (companies_categories.val() != null) {
//                 company_category_wise = [...company_category_wise, ...Object.keys(companies_categories.val())];
//             }
//         }

//         intersectCompanies = company_location_wise.filter(function (x) {
//             if (company_category_wise.indexOf(x) != -1)
//                 return true;
//             else
//                 return false;
//         });

//         if (intersectCompanies.length == 0) {
//             validateOwner(req, res, function (req) {
//                 res.json({
//                     companies: {
//                         companydetails: [],
//                         type: 'services',
//                         location_name: req.query.location_name,
//                         keyword: req.query.keyword,
//                         location_key: req.query.location_key,
//                         owner: req.body.owner
//                     }
//                 })
//             });
//             return;
//         }
//         var l = 0;

//         var tempcategories = [];
//         intersectCompanies.forEach(singleCompany => {
//             category_wise.forEach(singlecategory => {
//                 database.ref("companies/" + singleCompany + "/categories/" + singlecategory).once("value", function (category_details) {
//                     l++;

//                     if (category_details.exists()) {
//                         if (tempcategories.indexOf(category_details.val().category_id) < 0) {
//                             tempcategories.push(category_details.val().category_id);
//                             var v = category_details.val();
//                             v["category_key"] = singlecategory;
//                             v["number"] = 1;
//                             companiesData[singlecategory] = v;
//                         } else {
//                             companiesData[singlecategory]["number"] += 1;
//                         }
//                     } else {
//                         if (tempcategories.indexOf(singlecategory) < 0) {
//                             tempcategories.push(singlecategory);
//                             var v = category_wise_value[singlecategory];
//                             v["category_key"] = singlecategory;
//                             v["number"] = 0;
//                             companiesData[singlecategory] = v;
//                         }
//                     }
//                     if (l == ((intersectCompanies.length) * category_wise.length)) {
//                         var c = Object.values(companiesData);
//                         c.sort((a, b) => {
//                             return a.category_name.localeCompare(b.category_name)
//                         });
//                         validateOwner(req, res, function (req) {
//                             res.json({
//                                 companies: {
//                                     companydetails: c,
//                                     type: 'services',
//                                     location_name: req.query.location_name,
//                                     keyword: req.query.keyword,
//                                     location_key: req.query.location_key,
//                                     owner: req.body.owner
//                                 }
//                             })
//                         });
//                         return;
//                     }

//                 });
//             });
//         });
//     } else if (req.query.filter_type == 'brands') {
//         var companiesData = {};
//         var company_location_wise = {};
//         var company_brand_wise = [];
//         var brand_wise = [];
//         var intersectCompanies = {};
//         var brand_wise_value = [];
//         database.ref("company_locations/" + req.query.location_key).once("value", function (companies_location) {
//             if (!companies_location.exists()) {
//                 validateOwner(req, res, function (req) {
//                     res.json({
//                         companies: {
//                             companydetails: [],
//                             type: 'brands',
//                             location_name: req.query.location_name,
//                             keyword: req.query.keyword,
//                             location_key: req.query.location_key,
//                             owner: req.body.owner
//                         }
//                     })
//                 });
//                 return;
//             }
//             company_location_wise = Object.keys(companies_location.val());

//         }).then(function () {
//             database.ref("brands").orderByChild("brand_name_short").startAt(rewrite(req.query.keyword)).endAt(rewrite(req.query.keyword) + "\uf8ff")
//                 .once("value", function (brands) {
//                     if (!brands.exists()) {
//                         validateOwner(req, res, function (req) {
//                             res.json({
//                                 companies: {
//                                     companydetails: [],
//                                     type: 'brands',
//                                     location_name: req.query.location_name,
//                                     keyword: req.query.keyword,
//                                     location_key: req.query.location_key,
//                                     owner: req.body.owner
//                                 }
//                             })
//                         });
//                         return;
//                     }
//                     brand_wise = Object.keys(brands.val());
//                     brand_wise_value = brands.val();
//                 }).then(() => {
//                     database.ref("company_brands").once("value", function (companies_brands) {
//                         if (!companies_brands.exists()) {
//                             validateOwner(req, res, function (req) {
//                                 res.json( {
//                                     companies: {
//                                         companydetails: [],
//                                         type: 'brands',
//                                         location_name: req.query.location_name,
//                                         keyword: req.query.keyword,
//                                         location_key: req.query.location_key,
//                                         owner: req.body.owner
//                                     }
//                                 })
//                             });
//                             return;
//                         }
//                         brand_wise.forEach(brand => {
//                             if (typeof companies_brands.val()[brand] !== 'undefined') {
//                                 Array.prototype.push.apply(company_brand_wise, Object.keys(companies_brands.val()[brand]));
//                             }
//                         });
//                     }).then(() => {

//                         intersectCompanies = company_location_wise.filter(function (x) {
//                             if (company_brand_wise.indexOf(x) != -1)
//                                 return true;
//                             else
//                                 return false;
//                         });
//                         if (intersectCompanies.length == 0) {
//                             validateOwner(req, res, function (req) {
//                                 res.json({
//                                     companies: {
//                                         companydetails: [],
//                                         type: 'brands',
//                                         location_name: req.query.location_name,
//                                         keyword: req.query.keyword,
//                                         location_key: req.query.location_key,
//                                         owner: req.body.owner
//                                     }
//                                 })
//                             });
//                             return;
//                         }
//                         var l = 0;

//                         var tempBrands = [];
//                         intersectCompanies.forEach(singleCompany => {
//                             brand_wise.forEach(singleBrand => {
//                                 database.ref("companies/" + singleCompany + "/brands/" + singleBrand).once("value", function (brand_details) {
//                                     l++;
//                                     if (brand_details.exists()) {
//                                         if (tempBrands.indexOf(brand_details.val().brand_id) < 0) {
//                                             tempBrands.push(brand_details.val().brand_id);
//                                             var v = brand_details.val();
//                                             v["brand_key"] = singleBrand;
//                                             v["number"] = 1;
//                                             companiesData[singleBrand] = v;
//                                         } else {
//                                             companiesData[singleBrand]["number"] += 1;
//                                         }
//                                     } else {
//                                         if (tempBrands.indexOf(singleBrand) < 0) {
//                                             tempBrands.push(singleBrand);
//                                             var v = brand_wise_value[singleBrand];
//                                             v["brand_key"] = singleBrand;
//                                             v["number"] = 0;
//                                             companiesData[singleBrand] = v;
//                                         }
//                                     }

//                                     if (l == intersectCompanies.length + brand_wise.length - 1) {
//                                         var c = Object.values(companiesData);
//                                         c.sort((a, b) => {
//                                             return a.brand_name.localeCompare(b.brand_name)
//                                         });
//                                         validateOwner(req, res, function (req) {
//                                             res.json({
//                                                 companies: {
//                                                     companydetails: [],
//                                                     type: 'brands',
//                                                     location_name: req.query.location_name,
//                                                     keyword: req.query.keyword,
//                                                     location_key: req.query.location_key,
//                                                     owner: req.body.owner
//                                                 }
//                                             })
//                                         });
//                                         return;
//                                     }
//                                 });
//                             });
//                         });
//                     });
//                 });
//         });
//     }
// });

// // router.get("/brands_results", (req, res) => {
// //     var companiesData = {};
// //     var company_location_wise = {};
// //     var company_brand_wise = [];
// //     var intersectCompanies = {};

// //     database.ref("company_locations/" + req.query.location_key).once("value", function (companies_location) {
// //         if (!companies_location.exists()) {
// //             validateOwner(req, res, function (req) {
// //                 res.render("frontend/user_search", {
// //                     companies: {
// //                         companydetails: [],
// //                         type: 'brands_results',
// //                         owner: req.body.owner
// //                     }
// //                 });
// //             });
// //             return;
// //         }
// //         company_location_wise = Object.keys(companies_location.val());
// //     }).then(function () {
// //         database.ref("brands").orderByChild("brand_name_short").equalTo(rewrite(req.query.brand))
// //             .once("value", function (brands) {
// //                 if (!brands.exists()) {
// //                     validateOwner(req, res, function (req) {
// //                         res.render("frontend/user_search", {
// //                             companies: {
// //                                 companydetails: [],
// //                                 type: 'brands_results',
// //                                 owner: req.body.owner
// //                             }
// //                         });
// //                     });
// //                     return;
// //                 }

// //                 database.ref("company_brands/" + Object.keys(brands.val())[0]).once("value", function (companies_brands) {
// //                     if (!companies_brands.exists()) {
// //                         validateOwner(req, res, function (req) {
// //                             res.render("frontend/user_search", {
// //                                 companies: {
// //                                     companydetails: [],
// //                                     type: 'brands_results',
// //                                     owner: req.body.owner
// //                                 }
// //                             });
// //                         });
// //                         return;
// //                     }

// //                     company_brand_wise = Object.keys(companies_brands.val());

// //                     intersectCompanies = company_location_wise.filter(function (x) {
// //                         if (company_brand_wise.indexOf(x) != -1)
// //                             return true;
// //                         else
// //                             return false;
// //                     });

// //                     if (intersectCompanies.length == 0) {
// //                         validateOwner(req, res, function (req) {
// //                             res.render("frontend/user_search", {
// //                                 companies: {
// //                                     companydetails: [],
// //                                     type: 'brands_results',
// //                                     owner: req.body.owner
// //                                 }
// //                             });
// //                         });
// //                         return;
// //                     }

// //                     var l = 0;

// //                     intersectCompanies.forEach(singleCompany => {
// //                         database.ref("companies/" + singleCompany).once("value", function (company_details) {
// //                             l++;
// //                             var v = company_details.val();
// //                             v["company_key"] = singleCompany
// //                             companiesData[singleCompany] = v;
// //                             if (l == intersectCompanies.length) {
// //                                 var c = Object.values(companiesData);
// //                                 c.sort((a, b) => {
// //                                     return a.company_name_short.localeCompare(b.company_name_short)
// //                                 });

// //                                 validateOwner(req, res, function (req) {
// //                                     res.render("frontend/user_search", {
// //                                         companies: {
// //                                             companydetails: [],
// //                                             type: 'brands_results',
// //                                             brand_wise: Object.keys(brands.val())[0]
// //                                         }
// //                                     });
// //                                 });
// //                                 return;
// //                             }
// //                         });
// //                     });
// //                 });
// //             });
// //     });
// // });

// // router.get("/categories_results", (req, res) => {
// //     var companiesData = {};
// //     var company_location_wise = {};
// //     var company_category_wise = [];
// //     var category_wise = [];
// //     var intersectCompanies = {};
// //     database.ref("company_locations/" + req.query.location_key).once("value", function (companies_location) {
// //         if (!companies_location.exists()) {
// //             validateOwner(req, res, function (req) {
// //                 res.render("frontend/user_search", {
// //                     companies: {
// //                         companydetails: [],
// //                         type: 'services_results',
// //                         owner: req.body.owner
// //                     }
// //                 });
// //             });
// //             return;
// //         }
// //         company_location_wise = Object.keys(companies_location.val());
// //         // console.log(company_location_wise);

// //     }).then(function () {
// //         database.ref("categories").orderByChild("category_name_short").equalTo(req.query.category)
// //             .once("value", function (categories) {
// //                 // console.log(categories.val());

// //                 if (!categories.exists()) {
// //                     validateOwner(req, res, function (req) {
// //                         res.render("frontend/user_search", {
// //                             companies: {
// //                                 companydetails: [],
// //                                 type: 'services_results',
// //                                 owner: req.body.owner
// //                             }
// //                         });
// //                     });
// //                     return;
// //                 }
// //                 category_wise = Object.keys(categories.val());
// //             }).then(() => {
// //                 database.ref("company_categories").once("value", function (companies_categories) {
// //                     if (!companies_categories.exists()) {
// //                         validateOwner(req, res, function (req) {
// //                             res.render("frontend/user_search", {
// //                                 companies: {
// //                                     companydetails: [],
// //                                     type: 'services_results',
// //                                     owner: req.body.owner
// //                                 }
// //                             });
// //                         });
// //                         return;
// //                     }
// //                     category_wise.forEach(category => {
// //                         if (typeof companies_categories.val()[category] !== 'undefined') {
// //                             Array.prototype.push.apply(company_category_wise, Object.keys(companies_categories.val()[category]));
// //                         }
// //                     });
// //                 }).then(() => {
// //                     intersectCompanies = company_category_wise.filter(function (x) {
// //                         if (company_location_wise.indexOf(x) != -1)
// //                             return true;
// //                         else
// //                             return false;
// //                     });

// //                     if (intersectCompanies.length == 0) {
// //                         return res.render("frontend/user_search", {
// //                             companies: {
// //                                 companydetails: [],
// //                                 type: 'services_results',
// //                                 owner: req.body.owner
// //                             }
// //                         });
// //                     }
// //                     var l = 0;
// //                     intersectCompanies.forEach(singleCompany => {
// //                         database.ref("companies/" + singleCompany).once("value", function (company_details) {
// //                             l++;
// //                             var v = company_details.val();
// //                             v["company_key"] = singleCompany
// //                             companiesData[singleCompany] = v;
// //                             if (l == intersectCompanies.length) {
// //                                 var c = Object.values(companiesData);
// //                                 c.sort((a, b) => {
// //                                     return a.company_name_short.localeCompare(b.company_name_short)
// //                                 });
// //                                 validateOwner(req, res, function (req) {
// //                                     res.render("frontend/user_search", {
// //                                         companies: {
// //                                             companydetails: c,
// //                                             type: 'services_results',
// //                                             category_wise: category_wise[0],
// //                                             owner: req.body.owner,
// //                                         }
// //                                     });
// //                                     return;
// //                                 });
// //                             }
// //                         });
// //                     });
// //                 });
// //             });
// //     });
// // });


// /*
//     params {
//         location_key // key of state, country,city,
//         term // like a,b,c,6, {min three chars} 
//     }
// */
// router.get("/app/companiesName", async (req, res) => {
//     let companies_data = {};
//     if(req.query.term.length < 2){
//         return res.send([{
//             label: "Please enter more than 2 characters",
//             value: req.query.term,
//             key: ""
//         }]);
//     }
//     let company_locations = await database.ref("company_locations/" + req.query.location_key).once("value");
//     if (!company_locations.exists()) {
//         res.send([{
//             label: "No data found",
//             value: req.query.term,
//             key: ""
//         }]);
//         return;
//     }

//     let companies = await database.ref("company_names").orderByChild("short-name").startAt(rewrite(req.query.term)).endAt(rewrite(req.query.term) + "\uf8ff").once("value");
//     if (!companies.exists()) {
//         res.send([{
//             label: "No data found",
//             value: req.query.term,
//             key: ""
//         }]);
//         return;
//     }
//     company_locations = Object.keys(company_locations.val());
//     let companiesKeys = Object.keys(companies.val());

//     intersectCompanies = company_locations.filter(function (x) {
//         if (companiesKeys.indexOf(x) != -1)
//             return true;
//         else
//             return false;
//     });

//     if (intersectCompanies.length <= 0) {
//         res.send([{
//             label: "No data found",
//             value: req.query.term,
//             key: ""
//         }]);
//         return;
//     }


//     let c = [];
//     for (const company of intersectCompanies) {
//         let data = await database.ref(`companies_url/${company}`).once("value");
//         let v = {
//             value: companies.val()[company]["name"],
//             key: data.val()
//         };
//         // console.log(v);

//         c.push(v);
//         // v["company_key"] = data.key
//         // companies_data[data.key] = v;
//     };

//     // var c = Object.values(companies_data);
//     c.sort((a, b) => {
//         return a.value.localeCompare(b.value)
//     });

//     // console.log(c);

//     return res.send(c);
// });


// router.get("/app/locations", async (req, res) => {
//     var locations = [];
//     let i = 0;
//     let countries = await database.ref("countries").once("value");
//     let states = await database.ref("states").once("value");
//     let cities = await database.ref("cities").once("value");
//     countries = countries.val();
//     states = states.val();
//     cities = cities.val();
//     var ckeys = Object.keys(countries);
//     ckeys.forEach(country => {
//         locations[i] = {
//             key: country,
//             value: countries[country].country_name,
//         }
//         i++;
//         if (typeof states[country] !== 'undefined') {
//             var sKeys = Object.keys(states[country]);
//             sKeys.forEach(state => {
//                 locations[i] = {
//                     key: state,
//                     value: states[country][state].state_name + ", " + countries[country].country_name
//                 }
//                 i++;
//                 if (typeof cities[state] !== 'undefined') {
//                     var cKeys = Object.keys(cities[state]);
//                     cKeys.forEach(city => {
//                         locations[i] = {
//                             key: city,
//                             value:  cities[state][city].city_name + ", " + states[country][state].state_name + ", " + countries[country].country_name
//                         }
//                         i++;
//                     });
//                 }
//             });
//         }
//     });
//     // res.send(locations);
//     locations.sort(function (a, b) {
//         return a.value.localeCompare(b.value);
//     });

//     // return res.send(locations);

//     let j = 0;
//     while (j < locations.length) {
//         var citystatecountry = locations[j].value.replace(/\, /g, "-").split(",");
//         if (citystatecountry.length == 3) {
//             if (citystatecountry[2] == citystatecountry[1] && citystatecountry[1] == citystatecountry[
//                     0]) {
//                 delete locations[j]
//             } else if (citystatecountry[0] == citystatecountry[1]) {
//                 locations[j].value = citystatecountry[2] + ", " + citystatecountry[1];
//             } else if (citystatecountry[2] == citystatecountry[1]) {
//                 delete locations[j]
//             } else {
//                 locations[j].value = citystatecountry.reverse().join(", ").replace(/\-/g, ", ");
//             }
//         } else if (citystatecountry.length == 2) {
//             if (citystatecountry[1] == citystatecountry[0]) {
//                 delete locations[j]
//             } else {
//                 locations[j].value = citystatecountry.reverse().join(", ").replace(/\-/g, ", ");
//             }
//         } else {
//             locations[j].value = citystatecountry.reverse().join(", ").replace(/\-/g, ", ");
//         }
//         j++;
//     }

//     res.send(locations);

// });


// router.get("/app/locationsform", (req, res) => {
//     var countries;
//     var states;
//     var cities;
//     var locations = [];
//     var i = 0;
//     database.ref("countries").once("value", function (acountries) {
//         countries = acountries.val();
//     }).then(() => {
//         database.ref("states").once("value", function (astates) {
//             states = astates.val();
//         }).then(() => {
//             database.ref("cities").once("value", function (acities) {
//                 cities = acities.val();
//             }).then(() => {
//                 var ckeys = Object.keys(countries);
//                 ckeys.forEach(country => {
//                     // locations[i] = {
//                     //     key: country,
//                     //     value: countries[country].country_name
//                     // }
//                     if (typeof states[country] !== 'undefined') {
//                         var sKeys = Object.keys(states[country]);
//                         sKeys.forEach(state => {
//                             if (typeof cities[state] !== 'undefined') {
//                                 var cKeys = Object.keys(cities[state]);
//                                 cKeys.forEach(city => {
//                                     let value;
//                                     if (cities[state][city].city_name_short == states[country][state].state_name_short && states[country][state].state_name_short == countries[country].country_name_short) {
//                                         value = cities[state][city].city_name;
//                                     } else if (states[country][state].state_name_short == countries[country].country_name_short) {
//                                         value = cities[state][city].city_name + ", " + states[country][state].state_name
//                                     } else if (cities[state][city].city_name_short == states[country][state].state_name_short) {
//                                         value = states[country][state].state_name + ", " + countries[country].country_name;
//                                     } else {
//                                         value = cities[state][city].city_name + ", " + states[country][state].state_name + ", " + countries[country].country_name;
//                                     }
//                                     locations[i] = {
//                                         key: city + "," + state + "," + country,
//                                         value: value
//                                     }
//                                     i++;
//                                 });
//                             }
//                         });
//                     }
//                 });
//                 res.send(locations);
//             });
//         })
//     });
// });

// // router.get("/getcategories", (req, res) => {
// //     var send_subcategories = [];
// //     var category_keys = [];
// //     var ucategories;
// //     database.ref("/categories").once("value", function (categories) {
// //         ucategories = categories.val();
// //         categories.forEach((category) => {
// //             if (category.val().category_name_short.startsWith(rewrite(req.query.term))) {
// //                 send_subcategories.push({
// //                     key: category.key,
// //                     value: category.val().category_name
// //                 })
// //                 category_keys.push(category.key);
// //             }
// //         })
// //     }).then(function () {
// //         database.ref("/subcategories").once("value", function (subcategories) {
// //             var values = Object.values(subcategories.val());

// //             for (let i = 0; i < values.length; i++) {
// //                 const element = values[i];
// //                 var innerValues = Object.values(element);
// //                 for (let j = 0; j < innerValues.length; j++) {
// //                     const innerElement = innerValues[j];
// //                     if (innerElement.subcategory_name_short.startsWith(rewrite(req.query.term)) && category_keys.indexOf(innerElement.category_id) < 0) {
// //                         send_subcategories.push({
// //                             key: innerElement.category_id,
// //                             value: ucategories[innerElement.category_id].category_name
// //                         });
// //                         category_keys.push(innerElement.category_id);
// //                     }
// //                 }
// //             }
// //             res.send(send_subcategories);
// //         });
// //     });
// // });

// // router.get("/categoryName", (req, res) => {
// //     var send_subcategories = [];
// //     var category_keys = [];
// //     var ucategories;
// //     database.ref("/categories").once("value", function (categories) {
// //         ucategories = categories.val();
// //         categories.forEach((category) => {
// //             if (category.val().category_name_short.startsWith(rewrite(req.query.term))) {
// //                 send_subcategories.push({
// //                     key: category.val().category_name,
// //                     value: category.val().category_name
// //                 })
// //                 category_keys.push(category.key);
// //             }
// //         })
// //     }).then(function () {
// //         database.ref("/subcategories").once("value", function (subcategories) {
// //             var values = Object.values(subcategories.val());

// //             for (let i = 0; i < values.length; i++) {
// //                 const element = values[i];
// //                 var innerValues = Object.values(element);
// //                 for (let j = 0; j < innerValues.length; j++) {
// //                     const innerElement = innerValues[j];
// //                     if (innerElement.subcategory_name_short.startsWith(rewrite(req.query.term)) && category_keys.indexOf(innerElement.category_id) < 0) {
// //                         send_subcategories.push({
// //                             key: ucategories[innerElement.category_id].category_name,
// //                             value: ucategories[innerElement.category_id].category_name
// //                         });
// //                         category_keys.push(innerElement.category_id);
// //                     }
// //                 }
// //             }
// //             res.send({
// //                 data: send_subcategories,
// //                 location: req.query.location_key
// //             });
// //         });
// //     });
// // });

// router.get("/app/getbrands", (req, res) => {
//     var sendbrands = [];
//     database.ref("brands").once("value", function (brands) {
//         var i = 0;
//         brands.forEach(brand => {
//             sendbrands[i] = {
//                 key: brand.key,
//                 value: brand.val().brand_name
//             }
//             if (i == brands.numChildren() - 1) {
//                 res.send(sendbrands);
//             }
//             i++;
//         });
//     });
// });


// router.get("/app/register", (req, res) => {
//     res.json( {
//         ip: req.headers['fastly-client-ip']
//     });
// });


// router.post("/app/login_owner", isOwner, (req, res) => {
//     database.ref("owners/" + req.uid + "/companies").once("value", function (companies) {
//         var company = Object.keys(companies.val())[0];
//         database.ref("companies/" + company).once("value", function (companyData) {
//             res.json( {
//                 company_details: companyData.val(),
//                 company_key: companyData.key,
//                 ip: req.headers['fastly-client-ip']
//             });
//         });
//     });
// });


// router.get("/app/currentBanner", (req, res) => {
//     database.ref("currentBanner").once("value", function (snapshot) {
//         database.ref("banners").orderByChild("status").equalTo("on").once("value", function (banners) {
//             if (!banners.val()) {
//                 database.ref("currentBanner").set(null);
//                 res.send("No banners");
//                 return;
//             } else if (!snapshot.val()) {
//                 var bannersKeys = Object.keys(banners.val());
//                 var v = banners.val()[bannersKeys[0]];
//                 v["time"] = new Date().getTime();
//                 database.ref("currentBanner").set({
//                     [bannersKeys[0]]: v
//                 });
//                 res.send({
//                     [bannersKeys[0]]: v
//                 });
//                 return;
//             }
//             if ((new Date() - snapshot.val()[(Object.keys(snapshot.val())[0])].time) > 3000) {
//                 var k = 0;
//                 var bannersKeys = Object.keys(banners.val());
//                 var index = bannersKeys.indexOf(Object.keys(snapshot.val())[0]);
//                 var j = 0;
//                 if (index == bannersKeys.length - 1) {
//                     j = 0;
//                 } else {
//                     j = index + 1;
//                 }
//                 for (j; j < bannersKeys.length; j++) {
//                     if (banners.val()[bannersKeys[j]].status == "on" && k < 1) {
//                         k++;
//                         var v = banners.val()[bannersKeys[j]];
//                         v["time"] = new Date().getTime();
//                         database.ref("currentBanner").set({
//                             [bannersKeys[j]]: v
//                         });
//                         res.send({
//                             [bannersKeys[j]]: v
//                         });
//                         break;
//                     } else {
//                         if (j == bannersKeys.length - 1 && k < 1) {
//                             j = -1;
//                             k = 0;
//                         }
//                     }
//                 }
//             } else {
//                 database.ref("currentBanner").once("value", function (banner) {
//                     res.send(banner.val());
//                     return;
//                 });
//             }

//         });

//     });
// });

// const rewrite = (name) => name.replace(/[^a-z0-9A-z]/g, "-").replace(/-+$/, "").replace(/-+/g, "-").toLowerCase()


// function renderWithLogo(res, company, send) {
//     let logo = company.logo || null;
//     company.logo_url = null;
//     if (logo != null) {
//         storage.file(company.logo.url).getSignedUrl({
//             action: 'read',
//             expires: '03-09-2491'
//         }).then((url) => {
//             company.logo_url = url[0];
//             send(res, company);
//         });
//     } else {
//         send(res, company);
//     }

// }

// router.get("/app/:url?", (req, res) => {
//     const sessionCookie = req.cookies.__session || '';
//     // Verify the session cookie. In this case an additional check is added to detect
//     // if the user's Firebase session was revoked, user deleted/disabled, etc.
//     auth.verifySessionCookie(sessionCookie)
//         .then((decodedClaims) => {
//             if (decodedClaims.owner) {
//                 req.body.claims = decodedClaims;
//                 if (req.params.url != '') {
//                     database.ref("companies").orderByChild("company_url").equalTo(req.params.url).once("value", function (companyDetails) {
//                         if (companyDetails.exists()) {
//                             database.ref("owners/" + req.body.claims.uid).once("value", function (owner) {
//                                 if (Object.keys(owner.val().companies)[0] == Object.keys(companyDetails.val())[0]) {
//                                     renderWithLogo(res, Object.values(companyDetails.val())[0], function (res, company) {
//                                         res.json({
//                                             details: company,
//                                             owner: true
//                                         });
//                                     });
//                                 } else {
//                                     renderWithLogo(res, Object.values(companyDetails.val())[0], function (res, company) {
//                                         res.json({
//                                             details: company,
//                                             owner: false
//                                         });
//                                     });
//                                 }
//                             });
//                         } else {
//                             res.send("No Companies");
//                         }
//                     });
//                 }
//             } else {
//                 if (req.params.url != '') {
//                     database.ref("companies").orderByChild("company_url").equalTo(req.params.url.toLowerCase()).once("value", function (companyDetails) {
//                         if (companyDetails.exists()) {
//                             renderWithLogo(res, Object.values(companyDetails.val())[0], function (res, company) {
//                                 res.json({
//                                     details: company,
//                                     owner: false
//                                 });
//                             });
//                         } else {
//                             res.send("No Companies");
//                         }
//                     });
//                 }
//             }
//         })
//         .catch(error => {
//             if (req.params.url != '') {
//                 database.ref("companies").orderByChild("company_url").equalTo(req.params.url.toLowerCase()).once("value", function (companyDetails) {
//                     if (companyDetails.exists()) {
//                         renderWithLogo(res, Object.values(companyDetails.val())[0], function (res, company) {
//                             res.json({
//                                 details: company,
//                                 owner: false
//                             });
//                         });
//                     } else {
//                         res.send("No Companies");
//                     }
//                 });
//             }
//         });
// });


// module.exports = router;