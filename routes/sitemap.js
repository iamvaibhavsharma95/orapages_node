const express = require("express"),
  router = express.Router(),
  { selectPromise, queryPromise } = require("../database/database"),
  json2xml = require("js2xmlparser"),
  Company = require("../database/company"),
  BASE_URL = "https://www.orapages.com/";

router.get("/sitemap.xml", async (req, res) => {
  try {
    const xml = await companiesSiteMap();
    res.set("Content-Type", "text/xml");
    res.status(200).send(xml);
  } catch (error) {
    res.status(404).send("Sitemap is not found");
  }
});

router.get("/cities_category/sitemap_index_:from.xml", async (req, res) => {
  try {
    const xml = await categoriesSiteMap(req.params.from);
    res.set("Content-Type", "text/xml");
    res.status(200).send(xml);
  } catch (error) {
    console.log(error);
    res.status(404).send("Sitemap is not found");
  }
});

router.get("/sitemaps/main.xml", (req, res) => {
  try {
    const collection = [];
  } catch (error) {
    res.status(404).send("Sitemap is not found");
  }
});

router.get("/sitemaps/:first-to-:last.xml", async (req, res) => {
  try {
    const companies = await selectPromise(
      `select company_id, company_url, last_update from company order by created_at asc limit 20000 offset ${req.params.first}`
    );
    const collection = [];
    for (const company of companies) {
      const url = {};
      let r = new Company(company);
      url.loc = `${BASE_URL}${company.company_url}`;
      url.changefreq = "monthly";
      url.priority = "0.5";
      url.lastmod = new Date(company.last_update).toISOString().split("T")[0];
      collection.push(url);
    }
    const col = {
      "@": {
        xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9",
        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "xsi:schemaLocation": `http://www.sitemaps.org/schemas/sitemap/0.9
                                        http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd`,
      },
      url: collection,
    };
    const xml = json2xml.parse("urlset", col);
    res.set("Content-Type", "text/xml");
    res.status(200).send(xml);
  } catch (error) {
    res.status(404).send("Sitemap is not found");
  }
});

const companiesSiteMap = async () => {
  const count = await selectPromise(
    `select count(company_id) as total from company`
  );
  const sitemap_count = Math.ceil(count[0].total / 20000);
  const collection = [];
  // const sitemap = {};
  // sitemap.loc = `${BASE_URL}sitemaps/main.xml`;
  // collection.push(sitemap);
  for (let index = 0; index < sitemap_count; index++) {
    const sitemap = {};
    sitemap.loc = `${BASE_URL}sitemaps/${index * 20000}-to-${
      (index + 1) * 20000
    }.xml`;
    collection.push(sitemap);
  }
  const col = {
    "@": {
      xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9",
    },
    sitemap: collection,
  };
  const xml = json2xml.parse("sitemapindex", col);
  return xml;
};

router.get("/cities_category/:city_id.xml", async (req, res) => {
  try {
    const cities =
      await selectPromise(`select countries.country_name_short, states.state_name_short,cities.city_name_short,category.category_name_short, count(company.company_id) as 'count'
        from company_category inner join company_location on company_location.company_id = company_category.company_id
        inner join company on company_category.company_id = company.company_id INNER JOIN cities on cities.city_id = company_location.city_id INNER JOIN states on states.state_id = cities.state_id INNER JOIN countries ON countries.country_id = cities.country_id
        INNER JOIN category ON category.category_id = company_category.category_id and company_location.city_id = '${req.params.city_id}' group by company_category.category_id order by company.company_business_name`);
    const collection = [];
    for (const city of cities) {
      for (let i = 0; i < Math.ceil(city.count / 100); i++) {
        const url = {};
        url.loc = `${BASE_URL}ps/${city.country_name_short}/${
          city.state_name_short
        }/${city.city_name_short}/${city.category_name_short}/page-${i + 1}`;
        url.changefreq = "monthly";
        url.priority = "0.5";
        url.lastmod = new Date("07-06-2013").toISOString().split("T")[0];
        collection.push(url);
      }
    }
    const col = {
      "@": {
        xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9",
        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "xsi:schemaLocation": `http://www.sitemaps.org/schemas/sitemap/0.9
                                        http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd`,
      },
      url: collection,
    };
    const xml = json2xml.parse("urlset", col);
    res.set("Content-Type", "text/xml");
    res.status(200).send(xml);
  } catch (error) {
    res.status(404).send("Sitemap is not found");
  }
});

const categoriesSiteMap = async (offset) => {
  const cities = await selectPromise(
    `select cities.city_id,count(company_location.company_id) as 'count' from cities inner join company_location on company_location.city_id = cities.city_id GROUP BY cities.city_name_short LIMIT 1000 OFFSET ${
      offset * 1000
    }`
  );
  const collection = [];
  cities.forEach((city) => {
    const sitemap = {};
    sitemap.loc = `${BASE_URL}cities_category/${city.city_id}.xml`;
    collection.push(sitemap);
  });
  const col = {
    "@": {
      xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9",
    },
    sitemap: collection,
  };
  const xml = json2xml.parse("sitemapindex", col);
  return xml;
};

module.exports = router;
