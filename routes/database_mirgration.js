// const mssql = require('mssql');
// const {
//     selectPromise,
//     queryPromise
// } = require('../database/database');
// const fse = require('fs-extra');
// const http = require('http');
// const fs = require('fs');
// const bcrypt = require('bcryptjs');
// const random = require('randomstring');

// const rewrite = (name) => name.replace(/[^a-z0-9A-z]/g, "-").replace(/-+$/, "").replace(/-+/g, "-").toLowerCase();

// const databaseLocationSingapore = () => {
//     update = {
//         city_id: "city-188",
//         state_id: "state-188",
//         country_id: "country-188",
//         address: "Singapore, Singapore, Singapore"
//     }
//     return {
//         location_data: update,
//         company_locations: ["city-188", "state-188", "country-188"]
//     };
// }


// const databaseLocation = async (city_id = 0) => {
//     let update = {}
//     let company_locations = []
//     update = {};
//     let location_name = [];
//     if (city_id == 0) {
//         update = {
//             city_id: "",
//             state_id: "",
//             country_id: "",
//             address: ""
//         }
//         return {
//             location_data: update,
//             company_locations: []
//         };
//     }

//     var state
//     var country;
//     var city;
//     var stateU;
//     try {
//         city = await mssql.query(`select * from city where id_city = ${city_id}`)
//         state = await mssql.query(`select * from state where id_state = ${city.recordset[0].id_state}`);
//         stateU = await mssql.query `select * from union_terretories where id_city = ${city_id}`;
//         country = await mssql.query(`select * from country where id_country = ${city.recordset[0].id_country}`)
//     } catch (error) {
//         console.error(error)
//     }


//     if (city.recordset[0].tx_city_3 != null) {
//         update['city_id'] = 'city-' + city.recordset[0].id_city;
//         location_name.push(city.recordset[0].tx_city_3);
//         update["state_id"] = 'state-' + state.recordset[0].id_state;
//         company_locations.push("city-" + city.recordset[0].id_city);
//         company_locations.push('state-' + state.recordset[0].id_state);
//         location_name.push(state.recordset[0].nm_state);
//     } else {
//         update['city_id'] = 'city-' + city.recordset[0].id_city;
//         update["state_id"] = 'state-' + stateU.recordset[0].id_state;
//         company_locations.push("city-" + city.recordset[0].id_city);
//         company_locations.push('state-' + stateU.recordset[0].id_state);
//         location_name.push(stateU.recordset[0].nm_terretory);
//         location_name.push(stateU.recordset[0].nm_state);
//     }

//     location_name.push(country.recordset[0].nm_country);
//     update["country_id"] = 'country-' + country.recordset[0].id_country;
//     company_locations.push('country-' + country.recordset[0].id_country);
//     update["address"] = location_name.join(", ");
//     return {
//         location_data: update,
//         company_locations: company_locations
//     };
// }

// const databaseLocationMalasya = async (city_id = 0) => {
//     let update = {}
//     let company_locations = []
//     update = {};
//     let location_name = [];
//     if (city_id == 0) {
//         update = {
//             city_id: "",
//             state_id: "",
//             country_id: "",
//             address: ""
//         }
//         return {
//             location_data: update,
//             company_locations: []
//         };
//     }

//     var state
//     var country;
//     var city;
//     var stateU;
//     try {
//         city = await mssql.query(`select * from city where id_city = ${city_id} and id_country=131`);
//         country = await mssql.query(`select * from country where id_country = ${city.recordset[0].id_country}`)
//     } catch (error) {
//         console.error(error)
//     }

//     update['city_id'] = 'city-' + city.recordset[0].id_city;
//     update["state_id"] = 'state-' + city.recordset[0].id_city;
//     company_locations.push("city-" + city.recordset[0].id_city);
//     company_locations.push('state-' + city.recordset[0].id_city);
//     location_name.push(city.recordset[0].tx_city);
//     location_name.push(city.recordset[0].tx_city);

//     location_name.push(country.recordset[0].nm_country);
//     update["country_id"] = 'country-' + country.recordset[0].id_country;
//     company_locations.push('country-' + country.recordset[0].id_country);
//     update["address"] = location_name.join(", ");
//     return {
//         location_data: update,
//         company_locations: company_locations
//     };
// }


// const databaseLocationThai = async (city_id = 0) => {
//     let update = {}
//     let company_locations = []
//     update = {};
//     let location_name = [];
//     if (city_id == 0) {
//         update = {
//             city_id: "",
//             state_id: "",
//             country_id: "",
//             address: ""
//         }
//         return {
//             location_data: update,
//             company_locations: []
//         };
//     }

//     var state
//     var country;
//     var city;
//     var stateU;
//     try {
//         city = await mssql.query(`select * from city where id_city = ${city_id} and id_country=213`);
//         country = await mssql.query(`select * from country where id_country = 213`)
//     } catch (error) {
//         console.error(error)
//     }

//     update['city_id'] = 'city-' + city.recordset[0].id_city;
//     update["state_id"] = 'state-' + city.recordset[0].id_city;
//     company_locations.push("city-" + city.recordset[0].id_city);
//     company_locations.push('state-' + city.recordset[0].id_city);
//     location_name.push(city.recordset[0].tx_city);
//     location_name.push(city.recordset[0].tx_city);

//     location_name.push(country.recordset[0].nm_country);
//     update["country_id"] = 'country-' + country.recordset[0].id_country;
//     company_locations.push('country-' + country.recordset[0].id_country);
//     update["address"] = location_name.join(", ");
//     return {
//         location_data: update,
//         company_locations: company_locations
//     };
// }


// const hongkongLocation = () => {
//     update = {
//         city_id: "city-216",
//         state_id: "state-216",
//         country_id: "country-216",
//         address: "Hong Kong, Hong Kong, Hong Kong"
//     }
//     return {
//         location_data: update,
//         company_locations: ["city-216", "state-216", "country-216"]
//     };
// }

// const testLocation = () => {
//     update = {
//         city_id: "city-test",
//         state_id: "state-test",
//         country_id: "country-test",
//         address: "Test, Test, Test"
//     }
//     return {
//         location_data: update,
//         company_locations: ["city-test", "state-test", "country-test"]
//     };
// }

// // `http://www.yp4all.com${tx_path}/${nm_file}
// const saveFiles = (serverPath, nm_file, localPath) => {
//     new Promise((resolve, reject) => {
//         http.get(`http://www.yp4all.com/${serverPath}/${nm_file}`, function (response) {

//             try {
//                 if (!fse.existsSync(localPath)) {
//                     fs.mkdirSync(localPath, {
//                         recursive: true
//                     });
//                 }
//                 response.pipe(fse.createWriteStream(`${localPath}/${nm_file}`));
//                 resolve(localPath);
//             } catch (e) {
//                 reject(e);
//             }

//         });
//     });
// }


// const setUser = async (user_id, email, password, company_id, name = null) => {

//     let hashed_password = await bcrypt.hash(password, 8);

//     await queryPromise(`insert into users(user_id,user_name,user_email,password) values('${user_id}',${JSON.stringify(name)},'${email}', '${hashed_password}')`);
//     await queryPromise(`insert into company_user(user_id,company_id) values('${user_id}','${company_id}')`);
// }


// const database_fast = async () => {
//     let ids = [];
//     try {
//         var config = {
//             user: 'sa',
//             password: 'vaibhav',
//             server: 'localhost',
//             database: 'orapages_dir',
//             options: {
//                 enableArithAbort: true
//             }
//         };
//         await mssql.connect(config);
//         // return res.send(ids);
//         ids = await mssql.query(`select id_business from business 
//                                     where id_business not in (select * from MySQL_Company)
//                                     and tx_url_rewrite != ''
//                                     and tx_url_rewrite is not null
//                                     and id_country not in (select id_country from country where id_del = 0)`);
//     } catch (err) {
//         fs.appendFileSync('errors.txt', err + "\n");
//     }


//     console.log(ids);
    
//     let i = 0;
//     for (const id of ids["recordset"]) {
//         try {
//             let businesses = await mssql.query(`select * from business where
//              tx_url_rewrite is not null and tx_url_rewrite !='' and id_business=${id.id_business}`);
//             let business = businesses["recordset"][0];

//             let {
//                 id_business,
//                 id_member,
//                 nm_business,
//                 tx_url_rewrite,
//                 tx_address,
//                 tx_phone_1,
//                 tx_fax,
//                 tx_email,
//                 tx_url,
//                 tx_desc,
//                 id_city,
//                 dt_first_saved,
//                 dt_last_update,
//                 tx_ip_created,
//                 tx_ip_last_update,
//                 id_by_mohan,
//             } = business
//             if (id_by_mohan == 1) {
//                 id_by_mohan = 'admin'
//             } else {
//                 id_by_mohan = 'user'
//             }
//             let {
//                 location_data,
//                 company_locations
//             } = testLocation();


//             try {
//                 let business_des = await mssql.query(`select * from business_service where id_business = ${id_business} and id_unique_bs =1;`)
//                 let {
//                     tx_service_desc
//                 } = business_des.recordset[0];
//                 tx_desc = tx_service_desc;
//             } catch (err) {
//                 tx_desc = tx_desc;
//             }


//             let locationQuery = `INSERT INTO company_location
//             (company_id, city_id, state_id, country_id, complete_address, phone, fax, email,website)
//             VALUES
//             ("business-${id_business}","${location_data.city_id}","${location_data.state_id}","${location_data.country_id}"
//             ,${JSON.stringify(tx_address)},${JSON.stringify(tx_phone_1)},${JSON.stringify(tx_fax)},"${tx_email}","${tx_url}")`

            

//             let company_name = nm_business;

//             let member = await mssql.query(`select * from member_main where id_member=${id_member}`)
//             if (member.recordset.length > 0) {
//                 let {
//                     tx_email,
//                     tx_password,
//                     nm_member
//                 } = member.recordset[0];

//                 await setUser("owner-" + id_member, tx_email, tx_password, "business-" + id_business, nm_member);

//                 if (nm_member != '') {
//                     company_name = nm_member;
//                 }

//             }

//             await queryPromise(locationQuery)

//             let companyQuery = `INSERT INTO company(company_id,company_business_name,company_name,company_name_short,company_url,
//             company_email,company_website,created_at, last_update, uploaded_by, uploaded_ip, company_business_details,last_update_ip)
//             VALUES
//             ("business-${id_business}","${nm_business}","${nm_business}","${rewrite(nm_business)}","${tx_url_rewrite.toLowerCase()}",
//             "${tx_email}","${tx_url}",${new Date(dt_first_saved).getTime()},${new Date(dt_last_update).getTime()},"${id_by_mohan}",
//             "${tx_ip_created}",${JSON.stringify(tx_desc)},"${tx_ip_last_update}")`


//             await queryPromise(companyQuery);


//             try {
//                 let business_industries = await mssql.query(`select * from business_industry where id_business =${business.id_business}`)

//                 business_industries["recordset"].forEach(async (industry) => {
//                     let {
//                         id_industry,
//                         tx_bi_desc
//                     } = industry;


//                     let companyCategoryQuery = `INSERT INTO company_category(company_id, category_id, category_desc) 
//                     VALUES ("business-${id_business}","category-${id_industry}",${JSON.stringify(tx_bi_desc)})`

//                     await queryPromise(companyCategoryQuery);

//                 });

//                 let business_brands = await mssql.query(`select * from business_brand where id_business=${id_business}`)
//                 business_brands["recordset"].forEach(async (brands) => {
//                     let {
//                         id_brand,
//                         tx_desc_brand
//                     } = brands;

//                     let companybrandQuery = `INSERT INTO company_brand(company_id, brand_id, brand_desc) 
//                     VALUES ("business-${id_business}","brand-${id_brand}",${JSON.stringify(tx_desc_brand)})`

//                     await queryPromise(companybrandQuery);


//                 });

//                 let business_locations = await mssql.query(`select * from business_address where id_business=${id_business}`)
//                 business_locations["recordset"].forEach(async (location) => {
//                     let {
//                         tx_address,
//                         tx_phone_1,
//                         tx_fax,
//                         tx_email,
//                         id_city
//                     } = location
//                     let {
//                         location_data,
//                         company_locations
//                     } = testLocation();
//                     let location1Query = `INSERT INTO company_location
//                     (company_id, city_id, state_id, country_id, complete_address, phone, fax, email)
//                     VALUES
//                     ("business-${id_business}","${location_data.city_id}","${location_data.state_id}","${location_data.country_id}"
//                     ,${JSON.stringify(tx_address)},${JSON.stringify(tx_phone_1)},${JSON.stringify(tx_fax)},"${tx_email}")`

//                     await queryPromise(location1Query)

//                 });

//             } catch (error) {
//                 fs.appendFileSync('errors.txt', `\nerror ${id_business}: ${error}\n`)
//             }

//             let files;
//             let logo;
//             files = await mssql.query(`select * from file_gallery where tx_path like '%/${id_business}/service%'`);
//             let r = 0;
//             for (const file of files["recordset"]) {
//                 try {
//                     let {
//                         tx_path,
//                         nm_file,
//                         tx_caption,
//                         nm_file_original
//                     } = file

//                     let image = random.generate({
//                         length: 20,
//                         charset: 'hex'
//                     });

//                     await saveFiles(`${tx_path}`, nm_file, `company_images/business-${id_business}/`);

//                     let companyimageQuery = `INSERT INTO company_image(company_id, image_id, image_url, image_original_name,
//                     image_des, image_order, image_stored_name) VALUES 
//                     ("business-${id_business}","${image}","company_images/business-${id_business}/${nm_file}",
//                     "${nm_file_original}",${JSON.stringify(tx_caption)},${r},"${nm_file}")`

//                     await queryPromise(companyimageQuery);

//                     r++;

//                 } catch (e) {
//                     fs.appendFileSync('errors.txt', e + "\n");
//                 }
//             }


//             try {
//                 logo = await mssql.query(`select * from file_gallery where tx_path like '%/${id_business}/logo%'`);
                
//                 if (!!logo) {
//                     let {
//                         tx_path,
//                         nm_file,
//                         tx_caption,
//                         nm_file_original
//                     } = logo.recordset[0]
    
//                     await saveFiles(`${tx_path}`, nm_file.replace('_sq', ''), `company_images/business-${id_business}/logo/`)
    
//                     let logo_id = random.generate({
//                         length: 20,
//                         charset: 'hex'
//                     });

//                     console.log("yess");
                    
    
//                     const companyLogoQuery = `INSERT INTO company_logo(company_id, logo_id, logo_url, logo_original_name, logo_des,
//                     logo_stored_name) VALUES ("business-${id_business}","${logo_id}",
//                     "company_images/business-${id_business}/logo/${nm_file.replace('_sq','')}",
//                     ${JSON.stringify(nm_file_original)},${JSON.stringify(tx_caption)},"${nm_file.replace('_sq','')}")`
    
//                     await queryPromise(companyLogoQuery);   
//                 }


//             } catch (err) {
//                 fs.appendFileSync('errors.txt', err + "\n");
//             }
//         } catch (e) {
//             fs.appendFileSync('errors.txt', e + "\n");
//         }
//     }
// }


// database_fast();
