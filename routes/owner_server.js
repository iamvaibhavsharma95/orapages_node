const express = require("express");
const ownerRouter = express.Router();
const createError = require("http-errors");
const {
  setUser,
  getUser,
  logoutUser,
  getResetUser,
  setResetUser,
  updateUserPassword,
} = require("./../database/owner");

const fse = require("fs-extra");

const { owner_auth, owner_log_auth } = require("./../middlewares/owner_auth");

const { mailOptions, transporter } = require("../database/email");

const csurf = require("csurf");
const csrfProtection = csurf({
  cookie: true,
});

const FrontEndOperations = require("./../database/front_end");

const Company = require("./../database/company");

ownerRouter.use(csrfProtection);

const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `videos/`);
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    if (file.mimetype !== "video/mp4") {
      cb(
        null,
        file.fieldname +
          "-" +
          uniqueSuffix +
          "." +
          file.originalname.split(".")[1]
      );
    } else {
      cb(
        null,
        file.fieldname +
          "-" +
          uniqueSuffix +
          "." +
          file.mimetype.replace("video/", "")
      );
    }
  },
});

const storage2 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `bulk_attachments/`);
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    if (file.mimetype !== "video/mp4") {
      cb(
        null,
        file.fieldname +
          "-" +
          uniqueSuffix +
          "." +
          file.originalname.split(".")[1]
      );
    } else {
      cb(
        null,
        file.fieldname +
          "-" +
          uniqueSuffix +
          "." +
          file.mimetype.replace("video/", "")
      );
    }
  },
});

const upload = multer({
  storage: storage,
});

const upload2 = multer({
  storage: storage2,
});

ownerRouter.post(
  "/categoryEmail",
  upload2.array("attachment"),
  csrfProtection,
  async (req, res, next) => {
    try {
      let {
        nm_cust = "Unknown",
        number_cust,
        tx_cust_email,
        tx_message,
        countryCategory,
        stateCategory,
        cityCategory,
        keyCategoryForm,
      } = req.body;

      const { companies, comps } =
        await FrontEndOperations.frontEndQueryFormCategory(
          keyCategoryForm,
          cityCategory,
          "city"
        );

      let data = "";
      data += comps;

      let attachment;
      if (req.files.length > 0) {
        attachment = {
          // use URL as an attachment
          filename: `attachment.${req.files[0].filename.split(".")[1]}`,
          path: `https://www.orapages.com/bulk_attachments/${req.files[0].filename}`,
        };
      }

      let mailOptions = {
        from: `"${nm_cust}" <${tx_cust_email}>`,
        to: "info@orapages.com",
        bcc: companies.join(),
        subject: "Business Related Query (Orapages.com)",
        html: `<div style="FONT:10pt tahoma">
                    <div style="background:#f5f5f5;padding:20px" >
                        <div><b>Name: </b>${nm_cust}</div>
                        <div><b>Email: </b>${tx_cust_email}</div>
                        <div><b>Mobile: </b>${number_cust}</div>
                    </div>
                    <div>&nbsp;</div>
                    <div style="padding:20px">
                    ${tx_message.replace(/\n/g, "<br>")}
                    </div>
                </div>`,
        attachments: attachment,
      };

      const r = await FrontEndOperations.selectUserQueryOnce(tx_cust_email);
      if (r.length > 0) {
        res
          .status(400)
          .send("We limit to use bulk email Services only once in 24 hours.");
        return;
      }

      transporter.sendMail(mailOptions, async function (error, info) {
        if (error) {
          res.send(error);
        } else {
          await FrontEndOperations.insertUserQuery(
            nm_cust,
            number_cust,
            tx_message,
            tx_cust_email
          );
          res.attachment("Customer or Vendor list.csv");
          res.status(200).send(data);
        }
      });
    } catch (err) {
      next(createError(500, "Your query is not sent please try again later"));
    }
  }
);

ownerRouter.post(
  "/categoryEmailModal",
  upload2.array("attachment"),
  csrfProtection,
  async (req, res, next) => {
    try {
      let {
        nm_cust = "Unknown",
        number_cust,
        tx_cust_email,
        tx_message,
        countryCategoryModal,
        stateCategoryModal,
        cityCategoryModal,
        keyCategoryFormModal,
      } = req.body;

      const { companies, comps } =
        await FrontEndOperations.frontEndQueryFormCategory(
          keyCategoryFormModal,
          cityCategoryModal,
          "city"
        );

      let data = "";
      data += comps;

      let attachment;
      if (req.files.length > 0) {
        attachment = {
          // use URL as an attachment
          filename: `attachment.${req.files[0].filename.split(".")[1]}`,
          path: `https://www.orapages.com/bulk_attachments/${req.files[0].filename}`,
        };
      }

      let mailOptions = {
        from: `"${nm_cust}" <${tx_cust_email}>`,
        to: "info@orapages.com",
        bcc: companies.join(),
        subject: "Business Related Query (Orapages.com)",
        html: `<div style="FONT:10pt tahoma">
                    <div style="background:#f5f5f5;padding:20px" >
                        <div><b>Name: </b>${nm_cust}</div>
                        <div><b>Email: </b>${tx_cust_email}</div>
                        <div><b>Mobile: </b>${number_cust}</div>
                    </div>
                    <div>&nbsp;</div>
                    <div style="padding:20px">
                    ${tx_message.replace(/\n/g, "<br>")}
                    </div>
                </div>`,
        attachments: attachment,
      };

      const r = await FrontEndOperations.selectUserQueryOnce(tx_cust_email);
      if (r.length > 0) {
        res
          .status(400)
          .send("We limit to use bulk email Services only once in 24 hours.");
        return;
      }

      transporter.sendMail(mailOptions, async function (error, info) {
        if (error) {
          res.send(error);
        } else {
          await FrontEndOperations.insertUserQuery(
            nm_cust,
            number_cust,
            tx_message,
            tx_cust_email
          );
          res.attachment("Customer or Vendor list.csv");
          res.status(200).send(data);
        }
      });
    } catch (err) {
      next(createError(500, "Your query is not sent please try again later"));
    }
  }
);

ownerRouter.post(
  "/uploadVideo",
  upload.array("video"),
  async (req, res, next) => {
    try {
      let company = await FrontEndOperations.companyByUrl(req.body.company_url);
      let video = {};
      if (!req.body.video_uploaded) {
        video = req.files[0];
        video.video_description = req.body.video_description;
        await company.setVideo(video);
      } else {
        video.video_id = req.body.video_id;
        video.video_description = req.body.video_description;
        await company.updateVideo(video);
      }
      res.status(200).send("You Video has been uploaded");
    } catch (error) {
      next(createError("Your video has noe been uploaded"));
    }
  }
);

ownerRouter.post("/deleteVideo", owner_auth, async (req, res, next) => {
  try {
    let company = await FrontEndOperations.companyByUserId(req.user_id);
    await company.deleteExtraVideo();
    res.status(200).send("Video has been removed");
  } catch (error) {
    next(createError("Your video is not removed"));
  }
});

ownerRouter.get("/register", owner_log_auth, (req, res, next) => {
  res.render("frontend_new/registration", {
    ip: req.ipInfo.ip,
    csrfToken: req.csrfToken(),
    production: false,
  });
});

ownerRouter.get("/branch", (req, res, next) => {
  var id = req.query.id;
  res.render("frontend/branch_select", {
    id: id,
  });
});

const saveFile = (path, file) => {
  return new Promise((resolve, reject) => {
    fse.outputFile(path, Buffer.from(file, "base64"), async function (err) {
      if (!err) {
        resolve(path);
      } else {
        reject(err);
      }
    });
  });
};

ownerRouter.post(
  "/register_business",
  owner_log_auth,
  async (req, res, next) => {
    try {
      const {
        categories = [],
        images = [],
        brands = [],
        tags = [],
        company_business_name,
        company_business_details = "\n",
        company_email,
        company_password,
        company_name,
        company_name_short,
        company_url,
        company_website,
        locations = [],
        created_at = null,
        last_update = null,
        last_update_ip = "0.0.0.0",
        uploaded_by,
        uploaded_ip,
        logo,
        company_theme,
        other_emails,
        other_websites,
        youtube,
      } = req.body;
      let c = new Company({
        company_id: null,
        company_name,
        company_business_name,
        company_email,
        company_name_short,
        company_url,
        company_website,
        created_at,
        last_update,
        uploaded_by,
        uploaded_ip,
        last_update_ip,
        images,
        categories,
        tags,
        brands,
        locations,
        company_business_details,
        logo,
        user_id: null,
        company_theme,
        youtube,
        other_emails,
        other_websites,
      });

      if (!!c.logo) {
        await saveFile(
          `company_images/${c.company_id}/logo/${c.logo.file_name}`,
          c.logo.file64
        );
        const s = await c.setLogo(c.logo);
      }

      c.images.forEach(async function (image) {
        await saveFile(
          `company_images/${c.company_id}/${image.file_name}`,
          image.file64
        );
        await c.setImage(image);
      });

      await c.setCompany();
      let token = await setUser(
        company_email,
        company_password,
        c.company_id,
        company_name
      );
      req.session.user_token = token;
      req.session.owner = true;

      transporter.sendMail(
        mailOptions(c.company_email, c.company_business_name, c.company_url),
        function (error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log("Email sent: " + info.response);
          }
        }
      );
      res.status(200).send(token);
    } catch (e) {
      next(e);
    }
  }
);

ownerRouter.post("/update_business", async (req, res, next) => {
  try {
    let c = new Company(req.body);

    if (!!c.logo) {
      if (c.logo.uploaded === "false") {
        await saveFile(
          `company_images/${c.company_id}/logo/${c.logo.file_name}`,
          c.logo.file64
        );
        const s = await c.setLogo(c.logo);
      } else {
        await c.updateLogo(c.logo);
      }
    } else {
      await c.deleteLogoExtra();
    }

    let uploadedImage = [];
    for (const image of c.images) {
      if (!!image.uploaded) {
        uploadedImage.push(image.image_id);
        await c.updateImage(image);
      } else {
        await saveFile(
          `company_images/${c.company_id}/${image.file_name}`,
          image.file64
        );
        const s = await c.setImage(image);
        uploadedImage.push(s);
      }
    }

    await c.deleteImageExtra(uploadedImage);

    await c.updateCompany();

    res.status(200).json({
      url: c.company_url,
    });
  } catch (e) {
    next(e);
  }
});

ownerRouter.get("/login", owner_log_auth, (req, res, next) => {
  res.render("frontend/login", {
    csrfToken: req.csrfToken(),
  });
});

ownerRouter.get("/insertTags", async (req, res, next) => {
  try {
    let r = await FrontEndOperations.insertTag(req.query.tag);
    res.status(200).send(r);
  } catch (error) {
    next(createError(500, "Tags are not uploaded"));
  }
});

ownerRouter.get("/searchTags", async (req, res, next) => {
  try {
    let results = {};
    let r = await FrontEndOperations.searchTags(req.query.tag, req.query.page);
    results.results = r;
    res.status(200).send(results);
  } catch (error) {
    next(createError(500, "Unable to find tags"));
  }
});

ownerRouter.post("/login", owner_log_auth, async (req, res, next) => {
  try {
    const { email, password } = req.body;
    let token = await getUser(email, password);
    req.session.user_token = token;
    req.session.owner = true;
    res.status(200).send(token);
  } catch (e) {
    next(
      createError(
        404,
        "There must not be the user record for this id and password"
      )
    );
  }
});

ownerRouter.get("/owner_panel", owner_auth, async (req, res, next) => {
  try {
    const company = await FrontEndOperations.companyByUserId(req.user_id);
    company.owner = req.owner;
    company.owner_this = true;
    company.csrfToken = req.csrfToken();
    (company.last_update_ip = req.ipInfo.ip),
      res.render("frontend_new/registration_form_edit", company);
  } catch (e) {
    next(createError(400, "You have no company"));
  }
});

ownerRouter.get(
  "/owner_panel/analytics",
  owner_auth,
  async (req, res, next) => {
    try {
      const company = await FrontEndOperations.companyByUserId(req.user_id);
      res.render("frontend_new/registration_form_edit_analytics", company);
    } catch (e) {
      next(createError(400, "You have no company"));
    }
  }
);

ownerRouter.get(
  "/owner_panel/:anything",
  owner_auth,
  async (req, res, next) => {
    res.redirect("/owner_panel");
  }
);

ownerRouter.get("/check_email_auth", owner_auth, async (req, res, next) => {
  try {
    const r = await FrontEndOperations.checkEmail(
      req.query.company_email,
      req.user_id
    );

    if (r.length > 0) {
      res.send('""');
    } else {
      res.send('"true"');
    }
  } catch (e) {
    res.send('""');
  }
});

ownerRouter.get("/check_name_auth", owner_auth, async (req, res, next) => {
  try {
    const r = await FrontEndOperations.checkName(
      req.query.company_business_name,
      req.user_id
    );

    if (r.length > 0) {
      res.send('""');
    } else {
      res.send('"true"');
    }
  } catch (e) {
    res.send('""');
  }
});

ownerRouter.get(
  "/owner_delete_popup",
  owner_auth,
  async function (req, res, next) {
    return res.send(`<div class="jumbotron text-center orapages-url" 
        style="padding: 32px;" id="delete_popup">
        <h1 class="text-danger" style="color: #0070c0;">
          <strong>WARNING! You are about to delete your listing permanently.</strong>
          <br />
        </h1>
        <h4 class="text-danger" style="color: #0070c0;">
          <strong>Deletion of listings is a permanent operation and cannot be undone.
          </strong><br />
        </h4>
        <h4 style="color: #0070c0;">
          Are you sure you wish to delete your listing ?<br />
        </h4>
        <div class="form-row" style="margin-top: 21px;">
            <div class="col text-right">
              <a href="javascript:void(0)" onclick="$('#delete_popup').remove()"
              class="btn btn-secondary text-white" role="button">
              Cancel
              </a>
            </div>
            <div class="col text-left">
              <a href="/delete_profile?company_id=${req.query.company_id}" 
              class="btn btn-danger text-white" role="button">
              Yes. Delete my Listing Now<br />
              </a>
            </div>
        </div>
    </div>`);
  }
);

ownerRouter.get("/delete_profile", owner_auth, async function (req, res, next) {
  try {
    let company = new Company({
      company_id: req.query.company_id,
    });
    await company.deleteCompany();
    req.session.destroy();
    return res.send(
      `<h1>Your profile has been deleted </h1><a href="/">Go to Home Page<a>`
    );
  } catch (e) {
    // console.log(e);
    return res.send(
      `<h2>Your profile is not deleted yet due to some problem Please try again </h2>
      <a href="/delete_profile?company_id=${req.query.company_id}">
        Try Again
      <a>`
    );
  }
});

ownerRouter.get("/owner_logout", owner_auth, async (req, res, next) => {
  try {
    await logoutUser(req.session.token);
    req.session.destroy();
    res.redirect("/");
  } catch (e) {
    req.session.destroy();
    res.redirect("/");
  }
});

ownerRouter.get("/forget_password", (req, res, next) => {
  res.render("frontend/forget_password", {
    csrfToken: req.csrfToken(),
  });
});

ownerRouter.post("/forget_password", async (req, res, next) => {
  try {
    const token = await setResetUser(req.body.email);
    const mailOptions = {
      from: '"Orapages " <info@orapages.com>',
      to: req.body.email,
      subject: "Reset password",
      html: `Please click the link to reset your password \n 
      <a href='https://orapages.com/change_password/${token}'>
        Change your password
      </a>`,
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        res.send(error);
      } else {
        res.send("Your feedback is submitted");
      }
    });
  } catch (e) {
    next(createError(400, "Email is not sent please try again."));
  }
});

ownerRouter.get("/change_password/:token", async (req, res, next) => {
  try {
    let user_id = await getResetUser(req.params.token);
    res.render("frontend_new/reset-password", {
      user_id,
      csrfToken: req.csrfToken(),
      owner: false,
      production: process.env.PRODUCTION !== "false",
    });
  } catch (e) {
    next(createError(400, "This link has been expired"));
  }
});

ownerRouter.post("/change_password", async (req, res, next) => {
  try {
    if (req.body.password < 7 || req.body.password > 25) {
      res.send(
        "Please enter the password which is not more that 25 characters or not less then 7 characters"
      );
    }

    await updateUserPassword(req.body.user_id, req.body.password);
    res.send(
      'Your password has been changed <a href="https://orapages.com">Orapages.com</a>'
    );
  } catch (e) {
    next(
      createError(
        400,
        "There are some error please try to reset your password again"
      )
    );
  }
});

module.exports = ownerRouter;
