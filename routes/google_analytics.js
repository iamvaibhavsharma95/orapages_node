let express = require("express");
const router = express.Router();
const { getCode } = require("country-list");

const GoogleAnalytics = require("./../analytics/gaapi");

// Google Analytics starts
router.get("/getPageViewsGA", async (req, res) => {
  try {
    let gaPageViewsResults;
    if (!req.query.days) {
      gaPageViewsResults = await GoogleAnalytics.gaPageViews(
        req.query.company_url
      );
    } else {
      gaPageViewsResults = await GoogleAnalytics.gaPageViews(
        req.query.company_url,
        (startDate = req.query.days)
      );
    }
    let chart = {};
    chart.labels = [];
    chart.data = [];
    gaPageViewsResults.rows.forEach((row) => {
      chart.labels.push(GoogleAnalytics.convertDate(row[0]));
      chart.data.push(row[1]);
    });
    res.json(chart);
  } catch (e) {
    res.send(e);
  }
});

router.get("/getUsersGA", async (req, res) => {
  try {
    let gaUsersResults;
    if (!req.query.days) {
      gaUsersResults = await GoogleAnalytics.gaUsers(req.query.company_url);
    } else {
      gaUsersResults = await GoogleAnalytics.gaUsers(
        req.query.company_url,
        (startDate = req.query.days)
      );
    }
    let chart = {};
    chart.labels = [];
    chart.data = [];
    gaUsersResults.rows.forEach((row) => {
      chart.labels.push(GoogleAnalytics.convertDate(row[0]));
      chart.data.push(row[1]);
    });
    res.json(chart);
  } catch (e) {
    res.send(e);
  }
});

router.get("/getCountryCityGA", async (req, res) => {
  try {
    let gaCityResults;
    if (!req.query.days) {
      gaCityResults = await GoogleAnalytics.gaCity(req.query.company_url);
    } else {
      gaCityResults = await GoogleAnalytics.gaCity(
        req.query.company_url,
        (startDate = req.query.days)
      );
    }
    let chart = {};
    chart.labels = [];
    chart.data = [];
    chart.backgroundColor = [];
    gaCityResults.rows.forEach((row) => {
      chart.labels.push(`${row[0]}, ${row[1]}`);
      chart.data.push(row[2]);
      chart.backgroundColor.push(GoogleAnalytics.getRandomColor());
    });
    res.json(chart);
  } catch (e) {
    res.send(e);
  }
});

router.get("/getCountryGA", async (req, res) => {
  try {
    let gaCountryResults;
    if (!req.query.days) {
      gaCountryResults = await GoogleAnalytics.gaCountry(req.query.company_url);
    } else {
      gaCountryResults = await GoogleAnalytics.gaCountry(
        req.query.company_url,
        (startDate = req.query.days)
      );
    }
    let chart = {};
    chart.labels = [];
    chart.data = [];
    chart.backgroundColor = [];
    chart.map = {};
    gaCountryResults.rows.forEach((row) => {
      chart.labels.push(row[0]);
      chart.data.push(row[1]);
      chart.backgroundColor.push(GoogleAnalytics.getRandomColor());
      chart.map[getCode(row[0])] = row[1];
    });
    res.json(chart);
  } catch (e) {
    res.send(e);
  }
});

module.exports = router;
