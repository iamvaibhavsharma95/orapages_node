/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
			config.width = 900;     // 500 pixels wide.
			

		config.fontSize_sizes = '8/8px;9/9px;10/10px;11/11px;12/12px;13/13px;14/14px;15/15px;16/16px;17/17px;18/18px;19/19px;20/20px;22/22px;24/24px;26/26px;28/28px;36/36px;42/42px;42/42px';
	config.enterMode = CKEDITOR.ENTER_BR;
	
		config.extraPlugins = 'tableresize';
		
			
		config.toolbar = 'simple';
    config.toolbar_simple = [['Bold','Italic','Underline','BulletedList','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link','Unlink','Image','TextColor','BGColor','Format','Font','FontSize']];
		
		config.toolbar = 'simple_alex';
    config.toolbar_simple_alex = [['Bold','Italic','Underline','BulletedList','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link','Unlink','Image','TextColor','BGColor','Format','Font','FontSize']];
		config.language_simple_alex = 'ar';
		
		
		config.toolbar = 'verySimpleGM';
    config.toolbar_verySimpleGM = [['Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link','Unlink','Image','Table','FontSize']];
		
		config.toolbar = 'verySimpleGM_alex';
    config.toolbar_verySimpleGM_alex = [['Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link','Unlink','Image','Table','FontSize']];
		
		
		
		config.toolbar = 'verySimple';
    config.toolbar_verySimple = [['Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Image','Table']];
		
		
		config.toolbar = 'narrow230';
    config.toolbar_narrow230 = [['Bold','Italic','BulletedList','Indent'],'/',['JustifyLeft','JustifyCenter','JustifyBlock','Image','Table']];
		
		
		config.toolbar = 'verySimple_alex';
    config.toolbar_verySimple_alex = [['Bold','Italic','Underline','NumberedList','BulletedList','Outdent','Indent','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']];
		
		
		config.toolbar = 'ora';

    config.toolbar_ora =
    [
	        
    ['Cut','Copy','Paste','PasteText','PasteFromWord'],
    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		 ['Styles','Format','Font','FontSize'],
    
    '/',
    ['Bold','Italic','Underline','Strike'],
    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    ['Link','Unlink','Anchor'],
    ['Image','Table','HorizontalRule','Smiley','SpecialChar'],
		['TextColor','BGColor']
   

	    
	   	    
    ];
		
			config.toolbar = 'goodMealAdmin';

    config.toolbar_goodMealAdmin =
    [
	    ['Source','Templates','Cut','Copy','Paste','PasteText','PasteFromWord','-'],
	    ['Undo','Redo','-','Find','-','RemoveFormat'],
			['Link','Unlink'],
			 ['Image','MediaEmbed','Table','HorizontalRule','SpecialChar'],
			  ['TextColor','BGColor','CreateDiv','Blockquote'],
	    '/',
	    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
	    ['NumberedList','BulletedList','-','Outdent','Indent'],
	    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
			['Format','Font','FontSize']
	    
	   	    
    ];
		
		
			config.toolbar = 'goodMealAdmin_alex';

    config.toolbar_goodMealAdmin_alex =
    [
	    ['Templates','Cut','Copy','Paste','PasteText','PasteFromWord','-'],
	    ['Undo','Redo','-','Find','-','RemoveFormat'],
			['Link','Unlink'],
			 ['Image','MediaEmbed','Table','HorizontalRule','Smiley','SpecialChar'],
			  ['Maximize'],['TextColor','BGColor'],
	    '/',
	    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
	    ['NumberedList','BulletedList','-','Outdent','Indent'],
	    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
			['Format','Font','FontSize']
	    
	   	    
    ];
		
		
		
		config.toolbar = 'email';

    config.toolbar_email =
    [['Copy','Paste','Undo','Redo','Bold','Italic','Underline','BulletedList','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','Link','Unlink','Image','TextColor','BGColor','Font','FontSize','Smiley']];
		
		
		config.toolbar = 'mainToolBar';

    config.toolbar_mainToolBar =
    [
	    ['Source','Preview','Maximize'],
	    ['Cut','Copy','Paste','PasteText','PasteFromWord'],
	    ['Undo','Redo','RemoveFormat','SpellChecker', 'Scayt'],
			['Link','Unlink','Anchor'],
			 ['Image','MediaEmbed','Table','HorizontalRule','Smiley','SpecialChar'],
			 ['TextColor','BGColor'],
	    '/',
	    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
	    ['NumberedList','BulletedList','-','Outdent','Indent'],
	    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Blockquote'],
			['Format','Font','FontSize','CreateDiv']
	
	   	    
    ];
		
		config.toolbar = 'mainToolBar_alex';

    config.toolbar_mainToolBar_alex =
    [
	    ['Source','Preview','-','Templates'],
	    ['Cut','Copy','Paste','PasteText','PasteFromWord','-'],
	    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
			['Link','Unlink','Anchor'],
			 ['Image','MediaEmbed','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
			  ['Maximize', 'ShowBlocks'],
	    '/',
	    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
	    ['NumberedList','BulletedList','-','Outdent','Indent','CreateDiv'],
	    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
			['Format','Font','FontSize'],
	    ['TextColor','BGColor']
	   	    
    ];
		
		// config.extraPlugins = 'mediaembed,autogrow';	
		config.extraPlugins = 'mediaembed';	
		 config.disableNativeSpellChecker = false;
		config.scayt_sLang = 'en_GB';
		config.toolbar =[['Bold','Italic','Underline','BulletedList','JustifyLeft','JustifyCenter','JustifyBlock','TextColor','Format','Font','FontSize']];
config.baseFloatZIndex = 999999999999999;
	
};
