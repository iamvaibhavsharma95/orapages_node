function isInteger(sText)

{
   var ValidChars = "0123456789";
   var IsValid=true;
   var Char;
 
   for (i = 0; i < sText.length && IsValid == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsValid = false;
         }
      }
   return IsValid;
   
   }
