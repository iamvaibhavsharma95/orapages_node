let pageViewsChart,pageCityChart,pageCountryChart,pageUsersChart;

let initilizeChart = () => {
    let pageViews = document.getElementById('pageViews').getContext('2d');
    pageViewsChart = new Chart(pageViews, {
        type: 'line',
        data: {
            // labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: 'Page views',
                // data: [12, 19, 3, 5, 2, 3],
                backgroundColor: '#dc35458f',
                borderColor: 'orange',
                fill: false,
                // fillColor:'#000',
                borderWidth: 3
            }]
        },
        options: {
            responsive: true,
            // title: {
            //     display: true,
            //     text: 'Chart.js Line Chart'
            // },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Dates'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Number of Views'
                    }
                }]
            }
        }
    });

    let pageCity = document.getElementById('pageCity').getContext('2d');
    pageCityChart = new Chart(pageCity, {
        type: 'bar',
        data: {
            // labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: 'Cities and Counties',
                // data: [12, 19, 3, 5, 2, 3],
                // backgroundColor: '#dc35458f',
                // borderColor: '#dc3545',
                fill: true,
                fillColor: '#000',
                borderWidth: 2
            }]
        },
        options: {
            responsive: true,
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Cities'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Number of views'
                    }
                }]
            }
        }
    });


    let pageCountry = document.getElementById('pageCountry').getContext('2d');
    pageCountryChart = new Chart(pageCountry, {
        type: 'bar',
        data: {
            datasets: [{
                label: 'Number of Views',
                fill: true,
                fillColor: '#000',
                borderWidth: 2
            }]
        },
        options: {
            responsive: true,
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Countries'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Number of views'
                    }
                }]
            }
        }
    });

    let pageUsers = document.getElementById('pageUsers').getContext('2d');
    pageUsersChart = new Chart(pageUsers, {
        type: 'line',
        data: {
            // labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: 'Users',
                // data: [12, 19, 3, 5, 2, 3],
                backgroundColor: '#dc35458f',
                borderColor: 'green',
                fill: false,
                fillColor: '#000',
                borderWidth: 3
            }]
        },
        options: {
            responsive: true,
            // title: {
            //     display: true,
            //     text: 'Chart.js Line Chart'
            // },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Dates'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Number of Users'
                    },

                }]
            }
        }
    });

    getCityUsers();
    getPageViews();
    getUsers();
    getCountryUsers();
}



const getPageViews = (days = null) => {
    $.ajax({
        type: "get",
        url: "/index/getPageViewsGA",
        data: {
            company_url,
            days
        },
        success: function (response) {
            pageViewsChart.data.labels = response.labels;
            pageViewsChart.data.datasets[0].data = response.data;
            pageViewsChart.update();
        }
    });
}


const getUsers = (days = null) => {
    $.ajax({
        type: "get",
        url: "/index/getUsersGA",
        data: {
            company_url,
            days
        },
        success: function (response) {
            pageUsersChart.data.labels = response.labels;
            pageUsersChart.data.datasets[0].data = response.data;
            pageUsersChart.update();
        }
    });
}


const getCityUsers = (days = null) => {
    $.ajax({
        type: "get",
        url: "/index/getCountryCityGA",
        data: {
            company_url,
            days
        },
        success: function (response) {
            pageCityChart.data.labels = response.labels;
            pageCityChart.data.datasets[0].data = response.data;
            pageCityChart.data.datasets[0].backgroundColor = response.backgroundColor;
            pageCityChart.update();
        }
    });
}

let map;
const getCountryUsers = (days = null) => {

    $.ajax({
        type: "get",
        url: "/index/getCountryGA",
        data: {
            company_url,
            days
        },
        success: function (response) {
            pageCountryChart.data.labels = response.labels;
            pageCountryChart.data.datasets[0].data = response.data;
            pageCountryChart.data.datasets[0].backgroundColor = response.backgroundColor;
            pageCountryChart.update();
            $('#vmap').replaceWith("<div id='vmap' style='height: 400px;'></div>");
            $('#vmap').vectorMap({
                map: 'world_en',
                backgroundColor: '#fff',
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#666666',
                enableZoom: false,
                showTooltip: true,
                values: response.map,
                scaleColors: ['#C8EEFF', '#006491'],
                normalizeFunction: 'polynomial',
                onLabelShow: function (event, label, code) {
                    if (!!response.map[code]) {
                        label.html(label[0].textContent + '<br/> Views:' + response.map[code]);
                    }
                },
            });
            // $('#vmap').vectorMap('set', 'values', response.map);
            // $(document).ready(function () {

            // });
        }
    });
}

let count = 0;

$("#ga-btn").click(async function () {
    $("#analytics_modal").modal('show');
    let url = ["/assets/js/jquery.vmap.min.js", "/assets/js/jquery.vmap.world.js", "https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"];
    if (count == 0) {
        await $.getScript(url[0]);
        await $.getScript(url[1]);
        await $.getScript(url[2]);
        initilizeChart();
        count++;
    }
});