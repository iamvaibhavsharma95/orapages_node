function getActionsOnPost(type, post_id) {
  if (type == "comment") {
    $.ajax({
      type: "get",
      url: "/comments/get_all_comments",
      data: { post_id },
      success: function (response) {
        $(`.comments.${post_id}`).html(response);
      },
      error: function (a, b, c) {
        alert(b);
      },
    });
  } else if (type == "like") {
    $.ajax({
      type: "get",
      url: "/likes/get_all_likes",
      data: { post_id },
      success: function (response) {
        $("#request_modal").html(response);
        $(".request_modal").modal("show");
      },
      error: function (a, b, c) {
        alert(b.responseText);
      },
    });
  } else if (type == "share") {
    $.ajax({
      type: "get",
      url: "/share/get_shares",
      data: { post_id },
      success: function (response) {
        $("#request_modal").html(response);
        $(".request_modal").modal("show");
      },
      error: function (a, b, c) {
        alert(b.responseText);
      },
    });
  }
}

function deleteComment(comment_id, post_id) {
  $.ajax({
    type: "get",
    url: "/comments/delete_comment",
    data: { post_id, comment_id },
    success: function (response) {
      $(`.comments.${post_id}`).html(response);
    },
    error: function (a, b, c) {
      alert(b);
    },
  });
}

function editComment(comment_id, post_id, comment) {
  $.ajax({
    type: "get",
    url: "/comments/edit_comment",
    data: { post_id, comment_id, comment },
    success: function (response) {
      $(`.comments.${post_id}`).html(response);
    },
    error: function (a, b, c) {
      alert(b);
    },
  });
}

function addLike(post_id) {
  $.ajax({
    type: "get",
    url: "/likes/add_likes",
    data: { post_id },
    success: function (response) {
      $(`#likes_count_${post_id}`).html(response.likes_count.likes_count);
      let r = response.likes_count.liked ? "Liked" : "Like";
      console.log(response.likes_count.liked);
      $(`#liked_${post_id}`).html(r);
      // $()
    },
  });
}

function share(post_id) {
  $.ajax({
    type: "get",
    url: "/share",
    data: { post_id },
    success: function (response) {
      if (window.location.pathname.split("/")[1] == "owner_panel") {
        return timelineOption(
          $("#url_registration").attr("href").replace("/", ""),
          "posts",
          "registration"
        );
      }
      return timelineOption(
        `${window.location.pathname.split("/")[1]}`,
        "posts"
      );
    },
    failure: function () {
      timelineOption(`${window.location.pathname.split("/")[1]}`, "posts");
    },
  });
}

function editCommentForm(comment_id) {
  $(`#comment_form_${comment_id}`).toggle();
  $(`#comment_${comment_id}`).toggle();
}

function editPost(post_id) {
  $.ajax({
    type: "get",
    url: "/post_box_edit",
    data: { post_id },
    success: function (response) {
      $("#request_modal").html(response);
      $(".request_modal").modal("show");
    },
    failure: function () {
      $("#timeline").html("<h1>Please Try again later</h1>");
    },
  });
}

function reportPost(post_id) {
  $.ajax({
    type: "get",
    url: "/report_box",
    data: { post_id },
    success: function (response) {
      $("#request_modal").html(response);
      $(".request_modal").modal("show");
    },
    failure: function () {
      $("#request_modal").html("Post is not reported. Please try again later");
      $(".request_modal").modal("show");
    },
  });
}

function deletePost(post_id) {
  $.ajax({
    type: "get",
    url: "/posts/delete_post",
    data: { post_id },
    success: function (response) {
      if (window.location.pathname.split("/")[1] == "owner_panel") {
        return timelineOption(
          $("#url_registration").attr("href").replace("/", ""),
          "posts",
          "registration"
        );
      }
      return timelineOption(
        `${window.location.pathname.split("/")[1]}`,
        "posts"
      );

      // alert("Post has been removed")
    },
    failure: function () {
      timelineOption(`${window.location.pathname.split("/")[1]}`, "posts");
      // $("#timeline").html("<h1>Please Try again later</h1>");
    },
  });
}
