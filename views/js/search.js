let $city = $('#cityList');
let $state = $('#stateList');
let $country = $('#countryList');

let city_id = null;
let state_id = null;
let country_id = null;

$(document).ready(function () {

    $city.prop('disabled', true);
    $state.prop('disabled', true);

    $country.on('change', function () {
        country_id = $(this).val();
        if (country_id !== 'Choose') {
            state_id = null;
            city_id = null;
            Cookies.set("country_name", country_id);
            Cookies.remove("state_name");
            Cookies.remove("city_name");
            $state.prop('disabled', false);
            getStatesSearch(country_id);
            // filterOn();
        } else {
            country_id = null;
            state_id = null;
            city_id = null;
            Cookies.remove("country_name");
            Cookies.remove("state_name");
            Cookies.remove("city_name");
            $state.prop('disabled', true);
            $city.prop('disabled', true);
            getStatesSearch(country_id);
            filterOff();
        }
    });

    $state.on('change', function () {
        state_id = $(this).val();
        if (state_id.startsWith('all_states')) {
            city_id = null;
            Cookies.set("state_name", state_id);
            Cookies.remove("city_name");
            $city.prop('disabled', false);
            getCitiesSearch($(this).val(), country_id);
        } else if (state_id !== 'Choose') {
            city_id = null;
            Cookies.set("state_name", state_id);
            $city.prop('disabled', false);
            Cookies.remove("city_name");
            getCitiesSearch($(this).val(), country_id);
        } else {
            state_id = null;
            city_id = null;
            $city.prop('disabled', true);
            Cookies.remove("state_name");
            Cookies.remove("city_name");
            getCitiesSearch($(this).val());
        }
    });

    $city.on('change', function () {
        city_id = $(this).val();
        if (city_id.startsWith('all_cities')) {
            Cookies.set("city_name", city_id);
            filterOn();
        } else if (city_id !== 'Choose') {
            Cookies.set("city_name", city_id);
            filterOn();
        } else {
            city_id = null;
            Cookies.remove("city_name");
            filterOff();
        }
    });


    const getCountriesSearch = () => {
        $.ajax({
            type: "get",
            url: "/frontEndCountries",
            data: {
                home: 'home',
            },
            success: function (res) {
                $country.find('.options').remove();
                // country_id = null;
                for (const r of res) {
                    $country.append(`<option value='${r.key}' class='options'>${r.value}</option>`);
                }

                $country.select2({
                    theme: "bootstrap"
                });;
                $city.select2({
                    theme: "bootstrap"
                });;
                $state.select2({
                    theme: "bootstrap"
                });;


                if (!!country_id) {
                    $(`#countryList option[value='${country_id}']`).prop('selected', true);
                    $state.prop('disabled', false);
                    getStatesSearch(country_id);
                    $country.select2({
                        theme: "bootstrap"
                    });;
                }
            }
        });
    }


    const getStatesSearch = (country) => {
        $.ajax({
            type: "get",
            url: "/frontEndStates",
            data: {
                country: country,
                home: 'home',
            },
            success: function (response) {
                $state.find('.options').remove();
                $city.find('.options').remove();
                // state_id = null;
                $state.append(`<option value='all_states' class='options'>All States</option>`);
                for (const state of response) {
                    $state.append(`<option value='${state.key}' class='options'>${state.value}</option>`)
                }
                $state.select2({
                    theme: "bootstrap"
                });;
                // if (response.length == 1) {
                //     getCitiesSearch($state.val());
                // }

                if (!!state_id && state_id != 'all_states') {
                    $(`#stateList option[value='${state_id}']`).prop('selected', true);
                    $city.prop('disabled', false);
                    getCitiesSearch(state_id, country);
                    $state.select2({
                        theme: "bootstrap"
                    });;
                } else {
                    if (state_id == 'all_states') {
                        $(`#stateList option[value='${state_id}']`).prop('selected', true);
                        $city.prop('disabled', false);
                        getCitiesSearch(state_id, country_id);
                        $state.select2({
                            theme: "bootstrap"
                        });;
                    }
                }
            }
        });
    }

    const getCitiesSearch = (state, country = null) => {
        $.ajax({
            type: "get",
            url: "/frontEndCities",
            data: {
                state,
                country,
                home: 'home',
            },
            success: function (response) {
                $city.find('.options').remove();
                // city_id = null;
                $city.append(`<option value='all_cities' class='options'>All Cities</option>`);
                for (const city of response) {
                    $city.append(`<option value='${city.key}' class='options'>${city.value}</option>`);
                }
                $city.select2({
                    theme: "bootstrap"
                });;

                if (!!city_id) {
                    $(`#cityList option[value='${city_id}']`).prop('selected', true);
                    // city_id = `${city_id},${state},${country}`;
                    $city.select2({
                        theme: "bootstrap"
                    });;
                }
            }
        });
    }


    const filterOn = () => {
        $("#filter").prop("disabled", false);
        $("#search_box").prop("disabled", false);
        $("#submit_search").prop("disabled", false);
        $(".greyout").css("color", "black");
        $("#filter").css("background", "white");
        if (!!Cookies.get('filter_type')) {
            $(`#filter option[value='${Cookies.get("filter_type")}']`).prop('selected', true);
        }
        $('.btn-browse').prop("disabled", false);
    }

    const filterOff = () => {
        $("#filter").prop("disabled", true);
        $(".greyout").css("color", "#b2b2b2");
        $(".last").css("color", "#a6a6a6");
        $("#filter").css("background", "#dfdfdf");
        $("#submit_search").prop("disabled", true);
        $("#search_box").prop("disabled", true);
        $('.btn-browse').prop("disabled", true);
    }


    $.getScript("/js/cookies.js", function () {
        city_id = Cookies.get('city_name');
        state_id = Cookies.get('state_name');
        country_id = Cookies.get('country_name');
        getCountriesSearch();
        if (!!country_id && !!state_id && !!city_id) {
            filterOn();
        } else {
            filterOff();
        }
    });
});