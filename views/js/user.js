const rewrite = (name) =>
  name
    .replace(/[^a-z0-9A-z]/g, "-")
    .replace(/-+$/, "")
    .replace(/-+/g, "-")
    .toLowerCase();

$.validator.addMethod(
  "selectField",
  function (value, element) {
    return value != "Choose";
  },
  "Please select"
);

$.validator.addMethod(
  "selectField2",
  function (value, element) {
    return !!value;
  },
  "Please select"
);

$.validator.addMethod(
  "selectFieldLength",
  function (value, element) {
    return value.length <= 20 && value.length >= 1;
  },
  "Please select"
);

$.validator.addMethod(
  "alphanumeric",
  function (value, element) {
    return this.optional(element) || /^[a-zA-Z0-9_-]+$/.test(value);
  },
  "Letters, numbers, hyphens, and underscores only please"
);

$.validator.addMethod(
  "alphanumericUrl",
  function (value, element) {
    return this.optional(element) || /^[a-zA-Z0-9-_.:/]+$/.test(value);
  },
  "Letters, numbers, hyphens, dots, slashes, colons and underscores only please"
);

$.validator.addMethod(
  "dropdownlocation",
  function (value, element) {
    if ($(`[name =${key}]`).val() != "") {
      return true;
    } else {
      return false;
    }
  },
  "Please select location from dropdown"
);

$.validator.addMethod(
  "dropdowncategory",
  function (value, element) {
    let key = $(element).prop("name").replace("category", "category_key");

    if ($(`[name =${key}]`).val() != "") {
      return true;
    } else {
      return false;
    }
  },
  "Please select category from dropdown"
);

$.validator.addMethod(
  "dropdownbrand",
  function (value, element) {
    if (value == "") {
      return true;
    }

    let key = $(element).prop("name").replace("brand", "brand_key");
    if ($(`[name =${key}]`).val() == "") {
      return true;
    } else {
      return false;
    }
  },
  "Please select brand from dropdown"
);

$.validator.addMethod(
  "validUrl",
  function (value, element) {
    var url = $.validator.methods.url.bind(this);
    return (
      url(value, element) ||
      url("http://" + value, element) ||
      url("https://" + value, element)
    );
  },
  "Please check your website address"
);

$.validator.addMethod(
  "validUrlYoutube",
  function (value, element) {
    if (value.startsWith("https://www.youtube.com/") || value == "") {
      return true;
    }
    return false;
  },
  "Please enter youtube URL"
);

$("#form_search_companies")
  .submit(function (e) {
    e.preventDefault();
  })
  .validate({
    rules: {
      country: {
        selectField: true,
      },
      type: {
        required: true,
        selectField: true,
      },
      keyword: {
        required: true,
      },
    },
    messages: {
      location: {
        required: "",
      },
      type: {
        required: "",
        selectField: "",
      },
      keyword: {
        required: "",
      },
    },
    submitHandler: (form) => {
      let type = null;
      let location = null;
      if (!!city_id && !!state_id && !!country_id && city_id != "all_cities") {
        type = "city";
        location = city_id;
        location_name = $city.find("option:selected").text();
        Cookies.set("city_name", city_id);
      } else if (!!state_id && !!country_id && state_id != "all_states") {
        type = "state";
        location = state_id;
        location_name = $state.find("option:selected").text();
        Cookies.set("state_name", state_id);
      } else {
        type = "country";
        location = country_id;
        location_name = $country.find("option:selected").text();
        Cookies.set("country_name", country_id);
      }

      $tempForm = $(`<form method="GET" action="/search_results">
                                <input type="hidden" name="location_key" value="${location}" />
                                <input type="hidden" name="type" value="${type}" />
                                <input type="hidden" name="location_name" value="${location_name}" />
                                <input type="hidden" name="filter_type" value="${$(
                                  "#filter"
                                ).val()}" />
                                <input type="hidden" name="keyword" value="${$(
                                  "#search_box"
                                ).val()}" />
                            </form>`);

      Cookies.set("filter_type", $("#filter").val());
      $("body").append($tempForm);
      $tempForm.submit();
    },
  });

const browseChar = (char, filter, page = 1) => {
  let type = null;
  let location = null;
  if (!!city_id && !!state_id && !!country_id && city_id != "all_cities") {
    type = "city";
    location = city_id;
    let [c_id, s_id, co_id] = city_id.split(",");
    location_name = $city.find("option:selected").text();
    Cookies.set("city_name", city_id);
    if (filter == "services") {
      window.location.replace(
        `/ps-browse/${co_id}/${s_id}/${c_id}/${char}/page-${page}`
      );
    } else if (filter == "company") {
      window.location.replace(
        `/company-browse/${co_id}/${s_id}/${c_id}/${char}/page-${page}`
      );
    }
  } else if (!!state_id && !!country_id && state_id != "all_states") {
    type = "state";
    location = state_id;
    location_name = $state.find("option:selected").text();
    Cookies.set("state_name", state_id);
    if (filter == "services") {
      window.location.replace(
        `/ps-browse/${country_id}/${state_id}/${char}/page-${page}`
      );
    } else if (filter == "company") {
      window.location.replace(
        `/company-browse/${country_id}/${state_id}/${char}/page-${page}`
      );
    }
  } else {
    type = "country";
    location = country_id;
    location_name = $country.find("option:selected").text();
    Cookies.set("country_name", country_id);
    if (filter == "services") {
      window.location.replace(`/ps-browse/${country_id}/${char}/page-${page}`);
    } else if (filter == "company") {
      window.location.replace(
        `/company-browse/${country_id}/${char}/page-${page}`
      );
    }
  }

  // Cookies.set("filter_type", $("#filter").val());
  // $('body').append($tempForm);
  // $tempForm.submit();
};

const jumpPage = (url, max) => {
  let number = $("#search-field").val();
  if (number > parseInt(max)) {
    return alert(`Please select less than or equal to ${max}`);
  }
  window.location.replace(`${url}/page-${number}`);
};

var termTemplate = "<span class='ui-autocomplete-term'>%s</span>";

$("#search_box").autocomplete({
  search: function () {
    $(".working").css("display", "block");
  },
  open: function (e, ui) {
    $(".working").css("display", "none");
    var acData = $(this).data("uiAutocomplete");
    acData.menu.element
      .find("li.ui-menu-item .ui-menu-item-wrapper")
      .each(function () {
        var me = $(this);
        var regex = new RegExp(acData.term, "gi");
        me.html(
          me.text().replace(regex, function (matched) {
            return termTemplate.replace("%s", matched);
          })
        );
      });
  },
  source: function (req, res) {
    $.ajax({
      type: "get",
      url: "/categoryName",
      data: {
        term: req.term,
        home: "home",
      },
      success: function (response) {
        res(response);
      },
    });
  },
  minLength: 3,
  delay: 400,
  select: function (event, ui) {
    let type = null;
    let location = null;
    if (!!city_id && !!state_id && !!country_id && city_id != "all_cities") {
      type = "city";
      location = city_id.split(",").reverse().join("/");
      Cookies.set("city_name", city_id);
    } else if (!!state_id && !!country_id && state_id != "all_states") {
      type = "state";
      location = `${country_id}/${state_id}`;
      Cookies.set("state_name", state_id);
    } else {
      type = "country";
      location = country_id;
      Cookies.set("country_name", country_id);
    }

    $tempForm = "";

    if (ui.item.key == "") {
      return;
    }
    window.location = `/ps/${location}/${ui.item.key}/page-1`;
    return;
  },
});

let logo_file;
let logo_name;
let logo_caption;
let fileShow;

function locationValidate() {
  $('[name ^= "company_branch_country_"]').each(function (e) {
    $(this).rules("add", {
      selectField2: true,
      required: true,
      messages: {
        required: " Please select country",
      },
    });
  });

  $('[name ^= "company_branch_state_"]').each(function (e) {
    $(this).rules("add", {
      selectField2: true,
      required: true,
      messages: {
        required: " Please select state",
      },
    });
  });

  $('[name ^= "company_branch_city_"]').each(function (e) {
    $(this).rules("add", {
      selectField2: true,
      required: true,
      messages: {
        required: " Please select city",
      },
    });
  });

  $('[name ^= "company_branch_phone"]').each(function (e) {
    $(this).rules("add", {
      messages: {
        minlength: "Please enter atleast ten characters",
        maxlength: "Please do not enter more than 50 characters",
      },
    });
  });
  $('[name ^= "company_branch_email"]').each(function (e) {
    $(this).rules("add", {
      minlength: 2,
      required: true,
      email: true,
      messages: {
        required: "Please enter email",
        email: "Please enter valid email",
      },
    });
  });
  $('[name ^= "other_emails"]').each(function (e) {
    $(this).rules("add", {
      minlength: 2,
      email: true,
      messages: {
        email: "Please enter valid email",
      },
    });
  });

  $('[name ^= "company_branch_url"]').each(function (e) {
    $(this).rules("add", {
      minlength: 5,
      validUrl: true,
      messages: {
        validUrl: "Please check your website address",
        minlength: "Please enter more than 5 characters",
      },
    });
  });

  $('[name ^= "other_websites"]').each(function (e) {
    $(this).rules("add", {
      minlength: 5,
      validUrl: true,
      messages: {
        validUrl: "Please check your website address",
        minlength: "Please enter more than 5 characters",
      },
    });
  });

  $('[name ^= "company_branch_address"]').each(function (e) {
    $(this).rules("add", {
      required: true,
      messages: {
        required: "Please enter full address",
      },
    });
  });
}

function categoryValidate() {
  $('[name ^= "company_category"]').each(function (e) {
    $(this).rules("add", {
      required: true,
      dropdowncategory: true,
      messages: {
        required: "Please enter Product & Service",
      },
    });
  });

  $('[name ^= "company_des_category"]').each(function (e) {
    $(this).rules("add", {
      required: true,
      messages: {
        required: "Please enter the description of Product & Service",
      },
    });
  });
}

function brandValidate() {
  $('[name ^= "company_brand"]').each(function (e) {
    $(this).rules("add", {
      dropdownbrand: true,
      messages: {
        required: "Please enter the brand",
      },
    });
  });

  $('[name ^= "company_des_brand"]').each(function (e) {
    $(this).rules("add", {
      minlength: 2,
      required: true,
      messages: {
        required: "Please enter the brand description",
      },
    });
  });
}

function imageValidate() {
  $('[name ^= "company_photo_description"]').each(function (e) {
    $(this).rules("add", {
      required: true,
      messages: {
        required: "Please enter the Image description",
      },
    });
  });
}

$(document).ready(function () {
  locationValidate();
  brandValidate();
  categoryValidate();
  imageValidate();
});

function makeid(length) {
  let result = "";
  let characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-";
  let charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function insertImage(key, file, description, order, images) {
  images.push({
    file64: file.image,
    file_name: `${key}.${file.image_type}`,
    contentType: "image/" + file.image_type,
    description: description,
    name: file.image_name,
    order: order,
  });
  console.log(images);
}

function insertLogo(fileShow, dataToSave, logoKey) {
  let image_array = fileShow.split(";");
  let image_type = image_array[0].split("/");
  let image_array_2 = image_array[1].split(",");
  let fileShowSave = {
    image_type: image_type[1],
    image: image_array_2[1],
    image_name: logo_name,
    logo_caption: logo_caption,
  };

  dataToSave.logo = {
    file64: fileShowSave.image,
    file_name: `${logoKey}.${fileShowSave.image_type}`,
    contentType: `image/${fileShowSave.image_type}`,
    logo_name: fileShowSave.image_name,
    logo_caption: logo_caption,
    uploaded: false,
  };
}

$("#url-checker-btn").on("click", function () {
  $("#url-checker").show();
  $("#url-checker-text").html("Checking Availability");
  $.ajax({
    type: "get",
    url: "/check_url_button",
    data: {
      company_url: $('[name="company_url"]').val(),
    },
    success: function (response) {
      $("#url-checker").hide();
      $("#url-checker-text").html("Check Availability");
      $("#url-result").html(
        `<strong>Congratulations, your website address is www.orapages.com/` +
          $('[name="company_url"]').val() +
          `</strong>`
      );
      $("#url-result").addClass("text-success");
      $("#url-result").removeClass("text-danger");
    },
    error: function (a, b, c) {
      $("#url-checker").hide();
      $("#url-checker-text").html("Check Availability");
      $("#url-result").html(
        `<strong>Sorry, this word is already taken www.orapages.com/` +
          $('[name="company_url"]').val() +
          `</strong>`
      );
      $("#url-result").removeClass("text-success");
      $("#url-result").addClass("text-danger");
    },
  });
});

if ($("#company_registration_form").length > 0) {
  $("#company_registration_form")
    .submit(function (e) {
      e.preventDefault();
    })
    .validate({
      ignore: `:hidden:not(#company_business_details),note-editable.panel-body`,
      rules: {
        company_name: {
          required: true,
          maxlength: 100,
        },
        company_email: {
          required: true,
          email: true,
          remote: "/check_email",
        },
        company_password: {
          required: true,
          minlength: 7,
        },
        company_password_confirm: {
          required: true,
          equalTo: "#company_password",
        },
        company_business_name: {
          required: true,
          maxlength: 100,
          remote: "/check_name",
        },
        company_url: {
          required: true,
          minlength: 3,
          alphanumeric: true,
          remote: "/check_url",
        },
        company_business_details: {
          required: true,
        },
        tags: {
          required: true,
          selectField2: true,
          selectFieldLength: true,
        },
        agreeTerms: {
          required: true,
        },
        video_description: {
          required: function () {
            let r = $("#video-input").val();
            return !!r;
          },
          maxlength: 100,
        },
        youtube: {
          validUrlYoutube: true,
          minlength: 5,
        },
      },
      messages: {
        company_name: {
          required: "Please enter company name",
          maxlength: "Please enter less than 100 characters",
        },
        company_email: {
          required: "Please enter email",
          email: "Please enter valid email address",
          remote: "This email address has been taken",
        },
        company_password: {
          required: "Please enter password",
          minlength: "Password should not less than 7 characters",
        },
        company_password_confirm: {
          required: "Please confirm your password",
          equalTo: "Please enter same as password",
        },
        company_business_name: {
          required: "Please enter business name",
          maxlength: "Please enter less than 100 characters",
          remote:
            "This company name is already taken, Please change the name or add city, state or country name or any other word to make it unique.",
        },
        company_url: {
          required: "Please enter a word as explained above",
          minlength: "Please enter more than 3 characters",
          remote: "This word is already taken",
          alphanumeric: "Please use the word without any special character",
        },
        company_business_details: {
          required: "Please enter details of the company",
        },
        tags: {
          required: "Please add atleast one keyword",
          selectField2: "Please add atleast one keyword",
          selectFieldLength:
            "Please add keywords more than one and less than 20",
        },
        agreeTerms: {
          required: "Please agree terms and conditions to submit profile",
        },
        video_description: {
          required: "Please add the description of the video",
          maxlength: "Please enter less than 100 characters",
        },
        youtube: {
          validUrlYoutube: "Please enter youtube url",
          minlength: "Please enter more than 5 characters",
        },
      },
      errorElement: "div",
      errorPlacement: function (error, element) {
        error.addClass("invalid-feedback");
        element.closest(".form-group").append(error);
        $("#error-list").append(`<li>${error[0].outerText}</li>`);
        $("#error-modal").modal("show");
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass("is-invalid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass("is-invalid");
        $(element).addClass("is-valid");
      },
      submitHandler: function (form) {
        $("#submit")
          .val(`<span class="spinner-border text-warning submit-spinner" role="status"
                                    style="margin-right: 8px;display: none;"></span></span><span>Please Wait...<br></span>`);
        $("#submit").prop("disabled", "true");
        $(".submit-spinner").show();
        let dataToSave = {};
        dataToSave["images"] = [];
        let i = -1;

        if ($(".company_photos").length > 90) {
          alert("Please select only 90 images");
          $("#submit").val("Submit & View Profile");
          $("#submit").prop("disabled", false);
          $(".submit-spinner").hide();
          return;
        }

        if (typeof logo_file !== "undefined" && logo_file !== null) {
          insertLogo(fileShow, dataToSave, makeid(20));
        }

        if ($(".company_photos").length > 0) {
          $(".company_photos").each(function () {
            var file = files_row[$(this).attr("id").split("_")[2]];
            var description = $(
              `[name = "company_photo_description_` +
                $(this).attr("id").split("_")[2] +
                `"]`
            ).val();
            i++;
            insertImage(makeid(20), file, description, i, dataToSave["images"]);
          });
        }

        let update = {};
        let formData = $(form).serializeArray();
        let videoForm = new FormData(form);
        forloopOwner(formData, dataToSave, update, () => {
          if (!videoForm.getAll("video_description")) {
            window.location.href = `/on_boarding?next_button=1`;
            return;
          }
          if (
            !!$('[name="video_description"]').val() &&
            !$("#video").attr("uploaded")
          ) {
            $("#uploading-video-modal").modal("show");
            $.ajax({
              type: "post",
              url: `/uploadVideo?_csrf=${new FormData(form).getAll("_csrf")}`,
              data: videoForm,
              processData: false,
              contentType: false,
              success: function (response) {
                window.location.href = `/on_boarding?next_button=1`;
              },
              error: function (a, b, c) {
                window.location.href = `/on_boarding?next_button=1`;
              },
            });
          } else {
            window.location.href = `/on_boarding?next_button=1`;
          }
        });
      },
    });
}

if ($("#company_registration_edit_form").length > 0) {
  $("#company_registration_edit_form")
    .submit(function (e) {
      e.preventDefault();
    })
    .validate({
      ignore: `:hidden:not(#company_business_details)`,
      rules: {
        company_name: {
          required: true,
          maxlength: 100,
        },
        company_email: {
          required: true,
          email: true,
          remote: {
            url: "/check_email_auth",
            type: "get",
          },
        },
        company_password: {
          required: true,
          minlength: 7,
        },
        company_password_confirm: {
          equalTo: "#company_password",
        },
        company_business_name: {
          required: true,
          maxlength: 100,
          remote: {
            url: "/check_name_auth",
            type: "get",
          },
        },
        company_url: {
          required: true,
          minlength: 3,
          remote: {
            url: "/check_url",
            type: "get",
            data: {
              company_key: $("#company_key").val(),
            },
          },
        },
        company_business_details: {
          required: true,
        },
        tags: {
          required: true,
          selectField2: true,
          selectFieldLength: true,
        },
        agreeTerms: {
          required: true,
        },
        video_description: {
          required: function () {
            let r = $("#video-input").val();
            return !!r;
          },
          maxlength: 100,
        },
        youtube: {
          validUrlYoutube: true,
          minlength: 5,
        },
      },
      messages: {
        company_name: {
          required: "Please enter company name",
          maxlength: "Please enter less than 100 characters",
        },
        company_email: {
          required: "Please enter email",
          email: "Please enter valid email address",
        },
        company_password: {
          required: "Please enter password",
          minlength: "Password should not less than 7 characters",
        },
        company_password_confirm: {
          equalTo: "Please enter same as password",
        },
        company_business_name: {
          required: "Please enter business name",
          maxlength: "Please enter less than 100 characters",
          remote:
            "This company name is already taken, Please change the name or add city, state or country name or any other word to make it unique.",
        },
        company_url: {
          required: "Please enter company url",
          minlength: "Please enter more than 3 characters",
          remote: "This url is already taken",
        },
        company_business_details: {
          required: "Please enter details of the company",
        },
        tags: {
          required: "Please add atleast one keyword",
          selectField2: "Please add atleast one keyword",
          selectFieldLength:
            "Please add keywords more than one and less than 20",
        },
        agreeTerms: {
          required: "Please agree terms and conditions to submit profile",
        },
        video_description: {
          required: "Please add the description of the video",
          maxlength: "Please enter less than 100 characters",
        },
        youtube: {
          validUrlYoutube: "Please enter youtube url",
          minlength: "Please enter more than 5 characters",
        },
      },
      errorElement: "div",
      errorPlacement: function (error, element) {
        error.addClass("invalid-feedback");
        element.closest(".form-group").append(error);
        $("#error-list").append(`<li>${error[0].outerText}</li>`);
        $("#error-modal").modal("show");
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass("is-invalid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass("is-invalid");
        $(element).addClass("is-valid");
      },
      submitHandler: function (form) {
        $("#submit").val("Please wait......");
        $("#submit").prop("disabled", "true");
        $(".submit-spinner").show();
        let dataToSave = {};
        dataToSave["images"] = [];
        let i = -1;

        if ($(".company_photos").length > 90) {
          alert("Please select only 90 images");
          $("#submit").val("Submit Profile");
          $("#submit").prop("disabled", false);
          return;
        }

        if (typeof logo_file !== "undefined" && logo_file !== null) {
          if (typeof uploaded_logo_file !== "undefined") {
            if (uploaded_logo_file.logo_url != logo_file.logo_url) {
              insertLogo(fileShow, dataToSave, makeid(20));
            } else {
              logo_file.caption = logo_caption;
              logo_file.uploaded = true;
              dataToSave.logo = logo_file;
            }
          } else {
            insertLogo(fileShow, dataToSave, makeid(20));
          }
        }

        if ($(".company_photos").length > 0) {
          $(".company_photos").each(function () {
            i++;
            var file = files_row[$(this).attr("id").split("_")[2]];
            var description = $(
              `[name = "company_photo_description_` +
                $(this).attr("id").split("_")[2] +
                `"]`
            ).val();

            if ($(this).hasClass("un_uploaded")) {
              insertImage(
                makeid(20),
                file,
                description,
                i,
                dataToSave["images"]
              );
            } else {
              dataToSave.images.push({
                image_id: file.image_id,
                contentType: "image/" + file.image_type,
                description: description,
                name: file.image_name,
                order: i,
                uploaded: file.uploaded,
              });
            }
          });
        }

        let update = {};
        let formData = $(form).serializeArray();
        let videoForm = new FormData(form);
        forloop(formData, dataToSave, update, (data) => {
          if (!videoForm.getAll("video_description")) {
            window.location.href = `/${data.url}`;
            return;
          }
          if (!!$("#video").attr("uploaded")) {
            videoForm.append("video_uploaded", true);
            videoForm.append("video_id", $("#video").attr("video_id"));
          }
          if (
            !!$('[name="video_description"]').val() &&
            !$("#video").attr("uploaded")
          ) {
            $("#uploading-video-modal").modal("show");
            $.ajax({
              type: "post",
              url: `/uploadVideo?_csrf=${new FormData(form).getAll("_csrf")}`,
              data: videoForm,
              processData: false,
              contentType: false,
              success: function (response) {
                window.location.href = `/${data.url}`;
              },
              error: function (a, b, c) {
                window.location.href = `/${data.url}`;
              },
            });
          } else {
            window.location.href = `/${data.url}`;
          }
        });
      },
    });
}

function forloop(formData, $dataToSave, update, result) {
  $dataToSave["locations"] = [];
  $dataToSave["brands"] = [];
  $dataToSave["categories"] = [];
  $dataToSave["other_emails"] = [];
  $dataToSave["other_websites"] = [];

  formData.forEach((data) => {
    var website = "";
    if (
      typeof $(
        "[name = 'company_branch_url_" + data["name"].split("_")[3] + "']"
      ).val() == "undefined"
    ) {
      website = "";
    } else {
      website = $(
        "[name = 'company_branch_url_" + data["name"].split("_")[3] + "']"
      ).val();
      $dataToSave.company_website = website;
    }

    if (data["name"].includes("company_branch_key")) {
      var key = data["value"];
      $dataToSave["locations"].push({
        city_id: key.split(",")[0],
        state_id: key.split(",")[1],
        country_id: key.split(",")[2],
        location_id: key.split(",")[3] || null,
        address: $(
          "[name = 'company_branch_location_" +
            data["name"].split("_")[3] +
            "']"
        ).val(),
        complete_address: $(
          "[name = 'company_branch_address_" + data["name"].split("_")[3] + "']"
        ).val(),
        mobile: $(
          "[name = 'company_branch_phone_" + data["name"].split("_")[3] + "']"
        ).val(),
        fax: $(
          "[name = 'company_branch_fax_" + data["name"].split("_")[3] + "']"
        ).val(),
        email: $(
          "[name = 'company_branch_email_" + data["name"].split("_")[3] + "']"
        ).val(),
        website: website,
      });
    }
  });

  formData.forEach((data) => {
    if (data["name"].includes("company_brand_key")) {
      var key = data["value"];
      if (key == "") return;
      $dataToSave["brands"].push({
        brand_id: key,
        brand_name: $(
          "[name = 'company_brand_" + data["name"].split("_")[3] + "']"
        ).val(),
        brand_des: $(
          "[name = 'company_des_brand_" + data["name"].split("_")[3] + "']"
        ).val(),
      });
    }
  });

  formData.forEach((data) => {
    if (data["name"].includes("company_category_key")) {
      var key = data["value"];
      $dataToSave["categories"].push({
        category_id: key,
        category_name: $(
          "[name = 'company_category_" + data["name"].split("_")[3] + "']"
        ).val(),
        category_des: $(
          "[name = 'company_des_category_" + data["name"].split("_")[3] + "']"
        ).val(),
      });
    }
  });

  formData.forEach((data) => {
    if (data["name"].startsWith("other_emails")) {
      var key = data["value"];
      $dataToSave["other_emails"].push({
        email_id: $(
          `[name="key_other_emails_${data["name"].split("_")[2]}"]`
        ).val(),
        email: key,
      });
    }
  });

  formData.forEach((data) => {
    if (data["name"].startsWith("other_websites")) {
      var key = data["value"];
      $dataToSave["other_websites"].push({
        website_id: $(
          `[name="key_other_websites_${data["name"].split("_")[2]}"]`
        ).val(),
        website: key,
      });
    }
  });

  $dataToSave.company_name = $("[name = 'company_business_name']").val();
  $dataToSave.company_email = $("[name = 'company_email']").val();
  $dataToSave.company_business_name = $(
    "[name = 'company_business_name']"
  ).val();
  $dataToSave.company_url = rewrite($("[name = 'company_url']").val());
  $dataToSave.company_theme = $("[name='theme']:checked").val();
  $dataToSave.company_business_details = $(
    "[name = 'company_business_details']"
  ).val();
  $dataToSave.uploaded_by = $("[name='uploaded_by']").val();
  $dataToSave.company_name_short = rewrite(
    $("[name = 'company_business_name']").val()
  );
  $dataToSave.created_at = $("[name = 'created_at']").val();
  $dataToSave.last_update = new Date().toUTCString();
  $dataToSave.last_update_ip = $("[name = 'last_update_ip']").val();
  $dataToSave.uploaded_ip = $("[name = 'ip']").val();
  $dataToSave.company_id = $("[name = 'company_key']").val();
  $dataToSave.tags = $("[name='tags']").val();
  $dataToSave.youtube = $("[name='youtube']").val();
  $dataToSave._csrf = $("[name='_csrf']").val();

  update = $dataToSave;

  $.ajax({
    type: "post",
    url: "/update_business",
    data: update,
  })
    .done((res) => {
      result(res);
    })
    .fail(() => {
      $("[name = 'company_email']").val("");
      $("#submit").val("Submit & View Profile");
      $("#submit").prop("disabled", false);
      $(".submit-spinner").hide();
    });
}

function forloopOwner(formData, $dataToSave, update, result) {
  $dataToSave["locations"] = [];
  $dataToSave["brands"] = [];
  $dataToSave["categories"] = [];
  $dataToSave["other_emails"] = [];
  $dataToSave["other_websites"] = [];

  formData.forEach((data) => {
    let website = "";
    if (
      typeof $(
        "[name = 'company_branch_url_" + data["name"].split("_")[3] + "']"
      ).val() == "undefined"
    ) {
      website = "";
    } else {
      website = $(
        "[name = 'company_branch_url_" + data["name"].split("_")[3] + "']"
      ).val();
      $dataToSave.company_website = website;
    }

    if (data["name"].includes("company_branch_key")) {
      var key = data["value"];
      $dataToSave["locations"].push({
        city_id: key.split(",")[0],
        state_id: key.split(",")[1],
        country_id: key.split(",")[2],
        complete_address: $(
          "[name = 'company_branch_address_" + data["name"].split("_")[3] + "']"
        ).val(),
        mobile: $(
          "[name = 'company_branch_phone_" + data["name"].split("_")[3] + "']"
        ).val(),
        fax: $(
          "[name = 'company_branch_fax_" + data["name"].split("_")[3] + "']"
        ).val(),
        email: $(
          "[name = 'company_branch_email_" + data["name"].split("_")[3] + "']"
        ).val(),
        website: website,
      });
    }
  });

  formData.forEach((data) => {
    if (data["name"].includes("company_brand_key")) {
      var key = data["value"];
      if (key == "") return;
      // update["company_brands/" + key + "/" + $company_key] = true;
      $dataToSave["brands"].push({
        brand_id: key,
        brand_name: $(
          "[name = 'company_brand_" + data["name"].split("_")[3] + "']"
        ).val(),
        brand_des: $(
          "[name = 'company_des_brand_" + data["name"].split("_")[3] + "']"
        ).val(),
      });
    }
  });

  formData.forEach((data) => {
    if (data["name"].includes("company_category_key")) {
      var key = data["value"];
      // update["company_categories/" + key + "/" + $company_key] = true;
      $dataToSave["categories"].push({
        category_id: key,
        category_name: $(
          "[name = 'company_category_" + data["name"].split("_")[3] + "']"
        ).val(),
        category_des: $(
          "[name = 'company_des_category_" + data["name"].split("_")[3] + "']"
        ).val(),
      });
    }
  });

  formData.forEach((data) => {
    if (data["name"].startsWith("other_emails")) {
      var key = data["value"];
      $dataToSave["other_emails"].push({
        email_id: $(
          `[name="key_other_emails_${data["name"].split("_")[2]}"]`
        ).val(),
        email: key,
      });
    }
  });

  formData.forEach((data) => {
    if (data["name"].startsWith("other_websites")) {
      var key = data["value"];
      $dataToSave["other_websites"].push({
        website_id: $(
          `[name="key_other_websites_${data["name"].split("_")[2]}"]`
        ).val(),
        website: key,
      });
    }
  });

  $dataToSave.company_name = $("[name = 'company_business_name']").val();
  $dataToSave.company_email = $("[name = 'company_email']").val();
  $dataToSave.company_password = $("[name = company_password]").val();
  $dataToSave.company_business_name = $(
    "[name = 'company_business_name']"
  ).val();
  $dataToSave.company_url = rewrite($("[name = 'company_url']").val());
  $dataToSave.company_theme = $("[name='theme']:checked").val();
  $dataToSave.company_business_details = $(
    "[name = 'company_business_details']"
  ).val();
  $dataToSave.uploaded_by = "user";
  $dataToSave.company_name_short = rewrite(
    $("[name = 'company_business_name']").val()
  );
  $dataToSave.created_at = new Date().toUTCString();
  $dataToSave.tags = $("[name='tags']").val();
  $dataToSave.last_update = new Date().toUTCString();
  $dataToSave.uploaded_ip = $("[name = 'ip']").val();
  $dataToSave.other_emails = $("[name = 'other_emails']").val();
  $dataToSave.youtube = $("[name = 'youtube']").val();
  $dataToSave._csrf = $("[name='_csrf']").val();

  update = $dataToSave;
  $.ajax({
    type: "post",
    url: "/register_business",
    data: update,
  })
    .done(() => {
      result();
    })
    .fail(() => {
      $("[name = 'company_email']").val("");
      $("#submit").val("Submit & View Profile");
      $("#submit").prop("disabled", false);
      $(".submit-spinner").hide();
    });
}

let searchCat = {
  open: function (e, ui) {
    $(".working").css("display", "none");
    var acData = $(this).data("uiAutocomplete");
    // styledTerm = termTemplate.replace('%s', acData.term);

    acData.menu.element
      .find("li.ui-menu-item .ui-menu-item-wrapper")
      .each(function () {
        var me = $(this);
        // var keywords = acData.term.split(' ').join('|');
        // me.text(me.text().replace(new RegExp("(" + keywords + ")", "gi")),styledTerm);
        // me.text(me.text().replace(new RegExp("(" + acData.term + ")", "gi"), "<span class='ui-autocomplete-term'>$1</span>"));
        var regex = new RegExp(acData.term, "gi");
        me.html(
          me.text().replace(regex, function (matched) {
            return termTemplate.replace("%s", matched);
          })
        );
      });
  },
  source: function (req, res) {
    $.ajax({
      type: "get",
      url: "/categoryName",
      data: {
        term: req.term,
      },
      success: function (response) {
        res(response);
      },
    });
  },
  minLength: 2,
  delay: 400,
  select: function (event, ui) {
    if (!ui.item.key) {
      return;
    }
    if (category_selected.has(ui.item.key)) {
      alert(`You have already seleted ${ui.item.value} category`);
      return;
    }
    const number = $(this)[0].name.split("_")[2];
    $("#company_category_" + $(this)[0].name.split("_")[2]).css(
      "display",
      "block"
    );
    $(this).parent().parent().parent().parent().css("display", "none");
    $("#company_category_name_" + $(this)[0].name.split("_")[2]).html(
      ui.item.value
    );
    $(
      `[name = 'company_category_key_` + $(this)[0].name.split("_")[2] + `']`
    ).val(ui.item.key);

    category_selected.add(ui.item.key);
    if ($(`[name = 'company_des_category_${number}']`).val().length > 200) {
      category_map.set(ui.item.key, 10);
    } else {
      category_map.set(ui.item.key, 5);
    }
    let rm = 0;
    category_weight = 0;
    for (const [key, value] of category_map.entries()) {
      if (rm < 10) {
        category_weight += value;
      }
      rm++;
    }
    $("#company_category_progress").html(`${category_weight}/100`);

    $.ajax({
      type: "get",
      url: "/categories_results_select",
      data: {
        category: ui.item.key,
      },
      success: function (response) {
        let { category, subcategories, non_dis_subcategories } = response;
        $(`#company_category_name_${number}`).html(category[0].category_name);

        subcategories = [...subcategories, ...non_dis_subcategories];
        const subcats = subcategories.map((subcat) => {
          return `<li>${subcat.subcategory_name}</li>`;
        });
        $(`#company_category_name_list_${number}`).html(subcats.join(""));
      },
    });
  },
};

let openHigh = function (e, ui) {
  $(".working").css("display", "none");
  var acData = $(this).data("uiAutocomplete");
  // styledTerm = termTemplate.replace('%s', acData.term);

  acData.menu.element
    .find("li.ui-menu-item .ui-menu-item-wrapper")
    .each(function () {
      var me = $(this);
      // var keywords = acData.term.split(' ').join('|');
      // me.text(me.text().replace(new RegExp("(" + keywords + ")", "gi")),styledTerm);
      // me.text(me.text().replace(new RegExp("(" + acData.term + ")", "gi"), "<span class='ui-autocomplete-term'>$1</span>"));
      var regex = new RegExp(acData.term, "gi");
      me.html(
        me.text().replace(regex, function (matched) {
          return termTemplate.replace("%s", matched);
        })
      );
    });
};

function deleteUserProfile($company_key) {
  $.ajax({
    type: "get",
    url: "/owner_delete_popup?company_id=" + $company_key,
    success: function (response) {
      $("#main_content").prepend(response);
      $("html, body").animate({
        scrollTop: 0,
      });
    },
  });
}
