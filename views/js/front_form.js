let category_selected = new Set();
let category_weight = 0;
let category_map = new Map();
if ($('[name= "company_business_details"]').length > 0) {
  // CKEDITOR.replace("company_business_details", {
  // 	height: "100vh",
  // 	width: "100%",
  // 	toolbar: "ora",
  // });

  // for (var i in CKEDITOR.instances) {
  // 	CKEDITOR.instances[i].on('change', function () {
  // 		CKEDITOR.instances[i].updateElement()
  // 	});
  // }

  $('[name= "company_business_details"]').summernote({
    callbacks: {
      onchange: function (contents, $editable) {
        setTimeout(() => {
          companyDetailBlur(contents);
        }, 3000);
      },
      onPaste: function (e) {
        var bufferText = (
          (e.originalEvent || e).clipboardData || window.clipboardData
        ).getData("Text");
        e.preventDefault();
        setTimeout(function () {
          document.execCommand("insertText", false, bufferText);
        }, 10);
      },
    },
    placeholder: "Please enter the company details.",
    tabsize: 2,
    height: "80vh",
    toolbar: [
      // ['style', ['style']],
      ["font", ["bold", "underline", "italic"]],
      // ['color', ['color']],
      ["fontname", ["fontname"]],
      ["fontsize", ["fontsize"]],
      ["forecolor", ["forecolor"]],
      ["para", ["ul", "ol", "paragraph"]],
      ["table", ["table"]],
      ["insert", ["link", "picture", "video"]],
      ["view", ["undo", "redo", "help", "codeview"]],
    ],
  });

  $('[name= "company_business_details"]').on(
    "summernote.enter",
    function (we, e) {
      $(this).summernote("pasteHTML", "<br><br>");
      e.preventDefault();
    }
  );

  $('[name= "company_business_details"]').summernote("triggerEvent", "change");
}

var result = "";
var brands = "";
var brandCount = 0;
var catCount = 0;
var files_row = [];
let locationCount = 1;
var aLocation = "";
var termTemplate = "<span class='ui-autocomplete-term'>%s</span>";

$($(`input[type='text'][name^='company_category_']`)).each(function (e) {
  $(this).autocomplete(searchCat);

  const number = $(this)[0].name.split("_")[2];

  const category = $(`[name='company_category_key_${number}']`).val();
  if (!!category) {
    category_selected.add(category);
    if ($(`[name = 'company_des_category_${number}']`).val().length > 200) {
      category_map.set(category, 10);
    } else {
      category_map.set(category, 5);
    }
    let rm = 0;
    category_weight = 0;
    for (const [key, value] of category_map.entries()) {
      if (rm < 10) {
        category_weight += value;
      }
      rm++;
    }
    $("#company_category_progress").html(`${category_weight}/100`);
    $.ajax({
      type: "get",
      url: "/categories_results_select",
      data: {
        category,
      },
      success: function (response) {
        let { category, subcategories, non_dis_subcategories } = response;
        if (location.pathname == "/edit_company_admin") {
          subcategories.push({
            subcategory_name: "-------NON DISPLAY----------",
          });
        }
        $(`#company_category_name_${number}`).html(category[0].category_name);
        subcategories = [...subcategories, ...non_dis_subcategories];
        const subcats = subcategories.map((subcat) => {
          return `<li>${subcat.subcategory_name}</li>`;
        });
        $(`#company_category_name_list_${number}`).html(subcats.join(""));
      },
    });
  }

  catCount++;
});

$($(`input[type='text'][name^='company_brand_']`)).each(function (e) {
  $(this).autocomplete({
    open: openHigh,
    source: function (request, response) {
      $.ajax({
        type: "get",
        url: "/brandName",
        data: {
          term: request.term,
        },
        success: function (res) {
          response(res);
        },
      });
    },
    select: function (event, ui) {
      $("#company_brand_" + $(this).context.name.split("_")[2]).css(
        "display",
        "block"
      );
      $(this).parent().parent().css("display", "none");
      $("#company_brand_name_" + $(this).context.name.split("_")[2]).html(
        ui.item.value
      );
      $(
        `[name = 'company_brand_key_${$(this).context.name.split("_")[2]}']`
      ).val(ui.item.key);
    },
  });
  brandCount++;
});

$($(`input[type='text'][id^='id_location_']`)).each(function (e) {
  $(this).autocomplete({
    open: openHigh,
    source: function (request, response) {
      $.ajax({
        type: "get",
        url: "/locationsform",
        data: {
          term: request.term,
        },
        success: function (res) {
          response(res);
        },
      });
    },
    select: function (event, ui) {
      $("#id_location_" + $(this).context.name.split("_")[3]).val(
        ui.item.value
      );
      $(`[name = 'company_branch_key_${$(this)[0].name.split("_")[3]}']`).val(
        ui.item.key
      );
    },
  });
  locationCount++;
});

function imageCharCount($inputtextarea) {
  var $textarea = $($inputtextarea);
  var maxlength = $textarea.attr("maxlength");
  var length = $textarea.val().length;
  $textarea
    .closest("span[display_type]")
    .find("div.counter")
    .html(
      "Characters: Max " +
        maxlength +
        " | Used " +
        length +
        " | Remaining " +
        (maxlength - length)
    );
}

let image_weight = 0;
let image_map = new Map();
function charCount($inputtextarea, $type = null) {
  var $textarea = $($inputtextarea);
  var maxlength = $textarea.attr("maxlength");
  var length = $textarea.val().length;
  $textarea
    .siblings(".counter")
    .html(
      "Characters: Max " +
        maxlength +
        " | Used " +
        length +
        " | Remaining " +
        (maxlength - length)
    );
  if (!!$type) {
    if ($type == "image") {
      if (length > 0) {
        let n = $($inputtextarea).prop("name").split("_")[3];
        image_weight = 0;
        image_map.set(`company_photo_${n}`, 5);
        let rm = 0;
        for (const [key, value] of image_map.entries()) {
          if (rm < 10) {
            image_weight += value;
          }
          rm++;
        }
        $("#company_image_progress").html(`${image_weight}/50`);
      } else {
        let n = $($inputtextarea).prop("name").split("_")[3];
        image_weight = 0;
        image_map.set(`company_photo_${n}`, 2);
        let rm = 0;
        for (const [key, value] of image_map.entries()) {
          if (rm < 10) {
            image_weight += value;
          }
          rm++;
        }
        $("#company_image_progress").html(`${image_weight}/50`);
      }
    } else if (($type = "category")) {
      if (length > 200) {
        let n = $($inputtextarea).prop("name").split("_")[3];
        category_map.set($(`[name= "company_category_key_${n}"]`).val(), 10);
      } else {
        let n = $($inputtextarea).prop("name").split("_")[3];
        category_map.set($(`[name= "company_category_key_${n}"]`).val(), 5);
      }
      category_weight = 0;
      let rm = 0;
      for (const [key, value] of category_map.entries()) {
        if (rm < 10) {
          category_weight += value;
        }
        rm++;
      }
      $("#company_category_progress").html(`${category_weight}/100`);
    }
  }
}

$("#upload-logo").on("click", function () {
  $("#upload-logo-modal").modal("show");
});

$("#edit-logo").on("click", function () {
  $("#upload-logo-modal").modal("show");
});

function logoFileSelect(e) {
  if (!e.target.files || !window.FileReader) return;
  let files = e.target.files;
  let f = files[0];
  logo_file = f;
  logo_name = f.name;
  if (!f.type.match("image.*")) {
    return;
  }
  var reader = new FileReader();
  reader.onload = function (e) {
    fileShow = e.target.result;
    console.log("");
    $("#formLogoPic").attr("src", e.target.result);
  };
  reader.readAsDataURL(f);
}

$("#attachment").on("change", logoFileSelect);

$(document).ready(function () {
  $("#logo-form")
    .submit(function (e) {
      e.preventDefault();
    })
    .validate({
      rules: {
        attachment: {
          required: true,
        },
      },
      messages: {
        attachment: {
          required: "Please select your company logo",
        },
      },
      errorPlacement: function (error, element) {
        $(error).insertAfter(element.parent());
      },
      submitHandler: function (form) {
        $("#company_logo").html("<img src='" + fileShow + "'>");
        $("#uploadDiv").css("display", "none");
        $("#editDiv").css("display", "block");
        $("#company_logo_name").html(logo_name);
        $("#upload-logo-modal").modal("hide");
        $("#company_logo_weight").text("10/10");
        changeState("#company_logo_weight", "success");
      },
    });
});

function deleteLogo() {
  if (confirm("Want to delete the Logo!")) {
    $("#company_logo").html("");
    $("#editDiv").hide();
    $("#uploadDiv").show();
    $("#company_logo_name").html("");
    logo_file = null;
    logo_name = null;
    file_show = null;
    $("#company_logo_weight").text("0/10");
    changeState("#company_logo_weight", "secondary");
    $("#formLogoPic").removeAttr("src");
  }
}

$("#remove-logo").on("click", function () {
  deleteLogo();
});

function videoFileSelect(e) {
  if (!e.target.files || !window.FileReader) return;
  let files = e.target.files;
  let f = files[0];
  if ((files[0].size / 1024 / 1024).toFixed(4) > 150) {
    alert("File size must be less than 150 MB");
    return;
  }
  var reader = new FileReader();
  reader.onload = function (e) {
    let x = e.target.result;
    let type = f.type;
    if (f.type !== "video/mp4") {
      x = x.replace(f.type, `video/${f.name.split(".")[1]}`);
      type = "video/mp4";
    }
    $("[name='video_description']").prop("disabled", false);
    $("#uploaded_video")
      .html(`<video width="100%" height="100%" controls style="max-height">
				<source src="${x}" type="${type}" id="video">
		</video><button class="btn btn-danger" type="button" onclick="deleteVideo()">Delete Video</button>`);
    $("#company_video_progress").html("20/20");
  };
  reader.readAsDataURL(f);
}

$("#video-input").on("change", videoFileSelect);

function deleteVideo() {
  $("#video-input").val("");
  $("#uploaded_video").html("");
  $("[name='video_description']").prop("disabled", true);
  $("[name='video_description']").val("");
  $("#company_video_progress").html("0/20");
  if (!$("#video").attr("video_id")) {
    $.ajax({
      type: "post",
      url: `/deleteVideo`,
      data: {
        _csrf: $("[name='_csrf']").val(),
      },
      success: function (response) {
        $("#company_video_progress").html("0/20");
        alert("Your Video has been removed");
      },
    });
  }
}

$("#upload_company_images").on("click", function () {
  $tempFile =
    "<input type='file' name='company_photos' multiple style='display:none'>";
  if ($("[name = 'company_photos']").length <= 0) {
    if ($("#company_registration_edit_form").length > 0) {
      $("#company_registration_edit_form").append($tempFile);
    }
    if ($("#company_registration_form").length > 0) {
      $("#company_registration_form").append($tempFile);
    }
    $("[name='company_photos']").on("change", handleFileSelect);
  }
  $("[name='company_photos']").click();
});

$(".profileTable").sortable({
  items: "> .company_photos:not(.immovable)",
  placeholder: "col-md-6 col-lg-6 col-xl-6 company_photos ui-state-highlight",
  helper: function (event, $element) {
    return $element.css("background-color", "#ffffff");
  },
  stop: function (event, $ui) {
    // $ui.item.removeAttr("style");
  },
});

function handleFileSelect(e) {
  if (!e.target.files || !window.FileReader) return;
  globalFiles = e.target.files;
  var files = e.target.files;
  var filesArr = Object.keys(files);
  var i = files_row.length;
  filesArr.forEach(function (f) {
    var f = files[f];
    if (!f.type.match("image.*")) {
      return;
    }
    var reader = new FileReader();
    reader.onload = function (e) {
      var image_array = e.target.result.split(";");
      var image_type = image_array[0].split("/");
      var image_array_2 = image_array[1].split(",");
      files_row[i] = {
        image_type: image_type[1],
        image: image_array_2[1],
        image_name: f.name,
      };
      $(`<div class="col-md-6 col-lg-6 col-xl-6 company_photos un_uploaded" style="padding:15px" id="company_photo_${i}">
					<div class="form-row image-file" style="padding:0px">
						<div class="col-xl-12" style="padding:0px">
							<div class="d-flex d-sm-flex d-md-flex d-lg-flex d-xl-flex justify-content-center align-items-center justify-content-sm-center align-items-sm-center justify-content-md-center align-items-md-center justify-content-lg-center align-items-lg-center align-items-xl-center"
								style="height: 310px;" ><img class="d-xl-flex" style="height:100%;object-fit: contain;"
									src="${e.target.result}">
								<div class="bg-light image-update">
									<button class="btn btn-light" type="button" onclick="deleteImage('${i}')">Remove</button>
								</div>
							</div>
						</div>
						<div class="col">
							<div class="form-group">
							<small class="d-xl-flex justify-content-xl-center">
							<strong>${f.name}</strong>
							</small>
							<textarea class="form-control"  name="company_photo_description_${i}"
							placeholder="Image Description" rows="4" style="font-size: 11px;" minlength="0"
									maxlength="100" oninput="charCount(this,'image')" ></textarea>
							<small class="d-xl-flex justify-content-xl-end counter">Character : Max 100 | Used 0 |
									Remaining 100</small>
							</div>
						</div>
					</div>
				</div>`).insertBefore(".immovable");
      image_weight = 0;
      image_map.set(`company_photo_${i}`, 2);
      let rm = 0;
      for (const [key, value] of image_map.entries()) {
        if (rm < 10) {
          image_weight += value;
        }
        rm++;
      }
      i++;
      $("#company_image_progress").html(`${image_weight}/50`);
      $(".profileTable").sortable({
        items: "> .company_photos:not(.immovable)",
        placeholder:
          "col-md-6 col-lg-6 col-xl-6 company_photos ui-state-highlight",
        helper: function (event, $element) {
          return $element.css("background-color", "#ffffff");
        },
        stop: function (event, $ui) {
          // $ui.item.removeAttr("style");
        },
      });
      if (
        location.pathname == "/register" ||
        location.pathname == "/owner_panel"
      ) {
        imageValidate();
      }
    };
    reader.readAsDataURL(f);
  });
}

$("#add_another_category_button").on("click", function () {
  $(
    "#another_category_section"
  ).append(`<div class="form-row company_category_${catCount}" style="margin-top: 20px;">
		<div class="col-xl-12">
			<div class="form-row">
				<div class="col-sm-10 col-xl-9">
					<div class="form-row">
						<div class="col-xl-10">
							<div class="form-group">
								<input class="form-control" type="text" name="company_category_${catCount}">
								<input class="form-control" type="hidden" name="company_category_key_${catCount}">
								<small>Type the first few letters to view the categories to select from<br></small>
							</div>
						</div>
						<div class="col">
							<button class="btn btn-danger" type="button" onclick='$(".company_category_${catCount}").remove()'>Delete</button>
						</div>
					</div>
				</div>
				<div class="col-xl-12" id="company_category_${catCount}" style="display:none">
					<div class="form-row">
						<div class="col-md-3 col-xl-3">
						<strong>${
              catCount + 1
            }. <label id="company_category_name_${catCount}"></label></strong>
							<ul id="company_category_name_list_${catCount}" style="font-size: 12px;">
							</ul>
						</div>
						<div class="col">
							<div class="form-group"><small><strong>Brief Details *</strong>&nbsp;(This will appear against your company name under the category)<br></small>
							<textarea class="form-control" name="company_des_category_${catCount}" oninput="charCount(this,'category')" maxlength="400" rows="6"></textarea>
							<button class="btn btn-danger btn-sm d-inline float-right" type="button" style="margin-top: 4px;" onclick="deleteCatUi(${catCount})">Delete</button>
							<small class="counter">Character : Max 400 | Used 0 | Remaining 400<br></small>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>`);
  $("[name = 'company_category_" + catCount + "']").autocomplete(searchCat);
  categoryValidate();
  catCount++;
});

$("#add_branch_btn").on("click", function () {
  $(
    "#branch"
  ).append(`<fieldset class="border p-2" style="margin-bottom: 10px;">
							<legend class="text-primary w-auto" style="margin-left: 18px;"><strong>Branch Details</strong>
							</legend>
							<div class="form-row">
								<div class="col-lg-2 col-xl-2"><label class="col-form-label"><strong>Complete Address
											*</strong><br></label></div>
								<div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 col-sm-6">
									<div class="form-group">
									<textarea class="form-control" placeholder="Please enter COMPLETE address including CITY, STATE and COUNTRY."
											rows="7" name="company_branch_address_${locationCount}"></textarea></div>
								</div>
							</div>
							<div class="form-row">
								<div class="col-lg-2 col-xl-2"><label class="col-form-label"><strong>Telephone</strong></label>
								</div>
								<div class="col-lg-6 col-xl-6">
									<div class="form-group"><input class="form-control" type="text"
											name="company_branch_phone_${locationCount}"></div>
								</div>
							</div>
							<div class="form-row">
								<div class="col-lg-2 col-xl-2"><label class="col-form-label"><strong>Fax</strong></label></div>
								<div class="col-lg-6 col-xl-6">
									<div class="form-group"><input class="form-control" type="text" name="company_branch_fax_${locationCount}">
									</div>
								</div>
							</div>
							<div class="form-row">
								<div class="col-lg-2 col-xl-2"><label class="col-form-label"><strong>Email</strong></label>
								</div>
								<div class="col-lg-6 col-xl-6">
									<div class="form-group"><input class="form-control" type="text"
											name="company_branch_email_${locationCount}"></div>
								</div>
							</div>
							<div class="form-row">
								<div class="col-lg-2 col-xl-2"><label class="col-form-label"><strong>Location *</strong></label>
								</div>
								<div class="col-lg-3 col-xl-3">
									<div class="form-group"><select class="form-control" id="id_country_${locationCount}" 
											name="company_branch_country_${locationCount}">
											<option value="">Please select Country</option>
										</select></div>
								</div>
								<div class="col-lg-3 col-xl-3">
									<div class="form-group"><select class="form-control" id="id_state_${locationCount}"
											name="company_branch_state_${locationCount}">
											<option value="">Please select State</option>
										</select></div>
								</div>
								<div class="col-lg-3 col-xl-3">
									<div class="form-group"><select class="form-control" id="id_city_${locationCount}"
											name="company_branch_city_${locationCount}">
											<option value="">Please select City</option>
										</select></div>
								</div>
								<input type="hidden" name="company_branch_key_${locationCount}" />
							</div>
							<div class="form-row">
								<div class="col"><button class="btn btn-danger btn-sm" type="button" onclick="$(this).closest('fieldset').remove();locationCount--">Delete Branch</button>
								</div>
							</div>
						</fieldset>`);
  $(`[name="company_branch_country_${locationCount}"]`).on(
    "change",
    function () {
      const number = $(this)[0].name.split("_")[3];
      const country = `[name='company_branch_country_${number}']`;
      const state = `[name='company_branch_state_${number}']`;
      const city = `[name='company_branch_city_${number}']`;
      getStatesSearch(state, city, $(country).val());
    }
  );

  $(`[name="company_branch_state_${locationCount}"]`).on("change", function () {
    const number = $(this)[0].name.split("_")[3];
    const country = `[name='company_branch_country_${number}']`;
    const state = `[name='company_branch_state_${number}']`;
    const city = `[name='company_branch_city_${number}']`;
    getCitiesSearch(city, $(state).val());
  });

  $(`[name="company_branch_city_${locationCount}"]`).on("change", function () {
    const number = $(this)[0].name.split("_")[3];
    const country = `[name='company_branch_country_${number}']`;
    const state = `[name='company_branch_state_${number}']`;
    const city = `[name='company_branch_city_${number}']`;
    const key = `[name= 'company_branch_key_${number}']`;
    $(key).val(`${$(city).val()},${$(state).val()},${$(country).val()}`);
  });

  $(`[name="company_branch_country_${locationCount}"]`).select2({
    theme: "bootstrap",
    placeholder: "Please select country",
  });

  $(`[name="company_branch_state_${locationCount}"]`).select2({
    theme: "bootstrap",
    placeholder: "Please slect state",
  });

  $(`[name="company_branch_city_${locationCount}"]`).select2({
    theme: "bootstrap",
    placeholder: "Please select city",
  });

  getCountriesSearch(
    $(`[name="company_branch_country_${locationCount}"]`),
    $(`[name="company_branch_state_${locationCount}"]`),
    $(`[name="company_branch_city_${locationCount}"]`),
    $(`[name= 'company_branch_key_${locationCount}']`)
  );
  locationValidate();
  locationCount++;
});

function deleteCatUi(count) {
  if (count == 0) {
    if ($("#another_category_section").children().length > 0) {
      category_selected.delete(
        $(`[name='company_category_key_${count}']`).val()
      );
      category_map.delete($(`[name='company_category_key_${count}']`).val());
      category_weight = 0;
      let rm = 0;
      for (const [key, value] of category_map.entries()) {
        if (rm < 10) {
          category_weight += value;
        }
        rm++;
      }
      $("#company_category_progress").html(`${category_weight}/100`);
      $("#first-category").remove();
    }
    $("#company_category_0").hide();
    $("#topCategories").css("display", "block");
    $(`[name = 'company_des_category_0']`).val("");
    category_selected.delete($(`[name='company_category_key_0']`).val());
    category_map.delete($(`[name='company_category_key_0']`).val());
    category_weight = 0;
    let rm = 0;
    for (const [key, value] of category_map.entries()) {
      if (rm < 10) {
        category_weight += value;
      }
      rm++;
    }
    $("#company_category_progress").html(`${category_weight}/100`);
    $(`[name='company_category_key_0']`).val("");
    return;
  }
  category_selected.delete($(`[name='company_category_key_${count}']`).val());
  category_map.delete($(`[name='company_category_key_${count}']`).val());
  category_weight = 0;
  let rm = 0;
  for (const [key, value] of category_map.entries()) {
    if (rm < 10) {
      category_weight += value;
    }
    rm++;
  }
  $("#company_category_progress").html(`${category_weight}/100`);
  $(`.company_category_${count}`).remove();
  if (
    $("#another_category_section").children().length == 0 &&
    $("#first-category").length == 0
  ) {
    $("#another_category_section")
      .before(`<div class="form-row" id="first-category" style="margin-top: 20px;">
				<div class="col-xl-12"><label><strong>Products &amp; Services *</strong><br></label>
					<div class="form-row">
						<div class="col-sm-10 col-xl-9" id="topCategories">
							<div class="form-row">
								<div class="col-xl-10">
									<div class="form-group">
										<input class="form-control ui-autocomplete-input" type="text" name="company_category_0" autocomplete="off">
										<input class="form-control" type="hidden" name="company_category_key_0">
										<small>Type the first few letters to view the categories to select
											from<br></small>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-12" id="company_category_0" style="display: none;">
							<div class="form-row">
								<div class="col-md-3 col-xl-3">
									<strong>1. <label id="company_category_name_0"></label></strong>
									<ul id="company_category_name_list_0" style="font-size: 12px;">
									</ul>
								</div>
								<div class="col">
									<div class="form-group"><small><strong>Brief Details *</strong>&nbsp;(This will
											appear against your company name under the category)<br></small>
										<textarea class="form-control" name="company_des_category_0" rows="6" oninput="charCount(this,'category')" maxlength="400"></textarea>
										<button class="btn btn-danger btn-sm d-inline float-right" type="button" style="margin-top: 4px;" onclick="deleteCatUi(0)">Delete</button>
										<small class="counter">Character : Max 400 | Used 0 | Remaining
											400<br></small>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>`);
    $('[name="company_category_0"]').autocomplete(searchCat);
  }
}

function deleteImage(image) {
  delete files_row[image];
  image_weight = 0;
  image_map.delete(`company_photo_${image}`);
  let rm = 0;
  for (const [key, value] of image_map.entries()) {
    if (rm < 10) {
      image_weight += value;
    }
    rm++;
  }
  $("#company_image_progress").html(`${image_weight}/50`);
  $(`#company_photo_${image}`).remove();
}

$("#add_another_brand_button").on("click", function () {
  $("#another_brand_section").append(
    `<tr class="company_brand_${brandCount}">
			<td valign="top" width="750">
				<input type="text" style="width:250px" name="company_brand_${brandCount}">
				<input type="hidden" style="width:250px" name="company_brand_key_${brandCount}">
				<span class="small">
					Type the first few letters to view the categories
					to select
					from
				</span>
				<input type="button" value="Delete" onclick='$(".company_brand_${brandCount}").remove()'>
			</td>
		</tr>
		<tr id="company_brand_${brandCount}" class="company_brand_${brandCount}" style="display:none">
			<td>
				<table>
					<tbody>
						<tr>
							<td></td>
							<td><strong>Brief Details *</strong> <span class="small">(This will
									appear against your company name under the
									brand)</span><br>
							</td>
						</tr>
						<tr>
							<td valign="top" width="200">
								<strong>${brandCount + 1}.</strong>&nbsp;&nbsp;<span
									id="company_brand_name_${brandCount}"></span></td>
							<td>
								<textarea type="Text" maxlength="400" name="company_des_brand_${brandCount}"
									style="width:500px;height:80px;overflow:auto"
									oninput="CheckFieldLength(company_des_brand_${brandCount},
										'company_brand_des_charcount_${brandCount}',
										'company_brand_des_remaining_${brandCount}',
										400);"></textarea><br>
								<br>

								<div align="right">
									<small>Max&nbsp;400
										&nbsp;&nbsp;|&nbsp;&nbsp;
										<span id="company_brand_des_charcount_${brandCount}">0</span>
										&nbsp;&nbsp;|&nbsp;&nbsp;
										<span id="company_brand_des_remaining_${brandCount}">400</span></small>
								</div>

							</td>
							<td valign="top">
								<a onclick='$(".company_brand_${brandCount}").remove()'>[Delete]</a>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>`
  );
  $("[name = 'company_brand_" + brandCount + "']").autocomplete({
    open: openHigh,
    source: function (request, response) {
      $.ajax({
        type: "get",
        url: "/brandName",
        data: {
          term: request.term,
        },
        success: function (res) {
          response(res);
        },
      });
    },
    select: function (event, ui) {
      $("#company_brand_" + $(this).context.name.split("_")[2]).css(
        "display",
        "block"
      );
      $(this).parent().parent().css("display", "none");
      $("#company_brand_name_" + $(this).context.name.split("_")[2]).html(
        ui.item.value
      );
      $(
        `[name = 'company_brand_key_${$(this).context.name.split("_")[2]}']`
      ).val(ui.item.key);
    },
  });
  brandValidate();
  brandCount++;
});

let country_form = $('[name^="company_branch_country_"]');
let state_form = $('[name^="company_branch_state_"]');
let city_form = $('[name^="company_branch_city_"]');

country_form.on("change", function () {
  const number = $(this)[0].name.split("_")[3];
  const country = `[name='company_branch_country_${number}']`;
  const state = `[name='company_branch_state_${number}']`;
  const city = `[name='company_branch_city_${number}']`;
  const key = `[name= 'company_branch_key_${number}']`;
  $(key).val(``);
  getStatesSearch(state, city, $(country).val(), key);
});

state_form.on("change", function () {
  const number = $(this)[0].name.split("_")[3];
  const country = `[name='company_branch_country_${number}']`;
  const state = `[name='company_branch_state_${number}']`;
  const city = `[name='company_branch_city_${number}']`;
  const key = `[name= 'company_branch_key_${number}']`;
  $(key).val(``);
  getCitiesSearch(city, $(state).val(), key);
});

city_form.on("change", function () {
  const number = $(this)[0].name.split("_")[3];
  const country = `[name='company_branch_country_${number}']`;
  const state = `[name='company_branch_state_${number}']`;
  const city = `[name='company_branch_city_${number}']`;
  const key = `[name= 'company_branch_key_${number}']`;
  if (!!$(city).val()) {
    $(key).val(`${$(city).val()},${$(state).val()},${$(country).val()}`);
  } else {
    $(key).val(``);
  }
});

country_form.select2({
  theme: "bootstrap",
  placeholder: "Please select country",
});

state_form.select2({
  theme: "bootstrap",
  placeholder: "Please slect state",
});

city_form.select2({
  theme: "bootstrap",
  placeholder: "Please select city",
});

const getCountriesSearch = (
  element_country,
  element_state,
  element_city,
  key
) => {
  $.ajax({
    type: "get",
    url: "/frontEndCountries",
    success: function (res) {
      $(element_country).find(".options").remove();
      $(element_state).find(".options").remove();
      $(element_city).find(".options").remove();
      // country_id = null;
      for (const r of res) {
        $(element_country).append(
          `<option value='${r.key}' class='options'>${r.value}</option>`
        );
      }

      $(element_country).select2({
        theme: "bootstrap",
      });
      $(element_city).select2({
        theme: "bootstrap",
      });
      $(element_state).select2({
        theme: "bootstrap",
      });

      // if (res.length == 1) {
      // 	getStatesSearch(element_state, element_city, res[0].key, key);
      // }

      let r = $(key).val();
      if (!!r) {
        r = $(key).val().split(",")[2];
        $(element_country).find(`option[value='${r}']`).prop("selected", true);
        // $(element_state).prop('disabled',false);
        getStatesSearch(element_state, element_city, r, key);
        $(element_country).select2({
          theme: "bootstrap",
        });
      }
    },
  });
};

const getStatesSearch = (element_state, element_city, country, key) => {
  $.ajax({
    type: "get",
    url: "/frontEndStates",
    data: {
      country: country,
    },
    success: function (response) {
      $(element_state).find(".options").remove();

      $(element_city).find(".options").remove();
      // state_id = null;
      for (const state of response) {
        $(element_state).append(
          `<option value='${state.key}' class='options'>${state.value}</option>`
        );
      }
      $(element_state).select2({
        theme: "bootstrap",
      });

      // if (response.length == 1) {
      // 	getCitiesSearch(element_city,response[0].key,key);
      // }

      let state_id = $(key).val();
      if (!!state_id) {
        state_id = $(key).val().split(",")[1];
        $(element_state)
          .find(`option[value='${state_id}']`)
          .prop("selected", true);
        // $city.prop('disabled', false);
        getCitiesSearch(element_city, state_id, key);
        $(element_state).select2({
          theme: "bootstrap",
        });
      }
    },
  });
};

const getCitiesSearch = (element_city, state, key) => {
  $.ajax({
    type: "get",
    url: "/frontEndCities",
    data: {
      state,
    },
    success: function (response) {
      $(element_city).find(".options").remove();
      // city_id = null;
      for (const city of response) {
        $(element_city).append(
          `<option value='${city.key}' class='options'>${city.value}</option>`
        );
      }
      $(element_city).select2({
        theme: "bootstrap",
      });

      let city_id = $(key).val();
      if (!!city_id) {
        city_id = $(key).val().split(",")[0];
        $(element_city)
          .find(`option[value='${city_id}']`)
          .prop("selected", true);
        // getStatesSearch(country_id);
        $(element_city).select2({
          theme: "bootstrap",
        });
      }
    },
  });
};

$.each(country_form, function (indexInArray, valueOfElement) {
  const number = $(this)[0].name.split("_")[3];
  const country = `[name='company_branch_country_${number}']`;
  const state = `[name='company_branch_state_${number}']`;
  const city = `[name='company_branch_city_${number}']`;
  const key = `[name= 'company_branch_key_${number}']`;
  getCountriesSearch(country, state, city, key);
});

let insertedTags = [];
let companyTags = [];
$("#tags")
  .select2({
    theme: "bootstrap",
    tags: true,
    tokenSeparators: [","],
    placeholder: "Please add your keywords we already have or add your own",
    ajax: {
      url: "/searchTags",
      data: function (params) {
        var query = {
          tag: params.term,
          page: params.page || 1,
        };

        // Query parameters will be ?search=[term]&page=[page]
        return query;
      },
    },
    minimumInputLength: 3,
  })
  .on("select2:close", function () {
    let element = $(this);
    let tags = $.trim($(element).val()).split(",");

    if (tags != "") {
      let tag = tags[tags.length - 1];
      let toInsert = tags.filter((tag) => insertedTags.indexOf(tag) < 0);
      insertedTags.push(tag);
      if (!!toInsert[0]) {
        $.ajax({
          type: "get",
          url: "/insertTags",
          data: {
            tag: toInsert[0],
          },
          success: function (response) {
            insertedTags.push(tag);
          },
        });
      }
    }
  });

$("[name='company_business_name']").blur(() => {
  let count = $("[name='company_business_name']")
    .val()
    .trim()
    .split(" ").length;
  if (count >= 4) {
    $("#company_name_progress").text("10/10");
    changeState($("#company_name_progress"), "success");
  } else if (count >= 3) {
    $("#company_name_progress").text("8/10");
    changeState($("#company_name_progress"), "info");
  } else if (count >= 2) {
    $("#company_name_progress").text("6/10");
    changeState($("#company_name_progress"), "warning");
  } else {
    $("#company_name_progress").text("0/10");
    changeState($("#company_name_progress"), "secondary");
  }
});

$("[name='company_branch_address_0']").blur(() => {
  let count = $("[name='company_branch_address_0']")
    .val()
    .trim()
    .split(" ").length;
  if (!$("[name='company_branch_address_0']").val().trim()) {
    $("#company_address_progress").text("0/10");
    changeState($("#company_address_progress"), "danger");
    return;
  }
  switch (count) {
    case 1:
      $("#company_address_progress").text("1/10");
      changeState($("#company_address_progress"), "danger");
      break;
    case 2:
      $("#company_address_progress").text("2/10");
      changeState($("#company_address_progress"), "danger");
      break;
    case 3:
      $("#company_address_progress").text("3/10");
      changeState($("#company_address_progress"), "warning");
      break;
    case 4:
      $("#company_address_progress").text("4/10");
      changeState($("#company_address_progress"), "warning");
      break;
    case 5:
      $("#company_address_progress").text("5/10");
      changeState($("#company_address_progress"), "warning");
      break;
    case 6:
      $("#company_address_progress").text("6/10");
      changeState($("#company_address_progress"), "secondary");
      break;
    case 7:
      $("#company_address_progress").text("7/10");
      changeState($("#company_address_progress"), "info");
      break;
    case 8:
      $("#company_address_progress").text("8/10");
      changeState($("#company_address_progress"), "info");
      break;
    case 9:
      $("#company_address_progress").text("9/10");
      changeState($("#company_address_progress"), "danger");
      break;
    default:
      $("#company_address_progress").text("10/10");
      changeState($("#company_address_progress"), "success");
      break;
  }
});

$("[name='company_branch_phone_0']").blur(() => {
  const val = $("[name=company_branch_phone_0]").val();
  if (!!val) {
    $("#company_telephone_progress").text("10/10");
    changeState($("#company_telephone_progress"), "success");
  } else {
    $("#company_telephone_progress").text("0/10");
    changeState($("#company_telephone_progress"), "secondary");
  }
});

$("[name='company_branch_fax_0']").blur(() => {
  const val = $("[name=company_branch_fax_0]").val();
  if (!!val) {
    $("#company_fax_progress").text("10/10");
    changeState($("#company_fax_progress"), "success");
  } else {
    $("#company_fax_progress").text("0/10");
    changeState($("#company_fax_progress"), "secondary");
  }
});

$("[name='company_branch_email_0']").blur(() => {
  const val = $("[name=company_branch_email_0]").val();
  if (!!val) {
    $("#company_email_progress").text("10/10");
    changeState($("#company_email_progress"), "success");
  } else {
    $("#company_email_progress").text("0/10");
    changeState($("#company_email_progress"), "secondary");
  }
});

$("[name='company_branch_url_0']").blur(() => {
  const val = $("[name=company_branch_url_0]").val();
  if (!!val) {
    $("#company_url_progress").text("10/10");
    changeState($("#company_url_progress"), "success");
  } else {
    $("#company_url_progress").text("0/10");
    changeState($("#company_url_progress"), "secondary");
  }
});

$(document).ready(function () {
  if ($('[name="countryCategory"]').length > 0) {
    let country_category_form = $('[name="countryCategory"]');
    let state_category_form = $('[name="stateCategory"]');
    let city_category_form = $('[name="cityCategory"]');

    getCountriesSearch(
      $('[name="countryCategory"]'),
      $('[name="stateCategory"]'),
      $('[name="cityCategory"]')
    );

    country_category_form.on("change", function () {
      const country = '[name="countryCategory"]';
      const state = '[name="stateCategory"]';
      const city = '[name="cityCategory"]';
      const key = `[name= 'keyLocationCategoryForm']`;
      $(state).prop("disabled", false);
      $(key).val(``);
      getStatesSearch(state, city, $(country).val(), key);
    });

    state_category_form.on("change", function () {
      const number = $(this)[0].name.split("_")[3];
      const country = '[name="countryCategory"]';
      const state = '[name="stateCategory"]';
      const city = '[name="cityCategory"]';
      $(city).prop("disabled", false);
      const key = `[name= 'keyLocationCategoryForm']`;
      $(key).val(``);
      getCitiesSearch(city, $(state).val(), key);
    });

    city_category_form.on("change", function () {
      const country = '[name="countryCategory"]';
      const state = '[name="stateCategory"]';
      const city = '[name="cityCategory"]';
      const key = `[name= 'keyLocationCategoryForm']`;
      if ($('[name="categoryOnboardingForm"]').length > 0) {
        $('[name="categoryOnboardingForm"]').prop("disabled", false);
      }
      if (!!$(city).val()) {
        $(key).val(`${$(city).val()},${$(state).val()},${$(country).val()}`);
      } else {
        $(key).val(``);
      }
    });

    country_category_form.select2({
      theme: "bootstrap",
      placeholder: "Please select country",
    });

    state_category_form.select2({
      theme: "bootstrap",
      placeholder: "Please slect state",
    });

    city_category_form.select2({
      theme: "bootstrap",
      placeholder: "Please select city",
    });
  }

  if ($('[name="countryCategoryModal"]').length > 0) {
    let country_category_form = $('[name="countryCategoryModal"]');
    let state_category_form = $('[name="stateCategoryModal"]');
    let city_category_form = $('[name="cityCategoryModal"]');

    getCountriesSearch(
      $('[name="countryCategoryModal"]'),
      $('[name="stateCategoryModal"]'),
      $('[name="cityCategoryModal"]')
    );

    country_category_form.on("change", function () {
      const country = '[name="countryCategoryModal"]';
      const state = '[name="stateCategoryModal"]';
      const city = '[name="cityCategoryModal"]';
      const key = `[name= 'keyLocationCategoryFormModal']`;
      $(key).val(``);
      getStatesSearch(state, city, $(country).val(), key);
    });

    state_category_form.on("change", function () {
      const number = $(this)[0].name.split("_")[3];
      const country = '[name="countryCategoryModal"]';
      const state = '[name="stateCategoryModal"]';
      const city = '[name="cityCategoryModal"]';
      const key = `[name= 'keyLocationCategoryFormModal']`;
      $(key).val(``);
      getCitiesSearch(city, $(state).val(), key);
    });

    city_category_form.on("change", function () {
      const country = '[name="countryCategoryModal"]';
      const state = '[name="stateCategoryModal"]';
      const city = '[name="cityCategoryModal"]';
      const key = `[name= 'keyLocationCategoryFormModal']`;
      if (!!$(city).val()) {
        $(key).val(`${$(city).val()},${$(state).val()},${$(country).val()}`);
      } else {
        $(key).val(``);
      }
    });

    country_category_form.select2({
      theme: "bootstrap",
      placeholder: "Please select country",
    });

    state_category_form.select2({
      theme: "bootstrap",
      placeholder: "Please slect state",
    });

    city_category_form.select2({
      theme: "bootstrap",
      placeholder: "Please select city",
    });
  }
});

function companyDetailBlur(contents = "") {
  let count = contents.split(" ").length;
  switch (true) {
    case count < 50:
      $("#company_business_details_progress").text("0/40");
      changeState($("#company_business_details_progress"), "danger");
      break;
    case count > 50 && count < 100:
      $("#company_business_details_progress").text("2/40");
      changeState($("#company_business_details_progress"), "danger");
      break;
    case count > 100 && count < 200:
      $("#company_business_details_progress").text("5/40");
      changeState($("#company_business_details_progress"), "warning");
      break;
    case count > 200 && count < 300:
      $("#company_business_details_progress").text("10/40");
      changeState($("#company_business_details_progress"), "warning");
      break;
    case count > 300 && count < 400:
      $("#company_business_details_progress").text("20/40");
      changeState($("#company_business_details_progress"), "warning");
      break;
    case count > 400 && count < 500:
      $("#company_business_details_progress").text("30/40");
      changeState($("#company_business_details_progress"), "secondary");
      break;
    // case count > 500:
    // 	$("#company_business_details_progress").text('6/40');
    // 	changeState($("#company_business_details_progress"), 'secondary');
    // 	break;
    default:
      $("#company_business_details_progress").text("40/40");
      changeState($("#company_business_details_progress"), "success");
      break;
  }
}

$("#tags").change(() => {
  let count = $("#tags").val().length;
  switch (count) {
    case 0:
      $("#company_keywords_progress").text("0/10");
      changeState($("#company_keywords_progress"), "danger");
      break;
    case 1:
      $("#company_keywords_progress").text("1/10");
      changeState($("#company_keywords_progress"), "danger");
      break;
    case 2:
      $("#company_keywords_progress").text("2/10");
      changeState($("#company_keywords_progress"), "danger");
      break;
    case 3:
      $("#company_keywords_progress").text("3/10");
      changeState($("#company_keywords_progress"), "warning");
      break;
    case 4:
      $("#company_keywords_progress").text("4/10");
      changeState($("#company_keywords_progress"), "warning");
      break;
    case 5:
      $("#company_keywords_progress").text("5/10");
      changeState($("#company_keywords_progress"), "warning");
      break;
    case 6:
      $("#company_keywords_progress").text("6/10");
      changeState($("#company_keywords_progress"), "secondary");
      break;
    case 7:
      $("#company_keywords_progress").text("7/10");
      changeState($("#company_keywords_progress"), "info");
      break;
    case 8:
      $("#company_keywords_progress").text("8/10");
      changeState($("#company_keywords_progress"), "info");
      break;
    case 9:
      $("#company_keywords_progress").text("9/10");
      changeState($("#company_keywords_progress"), "danger");
      break;
    default:
      $("#company_keywords_progress").text("10/10");
      changeState($("#company_keywords_progress"), "success");
      break;
  }
});

function changeState($element, $case) {
  switch ($case) {
    case "success":
      $($element).removeClass("badge-danger");
      $($element).removeClass("badge-warning");
      $($element).removeClass("badge-secondary");
      $($element).addClass("badge-success");
      $($element).removeClass("badge-info");
      break;
    case "warning":
      $($element).removeClass("badge-danger");
      $($element).addClass("badge-warning");
      $($element).removeClass("badge-secondary");
      $($element).removeClass("badge-success");
      $($element).removeClass("badge-info");
      break;
    case "info":
      $($element).removeClass("badge-danger");
      $($element).removeClass("badge-warning");
      $($element).removeClass("badge-secondary");
      $($element).removeClass("badge-success");
      $($element).addClass("badge-info");
      break;
    case "secondary":
      $($element).removeClass("badge-danger");
      $($element).removeClass("badge-warning");
      $($element).addClass("badge-secondary");
      $($element).removeClass("badge-success");
      $($element).removeClass("badge-info");
      break;
    case "danger":
      $($element).addClass("badge-danger");
      $($element).removeClass("badge-warning");
      $($element).removeClass("badge-secondary");
      $($element).removeClass("badge-success");
      $($element).removeClass("badge-info");
      break;
    default:
      break;
  }
}

let other_email = 0;
if (!!$("[name ^= 'other_emails']").length) {
  other_email = $("[name ^= 'other_emails']").length;
}
const addMoreEmails = () => {
  if (other_email < 2) {
    $("[name='company_branch_email_0']")
      .parent()
      .append(
        `<input class="form-control mt-2" type="text" name="other_emails_${other_email}">`
      );
    other_email++;
    $('[name ^= "other_emails"]').each(function (e) {
      $(this).rules("add", {
        minlength: 2,
        email: true,
        messages: {
          email: "Please enter valid email",
        },
      });
    });
  }
};

let other_websites = 0;
if (!!$("[name ^= 'other_websites']").length) {
  other_websites = $("[name ^= 'other_websites']").length;
}
const addMoreWebsites = () => {
  if (other_websites < 2) {
    $("[name='company_branch_url_0']")
      .parent()
      .append(
        `<input class="form-control mt-2" type="text" name="other_websites_${other_websites}">`
      );
    other_websites++;
    $('[name ^= "other_websites"]').each(function (e) {
      $(this).rules("add", {
        minlength: 5,
        validUrl: true,
        messages: {
          url: "Please check your website address",
          minlength: "Please enter more than 5 characters",
        },
      });
    });
  }
};
