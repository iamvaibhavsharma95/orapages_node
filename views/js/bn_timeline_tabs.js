let company_theme = ``;
function timelineOption(profile, timeline, layout = "") {
  if (timeline == "about") {
    $(".orapages-timeline-button").addClass("btn-light");
    $(".orapages-timeline-button").removeClass("btn-primary");
    $("#about-us-button").removeClass("btn-light");
    $("#about-us-button").addClass("btn-primary");
    $("#timeline").removeClass("bg-grey");
    if (!!company_theme) {
      $(".company_theme").addClass(company_theme);
    }
  } else if (timeline == "posts") {
    $(".orapages-timeline-button").removeClass("btn-primary");
    $(".orapages-timeline-button").addClass("btn-light");
    $("#posts-button").addClass("btn-primary");
    $("#timeline").addClass("bg-grey");
    $("#posts-button").removeClass("btn-light");
    if (!!company_theme) {
      $(".company_theme").removeClass(company_theme);
    }
  } else if (timeline == "notifications") {
    $(".orapages-timeline-button").removeClass("btn-primary");
    $(".orapages-timeline-button").addClass("btn-light");
    $("#notification-button").addClass("btn-primary");
    $("#timeline").addClass("bg-grey");
    $("#notification-button").removeClass("btn-light");
    if (!!company_theme) {
      $(".company_theme").removeClass(company_theme);
    }
  }
  $.ajax({
    type: "get",
    url: `/${profile}/${timeline}?layout=${layout}`,
    success: function (response) {
      $("#timeline").html(response);
      preActionBusinessNetwork($("#user_id").val());
      if (timeline !== "about") {
        if (window.location.href.split("/post").length < 2) {
          gtag("config", "UA-161607363-1", {
            page_path: `/${window.location.href}/post`,
          });
          window.history.replaceState(
            "Post",
            `Page post`,
            `${window.location.href}/post`
          );
        }
      } else {
        gtag("config", "UA-161607363-1", {
          page_path: `/${window.location.href.split("/post")[0]}`,
        });
        window.history.replaceState(
          "Post",
          `Page post`,
          `${window.location.href.split("/post")[0]}`
        );
      }
    },
    failure: function () {
      $("#timeline").html("<h1>Please Try again later</h1>");
    },
  });
}

function post_box() {
  $.ajax({
    type: "get",
    url: "/post_box",
    success: function (response) {
      $("#request_modal").html(response);
      $(".request_modal").modal("show");
    },
    failure: function () {
      $("#timeline").html("<h1>Please Try again later</h1>");
    },
  });
}

$("#friend_request_button").on("click", function () {
  $.ajax({
    type: "post",
    url: "/friend_request",
    data: {
      _csrf: $("[name='_csrf']").val(),
      requestee_id: $(this).attr("requestee_id"),
    },
    success: function (response) {
      $("#friend_request_button").html(response);
    },
    error: function (a, b, c) {
      alert("Invalid");
    },
  });
});

$("#follow_button").on("click", function () {
  $.ajax({
    type: "post",
    url: "/follow",
    data: {
      _csrf: $("[name='_csrf']").val(),
      following: $(this).attr("following"),
    },
    success: function (response) {
      $("#follow_button").html(response);
    },
    error: function (a, b, c) {
      alert("Invalid");
    },
  });
});

function addFriend(requestee_id, button) {
  $.ajax({
    type: "post",
    url: "/friend_request",
    data: {
      _csrf: $("[name='_csrf']").val(),
      requestee_id: requestee_id,
    },
    success: function (response) {
      $(button).html(response);
    },
    error: function (a, b, c) {
      $(".request_modal").modal("hide");
    },
  });
}

function follow(following, button) {
  $.ajax({
    type: "post",
    url: "/follow",
    data: {
      _csrf: $("[name='_csrf']").val(),
      following: following,
    },
    success: function (response) {
      $(button).html(response);
    },
    error: function (a, b, c) {
      $(".request_modal").modal("hide");
    },
  });
}

function deleteFriend(user1_id, user2_id, button) {
  $.ajax({
    type: "post",
    url: "/friends/delete_friend",
    data: {
      _csrf: $("[name='_csrf']").val(),
      user1_id,
      user2_id,
    },
    success: function (response) {
      $(button).parents(".list-group-item").remove();
    },
    error: function (a, b, c) {
      $("#request_modal").html(response);
      $(".request_modal").modal("show");
    },
  });
}

function removeFollow(follower, button) {
  $.ajax({
    type: "post",
    url: "/follow/remove_follow",
    data: {
      _csrf: $("[name='_csrf']").val(),
      follower,
    },
    success: function (response) {
      $(button).parents(".list-group-item").remove();
    },
    error: function (a, b, c) {
      $("#request_modal").html(response);
      $(".request_modal").modal("show");
    },
  });
}

function unfollow(following, button) {
  $.ajax({
    type: "post",
    url: "/follow/unfollow",
    data: {
      _csrf: $("[name='_csrf']").val(),
      following: following,
    },
    success: function (response) {
      $(button).parents(".list-group-item").remove();
    },
    error: function (a, b, c) {
      $("#request_modal").html(response);
      $(".request_modal").modal("show");
    },
  });
}

function acceptRequest(sender, button) {
  $(button).prop("disabled", true);
  $.ajax({
    type: "get",
    url: "/friend_request/accept",
    data: {
      sender,
    },
    success: function (response) {
      $(button).parents(".list-group-item").remove();
      // $(button).html(response)
    },
  });
}

function declienRequest(sender, button) {
  $(button).prop("disabled", true);
  $.ajax({
    type: "get",
    url: "/friend_request/decline",
    data: {
      sender,
    },
    success: function (response) {
      $(button).prop("disabled", false);
      $(button).html(response);
    },
  });
}

function getFriendsCountFunc(url) {
  $.ajax({
    type: "get",
    url: url,
    success: function (response) {
      $("#friends_count").html(`( ${response.count} )`);
    },
  });
}

function preActionBusinessNetwork(user_id) {
  $("#get_requests").on("click", function () {
    $.ajax({
      type: "get",
      url: `/friend_request`,
      success: function (response) {
        $("#list_requests").html(response);
      },
      error: (a, b, c) => {
        $("#request_modal").html(b.response);
        $(".request_modal").modal("show");
      },
    });
  });

  let getFriends = `/friends`;
  let getFollowersUrl = `/follow`;
  let getFriendsCount = `/friends/friend_count`;
  if (!!user_id) {
    getFriends = `/friends/${user_id}`;
    getFriendsCount = `/friends/friend_count/${user_id}`;
    getFollowersUrl = `/follow/${user_id}`;
  }

  getFriendsCountFunc(getFriendsCount);

  $("#get_friends").on("click", function () {
    $.ajax({
      type: "get",
      url: getFriends,
      success: function (response) {
        $("#list_friends").html(response);
        // $(".request_modal").modal("show");
      },
      error: (a, b, c) => {
        $("#request_modal").html(b.response);
        $(".request_modal").modal("show");
      },
    });
  });

  $("#get_suggestions").on("click", function () {
    getSuggestions(this);
  });

  $("#get_followers").on("click", function () {
    getFollowers(this, getFollowersUrl);
  });

  $("#get_followings").on("click", function () {
    getFollowings(this);
  });
  // $(document).ready(function () {
  $.ajax({
    type: "get",
    url: `/friend_request/requestCount`,
    success: function (response) {
      $("#friend_request_count").html(`( ${response.count} )`);
    },
  });

  $.ajax({
    type: "get",
    url: "/friend_request/check",
    data: {
      requestee_id: $("#friend_request_button").attr("requestee_id"),
    },
    success: function (response) {
      $("#friend_request_button").prop("disabled", false);
      $("#friend_request_button").html(`<span>${response}</span>`);
    },
  });

  $.ajax({
    type: "get",
    url: "/follow/check",
    data: {
      following: $("#follow_button").attr("following"),
    },
    success: function (response) {
      $("#follow_button").prop("disabled", false);
      $("#follow_button").html(`<span>${response}</span>`);
    },
  });

  // initialSuggestions();
  // });
}

function getSuggestionsCount() {
  $.ajax({
    type: "get",
    url: "/suggestions/suggestion_count",
    success: function (response) {
      $("#suggestion_count").html(response.count);
    },
  });
}

function getSuggestions(button) {
  $(button).html(`<b>Getting Suggestions</b>`);
  $.ajax({
    type: "get",
    url: "/suggestions",
    success: function (response) {
      $(button).html(`<b>Friends' Suggestions</b>`);
      $("#list_suggestions").html(response);
    },
    failure: function () {
      $("#timeline").html("<h1>Please Try again later</h1>");
    },
  });
}

function getFollowers(button, url) {
  $(button).html(`<b>Getting Followers</b>`);
  $.ajax({
    type: "get",
    url: url,
    success: function (response) {
      $(button).html(`<b>Followers</b>`);
      $("#list_followers").html(response);
    },
    failure: function () {
      $("#timeline").html("<h1>Please Try again later</h1>");
    },
  });
}

function getFollowings(button) {
  $(button).html(`<b>Getting Followings</b>`);
  $.ajax({
    type: "get",
    url: "/follow/followings",
    success: function (response) {
      $(button).html(`<b>Followings</b>`);
      $("#list_followings").html(response);
    },
    failure: function () {
      $("#timeline").html("<h1>Please Try again later</h1>");
    },
  });
}

function initialSuggestions() {
  $.ajax({
    type: "get",
    url: "/suggestions/initial_suggestions",
    success: function (response) {
      $("#request_modal").html(response);
      $(".request_modal").modal("show");
    },
    failure: function () {
      // $("#timeline").html("<h1>Please Try again later</h1>");
    },
  });
}
