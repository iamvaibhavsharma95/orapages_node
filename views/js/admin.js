const rewrite = (name) =>
  name
    .replace(/[^a-z0-9A-z]/g, "-")
    .replace(/-+$/, "")
    .replace(/-+/g, "-")
    .toLowerCase();

function adminSignin() {
  let email = $('[name="admin_email"]').val();
  let password = $('[name="admin_password"]').val();
  let _csrf = $('[name="_csrf"]').val();
  $("#admin_signin").html("Please Wait..");
  $("#admin_signin").prop("disabled", "true");
  $.ajax({
    type: "post",
    url: "/admin_panel",
    data: {
      email,
      password,
      _csrf,
    },
    success: function (response) {
      window.location.href = "/admin_panel";
    },
    error: function (a, b, c) {
      $("[name='admin_password']").toggleClass("invalid");
      $(
        `<span id="admin_email-error" class="helper-text error">` +
          b.responseText +
          `</span>`
      ).insertAfter($("[name = 'admin_password'] + label"));
      $("#admin_signin").html("Sign in");
      $("#admin_signin").prop("disabled", false);
    },
  });
}

function addCountry($country, $form) {
  $.ajax({
    type: "post",
    url: "/admin/add_country",
    data: {
      country: $country,
    },
    success: function (response) {
      $($form)[0].reset();
      swal("Done!", "Country has been added!", "success");
    },
    error: function (a, b, c) {
      $($form)[0].reset();
      swal("Sorry!", "Country is already existed!", "warning");
    },
  });
}

function updateCountry($key, $new_name, $form) {
  $.ajax({
    type: "post",
    url: "/admin/update_country",
    data: {
      country: $new_name,
      country_id: $key,
    },
    success: function (response) {
      $($form)[0].reset();
      countryResult();
      swal("Done!", "Country has been changed!", "success");
    },
    error: function (a, b, c) {
      $($form)[0].reset();
      countryResult();
      swal("Sorry!", "Country is not changed!", "warning");
    },
  });
}

function deleteCountry($key, $form) {
  swal(
    {
      title: "Are you sure?",
      text: "You will not be able to recover this Country!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, cancel please!",
      closeOnConfirm: false,
      closeOnCancel: false,
    },
    function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          type: "post",
          url: "/admin/delete_country",
          data: {
            country_id: $key,
          },
          success: function (response) {
            $($form)[0].reset();
            countryResult();
            swal("Deleted", "Country has been removed", "success");
          },
          error: function (a, b, c) {
            countryResult();
            swal("Failed", a.responseText, "error");
          },
        });
      } else {
        swal("Cancelled", "Country is not removed :)", "error");
      }
    }
  );
}

function addState($country, $state, $form) {
  $.ajax({
    type: "post",
    url: "/admin/add_states",
    data: {
      states: $state,
      country_id: $country,
    },
    success: function (response) {
      countryResult();
      $($form)[0].reset();
      swal("Done!", response, "success");
    },
    error: function (a, b, c) {
      countryResult();
      $($form)[0].reset();
      swal("Sorry!", "There is some internal problem!", "warning");
    },
  });
}

function updateState($country, $state, $state_name_new, $form) {
  $.ajax({
    type: "post",
    url: "/admin/update_state",
    data: {
      country_id: $country,
      state_id: $state,
      state: $state_name_new,
    },
    success: function (response) {
      update_state();
      $('[name="state_name"]').remove();
      $(".state-edit").hide();
      $($form)[0].reset();
      swal("Done!", response, "success");
    },
    error: function (a, b, c) {
      update_state();
      $('[name="state_name"]').remove();
      $(".state-edit").hide();
      $($form)[0].reset();
      swal("Sorry!", "State is not updated!", "error");
    },
  });
}

function deleteState($country, $state, $form) {
  swal(
    {
      title: "Are you sure?",
      text: "You will not be able to recover this State!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, cancel please!",
      closeOnConfirm: false,
      closeOnCancel: false,
    },
    function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          type: "post",
          url: "/admin/delete_state",
          data: {
            country_id: $country,
            state_id: $state,
          },
          success: function (response) {
            update_state();
            $('[name="state_name"]').remove();
            $(".state-edit").hide();
            $($form)[0].reset();
            swal("Deleted", "State has been removed", "success");
          },
          error: function (a, b, c) {
            update_state();
            $('[name="state_name"]').remove();
            $(".state-edit").hide();
            $($form)[0].reset();
            swal("Failed", a.responseText, "error");
          },
        });
      } else {
        swal("Cancelled", "State is not removed :)", "error");
      }
    }
  );
}

function addCity($country, $state, $city, $form) {
  $.ajax({
    type: "post",
    url: "/admin/add_cities",
    data: {
      country_id: $country,
      state_id: $state,
      cities: $city,
    },
    success: function (response) {
      update_state_city();
      $($form)[0].reset();
      $('[name="state_name"]').remove();
      $('[name="city_name"]').hide();
      swal("Done!", response, "success");
    },
    error: function (a, b, c) {
      update_state_city();
      $($form)[0].reset();
      $('[name="state_name"]').remove();
      $('[name="city_name"]').hide();
      swal("Sorry!", "Cities are not added", "error");
    },
  });
}

function updateCity($country, $state, $city, $city_name_new, $form) {
  $.ajax({
    type: "post",
    url: "/admin/update_city",
    data: {
      country_id: $country,
      state_id: $state,
      city_id: $city,
      city: $city_name_new,
    },
    success: function (response) {
      cityResults();
      $(".city-edit").hide();
      $('[name="city_name"]').remove();
      $('[name="state_name"]').remove();
      $($form)[0].reset();
      swal("Done!", response, "success");
    },
    error: function (a, b, c) {
      cityResults();
      $(".city-edit").hide();
      $('[name="city_name"]').remove();
      $('[name="state_name"]').remove();
      $($form)[0].reset();
      swal("Sorry!", "City is not updated!", "error");
    },
  });
}

function deleteCity($country, $state, $city, $form) {
  swal(
    {
      title: "Are you sure?",
      text: "You will not be able to recover this City!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, cancel please!",
      closeOnConfirm: false,
      closeOnCancel: false,
    },
    function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          type: "post",
          url: "/admin/delete_city",
          data: {
            country_id: $country,
            state_id: $state,
            city_id: $city,
          },
          success: function (response) {
            cityResults();
            $(".city-edit").hide();
            $('[name="city_name"]').remove();
            $('[name="state_name"]').remove();
            $($form)[0].reset();
            swal("Done!", response, "success");
          },
          error: function (a, b, c) {
            cityResults();
            $(".city-edit").hide();
            $('[name="city_name"]').remove();
            $('[name="state_name"]').remove();
            $($form)[0].reset();
            swal("Sorry!", a.responseText, "error");
          },
        });
      } else {
        swal("Cancelled", "City is not removed :)", "error");
      }
    }
  );
}

/**
 * Category Section start
 *
 *
 */
function addCategory($category, $form) {
  $.ajax({
    type: "post",
    url: "/admin/add_category",
    data: {
      category: $category,
    },
    success: function (response) {
      $($form)[0].reset();
      swal("Done!", "Category has been added!", "success");
    },
    error: function (a, b, c) {
      $($form)[0].reset();
      swal("Sorry!", "Category is already existed!", "warning");
    },
  });
}

function updateCategory($key, $new_name, $form) {
  $.ajax({
    type: "post",
    url: "/admin/update_category",
    data: {
      category: $new_name,
      category_id: $key,
    },
    success: function (response) {
      getCategoriesAuto();
      $($form)[0].reset();
      swal("Done!", "Category has been updated!", "success");
    },
    error: function (a, b, c) {
      getCategoriesAuto();
      $($form)[0].reset();
      swal("Sorry!", "Category is not updated!", "warning");
    },
  });
}

function deleteCategory($key, $form) {
  swal(
    {
      title: "Are you sure?",
      text: "You will not be able to recover this Category!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, cancel please!",
      closeOnConfirm: false,
      closeOnCancel: false,
    },
    function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          type: "post",
          url: "/admin/delete_category",
          data: {
            category_id: $key,
          },
          success: function (response) {
            getCategoriesAuto();
            $($form)[0].reset();
            swal("Done!", "Category has been deleted!", "success");
          },
          error: function (a, b, c) {
            getCategoriesAuto();
            $($form)[0].reset();
            swal("Sorry!", a.responseText, "warning");
          },
        });
      } else {
        swal("Cancelled", "Category is not removed :)", "error");
      }
    }
  );
}

/**
 *
 *
 *  Subcategories Start
//  */

function addSubcategory($category_arr, $subcategory, $form) {
  $.ajax({
    type: "post",
    url: "/admin/add_subcategory",
    data: {
      category_arr: [...$category_arr],
      subcategory: $subcategory,
    },
    success: function (response) {
      getCategoriesAuto();
      $($form)[0].reset();
      $('[name="subcategories"]').html("");
      $("#selected_parent").hide();
      $("#selected_parent li").remove();
      swal(
        "Done!",
        "Subcategory [" + $subcategory + "] have been added!",
        "success"
      );
    },
    error: function (a, b, c) {
      getCategoriesAuto();
      $($form)[0].reset();
      $('[name="subcategories"]').html("");
      $("#selected_parent").hide();
      $("#selected_parent li").remove();
      swal(
        "Sorry!",
        "Subcategory [" + $subcategory + "] is not added!",
        "error"
      );
    },
  });
}

function addNonDisSubcategory($category_arr, $subcategory, $form) {
  $.ajax({
    type: "post",
    url: "/admin/add_non_dis_subcategory",
    data: {
      category_arr: [...$category_arr],
      subcategory: $subcategory,
    },
    success: function (response) {
      getCategoriesAuto();
      $($form)[0].reset();
      $('[name="subcategories"]').html("");
      $("#selected_parent").hide();
      $("#selected_parent li").remove();
      swal(
        "Done!",
        "Subcategory [" + $subcategory + "] have been added!",
        "success"
      );
    },
    error: function (a, b, c) {
      getCategoriesAuto();
      $($form)[0].reset();
      $('[name="subcategories"]').html("");
      $("#selected_parent").hide();
      $("#selected_parent li").remove();
      swal(
        "Sorry!",
        "Subcategory [" + $subcategory + "] is not added!",
        "error"
      );
    },
  });
}

function updateSubcategory($category_arr, $subcategory_id, $new_name, $form) {
  $.ajax({
    type: "post",
    url: "/admin/update_subcategory",
    data: {
      category_arr: [...$category_arr],
      subcategory_id: $subcategory_id,
      subcategory: $new_name,
    },
    success: function (response) {
      $($form)[0].reset();
      addedCategory.clear();
      $('[name="subcategory_name"]').autocomplete(searchSubCat);
      $(".category-edit").hide();
      $(".subcategory-edit").hide();
      $("#selected_parent").hide();
      $("#selected_parent li").remove();
      swal("Done!", "Subcategory has been updated!", "success");
    },
    error: function (a, b, c) {
      swal("Done!", "Subcategory is not updated!", "warning");
    },
  });
}

function updateNonDisSubcategory(
  $category_arr,
  $subcategory_id,
  $new_name,
  $form
) {
  $.ajax({
    type: "post",
    url: "/admin/update_non_dis_subcategory",
    data: {
      category_arr: [...$category_arr],
      subcategory_id: $subcategory_id,
      subcategory: $new_name,
    },
    success: function (response) {
      $($form)[0].reset();
      addedCategory.clear();
      $('[name="non_dis_subcategory_name"]').autocomplete(searchNonDisSubCat);
      $(".category-edit").hide();
      $(".subcategory-edit").hide();
      $("#selected_parent").hide();
      $("#selected_parent li").remove();
      swal("Done!", "Non Displayable Subcategory has been updated!", "success");
    },
    error: function (a, b, c) {
      swal("Done!", "Non Displayable Subcategory is not updated!", "warning");
    },
  });
}

function deleteSubcategory(
  $category_id,
  $subcategory_id,
  $form = null,
  $this = null
) {
  if (!!$form) {
    $.ajax({
      type: "post",
      url: "/admin/delete_subcategory",
      data: {
        subcategory_id: $subcategory_id,
      },
      success: function (response) {
        $($this).parent("li").remove();
        addedCategory.clear();
        swal("Deleted!", "Subcategory has been removed!", "success");
        $('[name="subcategory_name"]').autocomplete(searchSubCat);
        $(".category-edit").hide();
        $(".subcategory-edit").hide();
        $("#selected_parent").hide();
        $("#selected_parent li").remove();
      },
      error: function (a, b, c) {
        swal("Sorry!", "Subcategory is not removed!", "error");
      },
    });
  } else if (!!$this) {
    swal(
      {
        title: "Are you sure?",
        text: "You will not be able to recover this child under this category!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel please!",
        closeOnConfirm: false,
        closeOnCancel: false,
      },
      function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type: "post",
            url: "/admin/delete_subcat_cat",
            data: {
              category_id: $category_id,
              subcategory_id: $subcategory_id,
            },
            success: function (response) {
              $($this).parent("li").remove();
              addedCategory.delete($category_id);
              if (addedCategory.size <= 0) {
                $("#selected_parent").hide();
              }
              swal(
                "Deleted!",
                "Subcategory under category has been removed!",
                "success"
              );
            },
            error: function (a, b, c) {
              swal("Sorry!", "Subcategory is not removed!", "warning");
            },
          });
        } else {
          swal("Cancelled", "Subcategory is not removed :)", "error");
        }
      }
    );
  }
}

function deleteNonDisSubcategory(
  $category_id,
  $subcategory_id,
  $form = null,
  $this = null
) {
  if (!!$form) {
    $.ajax({
      type: "post",
      url: "/admin/delete_non_dis_subcategory",
      data: {
        subcategory_id: $subcategory_id,
      },
      success: function (response) {
        $($this).parent("li").remove();
        addedCategory.clear();
        swal(
          "Deleted!",
          "Non Displayable Subcategory has been removed!",
          "success"
        );
        $('[name="non_dis_subcategory_name"]').autocomplete(searchNonDisSubCat);
        $(".category-edit").hide();
        $(".subcategory-edit").hide();
        $("#selected_parent").hide();
        $("#selected_parent li").remove();
      },
      error: function (a, b, c) {
        swal("Sorry!", "Subcategory is not removed!", "error");
      },
    });
  } else if (!!$this) {
    swal(
      {
        title: "Are you sure?",
        text: "You will not be able to recover this child under this category!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel please!",
        closeOnConfirm: false,
        closeOnCancel: false,
      },
      function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            type: "post",
            url: "/admin/delete_non_dis_subcat_cat",
            data: {
              category_id: $category_id,
              subcategory_id: $subcategory_id,
            },
            success: function (response) {
              $($this).parent("li").remove();
              addedCategory.delete($category_id);
              if (addedCategory.size <= 0) {
                $("#selected_parent").hide();
              }
              swal(
                "Deleted!",
                "Non Displayable Subcategory under category has been removed!",
                "success"
              );
            },
            error: function (a, b, c) {
              swal(
                "Sorry!",
                "Non Displayable Subcategory is not removed!",
                "warning"
              );
            },
          });
        } else {
          swal(
            "Cancelled",
            "Non Displayable Subcategory is not removed :)",
            "error"
          );
        }
      }
    );
  }
}

function transferCompaniesCity($first, $second) {
  if ($first == $second) {
    swal("Sorry!", "Both locations are same", "error");
    return;
  }

  $.ajax({
    type: "get",
    url: "/admin/transfer_companies_by_city",
    data: {
      first_city: $first,
      second_city: $second,
    },
    success: function (response) {
      $('[name = "city_first_name"]').prop("location", "");
      $('[name = "city_first_name"]').val("");
      $('[name = "city_second_name"]').prop("location", "");
      $('[name = "city_second_name"]').val("");
      $("#selected_companies").html("");
      $("#selected_companies").hide();
      swal("Done!", response, "success");
    },
    error: function (a, b, c) {
      $('[name = "city_first_name"]').prop("location", "");
      $('[name = "city_first_name"]').val("");
      $('[name = "city_second_name"]').prop("location", "");
      $('[name = "city_second_name"]').val("");
      $("#selected_companies").html("");
      $("#selected_companies").hide();
      swal("Sorry!", a.responseText, "error");
    },
  });
}

function transferCompaniesCategory($first, $second) {
  if ($first == $second) {
    swal("Sorry!", "Both locations are same", "error");
    return;
  }

  $.ajax({
    type: "get",
    url: "/admin/transfer_companies_by_category",
    data: {
      first_category: $first,
      second_category: $second,
    },
    success: function (response) {
      $('[name = "category_first_name"]').prop("location", "");
      $('[name = "category_first_name"]').val("");
      $('[name = "category_second_name"]').prop("location", "");
      $('[name = "category_second_name"]').val("");
      $("#selected_companies").html("");
      $("#selected_companies").hide();
      swal("Done!", response, "success");
    },
    error: function (a, b, c) {
      $('[name = "category_first_name"]').prop("location", "");
      $('[name = "category_first_name"]').val("");
      $('[name = "category_second_name"]').prop("location", "");
      $('[name = "category_second_name"]').val("");
      $("#selected_companies").html("");
      $("#selected_companies").hide();
      swal("Sorry!", a.responseText, "error");
    },
  });
}

function uploadCSV($csv) {
  // console.log($csv);
  let formData = new FormData();
  formData.append("csv", $csv);

  $.ajax({
    type: "post",
    url: "/admin/upload_csv",
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      if (confirm("Take a view of the csv!")) {
        window.location.href = `/csv_edit?csv_id=${response.csv_id}`;
      }
    },
    error: function (a, b, c) {
      alert("CSV is not uploaded");
    },
  });
}

function uploadAutoCSV($csv) {
  // console.log($csv);
  let formData = new FormData();
  formData.append("csv", $csv);

  $.ajax({
    type: "post",
    url: "/admin/auto_upload_csv",
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      if (confirm("Take a view of the csv!")) {
        window.location.href = `/csv_edit?csv_id=${response.csv_id}&type=auto_bulk`;
      }
    },
    error: function (a, b, c) {
      alert("CSV is not uploaded");
    },
  });
}

function uploadLargeCSV($csv) {
  let formData = new FormData();
  formData.append("csv", $csv);

  $.ajax({
    type: "post",
    url: "/admin/upload_large_csv",
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      alert(response);
    },
    error: function (a, b, c) {
      alert("CSV is not uploaded");
    },
  });
}

function bulkLink($url) {
  if ($(".edited").length > 0) {
    swal(
      {
        title: "Are you sure?",
        text: "Your all data that is not saved will be lost please save it!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes save it!",
        cancelButtonText: "No, ingore please!",
        closeOnConfirm: false,
        closeOnCancel: false,
      },
      function (isConfirm) {
        if (isConfirm) {
          updateTempCompanies(csvId, $url);
        } else {
          window.location = $url;
        }
      }
    );
  } else {
    window.location = $url;
  }
}

let searchCat = {
  open: function (e, ui) {
    $(".working").css("display", "none");
    var acData = $(this).data("uiAutocomplete");

    acData.menu.element
      .find("li.ui-menu-item .ui-menu-item-wrapper")
      .each(function () {
        var me = $(this);
        var regex = new RegExp(acData.term, "gi");
        me.html(
          me.text().replace(regex, function (matched) {
            return termTemplate.replace("%s", matched);
          })
        );
      });
  },
  source: function (req, res) {
    $.ajax({
      type: "get",
      url: "/admin/get_categories",
      data: {
        term: req.term,
      },
      success: function (response) {
        res(response);
      },
      error: function (a, b, c) {
        res([
          {
            label: "No categories found",
          },
        ]);
      },
    });
  },
  minLength: 2,
  delay: 400,
  select: function (event, ui) {
    $('[name = "category_name"]').prop("key", ui.item.key);
    if ($('[name = "category_name"]').attr("only") == "category") {
      $(".category-edit").show();
      $(this).autocomplete("destroy");
    }
    if ($('[name="category_name"]').attr("only") == "add_subcategory") {
      setTimeout(() => {
        $(this).val("");
      }, 3);
      $("#selected_parent").show();
      if (!addedCategory.has(`${ui.item.key}`)) {
        addedCategory.add(ui.item.key);
        $("#selected_parent").append(`<li>${ui.item.value}
                    <button type="button" onclick="$(this).parent('li').remove();addedCategory.delete('${ui.item.key}');if(addedCategory.size <= 0){ $('#selected_parent').hide() }">
                    Remove</button></li>`);
      }
    }
  },
};

let addedCategory = new Set();

let searchSubCat = {
  open: function (e, ui) {
    $(".working").css("display", "none");
    var acData = $(this).data("uiAutocomplete");
    acData.menu.element
      .find("li.ui-menu-item .ui-menu-item-wrapper")
      .each(function () {
        var me = $(this);
        var regex = new RegExp(acData.term, "gi");
        me.html(
          me.text().replace(regex, function (matched) {
            return termTemplate.replace("%s", matched);
          })
        );
      });
  },
  source: function (req, res) {
    $.ajax({
      type: "get",
      url: "/admin/get_subcategories",
      data: {
        term: req.term,
      },
      success: function (response) {
        res(response);
      },
      error: function (a, b, c) {
        res([
          {
            label: "No subcategories found",
          },
        ]);
      },
    });
  },
  minLength: 2,
  delay: 400,
  select: function (event, ui) {
    $('[name = "subcategory_name"]').prop("key", ui.item.key);
    if ($('[name="subcategory_name"]').attr("only") == "edit_subcategory") {
      $('[name="subcategory_name"]').show();
      $(this).autocomplete("destroy");
      $(".category-edit").show();
      $(".subcategory-edit").show();
      addedCategory = new Set();
      $.ajax({
        type: "get",
        url: "/admin/get_subcat_cat",
        data: {
          subcategory_id: ui.item.key,
        },
        success: function (response) {
          for (const category of response) {
            addedCategory.add(category.key);
            $("#selected_parent").show();
            $("#selected_parent").append(`<li>${category.value}
                        <button type="button" onclick="deleteSubcategory('${category.key}', '${ui.item.key}',null,this);">
                        Remove</button></li>`);
          }
        },
        error: function (a, b, c) {
          alert([
            {
              label: "No subcategories found",
            },
          ]);
        },
      });
    }
  },
};

let searchNonDisSubCat = {
  open: function (e, ui) {
    $(".working").css("display", "none");
    var acData = $(this).data("uiAutocomplete");
    acData.menu.element
      .find("li.ui-menu-item .ui-menu-item-wrapper")
      .each(function () {
        var me = $(this);
        var regex = new RegExp(acData.term, "gi");
        me.html(
          me.text().replace(regex, function (matched) {
            return termTemplate.replace("%s", matched);
          })
        );
      });
  },
  source: function (req, res) {
    $.ajax({
      type: "get",
      url: "/admin/get_non_dis_subcategories",
      data: {
        term: req.term,
      },
      success: function (response) {
        res(response);
      },
      error: function (a, b, c) {
        res([
          {
            label: "No Non Displayable subcategories found",
          },
        ]);
      },
    });
  },
  minLength: 2,
  delay: 400,
  select: function (event, ui) {
    $('[name = "non_dis_subcategory_name"]').prop("key", ui.item.key);
    if (
      $('[name="non_dis_subcategory_name"]').attr("only") == "edit_subcategory"
    ) {
      $('[name="non_dis_subcategory_name"]').show();
      $(this).autocomplete("destroy");
      $(".category-edit").show();
      $(".subcategory-edit").show();
      addedCategory = new Set();
      $.ajax({
        type: "get",
        url: "/admin/get_non_dis_subcat_cat",
        data: {
          subcategory_id: ui.item.key,
        },
        success: function (response) {
          for (const category of response) {
            addedCategory.add(category.key);
            $("#selected_parent").show();
            $("#selected_parent").append(`<li>${category.value}
                        <button type="button" onclick="deleteNonDisSubcategory('${category.key}', '${ui.item.key}',null,this);">
                        Remove</button></li>`);
          }
        },
        error: function (a, b, c) {
          alert([
            {
              label: "No Non Displayable subcategories found",
            },
          ]);
        },
      });
    }
  },
};

var termTemplate = "<span class='ui-autocomplete-term'>%s</span>";

let openHigh = function (e, ui) {
  $(".working").css("display", "none");
  var acData = $(this).data("uiAutocomplete");

  acData.menu.element
    .find("li.ui-menu-item .ui-menu-item-wrapper")
    .each(function () {
      var me = $(this);
      var regex = new RegExp(acData.term, "gi");
      me.html(
        me.text().replace(regex, function (matched) {
          return termTemplate.replace("%s", matched);
        })
      );
    });
};

const countryResult = () => {
  if ($(".country-edit").length > 0) {
    $(".country-edit").hide();
  }
  $('[name="country_name"]').autocomplete({
    open: openHigh,
    source: function (request, response) {
      $.ajax({
        type: "get",
        url: "/admin/get_countries",
        data: {
          term: request.term,
        },
        success: function (res) {
          response(res);
        },
        error: function (a, b, c) {
          response([
            {
              label: "No countries found",
            },
          ]);
        },
      });
    },
    select: function (event, ui) {
      $('[name = "country_name"]').prop("key", ui.item.key);
      if ($('[name = "country_name"]').attr("only") == "country") {
        $(".country-edit").show();
        $(this).autocomplete("destroy");
      } else if ($('[name = "country_name"]').attr("only") == "state") {
        getCityStateCountry({
          type: "state",
          subtype: "update_state",
          country_id: ui.item.key,
        });
      } else if ($('[name = "country_name"]').attr("only") == "state_update") {
        getCityStateCountry({
          type: "state",
          subtype: "update_state_city",
          country_id: ui.item.key,
        });
      }
    },
  });
};

const update_state = (country_id) => {
  if ($(".state-edit").length > 0) {
    $(".state-edit").hide();
  }
  $('[name="state_name"]').autocomplete({
    open: openHigh,
    source: function (request, response) {
      $.ajax({
        type: "get",
        url: "/admin/get_states",
        data: {
          country_id,
          term: request.term,
        },
        success: function (res) {
          response(res);
        },
        error: function (a, b, c) {
          response([
            {
              label: "No results found",
            },
          ]);
        },
      });
    },
    select: function (event, ui) {
      $('[name = "state_name"]').prop("key", ui.item.key);
      $(".state-edit").show();
      $(this).autocomplete("destroy");
      if ($('[name="city_name"]').length > 0) {
        $('[name="city_name"]').show();
      }
    },
  });
};

const update_state_city = (country_id) => {
  if ($(".state-edit").length > 0) {
    $(".state-edit").hide();
  }
  $('[name="state_name"]').autocomplete({
    open: openHigh,
    source: function (request, response) {
      $.ajax({
        type: "get",
        url: "/admin/get_states",
        data: {
          country_id,
          term: request.term,
        },
        success: function (res) {
          response(res);
        },
        error: function (a, b, c) {
          response([
            {
              label: "No results found",
            },
          ]);
        },
      });
    },
    select: function (event, ui) {
      $('[name = "state_name"]').prop("key", ui.item.key);
      getCityStateCountry({
        type: "city",
        state_id: ui.item.key,
        country_id: country_id,
      });
    },
  });
};

const cityResults = (state_id, country_id) => {
  if (!state_id && !country_id) {
    return;
  }

  if ($(".city-edit").length > 0) {
    $(".city-edit").hide();
  }
  $('[name="city_name"]').autocomplete({
    open: openHigh,
    source: function (request, response) {
      $.ajax({
        type: "get",
        url: "/admin/get_cities",
        data: {
          country_id,
          state_id,
          term: request.term,
        },
        success: function (res) {
          response(res);
        },
        error: function (a, b, c) {
          response([
            {
              label: "No results found",
            },
          ]);
        },
      });
    },
    select: function (event, ui) {
      $('[name = "city_name"]').prop("key", ui.item.key);
      if ($('[name = "city_name"]').attr("only") == "city") {
        $(this).autocomplete("destroy");
        $(".city-edit").show();
      }
    },
  });
};

function getCityStateCountry($option) {
  switch ($option.type) {
    case "country":
      countryResult();

      break;

    case "state":
      switch ($option.subtype) {
        case "update_state":
          $("#state-row").html(`<div class="input-field col s12">
                        <input name="state_name" type="text" placeholder="Please select state">
                    </div>`);

          update_state($option.country_id);

          break;

        case "update_state_city":
          $("#state-row").html(`<div class="input-field col s12">
                        <input name="state_name" type="text" placeholder="Please select state">
                    </div>`);

          update_state_city($option.country_id);

          break;
      }
      break;
    case "city":
      $("#city-row").html(`<div class="input-field col s12">
                    <input name="city_name" type="text" placeholder="Please select the city" only="city">
                </div>`);

      cityResults($option.state_id, $option.country_id);

      break;
  }
}

function getCategoriesAuto() {
  $('[name = "category_name"]').autocomplete(searchCat);
  if ($(".category-edit").length > 0) {
    $(".category-edit").hide();
  }
}

function getSubCategoriesAuto() {
  $('[name = "category_name"]').autocomplete(searchCat);
  $('[name = "subcategory_name"]').autocomplete(searchSubCat);
  if ($('[name="subcategory_name"]').length > 0) {
    $('[name="subcategory_name"]').html("");
  }
}

function getNonDisSubCategoriesAuto() {
  $('[name = "category_name"]').autocomplete(searchCat);
  $('[name = "non_dis_subcategory_name"]').autocomplete(searchNonDisSubCat);
  if ($('[name="non_dis_subcategory_name"]').length > 0) {
    $('[name="non_dis_subcategory_name"]').html("");
  }
}

function updatePremiumUrlCompleted(payment_id) {
  $.ajax({
    type: "get",
    url: "/admin/updatePremiumUrlCompleted",
    data: {
      payment_id,
      premium_url: $(`#url_value_${payment_id}`).val(),
    },
    success: function (response) {
      $(`.before_premium_${payment_id}`).html("Completed");
      $(`#url_${payment_id}`).html(
        `<a href="${response.premium_url}">${response.premium_url}</a>`
      );
    },
    error: function (a, b, c) {
      alert(a.responseText);
    },
  });
}

function deletePost(post_id, button) {
  $(button).html("Please wait..");
  $(button).prop("disabled", true);
  $.ajax({
    type: "get",
    url: "/admin/delete_post",
    data: { post_id },
    success: function (response) {
      $(button).parents("tr").remove();
    },
    error: function (a, b, c) {
      $(button).prop("disabled", false);
      alert(b);
    },
  });
}

function deleteReport(post_id, report_id, button) {
  $(button).html("Please wait..");
  $(button).prop("disabled", true);
  $.ajax({
    type: "get",
    url: "/admin/delete_report",
    data: { post_id, report_id },
    success: function (response) {
      $(button).parents("tr").remove();
    },
    error: function (a, b, c) {
      $(button).prop("disabled", false);
      alert(b);
    },
  });
}
