var Pagination = {

    code: '',

    // --------------------
    // Utility
    // --------------------

    // converting initialize data
    Extend: function (data) {
        data = data || {};
        Pagination.size = data.size || 300;
        Pagination.page = data.page || 1;
        Pagination.step = data.step || 3;
    },

    // add pages by number (from [s] to [f])
    Add: function (s, f) {
        for (var i = s; i < f; i++) {
            Pagination.code += '<li class="waves-effect"><a href="javascript:void(0)">' + i + '</a></li>';
        }
    },

    // add last page with separator
    Last: function () {
        Pagination.code += '<i>...</i><li class="waves-effect"><a href="javascript:void(0)">' + Pagination.size + '</a></li>';
    },

    // add first page with separator
    First: function () {
        Pagination.code += '<li class="waves-effect"><a href="javascript:void(0)">1</a></li><i>...</i>';
    },



    // --------------------
    // Handlers
    // --------------------

    // change page
    Click: function () {
        Pagination.page = +this.innerHTML;
        Pagination.Start();
    },

    // previous page
    Prev: function () {
        Pagination.page--;
        if (Pagination.page < 1) {
            Pagination.page = 1;
        }
        Pagination.Start();
    },

    // next page
    Next: function () {
        Pagination.page++;
        if (Pagination.page > Pagination.size) {
            Pagination.page = Pagination.size;
        }
        Pagination.Start();
    },



    // --------------------
    // Script
    // --------------------

    // binding pages
    Bind: function () {
        var a = Pagination.e[0].getElementsByTagName('a');
        for (var i = 0; i < a.length; i++) {
            if (+a[i].innerHTML === Pagination.page) $(a[i]).parent("li").addClass('active');
            a[i].addEventListener('click', Pagination.Click, false);
        }
        var b = Pagination.e[1].getElementsByTagName('a');
        for (var i = 0; i < b.length; i++) {
            if (+b[i].innerHTML === Pagination.page) $(b[i]).parent("li").addClass('active');
            b[i].addEventListener('click', Pagination.Click, false);
        }
    },

    // write pagination
    Finish: function () {
        Pagination.e[0].innerHTML = Pagination.code;
        Pagination.e[1].innerHTML = Pagination.code;
        Pagination.code = '';
        Pagination.Bind();
    },

    // find pagination type
    Start: function () {
        getPage( (Pagination.page - 1) * companiesToShow);
        if (Pagination.size < Pagination.step * 2 + 6) {
            Pagination.Add(1, Pagination.size + 1);
        } else if (Pagination.page < Pagination.step * 2 + 1) {
            Pagination.Add(1, Pagination.step * 2 + 4);
            Pagination.Last();
        } else if (Pagination.page > Pagination.size - Pagination.step * 2) {
            Pagination.First();
            Pagination.Add(Pagination.size - Pagination.step * 2 - 2, Pagination.size + 1);
        } else {
            Pagination.First();
            Pagination.Add(Pagination.page - Pagination.step, Pagination.page + Pagination.step + 1);
            Pagination.Last();
        }
        Pagination.Finish();
    },



    // --------------------
    // Initialization
    // --------------------

    // binding buttons
    Buttons: function (e) {
        var nav = e.getElementsByTagName('a');
        nav[0].addEventListener('click', Pagination.Prev, false);
        nav[1].addEventListener('click', Pagination.Next, false);
    },

    // create skeleton
    Create: function (e) {

        var html = [
            '<li class="waves-effect left-pagination"><a href="javascript:void(0)"><i class="material-icons">chevron_left</i></a></li>', // previous button
            '<span></span>', // pagination container
            '<li class="waves-effect right-pagination"><a href="javascript:void(0)"><i class="material-icons">chevron_right</i></a></li>' // next button
        ];
        Pagination.e = [];
        e[0].innerHTML = html.join('');
        Pagination.e[0] = e[0].getElementsByTagName('span')[0];
        Pagination.Buttons(e[0]);
        e[1].innerHTML = html.join('');
        Pagination.e[1] = e[1].getElementsByTagName('span')[0];       
        Pagination.Buttons(e[1]);
        
    },

    // init
    Init: function (e, data) {        
        Pagination.Extend(data);
        Pagination.Create(e);
        Pagination.Start();
    }
};
