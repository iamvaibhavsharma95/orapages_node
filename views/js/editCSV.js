const updateRewrite = (name) => name.replace(/[^a-z0-9A-z]/g, "-").replace(/-+$/, "").replace(/-+/g, "-").toLowerCase()

const termRewrite = (nm) => nm.toLowerCase().trim().replace(/\ /g, "-")


var aLocation = '';
var termTemplate = "<span class='ui-autocomplete-term'>%s</span>";
var locationJson = {
    search: function () {
        $('.working').css("display", "block");
    },
    open: function (e, ui) {
        $('.working').css("display", "none");
        var
            acData = $(this).data('uiAutocomplete');
        acData
            .menu
            .element
            .find('li.ui-menu-item .ui-menu-item-wrapper')
            .each(function () {
                var me = $(this);
                var regex = new RegExp(acData.term, "gi");
                me.html(me.text().replace(regex, function (matched) {
                    return termTemplate.replace('%s', matched);
                }));
            });

    },
    source: function (req, res) {
        $.ajax({
            type: "get",
            url: "/locationsform",
            data: {
                term: req.term
            },
            success: function (response) {
                var filteredArray = $.map(response, function (item) {
                    if (typeof item != 'undefined') {
                        return item;
                    }
                });
                res(filteredArray);
            }
        });
    },
    minLength: 2,
    delay: 400,
    select: function (event, ui) {
        if (ui.item.key == '') {
            return;
        }
        $element = $(this).parent().parent().find('select');

        if (!$element.find(`option[value='` + ui.item.key + `']`).length) {
            $element.append(`<option value='` + ui.item.key + `' selected>` + ui.item.value + `</option>`)
        } else {
            $element.find(`option[value='` + ui.item.key + `']`).prop("selected", "true");
            $element.select2({
                theme: 'bootstrap',
                placeholder: "Please select categories"
            });
        }

        $(this).parent().parent().parent('.company_row').addClass('edited');
        $(this).parent().parent().parent('.company_row').removeClass('updated');
        setTimeout(() => {
            $('.locationauto').val('');
        }, 3);
        return;
    }
}


var autoJson = {
    search: function () {
        $('.working').css("display", "block");
    },
    open: function (e, ui) {
        $('.working').css("display", "none");
        var
            acData = $(this).data('uiAutocomplete');
        acData
            .menu
            .element
            .find('li.ui-menu-item .ui-menu-item-wrapper')
            .each(function () {
                var me = $(this);
                var regex = new RegExp(acData.term, "gi");
                me.html(me.text().replace(regex, function (matched) {
                    return termTemplate.replace('%s', matched);
                }));
            });

    },
    source: function (req, res) {
        $.ajax({
            type: "get",
            url: "/categoryName",
            data: {
                term: req.term
            },
            success: function (response) {
                res(response);
            }
        });
    },
    minLength: 2,
    delay: 400,
    select: function (event, ui) {
        if (ui.item.key == '') {
            return;
        }
        $element = $(this).parent().parent().find('select');
        // console.log(!$element.find(`option[value='`+ui.item.value+`']`));
        // console.log(ui.item.key);
        // console.log(ui.item.value);

        if (!$element.find(`option[value='` + ui.item.key + `']`).length) {
            $element.append(`<option value='` + ui.item.key + `' selected>` + ui.item.value + `</option>`)
        } else {
            $element.find(`option[value='` + ui.item.key + `']`).prop("selected", "true");
            $element.select2({
                theme: 'bootstrap',
                placeholder: "Please select categories"
            });
        }

        $(this).parent().parent().parent('.company_row').addClass('edited');
        $(this).parent().parent().parent('.company_row').removeClass('updated');
        setTimeout(() => {
            $('.categoriesauto').val('');
        }, 3);
        return;
    }
}


var categoryIdFinder = {
    search: function () {
        $('.working').css("display", "block");
    },
    open: function (e, ui) {
        $('.working').css("display", "none");
        var
            acData = $(this).data('uiAutocomplete');
        acData
            .menu
            .element
            .find('li.ui-menu-item .ui-menu-item-wrapper')
            .each(function () {
                var me = $(this);
                var regex = new RegExp(acData.term, "gi");
                me.html(me.text().replace(regex, function (matched) {
                    return termTemplate.replace('%s', matched);
                }));
            });

    },
    source: function (req, res) {
        $.ajax({
            type: "get",
            url: "/categoryName",
            data: {
                term: req.term
            },
            success: function (response) {
                res(response);
            }
        });
    },
    minLength: 2,
    delay: 400,
    select: function (event, ui) {
        if (ui.item.key == '') {
            return;
        }
        $("[name = 'category_id']").val(ui.item.key);
        $(`<tr>
            <td>${ui.item.value}</td>
            <td><input type="text" id="category_${ui.item.key}" value="${ui.item.key}"></td>
            <td><button class="btn btn-primary btn-sm" onclick="copyToClipboard('#category_${ui.item.key}')">Copy</button></td>
            <td><button class="btn btn-danger btn-sm" onclick="$(this).parents('tr').remove()">Delete</button></td>
        </tr>`).insertAfter("#category_copy");
    }
}


var locationIdFinder = {
    search: function () {
        $('.working').css("display", "block");
    },
    open: function (e, ui) {
        $('.working').css("display", "none");
        var
            acData = $(this).data('uiAutocomplete');
        acData
            .menu
            .element
            .find('li.ui-menu-item .ui-menu-item-wrapper')
            .each(function () {
                var me = $(this);
                var regex = new RegExp(acData.term, "gi");
                me.html(me.text().replace(regex, function (matched) {
                    return termTemplate.replace('%s', matched);
                }));
            });

    },
    source: function (req, res) {
        $.ajax({
            type: "get",
            url: "/locationsform",
            data: {
                term: req.term
            },
            success: function (response) {
                var filteredArray = $.map(response, function (item) {
                    if (typeof item != 'undefined') {
                        return item;
                    }
                });
                res(filteredArray);
            }
        });
    },
    minLength: 2,
    delay: 400,
    select: function (event, ui) {
        if (ui.item.key == '') {
            return;
        }
        $("[name = 'city_id']").val(ui.item.key);
    }
}




function getTempCompanies($csvId, $type = "admin") {
    var companyNames = {};
    var companyWebsites = {};
    var companyValue = {};
    var count = 0;
    $.ajax({
        type: "get",
        url: "/admin/get_temp_companies",
        data: {
            csvId: $csvId,
            type: $type
        },
        success: function (response) {
            responseData = JSON.parse(response.company_data);
            if ($type == "admin") {
                for (const company_id in responseData) {
                    if (responseData.hasOwnProperty(company_id)) {
                        const company = responseData[company_id];
                        var firstLocation = Object.values(company.locations)[0];

                        if (count > 0) {
                            $("#tempCompanies").append(`<tr id="` + company_id + `_row" class="company_row">
                                                            <td id="` + company_id + `_name">
                                                                <input type='hidden' class='key' value='` + company_id + `'>
                                                                <div class="form-group">
                                                                    <textarea class="form-control company_name" rows="3"  onchange='makeEdited("` + company_id + `")' required style="height:90px">` + company.company_name + `</textarea>
                                                                </div>
                                                                <div class='` + company_id + `_del_dup remove'><a href="#"  onclick="deleteTuple('` + company_id + `','` + $csvId + `')" class="db-list-edit">Remove</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <textarea class="form-control company_full_address" rows="3"   onchange='makeEdited("` + company_id + `")' required style="width:200px;">` + firstLocation.complete_address + `</textarea>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control locationauto" style="font-size:12px;width:200px;height:20px;margin-top:10px">
                                                                </div>
                                                                <div class="form-group">
                                                                    <select class="select_two cities_select" rows="3"  onchange='makeEdited("` + company_id + `")' required multiple style="width:200px" id="` + company_id + `_cities" name="cities[][]">
                                                                        
                                                                    </select>
                                                                </div>
                                                                <div style="display:table-cell" class='btn btn-sm fa fa-chevron-up' onclick='upperCities("` + company_id + `")'>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <textarea class="form-control company_number" rows="3"  onchange='makeEdited("` + company_id + `")' >` + firstLocation.mobile + `</textarea>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <textarea class="form-control company_fax" rows="3"  onchange='makeEdited("` + company_id + `")'  style="width:50px">` + firstLocation.fax + `</textarea>
                                                                </div>
                                                            </td>
                                                            <td id="` + company_id + `_email">
                                                                <div class="form-group">
                                                                    <input type="email" class="form-control company_email" value="` + company.company_email + `"  onchange='makeEdited("` + company_id + `")' required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control company_website" value="` + firstLocation.website + `"  onchange='makeEdited("` + company_id + `")'>
                                                                </div>
                                                                <div class="form-group">
                                                                    <a href="` + firstLocation.website + `" target="_blank" style="display:table-cell" class='fa fa-external-link'>
                                                                    </a>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <textarea class="form-control company_profile" rows="3"  onchange='makeEdited("` + company_id + `")' required style="width:200px;">` + company.company_business_details + `</textarea>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control categoriesauto" style="font-size:12px;width:200px;height:20px;margin-top:10px">
                                                                </div>
                                                                <div class="form-group">
                                                                    <select class="select_two_cat cats_select" rows="3" onchange='makeEdited("` + company_id + `")' multiple style="width:200px" id="` + company_id + `_cats" required name="cats[][]">
                                                                        
                                                                    </select>
                                                                </div>
                                                                <div style="display:table-cell" class='btn btn-sm fa fa-chevron-up' onclick='upperCats("` + company_id + `")'>
                                                                </div>
                                                            </td>
                                                            <!-- <td><a href="/tempPreview?key=` + company_id + `&csvName=` + $csvId + `" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td> -->
                                                        
                                                    </tr>`);
                            previousKey = company_id;
                        } else {
                            previousKey = company_id;
                            $("#tempCompanies").append(`<tr id="` + company_id + `_row" class="company_row">
                                                            <td id="` + company_id + `_name">
                                                                <input type='hidden' class='key' value='` + company_id + `'>
                                                                <div class="form-group">
                                                                    <textarea class="form-control company_name" rows="3"  onchange='makeEdited("` + company_id + `")' required style="height:90px">` + company.company_name + `</textarea>
                                                                </div>
                                                                <div class='` + company_id + `_del_dup remove'><a href="#"  onclick="deleteTuple('` + company_id + `','` + $csvId + `')" class="db-list-edit">Remove</a></div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <textarea class="form-control company_full_address" rows="3"   onchange='makeEdited("` + company_id + `")' required style="width:200px;">` + firstLocation.complete_address + `</textarea>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control locationauto" style="font-size:12px;width:200px;height:20px;margin-top:10px">
                                                                </div>
                                                                <div class="form-group">
                                                                    <select class="select_two cities_select" rows="3"  onchange='makeEdited("` + company_id + `")' multiple required style="width:200px" id="` + company_id + `_cities" name="cities[][]">
                                                                        
                                                                    </select>
                                                                </div>
                                                                
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <textarea class="form-control company_number" rows="3"  onchange='makeEdited("` + company_id + `")'>` + firstLocation.mobile + `</textarea>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <textarea class="form-control company_fax" rows="3"  onchange='makeEdited("` + company_id + `")'  style="width:50px">` + firstLocation.fax + `</textarea>
                                                                </div>
                                                            </td>
                                                            <td id="` + company_id + `_email">
                                                                <div class="form-group">
                                                                    <input type="email" class="form-control company_email" value="` + company.company_email + `"  onchange='makeEdited("` + company_id + `")' required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control company_website" value="` + firstLocation.website + `"  onchange='makeEdited("` + company_id + `")' >
                                                                </div>
                                                                <div class="form-group">
                                                                    <a href="` + firstLocation.website + `" target="_blank" style="display:table-cell" class='fa fa-external-link danger'>
                                                                    </a>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <textarea class="form-control company_profile" rows="3" onchange='makeEdited("` + company_id + `")' required style="width:200px;">` + company.company_business_details + `</textarea>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control categoriesauto" style="font-size:12px;width:200px;height:20px;margin-top:10px">
                                                                </div>
                                                                <div class="form-group">
                                                                    <select class="select_two_cat cats_select" rows="3" placeholder='Please select categories' onchange='makeEdited("` + company_id + `")' multiple style="width:200px" id="` + company_id + `_cats" required name="cats[][]" readonly>
                                                                        
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <!-- <td class='` + company_id + `_del_dup'><a href="#"  onclick="deleteTuple('` + company_id + `','` + $csvId + `')" class="db-list-edit">Remove</a></td> -->
                                                            <!-- <td><a href="/tempPreview?key=` + company_id + `&csvName=` + $csvId + `" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td> -->
                                                        
                                                    </tr>`);
                        }

                        count++;

                        if (company.updated === "true") {
                            $(".company_row").addClass('updated');
                        }

                        if (validateEmail(company.company_email)) {
                            $("#" + company_id + "_email").css("background", "#c38b5f");
                            $("#" + company_id + "_row").css("border", "5px solid red");
                        }

                        if (!!company.categories) {
                            var categories = Object.keys(company.categories)
                            categories.forEach(category => {
                                $.ajax({
                                    type: "get",
                                    url: "/admin/get_category_id",
                                    data: {
                                        category
                                    },
                                    success: function (response) {
                                        $('#' + company_id + "_cats").append(`<option value='` + response.category_id + `' selected>` + response.category_name + `</option>`);
                                        $(`#` + company_id + `_cats`).select2({
                                            theme: 'bootstrap',
                                            minimumInputLength: 5000,
                                        });
                                    },
                                    error: function (a, b, c) {
                                        $(`#` + company_id + `_cats`).select2({
                                            theme: 'bootstrap',
                                            minimumInputLength: 5000,
                                        });
                                    }
                                });
                            });
                        } else {
                            $(`#` + company_id + `_cats`).select2({
                                theme: 'bootstrap',
                                minimumInputLength: 5000,
                            });
                        }

                        $.ajax({
                            type: "get",
                            url: "/admin/validate_name_url",
                            data: {
                                url: company.company_name_short
                            },
                            success: function (response) {
                                if (response.status) {
                                    $("#" + company_id + "_name").css("background", "red");
                                    $("#" + company_id + "_row").addClass('upload_error');
                                    $("." + company_id + "_del_dup").append(`<a href="/` + response.company_url + `" target='_blank' class="db-list-edit" style="display:block;margin-top:10px">Duplicate</a>`)
                                    $("." + company_id + "_del_dup").append(`<a href="/edit_company_admin?companyKey=` + response.company_id + `" target='_blank' class="db-list-edit" style="display:block;margin-top:10px">Edit first company</a>`)
                                }
                            },
                            error: function (a, b, c) {
                                $("#" + company_id + "_name").css("background", "red");
                                $("#" + company_id + "_row").addClass('upload_error');
                                // $("." + company.key + "_del_dup").append(`<a href="/` + response.company_url + `" target='_blank' class="db-list-edit" style="display:block;margin-top:10px">Duplicate</a>`)
                                // $("." + company.key + "_del_dup").append(`<a href="/edit_company_admin?companyKey=` + response.company_id + `" target='_blank' class="db-list-edit" style="display:block;margin-top:10px">Edit first company</a>`)
                            }
                        });

                        $.ajax({
                            type: "get",
                            url: "/check_email",
                            data: {
                                company_email: company.company_email
                            },
                            success: function (response) {
                                if (response === '""') {
                                    $("#" + company_id + "_email").css("background", "red");
                                    $("#" + company_id + "_row").addClass('upload_error');
                                }
                            },
                            error: function (a, b, c) {
                                $("#" + company_id + "_name").css("background", "red");
                                $("#" + company_id + "_row").addClass('upload_error');

                            }
                        });

                        if (!!company.locations) {
                            var locations = Object.keys(company.locations);
                            locations.forEach(location => {
                                if (!!company.locations[location].location_id) {
                                    var city = company.locations[location].location_id.city_id;
                                    var state = company.locations[location].location_id.state_id;
                                    var country = company.locations[location].location_id.country_id;
                                    $('#' + company_id + "_cities").append(`<option value='` + city + `,` + state + `,` + country + `' selected>` + company.locations[location].address + `</option>`);
                                }
                            });
                            $("#" + company_id + "_cities").select2({
                                // placeholder: "Please select cities",
                                theme: 'bootstrap',
                                minimumInputLength: 5000
                            });
                        }

                        var i = Object.values(companyNames).indexOf(company.company_name_short);

                        if (Object.values(companyNames).indexOf(company.company_name_short) >= 0) {
                            $("#" + company_id + "_name").css("background", "grey");
                            $("#" + company_id + "_row").addClass('upload_error');
                            $("#" + Object.keys(companyNames)[i] + "_name").css("background", "grey");
                            $("#" + Object.keys(companyNames)[i] + "_row").css("border", "2px solid red");
                        }

                        // if (Object.values(companyWebsites).indexof(company.company_name_short) > 0) {
                        //     $("#" + company_id + "_row").css("background", "grey");
                        // }

                        // if (Object.values(companyValue).indexof(JSON.stringify(company.val())) > 0) {
                        //     $("#" + company_id + "_row").css("background", "grey");
                        // }

                        companyNames[company_id] = company.company_name_short;
                        companyWebsites[company_id] = company.company_website;
                        companyValue[company_id] = JSON.stringify(company);
                    }
                }
            } else if ($type == "auto_bulk") {
                for (const company_id in responseData) {
                    if (responseData.hasOwnProperty(company_id)) {
                        const company = responseData[company_id];
                        var firstLocation = Object.values(company.locations)[0];
                        let categories = Object.values(company.categories);
                        let textCat = categories.map(category => category.category_name);
                        $("#tempCompanies").append(`<tr id="` + company_id + `_row" class="company_row">
                                                            <td id="` + company_id + `_name">
                                                                ${company.company_name}                                                                
                                                            </td>
                                                            <td>
                                                                ${firstLocation.complete_address}
                                                            </td>
                                                            <td>
                                                                ${firstLocation.address}                                                                
                                                            </td>
                                                            <td>
                                                                ${firstLocation.mobile}
                                                            </td>
                                                            <td>
                                                                ${firstLocation.fax}
                                                            </td>
                                                            <td id="` + company_id + `_email">
                                                                <div class="form-group">
                                                                    ${company.company_email}
                                                                </div>
                                                                <div class="form-group">
                                                                    ${firstLocation.website}
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    ${company.company_business_details}
                                                                </div>
                                                            </td>
                                                            <td>
                                                                ${textCat.join(", ")}
                                                            </td>
                                                    </tr>`);


                        var i = Object.values(companyNames).indexOf(company.company_name_short);

                        if (Object.values(companyNames).indexOf(company.company_name_short) >= 0) {
                            $("#" + company_id + "_name").css("background", "grey");
                            $("#" + company_id + "_row").addClass('upload_error');
                            $("#" + Object.keys(companyNames)[i] + "_name").css("background", "grey");
                            $("#" + Object.keys(companyNames)[i] + "_row").css("border", "2px solid red");
                        }

                        $.ajax({
                            type: "get",
                            url: "/admin/validate_name_url",
                            data: {
                                url: company.company_name_short
                            },
                            success: function (response) {
                                if (response.status) {
                                    $("#" + company_id + "_name").css("background", "red");
                                    $("#" + company_id + "_row").addClass('upload_error');
                                    $("." + company_id + "_del_dup").append(`<a href="/` + response.company_url + `" target='_blank' class="db-list-edit" style="display:block;margin-top:10px">Duplicate</a>`)
                                    $("." + company_id + "_del_dup").append(`<a href="/edit_company_admin?companyKey=` + response.company_id + `" target='_blank' class="db-list-edit" style="display:block;margin-top:10px">Edit first company</a>`)
                                }
                            },
                            error: function (a, b, c) {
                                $("#" + company_id + "_name").css("background", "red");
                                $("#" + company_id + "_row").addClass('upload_error');
                                // $("." + company.key + "_del_dup").append(`<a href="/` + response.company_url + `" target='_blank' class="db-list-edit" style="display:block;margin-top:10px">Duplicate</a>`)
                                // $("." + company.key + "_del_dup").append(`<a href="/edit_company_admin?companyKey=` + response.company_id + `" target='_blank' class="db-list-edit" style="display:block;margin-top:10px">Edit first company</a>`)
                            }
                        });

                        companyNames[company_id] = company.company_name_short;

                    }
                }
            }

        }
    });


    // });
}










function upperCities($key) {
    var cities = $("#" + $key + "_row").prev('.company_row').find('.cities_select').val();
    // console.log($previousKey);
    $(`#` + $key + `_cities`).html('');
    for (let index = 0; index < cities.length; index++) {
        const element = cities[index];
        $(`#` + $key + `_cities`).append(`<option value='` + element + `'>` + $("#" + $key + "_row").prev('.company_row').find(`.cities_select option[value='` + element + `']`).text() + `</option>`);
        $(`#` + $key + `_cities option[value='` + element + `']`).prop('selected', true);
    };

    $("#" + $key + "_row").addClass("edited");
    // $("#" + $key + "_cities").select2({
    //     theme: 'bootstrap'
    // });
}

function upperCats($key) {
    var cats = $("#" + $key + "_row").prev('.company_row').find('.cats_select').val();
    $(`#` + $key + `_cats`).html('');
    for (let index = 0; index < cats.length; index++) {
        const element = cats[index];
        $(`#` + $key + `_cats`).append(`<option value='` + element + `'>` + $("#" + $key + "_row").prev('.company_row').find(`.cats_select option[value='` + element + `']`).text() + `</option>`);
        $(`#` + $key + `_cats option[value='` + element + `']`).prop('selected', true);
    }

    $("#" + $key + "_row").addClass("edited");

    // $(`#` + $key + `_cats`).select2({
    //     theme: 'bootstrap',
    //     placeholder: "Please select categories",
    //     ajax: {
    //         url: "/getcategoriesAdmin",
    //         dataType: "json"
    //     }
    // });
}

function makeEdited($key) {
    $("#" + $key + "_row").addClass("edited");
    $("#" + $key + "_row").removeClass("updated");
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return !re.test(email);
}

function deleteTuple($key, $csv) {
    swal({
            title: "Are you sure?",
            text: "You will not be able to recover this CSV!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $("#" + $key + "_row").remove();
                // uploadTempCSV($csv,{});
                swal("Deleted", "Row has been removed", "success");
            } else {
                swal("Cancelled", "Row is not removed :)", "error");
            }
        });
}

function csvDelete($key, $type) {
    swal({
            title: "Are you sure?",
            text: "You will not be able to recover this CSV!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "post",
                    url: "/admin/delete_csv",
                    data: {
                        csv: $key,
                        type: $type
                    },
                    success: function (response) {
                        $("#" + $key).remove();
                        swal("Deleted", "CSV has been removed", "success");
                    },
                    error: function (a, b, c) {
                        swal("Failed", "CSV is not removed", "warning");
                    }
                });
            } else {
                swal("Cancelled", "CSV is not removed :)", "error");
            }
        });
}


function toEmpty($value) {
    if (typeof $value == 'undefined')
        return "";
    else
        return $value;
}

function makeid(length) {
    let result = '';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


function updateTempCompanies($csv, $url = null) {
    let tempCompanies = {};
    if ($('.edited').length > 0) {
        if (!$url) {
            uploadTempCSV($csv, tempCompanies);
            return;
        }
        swal({
                title: "Are you sure?",
                text: "You are updating the CSV!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, update it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {

                    uploadTempCSV($csv, tempCompanies, $url);

                } else {

                    swal("Cancelled", "CSV is not updated :)", "error");

                }
            });
    } else {
        swal("No updation", "No updation found ", "error");
    }
}



function uploadTempCSV($csv, tempCompanies, url = null) {
    var v = $(".company_row");
    for (let index = 0; index < v.length; index++) {
        $single = $(v[index]);
        var key = $single.find('.key').val();
        let updated = false;
        if ($(v[index]).hasClass('edited') || $(v[index]).hasClass('updated')) {
            updated = true;
        }
        tempCompanies[key] = {
            "company_business_details": toEmpty($single.find(".company_profile").val()),
            "company_business_name": toEmpty($single.find(".company_name").val()),
            "company_email": toEmpty($single.find(".company_email").val()),
            "company_website": toEmpty($single.find(".company_website").val()),
            "company_name": toEmpty($single.find(".company_name").val()),
            "company_name_short": updateRewrite(toEmpty($single.find(".company_name").val())),
            "company_url": encodeURI(updateRewrite(toEmpty($single.find(".company_name").val()))),
            "updated": updated
        }
        var locations = $single.find(".cities_select").val();
        tempCompanies[key]["locations"] = {};
        if (locations.length > 0) {
            locations.forEach(location => {
                tempCompanies[key]["locations"][makeid(20)] = {
                    address: $single.find(`.cities_select option[value='` + location + `']`).text(),
                    complete_address: toEmpty($single.find('.company_full_address').val()),
                    email: toEmpty($single.find('.company_email').val()),
                    fax: toEmpty($single.find('.company_fax').val()),
                    mobile: toEmpty($single.find('.company_number').val()),
                    website: toEmpty($single.find('.company_website').val()),
                    location_id: {
                        city_id: location.split(",")[0],
                        state_id: location.split(",")[1],
                        country_id: location.split(",")[2],
                    }
                }
            });
        } else {
            tempCompanies[key]["locations"][makeid(20)] = {
                address: "",
                complete_address: toEmpty($single.find('.company_full_address').val()),
                email: toEmpty($single.find('.company_email').val()),
                fax: toEmpty($single.find('.company_fax').val()),
                mobile: toEmpty($single.find('.company_number').val()),
                website: toEmpty($single.find('.company_website').val()),
            }
        }
        var cats = $single.find(".cats_select").val();
        tempCompanies[key]["categories"] = {};
        if (cats.length > 0) {
            cats.forEach(cat => {
                tempCompanies[key]["categories"][cat] = {
                    category_des: toEmpty($single.find(".company_profile").val()),
                    category_id: cat,
                    category_name: $single.find(`.cats_select option[value='` + cat + `']`).text(),
                }
            });
        }
    }


    $.ajax({
        type: "post",
        url: "/admin/update_csv",
        data: {
            csv: $csv,
            company_data: tempCompanies
        },
        success: function (response) {
            swal("Updated", "CSV has been updated", "success");
            if (!url) {
                window.location.reload();
            } else {
                window.location.href = url;
            }
        },
        error: function (a, b, c) {
            alert(a.responseText);
        }
    });
}


function uploadCompanies($csv, $ip, $uploaded_by) {
    if ($('.edited').length > 0) {
        swal("Not Updated", "Please save update !", "error");
    } else if ($('.upload_error').length > 0 && $uploaded_by == 'admin') {
        swal("Has error", "Please remove error and save update !", "error");
    } else {
        swal({ 
                title: "Are you sure?",
                text: "You are Uploading the CSV!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, upload it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "post",
                        url: "/admin/transfer_companies",
                        data: {
                            csv: $csv,
                            ip: $ip,
                            uploaded_by: $uploaded_by
                        },
                        success: function (response) {
                            swal("Uploaded", "CSV has been uploaded", "success");
                            window.location.reload();
                        },
                        error: function (a, b, c) {
                            swal("Not Uploaded", "CSV is not uploaded", "error");
                            // window.location.reload();
                        }
                    });
                } else {
                    swal("Cancelled", "CSV is not uploaded :)", "error");
                }
            });
    }
}