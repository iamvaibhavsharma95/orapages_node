let pageViews = document.getElementById('pageViews').getContext('2d');
let pageViewsChart = new Chart(pageViews, {
    type: 'line',
    data: {
        // labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: 'Page views',
            // data: [12, 19, 3, 5, 2, 3],
            backgroundColor: '#dc35458f',
            borderColor: 'orange',
            fill: false,
            // fillColor:'#000',
            borderWidth: 3
        }]
    },
    options: {
        responsive: true,
        // title: {
        //     display: true,
        //     text: 'Chart.js Line Chart'
        // },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Dates'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Number of Views'
                }
            }]
        }
    }
});



let pageCity = document.getElementById('pageCity').getContext('2d');
let pageCityChart = new Chart(pageCity, {
    type: 'bar',
    data: {
        // labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: 'Cities and Counties',
            // data: [12, 19, 3, 5, 2, 3],
            // backgroundColor: '#dc35458f',
            // borderColor: '#dc3545',
            fill: true,
            fillColor: '#000',
            borderWidth: 2
        }]
    },
    options: {
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Cities'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Number of views'
                }
            }]
        }
    }
});


let pageCountry = document.getElementById('pageCountry').getContext('2d');
let pageCountryChart = new Chart(pageCountry, {
    type: 'bar',
    data: {
        datasets: [{
            label: 'Number of Views',
            fill: true,
            fillColor: '#000',
            borderWidth: 2
        }]
    },
    options: {
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Countries'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Number of views'
                }
            }]
        }
    }
});

let pageUsers = document.getElementById('pageUsers').getContext('2d');
let pageUsersChart = new Chart(pageUsers, {
    type: 'line',
    data: {
        // labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: 'Users',
            // data: [12, 19, 3, 5, 2, 3],
            backgroundColor: '#dc35458f',
            borderColor: 'green',
            fill: false,
            fillColor: '#000',
            borderWidth: 3
        }]
    },
    options: {
        responsive: true,
        // title: {
        //     display: true,
        //     text: 'Chart.js Line Chart'
        // },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Dates'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Number of Users'
                },

            }]
        }
    }
});



const getPageViews = (days = null) => {
    $.ajax({
        type: "get",
        url: "/admin/getPageViewsGA",
        data: {
            company_url,
            days
        },
        success: function (response) {
            pageViewsChart.data.labels = response.labels;
            pageViewsChart.data.datasets[0].data = response.data;
            pageViewsChart.update();
        }
    });
}


const getUsers = (days = null) => {
    $.ajax({
        type: "get",
        url: "/admin/getUsersGA",
        data: {
            company_url,
            days
        },
        success: function (response) {
            pageUsersChart.data.labels = response.labels;
            pageUsersChart.data.datasets[0].data = response.data;
            pageUsersChart.update();
        }
    });
}


const getCityUsers = (days = null) => {
    $.ajax({
        type: "get",
        url: "/admin/getCountryCityGA",
        data: {
            company_url,
            days
        },
        success: function (response) {
            pageCityChart.data.labels = response.labels;
            pageCityChart.data.datasets[0].data = response.data;
            pageCityChart.data.datasets[0].backgroundColor = response.backgroundColor;
            pageCityChart.update();
        }
    });
}

let map;
const getCountryUsers = (days = null) => {
    
    $.ajax({
        type: "get",
        url: "/index/getCountryGA",
        data: {
            company_url,
            days
        },
        success: function (response) {
            pageCountryChart.data.labels = response.labels;
            pageCountryChart.data.datasets[0].data = response.data;
            pageCountryChart.data.datasets[0].backgroundColor = response.backgroundColor;
            pageCountryChart.update();
            $('#vmap').replaceWith("<div id='vmap' style='height: 400px;'></div>");
            $('#vmap').vectorMap({
                map: 'world_en',
                backgroundColor: '#fff',
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#666666',
                enableZoom: false,
                showTooltip: true,
                values: response.map,
                scaleColors: ['#C8EEFF', '#006491'],
                normalizeFunction: 'polynomial',
                onLabelShow: function (event, label, code) {
                    if (!!response.map[code]) {
                        label.html(label[0].textContent + '<br/> Views:' + response.map[code]);   
                    }
                },
            });
            // $('#vmap').vectorMap('set', 'values', response.map);
            // $(document).ready(function () {
            
            // });
        }
    });
}


$(document).ready(function () {
    getCityUsers();
    getPageViews();
    getUsers();
    getCountryUsers();
    
});












/** const getPageSessions = (days = null) => {
    $.ajax({
        type: "get",
        url: "/admin/getPageSessionsGA",
        data: {
            company_url,
            days
        },
        success: function (response) {
            pageSessionChart.data.labels = response.labels;
            pageSessionChart.data.datasets[0].data = response.data;
            pageSessionChart.update();
        }
    });
}


const getBounceRate = (days = null) => {
    $.ajax({
        type: "get",
        url: "/admin/getBounceRateGA",
        data: {
            company_url,
            days
        },
        success: function (response) {
            pageBounceRateChart.data.labels = response.labels;
            pageBounceRateChart.data.datasets[0].data = response.data;
            pageBounceRateChart.update();
        }
    });
}


let pageSession = document.getElementById('pageSession').getContext('2d');
let pageSessionChart = new Chart(pageSession, {
    type: 'line',
    data: {
        // labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: 'Page Session',
            // data: [12, 19, 3, 5, 2, 3],
            backgroundColor: '#dc35458f',
            borderColor: 'blue',
            fill: false,
            fillColor: '#000',
            borderWidth: 3
        }]
    },
    options: {
        responsive: true,
        // title: {
        //     display: true,
        //     text: 'Chart.js Line Chart'
        // },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Dates'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Number of Sessions'
                }
            }]
        }
    }
});


let pageBounceRate = document.getElementById('pageBounceRate').getContext('2d');
let pageBounceRateChart = new Chart(pageBounceRate, {
    type: 'line',
    data: {
        datasets: [{
            label: 'Bounce Rate',
            backgroundColor: '#dc35458f',
            borderColor: 'purple',
            fill: false,
            fillColor: '#000',
            borderWidth: 3
        }]
    },
    options: {
        responsive: true,
        // title: {
        //     display: true,
        //     text: 'Chart.js Line Chart'
        // },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Dates'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Percentage'
                },
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

**/