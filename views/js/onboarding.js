let $city = $("#cityList");
let $state = $("#stateList");
let $country = $("#countryList");

let city_id = null;
let state_id = null;
let country_id = null;

$(document).ready(function () {
  $city.prop("disabled", true);
  $state.prop("disabled", true);

  $country.on("change", function () {
    country_id = $(this).val();
    if (country_id !== "Choose") {
      state_id = null;
      city_id = null;

      $state.prop("disabled", false);
      getStatesSearch(country_id);
      // filterOn();
    } else {
      country_id = null;
      state_id = null;
      city_id = null;

      $state.prop("disabled", true);
      $city.prop("disabled", true);
      getStatesSearch(country_id);
      filterOff();
    }
    $("[name='categoryOnboardingForm']").prop("disabled", false);
  });

  $state.on("change", function () {
    state_id = $(this).val();
    if (state_id.startsWith("all_states")) {
      city_id = null;
      state_id = null;
      // populateSuggestionsWithLocationCategory(
      //   `${city_id},${state_id},${country_id}`,
      //   null
      // );

      $city.prop("disabled", false);
      getCitiesSearch($(this).val(), country_id);
    } else if (state_id !== "Choose") {
      city_id = null;

      $city.prop("disabled", false);

      getCitiesSearch($(this).val(), country_id);
      // populateSuggestionsWithLocationCategory(
      //   `${city_id},${state_id},${country_id}`,
      //   null
      // );
    } else {
      state_id = null;
      city_id = null;
      $city.prop("disabled", true);

      getCitiesSearch($(this).val());
      // populateSuggestionsWithLocationCategory(
      //   `${city_id},${state_id},${country_id}`,
      //   null
      // );
    }
  });

  $city.on("change", function () {
    city_id = $(this).val();
    if (city_id.startsWith("all_cities")) {
      city_id = null;
      populateSuggestionsWithLocationCategory(
        `${city_id},${state_id},${country_id}`,
        null
      );
      filterOn();
    } else if (city_id !== "Choose") {
      filterOn();
      populateSuggestionsWithLocationCategory(
        `${city_id},${state_id},${country_id}`,
        null
      );
    } else {
      city_id = null;

      filterOff();
      populateSuggestionsWithLocationCategory(
        `${city_id},${state_id},${country_id}`,
        null
      );
    }
  });

  const getCountriesSearch = () => {
    $.ajax({
      type: "get",
      url: "/frontEndCountries",
      data: {},
      success: function (res) {
        $country.find(".options").remove();
        // country_id = null;
        for (const r of res) {
          $country.append(
            `<option value='${r.key}' class='options'>${r.value}</option>`
          );
        }

        $country.select2({
          theme: "bootstrap",
        });
        $city.select2({
          theme: "bootstrap",
        });
        $state.select2({
          theme: "bootstrap",
        });

        if (!!country_id) {
          $(`#countryList option[value='${country_id}']`).prop(
            "selected",
            true
          );
          $state.prop("disabled", false);
          getStatesSearch(country_id);
          $country.select2({
            theme: "bootstrap",
          });
        }
      },
    });
  };

  const getStatesSearch = (country) => {
    $.ajax({
      type: "get",
      url: "/frontEndStates",
      data: {
        country: country,
      },
      success: function (response) {
        $state.find(".options").remove();
        $city.find(".options").remove();
        // state_id = null;
        $state.append(
          `<option value='all_states' class='options text-danger'><b>All States</b></option>`
        );
        for (const state of response) {
          $state.append(
            `<option value='${state.key}' class='options'>${state.value}</option>`
          );
        }
        $state.select2({
          theme: "bootstrap",
        });
        // if (response.length == 1) {
        //     getCitiesSearch($state.val());
        // }

        if (!!state_id && state_id != "all_states") {
          $(`#stateList option[value='${state_id}']`).prop("selected", true);
          $city.prop("disabled", false);
          getCitiesSearch(state_id, country);
          $state.select2({
            theme: "bootstrap",
          });
        } else {
          if (state_id == "all_states") {
            $(`#stateList option[value='${state_id}']`).prop("selected", true);
            $city.prop("disabled", false);
            getCitiesSearch(state_id, country_id);
            $state.select2({
              theme: "bootstrap",
            });
          }
        }
      },
    });
  };

  const getCitiesSearch = (state, country = null) => {
    $.ajax({
      type: "get",
      url: "/frontEndCities",
      data: {
        state,
        country,
      },
      success: function (response) {
        $city.find(".options").remove();
        $city.append(
          `<option value='all_cities' class='options text-danger'><b>All Cities</b></option>`
        );
        for (const city of response) {
          $city.append(
            `<option value='${city.key}' class='options'>${city.value}</option>`
          );
        }
        $city.select2({
          theme: "bootstrap",
        });

        if (!!city_id) {
          $(`#cityList option[value='${city_id}']`).prop("selected", true);
          $city.select2({
            theme: "bootstrap",
          });
        }
      },
    });
  };

  const filterOn = () => {
    $("#filter").prop("disabled", false);
    $("#search_box").prop("disabled", false);
    $("#submit_search").prop("disabled", false);
    $(".greyout").css("color", "black");
    $("#filter").css("background", "white");
    $(".btn-browse").prop("disabled", false);
  };

  const filterOff = () => {
    $("#filter").prop("disabled", true);
    $(".greyout").css("color", "#b2b2b2");
    $(".last").css("color", "#a6a6a6");
    $("#filter").css("background", "#dfdfdf");
    $("#submit_search").prop("disabled", true);
    $("#search_box").prop("disabled", true);
    $(".btn-browse").prop("disabled", true);
  };

  getCountriesSearch();
});
