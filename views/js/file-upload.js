$("#images-to-upload").on("change", handleFileSelect);

var globalFiles;
var files_row = [];

function handleFileSelect(e) {
    if (!e.target.files || !window.FileReader) return;
    globalFiles = e.target.files;
    var files = e.target.files;
    var filesArr = Object.keys(files);
    var i = 0;
    filesArr.forEach(function (f) {
        var f = files[f];
        if (!f.type.match("image.*")) {
            return;
        }
        $('#banner-images').html('');
        var reader = new FileReader();
        reader.onload = function (e) {
            var image_array = e.target.result.split(";");
            var image_type = image_array[0].split("/");
            var image_array_2 = image_array[1].split(",");
            files_row[i] = {
                image_type: image_type[1],
                image: image_array_2[1]
            };
            $('#banner-images').append(`<div class="col s12 banner" id="` + i + `"
                                            style="padding: 20px;background:#fff;margin-top:20px;box-shadow: 2px 2px 6px rgb(134, 133, 133)">
                                            <div class="row" style="margin:0px">
                                                <img class="col s2" style='max-width:50%;margin-bottom:20px'
                                                    src="` + e.target.result + `">
                                                <div class="col s10" style="margin-bottom:20px;">
                                                    <div class="col s12 input-field">
                                                        <input type="text" class="validate" name="banner_name_` + i + `" required="true">
                                                        <label>Banner name / Description</label>
                                                    </div>
                                                    <div class="col s12 switch" style="margin-top:20px">
                                                        <label> Deactivate
                                                            <input type="checkbox" name="banner_status_` + i + `" value="off"> <span
                                                                class="lever"></span> Activate </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>`);
            i++;
            $("#banner-images").sortable({
                tolerance: 'pointer'
            });
        }
        reader.readAsDataURL(f);

    });

}


var updated_file;

function updatedFileSelect(e) {
    if (!e.target.files || !window.FileReader) return;
    var files = e.target.files;
    var f = files[0];
    updated_file = f;
    if (!f.type.match("image.*")) {
        return;
    }
    $('#updated_image').html('');
    var reader = new FileReader();
    reader.onload = function (e) {
        $('#updated_image').append(`<div class="col s12 banner"
                                        style="padding: 20px;background:#fff;margin-top:20px;)">
                                        <div class="row" style="margin:0px">
                                            <img class="col s12" style='max-width:100%;margin-bottom:20px'
                                                src="` + e.target.result + `">
                                        </div>
                                    </div>`);
    }
    reader.readAsDataURL(f);
};

function update_image($id) {
    var task = storage.ref().child("banners/" + $id + "." + updated_file.name.split(".")[1]).put(updated_file);
    var progress_id = $(this).attr("id");
    task.on('state_changed', function progress(snapshot) {
        $("#progress_updated").css("width", ((snapshot.bytesTransferred / snapshot.totalBytes) * 100) + "%");
        $("#progress_updated").prop("aria-valuenow", (snapshot.bytesTransferred / snapshot.totalBytes) * 100);
    }, function error(err) {
        alert(err);
    }, function complete() {
        database.ref("banners/" + $id + "/url").set("banners/" + $id + "." + updated_file.name.split(".")[1], function (err) {
            if (!err) {
                $('#updated_image').html('');
                $("#modal_banner").modal('hide');
                alert("Banner image has been changed");
                location.reload();
            }
        });
    });
}

// $.validator.addMethod("allRequired", function(value, elem){
//     // Use the name to get all the inputs and verify them
//     var name = elem.name;
//     return  $('input[name="'+name+'"]').map(function(i,obj){return $(obj).val();}).get().every(function(v){ return v; });
// },"Please select these fields");

$('#upload_banners').submit(function (e) {
    e.preventDefault();
}).validate({
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $(element).toggleClass("invalid");
    },
    unhighlight: function (element) {
        $(element).toggleClass("validate");
    },
    submitHandler: function (form) {
        let i = 0;
        let file_arr = [];
        $('div.banner.col.s12').each(function () {
            var file = files_row[$(this).attr("id")];
            var name = $(`[name = "banner_name_` + $(this).attr("id") + `"]`).val();
            var status = $(`[name = "banner_status_` + $(this).attr("id") + `"]`).prop('checked');

            if (status) {
                status = 'on';
            } else {
                status = "off";
            }
            file_arr[i] = {
                file,
                name,
                status
            }
            i++;
        });
        $.ajax({
            type: "post",
            url: "/admin/upload_banners",
            data: {
                banner_arr: file_arr
            },
            success: function (response) {
                alert(response);
                window.location.reload();
            },
            error: function (a, b, c) {
                alert(a.responseText);
            }
        });
    }
});


if ($("#update-banner-images").length > 0) {
    $.ajax({
        type: "get",
        url: "/admin/get_banners",
        success: function (response) {
            for (const banner of response) {
                files_row[banner.banner_id] = banner;
                $('#update-banner-images').append(`<div class="col s12 banner" id="` + banner.banner_id + `"
                                    style="padding: 20px;background:#fff;margin-top:20px;box-shadow: 2px 2px 6px rgb(134, 133, 133)">
                                    <div class="row" style="margin:0px">
                                        <img id="image_` + banner.banner_id + `" class="col-md-4" style='max-width:100%;margin-bottom:20px'
                                            src="${banner.url}">    
                                        <div class="col-md-8" style="margin-bottom:20px;">
                                            <div class="col s12 input-field">
                                                <input type="text" class="validate" name="banner_name_` + banner.banner_id + `" required="true" value="` + banner.name + `">
                                            </div>
                                            <div class="col s12 switch" style="margin-top:20px">
                                                <label> Deactivate
                                                    <input type="checkbox" name="banner_status_` + banner.banner_id + `"> <span
                                                        class="lever"></span> Activate </label>
                                            </div>
                                            <div class="col s12">
                                                <div class="list-enqu-btn">
                                                    <ul>
                                                        <!-- <li><a onclick='update_banner("` + banner.banner_id + `")' style="cursor:pointer"><i class="fa fa-pencil" aria-hidden="true" ></i>Update</a> </li> -->
                                                        <li><a onclick='del_banner("` + banner.banner_id + `")' style="cursor:pointer"><i class="fa fa-trash" aria-hidden="true"></i>Delete Banner</a> </li>
                                                     </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>`);
                if (banner.flag == "on") {
                    $('[name = "banner_status_' + banner.banner_id + '"]').prop("checked", "true");
                }
                $("#update-banner-images").sortable({
                    tolerance: 'pointer'
                });
            }
        },
        error: function (a, b, c) {
            alert(a.responseText);
        }
    });

}


$('#update_banners').submit(function (e) {
    e.preventDefault();
}).validate({
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $(element).toggleClass("invalid");
    },
    unhighlight: function (element) {
        $(element).toggleClass("validate");
    },
    submitHandler: function (form) {
        var i = 1;
        var updateData = [];
        $('div.banner.col.s12').each(function () {
            var file = files_row[$(this).attr("id")];
            var name = $(`[name = "banner_name_` + $(this).attr("id") + `"]`).val();
            var status = $(`[name = "banner_status_` + $(this).attr("id") + `"]`).prop("checked");
            if (status) {
                status = 'on';
            } else {
                status = 'off';
            }
            file.name = name;
            file.flag = status;
            file.banner_order = i;
            updateData.push(file);
            i++;
        });

        $.ajax({
            type: "post",
            url: "/admin/update_banners",
            data: {
                banner_arr: updateData
            },
            success: function (response) {
                alert(response);
                window.location.reload();
            },
            function (a, b, c) {
                alert(a.responseText);
            }
        });

    }
});

function update_banner($id) {
    $('body').append(`<div class="modal fade" id="modal_banner" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div style="margin-top:30vh">
      <div class="modal-content" style="max-width:600px;margin:auto">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        </div>
        <div class="modal-body">
            <div class="tz-file-upload">
                <div class="file-field input-field">
                    <div class="tz-up-btn"> <span>File</span>
                        <input type="file" style="z-index: 50000" 
                            id="input_update"> </div>
                    <div class="file-path-wrapper db-v2-pg-inp">
                        <input class="file-path validate" type="text" style="outline:none;border:none;border-bottom:1px solid grey">
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="updated_image"></div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="modal-close waves-effect waves-red btn-flat" data-dismiss="modal">Close</button>
          <button type="button" class="modal-close waves-effect waves-green btn-flat" onclick="update_image('` + $id + `')">Save changes</button>
        </div>
      </div>
    </div>
  </div>`);
    $("#modal_banner").modal('show');
    $("#input_update").on("change", updatedFileSelect);
    $('#modal_banner').on('hidden.bs.modal', function (e) {
        $('#modal_banner').remove();
    });
}

function del_banner($id) {
    $.ajax({
        type: "post",
        url: "/admin/delete_banner",
        data: {
            banner_id: $id
        },
        success: function (response) {
            alert("Banner has been removed");
            window.location.reload();
        },  
        error: function (a, b, c) {
            alert("Some thing went wrong");
        }
    });
}