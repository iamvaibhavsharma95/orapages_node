let com;
let total = 0;
let totalGlobal = total;
let flag = false;
let companiesToShow = 20;
let currentPage = 1;
let numbers;

var init = function () {
    Pagination.Init($('.pagination'), {
        size: numbers, // pages size
        page: currentPage, // selected page
        step: 5 // pages before and after current
    });
};

function getCompanies() {
    $("#companies").html('');
    $.ajax({
        type: "get",
        url: "/company_count",
        success: function (response) {
            total = response.company_count;
            console.log(total);

            totalGlobal = total;
            numbers = parseInt(Math.ceil(response.company_count / companiesToShow));
            if (!flag) {
                $('.pagination.list-pagenat').html('');
                init();
                flag = true;
            }
        },
        error: function (jqHXR, response, error) {
            alert(error);
        }
    });

}


$("#page_direct").on('click', function () {
    currentPage = parseInt($('#page_number').val());
    init();
    $('#page_number').val('');
});

$("#page_first").on('click', function () {
    $("#total_companies").DataTable().destroy();
    currentPage = 1;
    init();
});

function getPage($page = 1) {
    if (currentPage != $page) {
        $("#companies").html('');
        totalGlobal = total;
        totalGlobal = totalGlobal - $page;
        currentPage = $page;
        $.ajax({
            type: "get",
            url: "/companies",
            data: {
                limit: companiesToShow,
                offset: $page
            },
            success: function (response) {
                for (const company of response) {
                    setCompanies(company, totalGlobal--);
                }
            },
            error: function (jqHXR, response, error) {
                alert(error);
            }
        });
    }
}


var getBrandsByKey = async ($key) => {
    $("#total_companies").DataTable().destroy();
    $("#companies").html('');
    $('.pagination.list-pagenat').html('');
    $.ajax({
        type: "get",
        url: '/admin/brand_company_list',
        data: {
            brand_id: $key
        },
        success: function (response) {
            let r = response.length;
            let i = 0;
            for (const company of response) {
                setCompanies(company, totalGlobal--);
                i++;
                if (i == r) {
                    $("#total_companies").DataTable();
                }
            }
        },
        error: function (a, b, c) {
            alert(a.responseText);
        }
    });
}



var getCategoriesByKey = async ($key) => {
    $("#total_companies").DataTable().destroy();
    $("#companies").html('');
    $('.pagination.list-pagenat').html('');
    $.ajax({
        type: "get",
        url: '/admin/category_company_list',
        data: {
            category_id: $key
        },
        success: function (response) {
            let r = response.length;
            let i = 0;
            for (const company of response) {
                setCompanies(company, totalGlobal--);
                i++;
                if (i == r) {
                    $("#total_companies").DataTable();
                }
            }
        },
        error: function (a, b, c) {
            alert(a.responseText);
        }
    });
}



var getCompanyBykey = async ($key) => {
    $.ajax({
        type: "get",
        url: "/admin/company_by_id",
        data: {
            company_id: $key
        },
        success: function (response) {
            console.log(response);

            $("#total_companies").DataTable().destroy();
            $("#companies").html('');
            $('.pagination.list-pagenat').html('');
            setCompanies(response);
        },
        error: function (a, b, c) {
            alert(a.responseText);
        }
    });
}


function setCompanies(company, number = 1) {
    const {
        company_id,
        company_business_name = "",
        company_url,
        company_name,
        created_at = '',
    } = company;

    let uploaded_ip;
    let uploaded_by = "user"
    if (!!company.uploaded_ip && company.uploaded_ip !== 'undefined') {
        uploaded_ip = company.uploaded_ip;
    } else {
        uploaded_ip = "NULL"
    }

    if (!!company.uploaded_by) {
        uploaded_by = company.uploaded_by;
    }

    let locationName = '';
    if (company.locations.length > 0) {
        const companyLocation = company.locations[0];
        if (companyLocation.city_name == companyLocation.state_name && companyLocation.state_name == companyLocation.country_name) {
            locationName = companyLocation.city_name;
        } else if (companyLocation.city_name == companyLocation.state_name || companyLocation.state_name == companyLocation.country_name) {
            locationName = [companyLocation.city_name, companyLocation.country_name].join(', ');
        } else {
            locationName = [companyLocation.city_name, companyLocation.state_name, companyLocation.country_name].join(', ');
        }

    }


    let date = "Invalid Date";
    if (!!created_at) {
        date = new Date(created_at).toUTCString().split(", ")[1].replace("GMT", "");
        date = date.split(" ")
        date = date[0] + " " + date[1] + " " + date[2] + " at " + date[3];
    }


    $("#companies").append(`<tr id="` + company_id + `" class="company_row ` + uploaded_by + `">
                                <td>` + number + `</td>
                                <td id="` + company_id + `_name"><a href="/` + company_url + `"  target="_blank" style='text-transform: capitalize;'>` + company_business_name + `</a></td>
                                <td>` + locationName + `</td>
                                <td>` + company_name + `</td>
                                <td>` + uploaded_ip + `</td>
                                <td>` + date + `</td>
                                <td><a href="#"  onclick="deleteCompany('` + company_id + `')" class="db-list-edit">Delete</a></td>
                                <td><a href="/edit_company_admin?companyKey=` + company_id + `" target='_blank' class="db-list-edit">Edit</a></td>
                                <!-- <td><a href="/` + company_url + `" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td> -->
                        </tr>`);
}



const highlight = function (e, ui) {
    $('.working').css("display", "none");
    var
        acData = $(this).data('uiAutocomplete');
    acData
        .menu
        .element
        .find('li.ui-menu-item .ui-menu-item-wrapper')
        .each(function () {
            var me = $(this);
            var regex = new RegExp(acData.term, "gi");
            me.html(me.text().replace(regex, function (matched) {
                return termTemplate.replace('%s', matched);
            }));
        });

}


function deleteCompany($companyId) {
    swal({
            title: "Are you sure?",
            text: "This company will be deleted forever!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes Delete!",
            cancelButtonText: "No, ingore please!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "post",
                    url: "/admin_delete_company",
                    data: {
                        company_id: $companyId
                    },
                    success: function (response) {
                        if (response == 'Deleted') {
                            swal("Done!", "Company has been deleted!", "success");
                            $(`#${$companyId}`).remove();
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Error found" + XMLHttpRequest.responseText);
                    }
                });

            }
        });
}


function getAutoJson($field) {
    var update;

    if ($field == 'brands') {
        update = {
            search: function () {
                $('.working').css("display", "block");
            },
            open: highlight,
            source: function (req, res) {
                $.ajax({
                    type: "get",
                    url: "/brandName",
                    data: {
                        term: req.term
                    },
                    success: function (response) {
                        res(response);
                    }
                });
            },
            minLength: 2,
            delay: 800,
            select: function (event, ui) {
                if (ui.item.key == '') {
                    return;
                }

                setTimeout(() => {
                    $(this).val('');
                }, 30);

                getBrandsByKey(ui.item.key);

            }
        }
    } else if ($field == 'names') {
        update = {
            search: function () {
                $('.working').css("display", "block");
            },
            open: highlight,
            source: function (req, res) {
                $.ajax({
                    type: "get",
                    url: "/admin/companiesName",
                    data: {
                        term: req.term,
                    },
                    success: function (response) {
                        res(response);
                    }
                });
            },
            minLength: 2,
            delay: 600,
            select: function (event, ui) {
                if (ui.item.key == '') {
                    return;
                }
                setTimeout(() => {
                    $(this).val('');
                }, 30);

                getCompanyBykey(ui.item.key)

            }
        }
    } else if ($field == 'emails') {
        update = {
            search: function () {
                $('.working').css("display", "block");
            },
            open: highlight,
            source: function (req, res) {
                $.ajax({
                    type: "get",
                    url: "/admin/companiesEmail",
                    data: {
                        term: req.term,
                    },
                    success: function (response) {
                        res(response);
                    }
                });
            },
            minLength: 2,
            delay: 600,
            select: function (event, ui) {
                if (ui.item.key == '') {
                    return;
                }
                setTimeout(() => {
                    $(this).val('');
                }, 30);

                getCompanyBykey(ui.item.key);
            }
        }
    } else {
        update = {
            search: function () {
                $('.working').css("display", "block");
            },
            open: highlight,
            source: function (req, res) {
                $.ajax({
                    type: "get",
                    url: "/categoryName",
                    data: {
                        term: req.term
                    },
                    success: function (response) {
                        res(response);
                    }
                });
            },
            minLength: 2,
            delay: 800,
            select: function (event, ui) {
                if (ui.item.key == '') {
                    return;
                }
                setTimeout(() => {
                    $(this).val('');
                }, 30);

                getCategoriesByKey(ui.item.key);
            }
        }
    }
    return update;
}




if ($("#categories").length > 0)
    $("#categories").autocomplete(getAutoJson('categories'));

if ($("#brands").length > 0)
    $("#brands").autocomplete(getAutoJson('brands'));

if ($("#filter_company_name").length > 0)
    $("#filter_company_name").autocomplete(getAutoJson("names"));

if ($("#filter_email").length > 0)
    $("#filter_email").autocomplete(getAutoJson("emails"));