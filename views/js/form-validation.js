$.validator.addMethod('selectField', function (value, element) {
    return value != 'Choose';
}, 'Please select');

if ($(".select_two_mat").length > 0) {
    $(".select_two_mat").select2({
        tags: true
    });

    if ($(".select_two_mat.brands") > 0) {
        $(".select_two_mat.brands").select2({
            tags: true,
            placeholder: "Please enter brands"
        });
    }

    if ($(".select_two_mat.subcategories") > 0) {
        $(".select_two_mat.subcategories").select2({
            tags: true,
            placeholder: "Please enter subcategories"
        });
    }

    if ($(".select_two_mat.synonyms") > 0) {
        $(".select_two_mat.synonyms").select2({
            tags: true,
            placeholder: "Please enter Synonyms"
        });
    }
}


$("#admin_login").submit(function (e) {
    e.preventDefault();
}).validate({
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $(element).toggleClass("invalid");
    },
    unhighlight: function (element) {
        $(element).toggleClass("validate");
    },
    rules: {
        admin_email: {
            required: true,
            email: true
        },
        admin_password: {
            required: true,
        }
    },
    messages: {
        admin_email: {
            required: "Please enter email address",
            email: "Please enter valid email address"
        },
        admin_password: {
            required: "Please enter password"
        }
    },
    submitHandler: function (form) {
        adminSignin();
    }
});

// Country Start

$("#country_form").submit(function (e) {
    e.preventDefault();
}).validate({
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $(element).toggleClass("invalid");
    },
    unhighlight: function (element) {
        $(element).toggleClass("validate");
    },
    rules: {
        country_name: {
            required: true,
            maxlength: 40,
        }
    },
    messages: {
        country_name: {
            required: "Please enter country name",
            maxlength: "Please enter less that 40 characters"
        }
    },
    submitHandler: function (form) {
        addCountry($("[name = 'country_name_add']").val(), form);
    }
});


$('#country_del_form').submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    rules: {
        country_name: {
            required: true,
            selectField: true
        }
    },
    submitHandler: function (form) {
        deleteCountry($("[name = 'country_name']").prop('key'), form);
    }
});


$('#update_country').click(function (e) {
    $('#country_edit_form').prop('call', 'edit');
    $('#country_edit_form').submit();
});

$('#delete_country').click(function (e) {
    $('#country_edit_form').prop('call', 'delete');
    $('#country_edit_form').submit();
});

$('#update_state').click(function (e) {
    $('#state_edit_form').prop('call', 'edit');
    $('#state_edit_form').submit();
});

$('#delete_state').click(function (e) {
    $('#state_edit_form').prop('call', 'delete');
    $('#state_edit_form').submit();
});

$('#update_city').click(function (e) {
    $('#city_edit_form').prop('call', 'edit');
    $('#city_edit_form').submit();
});

$('#delete_city').click(function (e) {
    $('#city_edit_form').prop('call', 'delete');
    $('#city_edit_form').submit();
})


$('#update_category').click(function (e) {
    $('#category_edit_form').prop('call', 'edit');
    $('#category_edit_form').submit();
});

$('#delete_category').click(function (e) {
    $('#category_edit_form').prop('call', 'delete');
    $('#category_edit_form').submit();
});

// $("#country_edit_form").submit(function (e) {
//     e.preventDefault();
// }).validate({
//     ignore: [],
//     errorElement: "span",
//     errorClass: "helper-text error",
//     highlight: function (element) {
//         $('input.select-dropdown').addClass("invalid");
//         $('input.select-dropdown').removeClass("valid");
//     },
//     unhighlight: function (element) {
//         $('input.select-dropdown').addClass("valid");
//         $('input.select-dropdown').removeClass("invalid");
//     },
//     rules: {
//         country_name: {
//             required: true,
//             selectField: true,
//             maxlength: 40
//         },
//         country_name_new: {
//             required: true,
//             maxlength: 40
//         }
//     },
//     messages: {
//         country_name: {
//             selectField: "Please select any country",
//             maxlength: "Please enter less that 40 characters"
//         },
//         country_name_new: {
//             required: "Please enter country name",
//             maxlength: "Please enter less that 40 characters"
//         }
//     },
//     submitHandler: function (form) {
//         updateCountry($('[name ="country_name"]').prop('key'), $('[name ="country_name"]').val(), form);
//     }
// });

$("#country_edit_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    rules: {
        country_name: {
            required: true,
            selectField: true,
            maxlength: 40
        }
    },
    messages: {
        country_name: {
            required: "Please select any country",
            selectField: "Please select any country",
            maxlength: "Please enter less that 40 characters"
        }
    },
    submitHandler: function (form) {
        if ($(form).prop('call') == 'edit') {
            updateCountry($('[name ="country_name"]').prop('key'), $('[name ="country_name"]').val(), form);
        } else {
            deleteCountry($("[name = 'country_name']").prop('key'), form);
        }
    }
});

// Country End


// State Start
$("#state_add_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text errorselect",
    highlight: function (element) {
        $(element).toggleClass("invalid");
        $('.select2.select2-container').addClass("invalid");
        $('.select2.select2-container').removeClass("valid");
    },
    unhighlight: function (element) {
        $(element).toggleClass("valid");
        $('.select2.select2-container').addClass("valid");
        $('.select2.select2-container').removeClass("invalid");
    },
    errorPlacement: function (error, element) {
        $(error).insertAfter(element.parent());
    },
    rules: {
        country_name: {
            required: true,
            selectField: true
        },
        state_name: {
            required: true,
            maxlength: 500
        }
    },
    messages: {
        country_name: {
            required: "Please select any country",
            selectField: "Please select any country"
        },
        state_name: {
            required: "Please enter state name",
            maxlength: "Please enter less that 500 characters"
        },
    },
    submitHandler: function (form) {
        addState($("[name = 'country_name']").prop("key"), $('[name="state_name"]').val(), form);
    }
});

// $("#state_edit_form").submit(function (e) {
//     e.preventDefault();
// }).validate({
//     ignore: [],
//     errorElement: "span",
//     errorClass: "helper-text error",
//     highlight: function (element) {
//         $('input.select-dropdown').addClass("invalid");
//         $('input.select-dropdown').removeClass("valid");
//     },
//     unhighlight: function (element) {
//         $('input.select-dropdown').addClass("valid");
//         $('input.select-dropdown').removeClass("invalid");
//     },
//     rules: {
//         country_name: {
//             required: true,
//             selectField: true
//         },
//         state_name: {
//             required: true,
//             selectField: true
//         },
//         state_new_name: {
//             required: true,
//             maxlength: 40
//         }
//     },
//     messages: {
//         country_name: {
//             selectField: "Please select any country"
//         },
//         state_name: {
//             selectField: "Please select any state"
//         },
//         state_new_name: {
//             required: "Please enter country name",
//             maxlength: "Please enter less that 40 characters"
//         }
//     },
//     submitHandler: function (form) {
//         updateState($("[name = 'country_name']").val(), $("[name = 'state_name']").val(), $("[name = 'state_new_name']").val(), form);
//     }
// });


$("#state_edit_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    rules: {
        country_name: {
            required: true,
            selectField: true
        },
        state_name: {
            required: true,
            selectField: true
        }
    },
    messages: {
        country_name: {
            required: "Please select any country",
            selectField: "Please select any country"
        },
        state_name: {
            required: "Please select any state",
            selectField: "Please select any state"
        }
    },
    submitHandler: function (form) {
        if ($(form).prop('call') == 'edit') {
            updateState($("[name = 'country_name']").prop("key"), $("[name = 'state_name']").prop('key'), $("[name = 'state_name']").val(), form);
        } else if ($(form).prop('call') == 'delete') {
            deleteState($("[name = 'country_name']").prop('key'), $("[name = 'state_name']").prop("key"), form);
        }
    }
});

$("#state_del_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    rules: {
        country_name: {
            required: true,
            selectField: true
        },
        state_name: {
            required: true,
            selectField: true
        }
    },
    messages: {
        country_name: {
            selectField: "Please select any country"
        },
        state_name: {
            selectField: "Please select any state"
        }
    },
    submitHandler: function (form) {
        deleteState($("[name = 'country_name']").val(), $("[name = 'state_name']").val(), form);
    }
});
//State End

// City Start
$("#city_add_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text errorselect",
    highlight: function (element) {
        $(element).toggleClass("invalid");
        $('.select2.select2-container').addClass("invalid");
        $('.select2.select2-container').removeClass("valid");
    },
    unhighlight: function (element) {
        $(element).toggleClass("valid");
        $('.select2.select2-container').addClass("valid");
        $('.select2.select2-container').removeClass("invalid");
    },
    errorPlacement: function (error, element) {
        $(error).insertAfter(element.parent());
    },
    rules: {
        country_name: {
            required: true,
            selectField: true,
            maxlength: 100
        },
        state_name: {
            required: true,
            selectField: true
        },
        city_name: {
            required: true
        }
    },
    messages: {
        country_name: {
            required: "Please select any country",
            selectField: "Please select any country",
            maxlength: "Please enter less that 100 characters"
        },
        state_name: {
            required: "Please select any state",
            selectField: "Please select any state",
            maxlength: "Please enter less that 100 characters"
        },
        city_name: {
            required: "Please enter at least one city"
        },
    },
    submitHandler: function (form) {
        addCity($("[name = 'country_name']").prop('key'), $("[name = 'state_name']").prop('key'), $("[name = 'city_name']").val(), form);
    }
});

$("#city_edit_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    rules: {
        country_name: {
            required: true,
            selectField: true
        },
        state_name: {
            required: true,
            selectField: true
        },
        city_name: {
            required: true,
            selectField: true
        },
        city_new_name: {
            required: true
        }
    },
    messages: {
        country_name: {
            selectField: "Please select any country"
        },
        state_name: {
            selectField: "Please select any state"
        },
        city_name: {
            selectField: "Please select any city"
        },
        city_new_name: {
            required: "Please enter country name"
        }
    },
    submitHandler: function (form) {
        if ($(form).prop('call') == 'edit') {
            updateCity($("[name = 'country_name']").prop('key'), $("[name = 'state_name']").prop('key'), $("[name = 'city_name']").prop('key'), $("[name = 'city_name']").val(), form);
        } else {
            deleteCity($("[name = 'country_name']").prop('key'),$("[name = 'state_name']").prop('key'), $("[name = 'city_name']").prop('key'), form);
        }
    }
});

$("#city_deletion_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    rules: {
        country_name: {
            required: true,
            selectField: true
        },
        state_name: {
            required: true,
            selectField: true
        },
        city_name: {
            required: true,
            selectField: true
        }
    },
    messages: {
        country_name: {
            selectField: "Please select any country"
        },
        state_name: {
            selectField: "Please select any state"
        },
        city_name: {
            selectField: "Please select any city"
        }
    },
    submitHandler: function (form) {
        deleteCity($("[name = 'state_name']").prop('key'), $("[name = 'city_name']").prop('key'), form);
    }
});

// City End




// Tagline Start 

$("#add_tagline_form").submit(function (e) {
    e.preventDefault();
}).validate({
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $(element).toggleClass("invalid");
    },
    unhighlight: function (element) {
        $(element).toggleClass("validate");
    },
    rules: {
        tagline: {
            required: true,
            maxlength: 250
        }
    },
    messages: {
        tagline: {
            required: "Please enter tagline",
            maxlength: "Please enter less that 250 characters"
        }
    },
    submitHandler: function (form) {
        addTagline($('[name = "tagline"]').val(), form);
    }
});


$('#del_tagline_form').submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    rules: {
        tagline: {
            required: true,
            selectField: true
        }
    },
    messages: {
        tagline: {
            selectField: "Please select tagline"
        }
    },
    submitHandler: function (form) {
        deleteTagline($('[name = "tagline"]').val(), form);
    }
});


$("#edit_tagline_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    rules: {
        tagline: {
            required: true,
            selectField: true
        },
        new_tagline: {
            required: true,
            maxlength: 250
        }
    },
    messages: {
        tagline: {
            selectField: "Please select any tagline"
        },
        new_tagline: {
            required: "Please enter tagline",
            maxlength: "Please enter less that 250 characters"
        }
    },
    submitHandler: function (form) {
        updateTagline($('[name = "tagline"]').val(), $('[name= "new_tagline"]').val(), form)
    }
});

// Tagline End


// Brands Start 

$("#add_brands_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text errorselect",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    errorPlacement: function (error, element) {
        $(error).insertAfter(element.parent());
    },
    rules: {
        brands: {
            required: true
        }
    },
    messages: {
        brands: {
            required: "Please enter brands",
        }
    },
    submitHandler: function (form) {
        addBrands($('[name= "brands"]').val(), form);
    }
});


$('#del_brands_form').submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text errorselect",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    errorPlacement: function (error, element) {
        $(error).insertAfter(element.parent());
    },
    rules: {
        brands: {
            required: true
        }
    },
    messages: {
        brands: {
            required: "Please enter any brand"
        }
    },
    submitHandler: function (form) {
        deleteBrand($("[name = 'brands']").val(), form);
    }
});


$("#edit_brands_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    rules: {
        brands: {
            required: true,
            selectField: true
        },
        brand_new_name: {
            required: true,
            maxlength: 40
        }
    },
    messages: {
        brands: {
            selectField: "Please select any brand"
        },
        brand_new_name: {
            required: "Please enter new brand",
            maxlength: "Please enter less that 40 characters"
        }
    },
    submitHandler: function (form) {
        updateBrand($("[name = 'brands']").val(), $("[name = 'brand_new_name']").val(), form);
    }
});

// Brands End


// category Start

$("#category_form").submit(function (e) {
    e.preventDefault();
}).validate({
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $(element).toggleClass("invalid");
    },
    unhighlight: function (element) {
        $(element).toggleClass("validate");
    },
    rules: {
        category_name: {
            required: true,
            maxlength: 250
        }
    },
    messages: {
        category_name: {
            required: "Please enter category name",
            maxlength: "Please enter less that 250 characters"
        }
    },
    submitHandler: function (form) {
        addCategory($("[name = 'category_name']").val(), form);
    }
});


$('#category_del_form').submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    rules: {
        category_name: {
            required: true,
            selectField: true
        }
    },
    submitHandler: function (form) {
        deleteCategory($("[name = 'category_name']").val(), form);
    }
});


$("#category_edit_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    rules: {
        category_name: {
            required: true,
            selectField: true,
            maxlength: 250
        }
    },
    messages: {
        category_name: {
            selectField: "Please select any category",
            maxlength: "Please enter less that 250 characters"
        }
    },
    submitHandler: function (form) {
        if ($(form).prop('call') == 'edit') {
            updateCategory($('[name ="category_name"]').prop('key'), $('[name ="category_name"]').val(), form);
        } else {
            deleteCategory($("[name = 'category_name']").prop('key'), form);
        }
    }
});

// category End

/**
 * 
 * Subcategory Start
 * 
 */

$('#update_subcategory').click(function (e) {
    $('#edit_subcategories_form').prop('call', 'update');
    $('#edit_subcategories_form').submit();
});

$('#delete_subcategory').click(function (e) {
    $('#edit_subcategories_form').prop('call', 'delete');
    $('#edit_subcategories_form').submit();
});

$('#update_non_dis_subcategory').click(function (e) {
    $('#edit_non_dis_subcategories_form').prop('call', 'update');
    $('#edit_non_dis_subcategories_form').submit();
});

$('#delete_non_dis_subcategory').click(function (e) {
    $('#edit_non_dis_subcategories_form').prop('call', 'delete');
    $('#edit_non_dis_subcategories_form').submit();
});


$("#add_subcategories_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text errorselect",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    errorPlacement: function (error, element) {
        $(error).insertAfter(element.parent());
    },
    rules: {
        subcategories: {
            required: true,
            maxlength: 250
        }
    },
    messages: {
        subcategories: {
            required: "Please enter Subcategories name",
            maxlength: "Please enter less that 250 characters"
        },
    },
    submitHandler: function (form) {
        addSubcategory(addedCategory, $('[name="subcategories"]').val(), form);
        addedCategory.clear();
    }
});


$("#add_non_dis_subcategories_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text errorselect",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    errorPlacement: function (error, element) {
        $(error).insertAfter(element.parent());
    },
    rules: {
        subcategories: {
            required: true,
            maxlength: 250
        }
    },
    messages: {
        subcategories: {
            required: "Please enter Subcategories name",
            maxlength: "Please enter less that 250 characters"
        },
    },
    submitHandler: function (form) {
        addNonDisSubcategory(addedCategory, $('[name="non_dis_subcategories"]').val(), form);
        addedCategory.clear();
    }
});



$("#add_subcategories_syn_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text errorselect",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    errorPlacement: function (error, element) {
        $(error).insertAfter(element.parent());
    },
    rules: {
        category_name: {
            required: true,
            selectField: true
        },
        subcategory_name: {
            required: true,
            selectField: true
        },
        subcategory_synonmys: {
            required: true,
            maxlength: 250
        }
    },
    messages: {
        category_name: {
            selectField: "Please select any Category"
        },
        subcategory_name: {
            selectField: "Please select any Subcategory"
        },
        subcategory_synonmys: {
            required: "Please enter Synonyms name",
            maxlength: "Please enter less that 250 characters"
        },
    },
    submitHandler: function (form) {
        addSubcategorySyn($('[name="category_name"]').val(), $('[name="subcategory_name"]').val(), $('[name="subcategory_synonmys"]').val(), form);
    }
});


$("#edit_subcategories_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text errorselect",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    errorPlacement: function (error, element) {
        $(error).insertAfter(element.parent());
    },
    rules: {
        subcategory_name: {
            required: true,
            maxlength: 250,
        }
    },
    messages: {
        subcategory_name: {
            selectField: "Please select any Subcategory",
            maxlength: "Please enter less that 250 characters"
        }
    },
    submitHandler: function (form) {
        if ($(form).prop('call') == 'delete') {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this child!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {               
                    deleteSubcategory(null, $('[name="subcategory_name"]').prop('key'), form, null);
                }else{
                    swal("Cancelled", "Child is not removed :)", "error");
                }
            });
        }
        if($(form).prop('call') == 'update'){
            updateSubcategory(addedCategory, $('[name="subcategory_name"]').prop('key'), $('[name="subcategory_name"]').val(), form);
        }
    }
});

$("#edit_non_dis_subcategories_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text errorselect",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    errorPlacement: function (error, element) {
        $(error).insertAfter(element.parent());
    },
    rules: {
        subcategory_name: {
            required: true,
            maxlength: 250,
        }
    },
    messages: {
        subcategory_name: {
            selectField: "Please select any Non Displayable Subcategory",
            maxlength: "Please enter less that 250 characters"
        }
    },
    submitHandler: function (form) {
        if ($(form).prop('call') == 'delete') {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this Non Displayable child!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {               
                    deleteNonDisSubcategory(null, $('[name="non_dis_subcategory_name"]').prop('key'), form, null);
                }else{
                    swal("Cancelled", "Non Displable Child is not removed :)", "error");
                }
            });
        }
        if($(form).prop('call') == 'update'){
            updateNonDisSubcategory(addedCategory, $('[name="non_dis_subcategory_name"]').prop('key'), $('[name="non_dis_subcategory_name"]').val(), form);
        }
    }
});


$("#del_subcategories_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text errorselect",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    errorPlacement: function (error, element) {
        $(error).insertAfter(element.parent());
    },
    rules: {
        subcategory_name: {
            required: true,
            selectField: true
        }
    },
    messages: {
        subcategory_name: {
            selectField: "Please select any Subcategory"
        }
    },
    submitHandler: function (form) {
        deleteSubcategory($('[name="category_name"]').prop(), $('[name="subcategory_name"]').val(), form);
    }
});


$("#transfer_categories_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text errorselect",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    errorPlacement: function (error, element) {
        $(error).insertAfter(element);
    },
    rules: {
        category_first_name: {
            required: true,
            selectField: true
        },
        category_second_name: {
            required: true,
            selectField: true
        }
    },
    messages: {
        category_first_name: {
            selectField: "Please select any Category"
        },
        category_second_name: {
            selectField: "Please select any Category"
        }
    },
    submitHandler: function (form) {
        // return console.log($('[name="category_first_name"]').val().split("#")[0],$('[name="category_second_name"]').val().split("#")[0]);
        transferCompaniesCategory($('[name="category_first_name"]').prop('category'), $('[name="category_second_name"]').prop('category'));
        // transferCompaniesCategory($('[name="category_first_name"]').val().split("#")[0], $('[name="category_second_name"]').val().split("#")[0]);
    }
});


$("#transfer_cities_form").submit(function (e) {
    e.preventDefault();
}).validate({
    ignore: [],
    errorElement: "span",
    errorClass: "helper-text errorselect",
    highlight: function (element) {
        $('input.select-dropdown').addClass("invalid");
        $('input.select-dropdown').removeClass("valid");
    },
    unhighlight: function (element) {
        $('input.select-dropdown').addClass("valid");
        $('input.select-dropdown').removeClass("invalid");
    },
    errorPlacement: function (error, element) {
        $(error).insertAfter(element);
    },
    rules: {
        city_first_name: {
            required: true
        },
        city_second_name: {
            required: true
        }
    },
    messages: {
        city_first_name: {
            required: "Please select any City"
        },
        city_second_name: {
            required: "Please select any City"
        }
    },
    submitHandler: function (form) {
        transferCompaniesCity($('[name="city_first_name"]').prop('location'), $('[name="city_second_name"]').prop('location'));
    }
});

$('#upload_csv').submit(function (e) {
    e.preventDefault();
}).validate({
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $(element).toggleClass("invalid");
    },
    unhighlight: function (element) {
        $(element).toggleClass("validate");
    },
    submitHandler: function (form) {
        uploadCSV(files[0]);
    }
});


$('#auto_upload_csv').submit(function (e) {
    e.preventDefault();
}).validate({
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $(element).toggleClass("invalid");
    },
    unhighlight: function (element) {
        $(element).toggleClass("validate");
    },
    submitHandler: function (form) {
        uploadAutoCSV(files[0]);
    }
});


$('#upload_large_csv').submit(function (e) {
    e.preventDefault();
}).validate({
    errorElement: "span",
    errorClass: "helper-text error",
    highlight: function (element) {
        $(element).toggleClass("invalid");
    },
    unhighlight: function (element) {
        $(element).toggleClass("validate");
    },
    submitHandler: function (form) {
        uploadLargeCSV(files[0]);
    }
});