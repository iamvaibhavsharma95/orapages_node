$.validator.addMethod("alphanumericUrl", function (value, element) {
    return this.optional(element) || /^[a-zA-Z0-9-_.:/]+$/.test(value);
}, "Letters, numbers, hyphens, dots, slashes, colons and underscores only please");


$('[name="pics_website"]').on('input',function(){
    $("#check_pics_website").prop('checked','on');
});

$('[name="pics_enclosed"]').on('input',function(){
    $("#check_pics_enclosed").prop('checked','on');
});

$('[name="details"]').on('input',function(){
    $("#check_details").prop('checked','on');
});

if ($("#premium_profile_form").length > 0) {
    $("#premium_profile_form").validate({
        ignore: `:hidden:not(#company_business_details)`,
        rules: {
            name: {
                required: true,
                maxlength: 100
            },
            email: {
                required: true,
                email: true,
                remote: "/check_email"
            },
            designation: {
                // required: true,
                maxlength: 200,
                minlength: 2
            },
            company_name: {
                required: true,
                maxlength: 200,
            },
            pics_website: {
                required: "#check_pics_website:checked",
                alphanumericUrl:true,
                maxlength:100
            },
            pics_enclosed:{
                required:"#check_pics_enclosed:checked",
                maxlength:100
            },
            details:{
                required:"#check_details:checked",
                maxlength:2500
            },
            agreeTerms: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Please enter Your name",
                maxlength: "Please enter less than 100 characters"
            },
            email: {
                required: "Please enter email",
                email: "Please enter valid email address",
                remote:"This email address is already taken please enter another"
            },
            designation: {
                // required: "Please enter your designation",
                minlength: "Please enter more than 2 characters",
                maxlength:"Please enter less than 200 chracters"
            },
            pics_website: {
                required: "Please enter the website address",
                maxlength:"Please enter less than 100 chracters"
            },
            pics_enclosed: {
                required: "Please enter enclosed",
                maxlength: "Please enter less than 100 characters"
            },
            details: {
                required: "Please enter Business information",
                minlength: "Please enter more than 3 characters",
                remote: "This url is already taken"
            },
            company_business_details: {
                required: "Please enter details of the business",
                maxlength:"Please enter less than 2500 characters"
            },
            agreeTerms: {
                required: "Please agree terms and conditions to submit profile"
            }
        },
        errorElement: 'div',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
            $(element).addClass('is-valid');
        },
        submitHandler:function(form){
            $("[type='submit']").html(`<span><span class="spinner-border text-primary spinner-pr" role="status" style="margin-right: 8px;display: none;" ></span></span><span>Please Wait......<br></span>`)        
            $(".spinner-pr").show();
            $("[type='submit']").prop('disabled',true);
            $(form)[0].submit();
        }
        // submitHandler: function (form) {
        //     $("[type='submit']").html(`<span><span class="spinner-border text-warning spinner-pr" role="status" style="margin-right: 8px;display: none;" ></span></span><span>Please Wait......<br></span>`)        
        //     $(".spinner-pr").show();
        //     $("[type='submit']").prop('disabled',true);
            
        //     $.ajax({
        //         type: "post",
        //         url: "/premium/register",
        //         data: $(form).serializeArray(),
        //         success: function (response) {
        //             $(form)[0].reset();
        //             $("[type='submit']").html(`<span><span class="spinner-border text-warning spinner-pr" role="status" style="margin-right: 8px;display: none;" ></span></span><span>Pay 100$ and Get Your Premium Profile<br></span>`);
        //             $("[type='submit']").prop('disabled',false);
        //             $(".spinner-pr").hide();
        //             alert(response);        
        //         },
        //         error:function(a,b,c){
        //             $(form)[0].reset();
        //             $(".spinner-pr").show();
        //             $("[type='submit']").html(`<span><span class="spinner-border text-warning spinner-pr" role="status" style="margin-right: 8px;display: none;" ></span></span><span>Pay 100$ and Get Your Premium Profile<br></span>`)        
                
        //         }
        //     });
        // }
    })
}