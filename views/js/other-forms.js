function populateSuggestionsWithLocationCategory(location, category) {
  $("#your_suggestions").html(`
    <div class="card mt-2 mb-2">
        <div class="card-header orapages-card-header" id="headingOne">
          <strong>Friends' Suggestions</strong>
      </div>
    </div>
    <table id="suggestions_table"
      class="table table-striped table-bordered table-hover table-sm dataTable no-footer">
          <thead class="thead-dark">
              <th>Company Name</th>
              <th>Location</th>
              <th>Add</th>
          </thead>
          <ul class="list-group suggestions_list">
            <tbody>
            </tbody>
          </ul>
      </table>`);
  initializeDataTable("/on_boarding/populate_suggestions_location_category", {
    location,
    category,
  });
}

function populateSuggestionsWithCompany(company_id) {
  $("#your_suggestions").html(`
    <div class="card mt-2 mb-2">
        <div class="card-header orapages-card-header" id="headingOne">
          <strong>Friends' Suggestions</strong>
      </div>
    </div>
    <table id="suggestions_table"
      class="table table-striped table-bordered table-hover table-sm dataTable no-footer">
          <thead class="thead-dark">
              <th>Company Name</th>
              <th>Location</th>
              <th>Add</th>
          </thead>
          <ul class="list-group suggestions_list">
            <tbody>
            </tbody>
          </ul>
      </table>`);
  initializeDataTable("/on_boarding/populate_suggestions_company_name", {
    company_id,
  });
}

const initializeDataTable = (url, data) => {
  var oldStart = 0;
  $.fn.DataTable.ext.pager.numbers_length = 12;
  let table = $("#suggestions_table").DataTable({
    pageLength: 15,
    lengthChange: false,
    ordering: "asc",
    searching: false,
    processing: true,
    serverSide: true,
    columnDefs: [
      { orderable: true, targets: 0 },
      { orderable: false, targets: "_all" },
      { orderable: false, targets: "_all" },
    ],
    ajax: {
      url: url,
      data: data,
    },
    dom:
      "<'row'<'col-sm-2 search'l><'col-sm-2 search-btn'f><'col-sm-8'p>>" +
      "<'row'<'col-sm-12'tr>>" +
      "<'row'<'col-sm-4'i><'col-sm-8'p>>",
    columns: [
      {
        data: "name",
        render: function (data, type, row, meta) {
          let { company_url, name } = row;
          return `<a class="ora-text" href="/${company_url}" target="_blank">
                    ${name}
                  </a>`;
        },
      },
      {
        data: "location",
        render: function (data, type, row, meta) {
          return `<p>${data}</p>`;
        },
      },
      {
        data: "add",
        render: function (data, type, row, meta) {
          let { user_id } = row;
          return `<button type="button" class="btn btn-success" onclick="addFriend('${user_id}',this)">Add Friend</button>`;
        },
      },
    ],
    initComplete: function () {
      $("div.search").html(
        `<input type="number" placeholder="Page Number" class="form-control" style="height:30px" id="search-field" min="0" max="${
          table.page.info().pages
        }">`
      );
      $("div.search-btn").html(
        '<button type="button" class="btn btn-sm btn-primary" id="search-btn">Jump</button>'
      );
      $("#search-btn").on("click", function () {
        table.page(parseInt($("#search-field").val()) - 1).draw("page");
      });
    },
    fnDrawCallback: function (o) {
      if (o._iDisplayStart != oldStart) {
        $("html,body").animate(
          {
            scrollTop: 0,
          },
          100
        );
        oldStart = o._iDisplayStart;
      }
    },
  });
};

$(document).ready(function () {
  $.validator.addMethod(
    "selectField",
    function (value, element) {
      return value != "Choose";
    },
    "Please select"
  );

  let searchCatCategoryForm = {
    open: function (e, ui) {
      $(".working").css("display", "none");
      var acData = $(this).data("uiAutocomplete");
      // styledTerm = termTemplate.replace('%s', acData.term);

      acData.menu.element
        .find("li.ui-menu-item .ui-menu-item-wrapper")
        .each(function () {
          var me = $(this);
          var regex = new RegExp(acData.term, "gi");
          me.html(
            me.text().replace(regex, function (matched) {
              return termTemplate.replace("%s", matched);
            })
          );
        });
    },
    source: function (req, res) {
      $.ajax({
        type: "get",
        url: "/categoryName",
        data: {
          term: req.term,
        },
        success: function (response) {
          res(response);
        },
      });
    },
    minLength: 2,
    delay: 400,
    select: function (event, ui) {
      if (!ui.item.key) {
        return;
      }
      $(this).prop("key", ui.item.key);
      $('[name="keyCategoryForm"]').val(ui.item.key);
      $('[name="keyCategoryFormModal"]').val(ui.item.key);
    },
  };

  let onBoardingCategoryForm = {
    open: function (e, ui) {
      $(".working").css("display", "none");
      var acData = $(this).data("uiAutocomplete");
      // styledTerm = termTemplate.replace('%s', acData.term);

      acData.menu.element
        .find("li.ui-menu-item .ui-menu-item-wrapper")
        .each(function () {
          var me = $(this);
          var regex = new RegExp(acData.term, "gi");
          me.html(
            me.text().replace(regex, function (matched) {
              return termTemplate.replace("%s", matched);
            })
          );
        });
    },
    source: function (req, res) {
      $.ajax({
        type: "get",
        url: "/categoryName",
        data: {
          term: req.term,
        },
        success: function (response) {
          res(response);
        },
      });
    },
    minLength: 2,
    delay: 400,
    select: function (event, ui) {
      if (!ui.item.key) {
        return;
      }
      $(this).prop("key", ui.item.key);
      $('[name="keyCategoryForm"]').val(ui.item.key);
      $('[name="keyCategoryFormModal"]').val(ui.item.key);
      populateSuggestionsWithLocationCategory(
        `${city_id},${state_id},${country_id}`,
        ui.item.key
      );
    },
  };

  let onBoardingCompanyForm = {
    open: function (e, ui) {
      $(".working").css("display", "none");
      var acData = $(this).data("uiAutocomplete");
      // styledTerm = termTemplate.replace('%s', acData.term);

      acData.menu.element
        .find("li.ui-menu-item .ui-menu-item-wrapper")
        .each(function () {
          var me = $(this);
          var regex = new RegExp(acData.term, "gi");
          me.html(
            me.text().replace(regex, function (matched) {
              return termTemplate.replace("%s", matched);
            })
          );
        });
    },
    source: function (req, res) {
      $.ajax({
        type: "get",
        url: "/onBoardingCompaniesName",
        data: {
          term: req.term,
        },
        success: function (response) {
          res(response);
        },
      });
    },
    minLength: 2,
    delay: 400,
    select: function (event, ui) {
      if (!ui.item.key) {
        return;
      }
      $(this).prop("key", ui.item.key);
      $('[name="keyCompanyForm"]').val(ui.item.key);
      populateSuggestionsWithCompany(ui.item.key);
    },
  };

  $.validator.addMethod(
    "dropdowncategory",
    function (value, element) {
      let key = $(element).prop("key");
      if (!!key) {
        return true;
      } else {
        return false;
      }
    },
    "Please select category from dropdown"
  );

  $("#login-button").on("click", function () {
    $("#login").modal("show");
  });

  $("#forgot-password-button").on("click", function () {
    $("#login").modal("hide");
    $("#forgot-password").modal("show");
  });

  $(".contact-us").on("click", function () {
    $("#contact_form_type").val("Contact");
    $("#contact-modal").modal("show");
  });

  $(".country-report").on("click", function () {
    $("#contact_form_type").val("Location");
    $("#contact-modal").modal("show");
  });

  $(".feedback").on("click", function () {
    $("#contact_form_type").val("Feedback");
    $("#contact-modal").modal("show");
  });

  $("#report-button").on("click", function () {
    $("#contact_form_type").val(`Report: ${window.location.href}`);
    $("#contact-modal").modal("show");
  });

  $(".contact-category").on("click", function () {
    $("#category-contact-modal").modal("show");
  });

  $("#login-form")
    .submit(function (e) {
      e.preventDefault();
    })
    .validate({
      rules: {
        email: {
          required: true,
          email: true,
        },
        password: {
          required: true,
        },
      },
      messages: {
        email: {
          required: "Please enter your email address",
          email: "Please enter valid email address",
        },
        password: {
          required: "Please enter the password",
        },
      },
      errorElement: "div",
      errorPlacement: function (error, element) {
        error.addClass("invalid-feedback");
        element.closest(".form-group").append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass("is-invalid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass("is-invalid");
        $(element).addClass("is-valid");
      },
      submitHandler: function (form) {
        $(form).find(".spinner-border").show();
        $(form).find(".submit-text").html("Please wait");
        $(form).find('[type="submit"]').prop("disabled", true);
        $.ajax({
          type: "post",
          url: "/login",
          data: $(form).serializeArray(),
          success: function (response) {
            window.location.href = "/owner_panel";
          },
          error: (jqXHR, response, errorThrown) => {
            alert(jqXHR.responseText);
            $(form).find(".spinner-border").hide();
            $(form).find(".submit-text").html("Login");
            $(form).find('[type="submit"]').prop("disabled", false);
          },
        });
      },
    });

  $("#forgot-password-form")
    .submit(function (e) {
      e.preventDefault();
    })
    .validate({
      rules: {
        email: {
          required: true,
          email: true,
        },
      },
      messages: {
        email: {
          required: "Please enter your email address",
          email: "Please enter valid email address",
        },
      },
      errorElement: "div",
      errorPlacement: function (error, element) {
        error.addClass("invalid-feedback");
        element.closest(".form-group").append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass("is-invalid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass("is-invalid");
        $(element).addClass("is-valid");
      },
      submitHandler: function (form) {
        $(form).find(".spinner-border").show();
        $(form).find(".submit-text").html("Please wait");
        $(form).find('[type="submit"]').prop("disabled", true);
        $.ajax({
          type: "post",
          url: "/forget_password",
          data: $(form).serializeArray(),
          success: function (response) {
            $(form).find(".spinner-border").hide();
            $(form).find(".submit-text").html("Send me the reset Email");
            $(form).find('[type="submit"]').prop("disabled", false);
            alert("Reset email has been sent");
            $("#forgot-password").modal("hide");
          },
          error: function (jqXHR, textStatus, errorThrown) {
            $(form).find(".spinner-border").hide();
            $(form).find(".submit-text").html("Send me the reset Email");
            $(form).find('[type="submit"]').prop("disabled", false);
            alert(jqXHR.responseText);
          },
        });
      },
    });

  $("#contact-form")
    .submit(function (e) {
      e.preventDefault();
    })
    .validate({
      rules: {
        tx_cust_email: {
          required: true,
          email: true,
        },
        tx_message: {
          required: true,
        },
      },
      messages: {
        tx_cust_email: {
          required: "Please enter the email address",
          email: "Please enter valid email address",
        },
        tx_message: {
          required: "Please enter your message",
          maxlength: "Please do not enter more than 500 characters",
        },
      },
      errorElement: "div",
      errorPlacement: function (error, element) {
        error.addClass("invalid-feedback");
        element.closest(".form-group").append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass("is-invalid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass("is-invalid");
        $(element).addClass("is-valid");
      },
      submitHandler: (form) => {
        $(form).find(".spinner-border").show();
        $(form).find(".submit-text").html("Please wait");
        $(form).find('[type="submit"]').prop("disabled", true);
        let data = $(form).serializeArray();
        console.log(data);
        $.ajax({
          type: "post",
          url: "/feedback",
          data: data,
          success: function (response) {
            $(form).find(".spinner-border").hide();
            $(form).find(".submit-text").html("Submit");
            $(form).find('[type="submit"]').prop("disabled", false);
            $(form)[0].reset();
            alert("Your message has been submitted");
          },
          error: function (a, b, c) {
            alert("There is some internal server error.");
            $(form).find(".spinner-border").hide();
            $(form).find(".submit-text").html("Submit");
            $(form).find('[type="submit"]').prop("disabled", false);
          },
        });
      },
    });

  $("#categorySearchForm").autocomplete(searchCatCategoryForm);

  $("#categoryOnboardingForm").autocomplete(onBoardingCategoryForm);

  $("#companyOnBoardingForm").autocomplete(onBoardingCompanyForm);

  $("#categorySearchFormModal").autocomplete(searchCatCategoryForm);

  $("#category-contact-form")
    .submit(function (e) {
      e.preventDefault();
    })
    .validate({
      rules: {
        nm_cust: {
          required: true,
        },
        tx_cust_email: {
          required: true,
          email: true,
        },
        tx_message: {
          required: true,
        },
        number_cust: {
          required: true,
          // digits: true,
          // minLength:10,
          maxlength: 20,
        },
        countryCategory: {
          required: true,
          selectField: true,
        },
        stateCategory: {
          required: true,
          selectField: true,
        },
        cityCategory: {
          required: true,
          selectField: true,
        },
        categorySearchForm: {
          required: true,
          dropdowncategory: true,
        },
      },
      messages: {
        nm_cust: {
          required: "Please enter your name",
        },
        tx_cust_email: {
          required: "Please enter the email address",
          email: "Please enter valid email address",
        },
        tx_message: {
          required: "Please enter your message",
          maxlength: "Please do not enter more than 500 characters",
        },
        number_cust: {
          required: "Please enter your number",
          maxlength: "Please enter less then 20 characters",
        },
        countryCategory: {
          required: "Please select Country",
          selectField: "Please select Country",
        },
        stateCategory: {
          required: "Please select State",
          selectField: "Please select State",
        },
        cityCategory: {
          required: "Please select City",
          selectField: "Please select City",
        },
        categorySearchForm: {
          required: "Please select category from drop down",
          dropdowncategory: "Please select category from drop down",
        },
      },
      errorElement: "div",
      errorPlacement: function (error, element) {
        error.addClass("invalid-feedback");
        element.closest(".form-group").append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass("is-invalid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass("is-invalid");
        $(element).addClass("is-valid");
      },
      submitHandler: (form) => {
        $(form).find(".spinner-border").show();
        $(form).find(".submit-text").html("Please wait");
        $(form).find('[type="submit"]').prop("disabled", true);
        let data = new FormData($(form)[0]);
        console.log(data);
        $.ajax({
          type: "post",
          url: `/categoryEmail?_csrf=${$("[name='_csrf']").val()}`,
          data: data,
          processData: false,
          contentType: false,
          success: function (response) {
            $(form).find(".spinner-border").hide();
            $(form).find(".submit-text").html("Submit");
            $(form).find('[type="submit"]').prop("disabled", false);
            $(form)[0].reset();
            // console.log(response);
            if (!!response) {
              $("#download-csv-button").prop(
                "href",
                "data:text/csv;charset=utf-8," +
                  encodeURI(
                    "Company Name,Company Email,Company Website,Company Phone Number\n" +
                      response
                  )
              );
              $("#download-csv-button").prop("target", "_blank");
              $("#download-csv-button").prop(
                "download",
                "Customer or Vendor list.csv"
              );
              $("#download-csv").modal("show");
            } else {
              $("#error-modal").modal("show");
              $("#error-text").html("No data found in these options");
            }
            // alert('Your message has been submitted');
          },
          error: function (a, b, c) {
            $("#error-modal").modal("show");
            $("#error-text").html(a.responseText);
            $(form).find(".spinner-border").hide();
            $(form).find(".submit-text").html("Submit");
            $(form).find('[type="submit"]').prop("disabled", false);
          },
        });
      },
    });

  $("#category-contact-form-modal")
    .submit(function (e) {
      e.preventDefault();
    })
    .validate({
      rules: {
        nm_cust: {
          required: true,
        },
        tx_cust_email: {
          required: true,
          email: true,
        },
        tx_message: {
          required: true,
        },
        number_cust: {
          required: true,
          maxlength: 20,
        },
        countryCategoryModal: {
          required: true,
          selectField: true,
        },
        stateCategoryModal: {
          required: true,
          selectField: true,
        },
        cityCategoryModal: {
          required: true,
          selectField: true,
        },
        categorySearchFormModal: {
          required: true,
          dropdowncategory: true,
        },
      },
      messages: {
        nm_cust: {
          required: "Please enter your name",
        },
        tx_cust_email: {
          required: "Please enter the email address",
          email: "Please enter valid email address",
        },
        tx_message: {
          required: "Please enter your message",
          maxlength: "Please do not enter more than 500 characters",
        },
        number_cust: {
          required: "Please enter your number",
          maxlength: "Please enter less then 20 characters",
        },
        countryCategoryModal: {
          required: "Please select Country",
          selectField: "Please select Country",
        },
        stateCategoryModal: {
          required: "Please select State",
          selectField: "Please select State",
        },
        cityCategoryModal: {
          required: "Please select City",
          selectField: "Please select City",
        },
        categorySearchFormModal: {
          required: "Please select category from drop down",
          dropdowncategory: "Please select category from drop down",
        },
      },
      errorElement: "div",
      errorPlacement: function (error, element) {
        error.addClass("invalid-feedback");
        element.closest(".form-group").append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass("is-invalid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass("is-invalid");
        $(element).addClass("is-valid");
      },
      submitHandler: (form) => {
        $(form).find(".spinner-border").show();
        $(form).find(".submit-text").html("Please wait");
        $(form).find('[type="submit"]').prop("disabled", true);
        let data = new FormData($(form)[0]);
        $.ajax({
          type: "post",
          url: `/categoryEmailModal?_csrf=${$("[name='_csrf']").val()}`,
          data: data,
          processData: false,
          contentType: false,
          success: function (response) {
            $(form).find(".spinner-border").hide();
            $(form).find(".submit-text").html("Submit");
            $(form).find('[type="submit"]').prop("disabled", false);
            $(form)[0].reset();
            // console.log(response);
            if (!!response) {
              $("#download-csv-button").prop(
                "href",
                "data:text/csv;charset=utf-8," +
                  encodeURI(
                    "Company Name,Company Email,Company Website,Company Phone Number\n" +
                      response
                  )
              );
              $("#download-csv-button").prop("target", "_blank");
              $("#download-csv-button").prop(
                "download",
                "Customer or Vendor list.csv"
              );
              $("#download-csv").modal("show");
            } else {
              $("#error-modal").modal("show");
              $("#error-text").html("No data found in these options");
            }
            // alert('Your message has been submitted');
          },
          error: function (a, b, c) {
            $("#error-modal").modal("show");
            $("#error-text").html(a.responseText);
            $(form).find(".spinner-border").hide();
            $(form).find(".submit-text").html("Submit");
            $(form).find('[type="submit"]').prop("disabled", false);
          },
        });
      },
    });
});

const initializeFriendsDataTable = (url) => {
  var oldStart = 0;
  $.fn.DataTable.ext.pager.numbers_length = 5;
  let table = $("#friends_table").DataTable({
    pageLength: 15,
    lengthChange: false,
    columnDefs: [
      { orderable: true, targets: 0 },
      { orderable: true, targets: 2 },
      { orderable: false, targets: "_all" },
    ],
    searching: true,
    processing: true,
    serverSide: true,
    ajax: {
      url,
    },
    dom:
      "<'row'<'col-sm-2 search'l><'col-sm-2 search-btn'f><'col-sm-8'p>>" +
      "<'row'<'col-sm-12'tr>>" +
      "<'row'<'col-sm-4'i><'col-sm-8'p>>",
    columns: [
      {
        data: "name",
        render: function (data, type, row, meta) {
          let { company_url, name } = row;
          return `<a href="/${company_url}" target="_blank">
                    ${name}
                  </a>`;
        },
      },
      {
        data: "locations",
        render: function (data, type, row, meta) {
          let locations = data.map(
            (location) =>
              `<p>${location.city_name}, ${location.state_name}, ${location.country_name}</p>`
          );
          return locations.join(",<br>");
        },
      },
      {
        data: "date",
        render: function (data, type, row, meta) {
          return `<p>${new Date(data).toDateString()}</p>`;
        },
      },
      {
        data: "delete",
        render: function (data, type, row, meta) {
          let { user1_id, user2_id } = row;
          return `<button type="button" class="btn btn-danger btn-sm"
                  onclick="deleteFriend('${user1_id}','${user2_id}',this)">Delete
                Friend</button>`;
        },
      },
    ],
    initComplete: function () {
      $("div.search").html(
        `<input type="number" placeholder="Page Number" class="form-control" style="height:30px" id="search-field-friends" min="0" max="${
          table.page.info().pages
        }">`
      );
      $("div.search-btn").html(
        '<button type="button" class="btn btn-sm btn-primary" id="search-btn">Jump</button>'
      );
      $("#search-btn").on("click", function () {
        table.page(parseInt($("#search-field-friends").val()) - 1).draw("page");
      });
    },
    fnDrawCallback: function (o) {
      if (o._iDisplayStart != oldStart) {
        $("html,body").animate(
          {
            scrollTop: 0,
          },
          100
        );
        oldStart = o._iDisplayStart;
      }
    },
  });
};

function populateFriends(url = "/friends/getFriends") {
  $(".friends_list").html(`
        <table id="friends_table"
        class="table table-striped table-bordered table-hover table-sm dataTable no-footer">
            <thead class="thead-dark">
                <th>Company Name</th>
                <th>Locations</th>
                <th>Friends From</th>
                <th>Delete</th>
            </thead>
            <ul class="list-group friends_list">
              <tbody>
              </tbody>
            </ul>
        </table>
    `);
  initializeFriendsDataTable(url);
}

function addAll(button) {
  const category = $("[name='keyCategoryForm']").val();
  const location = `${city_id},${state_id},${country_id}`;
  $(button).prop("disabled", true);
  $(button).html("Please wait .....");
  $.ajax({
    type: "get",
    url: "/on_boarding/add_suggestions_category",
    data: {
      category,
      location,
    },
    success: function (response) {
      window.location.reload();
    },
    error: function (textStatus, b, errorThrown) {
      $(button).prop("disabled", false);
      $(button).html("Add All");
      alert(textStatus.responseText);
    },
  });
}
