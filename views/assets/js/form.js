let logo_file;
let logo_name;
let file_show;
let result = "";
let brands = "";
let brandCount = 0;
let catCount = 0;
let locationCount = 0;
let files_row = [];

let aLocation = "";

$('#upload-logo').on('click',function(){
   $('#upload-logo-modal').modal('show'); 
});

$('#edit-logo').on('click',function(){
   $('#upload-logo-modal').modal('show'); 
});

function logoFileSelect(e) {
    if (!e.target.files || !window.FileReader) return;
    let files = e.target.files;
    let f = files[0];
    logo_file = f;
    logo_name = f.name;
    if (!f.type.match("image.*")) {
        return;
    }
    var reader = new FileReader();
    reader.onload = function (e) {
        fileShow = e.target.result;
        $("#formLogoPic").attr("src", e.target.result);
    }
    reader.readAsDataURL(f);
};

$("#attachment").on("change", logoFileSelect);

function charCount($inputtextarea,$type) {
	var $textarea = $($inputtextarea);
	var maxlength = $textarea.attr("maxlength");
	var length = $textarea.val().length;
	$textarea
		.siblings(".counter")
		.html(
			"Characters: Max " +
			maxlength +
			" | Used " +
			length +
			" | Remaining " +
			(maxlength - length)
		);
    console.log("yess");
    if ($type=='image') {
        if (lenght > 0) {
            $textarea
            .siblings(".badge")
            .html("10/10");
        }else{
            $textarea
            .siblings(".badge")
            .html("0/10");
        }
    }else{
        if (lenght >= 200) {
            $textarea
            .siblings(".badge")
            .html("10/10");
        }else{
            $textarea
            .siblings(".badge")
            .html("0/10");
        }
    }
    
}

$(document).ready(function () {
  $("#logo-form").submit(function (e) {
    e.preventDefault();
}).validate({
    rules: {
        attachment: {
            required: true
        }
    },
    messages: {
        attachment: {
            required: "Please select your company logo"
        }
    },
    errorPlacement: function (error, element) {
        $(error).insertAfter(element.parent());
    },
    submitHandler: function (form) {
        $("#company_logo").html("<img src='" + fileShow + "'>");
        $("#uploadDiv").css("display", "none");
        $("#editDiv").css("display", "block");
        $("#company_logo_name").html(logo_name);
        $('#upload-logo-modal').modal('hide');
    }
});  
});


function deleteLogo() {
    if (confirm("Want to delete the Logo!")) {
        $("#company_logo").html("");
        $("#editDiv").hide();
        $("#uploadDiv").show();
        $("#company_logo_name").html("");
        logo_file = null;
        logo_name = null;
        file_show = null;
        $("#formLogoPic").removeAttr('src');
    }
}

$('#remove-logo').on('click',function(){
    deleteLogo();
});
	
