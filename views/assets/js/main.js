$('#login-button').on('click', function () {
    $('#login').modal('show');
});

$('#forgot-password-button').on('click', function () {
    $('#login').modal('hide');
    $('#forgot-password').modal('show');
});

$(document).ready(function () {
    $('#login-form').submit(function (e) {
        e.preventDefault();
    }).validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
            }
        },
        messages: {
            email: {
                required: 'Please enter your email address',
                email: 'Please enter valid email address'
            },
            password: {
                required: 'Please enter the password',
            }
        },
        errorElement: 'div',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
            $(element).addClass('is-valid');
        }
    });

    $('#forgot-password-form').submit(function (e) {
        e.preventDefault();
    }).validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: {
                required: 'Please enter your email address',
                email: 'Please enter valid email address'
            }
        },
        errorElement: 'div',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
            $(element).addClass('is-valid');
        }
    });

    $('#report-form').submit(function (e) {
        e.preventDefault();
    }).validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            name: {
                minlength: 2,
                maxlength: 50
            },
            message: {
                required: true,
                maxlength: 500
            }
        },
        messages: {
            email: {
                required: 'Please enter your email address',
                email: 'Please enter valid email address'
            },
            name: {
                minlength: 'Please enter more than 2 characters',
                maxlength: 'Please do not enter more than 50 characters'
            },
            message: {
                required: 'Please enter your message',
                maxlength: 'Please do not enter more than 500 characters'
            }
        },
        errorElement: 'div',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
            $(element).addClass('is-valid');
        }
    });
});

$('#report-button').on('click', function () {
    $('#report').modal('show');
});