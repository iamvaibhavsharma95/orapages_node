
function locationValidate() {
    $('[name ^= "company_branch_country_"]').each(function (e) {
        $(this).rules('add', {
            selectField2: true,
            required: true,
            messages: {
                required: " Please select country"
            }
        });
    });

    $('[name ^= "company_branch_state_"]').each(function (e) {
        $(this).rules('add', {
            selectField2: true,
            required: true,
            messages: {
                required: " Please select state"
            }
        });
    });

    $('[name ^= "company_branch_city_"]').each(function (e) {
        $(this).rules('add', {
            selectField2: true,
            required: true,
            messages: {
                required: " Please select city"
            }
        });
    });

    $('[name ^= "company_branch_phone"]').each(function (e) {
        $(this).rules('add', {
            messages: {
                minlength: "Please enter atleast ten characters",
                maxlength: "Please do not enter more than 50 characters"
            }
        });
    });
    $('[name ^= "company_branch_email"]').each(function (e) {
        $(this).rules('add', {
            minlength: 2,
            required: true,
            email: true,
            messages: {
                required: "Please enter email",
                email: "Please enter valid email"
            }
        });
    });
    $('[name ^= "company_branch_url"]').each(function (e) {
        $(this).rules('add', {
            minlength: 5,
            url: true,
            messages: {
                url: "Please include http:// or https:// proceeding your website address",
                minlength: "Please enter more than 5 characters"
            }
        });
    });

    $('[name ^= "company_branch_address"]').each(function (e) {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Please enter full address"
            }
        });
    });
}


function categoryValidate() {
    $('[name ^= "company_category"]').each(function (e) {
        $(this).rules('add', {
            required: true,
            dropdowncategory: true,
            messages: {
                required: "Please enter the category"
            }
        });
    });

    $('[name ^= "company_des_category"]').each(function (e) {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Please enter the description of Product & Service"
            }
        });
    });
}


    

$(document).ready(function () {
    $.validator.addMethod('selectField2', function (value, element) {
        return !!value;
    }, 'Please select');
    $.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^\w+$/i.test(value);
    }, "Letters, numbers, and underscores only please");
$( "#company_registration_form" ).submit(function (e) {
        e.preventDefault();
}).validate( {
    // ignore: `:hidden:not(#company_business_details,[name^='company_branch_city_']
    //     ,[name^='company_branch_state_'],[name^='company_branch_country_'])`,
    rules: {
        company_name: {
            required: true,
            maxlength: 100
        },
        company_email: {
            required: true,
            email: true,
            remote: "/check_email"
        },
        company_password: {
            required: true,
            minlength: 7
        },
        company_password_confirm: {
            required: true,
            equalTo: "#company_password"
        },
        company_business_name: {
            required: true,
            maxlength: 100,
            remote: "/check_name"
        },
        company_url: {
            required: true,
            minlength: 3,
            alphanumeric:true,
            remote: "/check_url"
        },
        company_business_details: {
            required: true
        },
        agreeTerms: {
            required: true
        }
    },
    messages: {
        company_name: {
            required: "Please enter company name",
            maxlength: "Please enter less than 100 characters"
        },
        company_email: {
            required: "Please enter email",
            email: "Please enter valid email address",
            remote: "This email address has been taken"
        },
        company_password: {
            required: "Please enter password",
            minlength: "Password should not less than 7 characters"
        },
        company_password_confirm: {
            required : "Please confirm the password",
            equalTo: "Please enter same as password"
        },
        company_business_name: {
            required: "Please enter company name",
            maxlength: "Please enter less than 100 characters"
        },
        company_url: {
            required: "Please enter a word as explained above",
            minlength: "Please enter more than 3 characters",
            remote: "This word is already taken",
            alphanumeric:"Please use the word without any special character"
        },
        company_business_details: {
            required: "Please enter details of the company"
        },
        agreeTerms: {
            required: "Please agree terms and conditions to submit profile"
        }
    },
    errorElement: 'div',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
        $(element).addClass('is-valid');
    }
});
    
    
    locationValidate();
    categoryValidate();
});