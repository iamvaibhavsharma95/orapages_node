const createError = require("http-errors");
const csurf = require("csurf");

const { app, fse } = require("./modules");

const { home_auth } = require("./middlewares/owner_auth");

const FrontEndOperations = require("./database/front_end");
const { transporter } = require("./database/email");

const csrfProtection = csurf({
  cookie: true,
});

let port = process.env.PORT || 7070;

app.get("/frontEndLocations", async (req, res, next) => {
  try {
    const results = await FrontEndOperations.frontEndLocations(req.query.term);
    var i = 0;
    while (i < results.length) {
      var citystatecountry = results[i].value.split("#$%");
      if (citystatecountry.length == 3) {
        if (
          citystatecountry[2] == citystatecountry[1] &&
          citystatecountry[1] == citystatecountry[0]
        ) {
          delete results[i];
        } else if (citystatecountry[0] == citystatecountry[1]) {
          results[i].value = citystatecountry[2] + ", " + citystatecountry[1];
        } else if (citystatecountry[2] == citystatecountry[1]) {
          delete results[i];
        } else {
          results[i].value = citystatecountry.reverse().join(", ");
        }
      } else if (citystatecountry.length == 2) {
        if (citystatecountry[1] == citystatecountry[0]) {
          delete results[i];
        } else {
          results[i].value = citystatecountry.reverse().join(", ");
        }
      } else {
        results[i].value = citystatecountry.reverse().join(", ");
      }
      i++;
    }
    res.status(200).json(results);
  } catch (e) {
    res.status(404).json([
      {
        key: "",
        value: "No data found",
      },
    ]);
  }
});

app.get("/frontEndCountries", async (req, res, next) => {
  try {
    let countries;

    if (!!req.query.home) {
      countries = await FrontEndOperations.frontEndCountriesHome();
    } else {
      countries = await FrontEndOperations.frontEndCountries();
    }

    res.status(200).json(countries);
  } catch (e) {
    res.status(400).json([
      {
        key: "",
        value: "No Country found",
      },
    ]);
  }
});

app.get("/frontEndStates", async (req, res, next) => {
  try {
    let states;

    if (!!req.query.home) {
      states = await FrontEndOperations.frontEndStatesHome(req.query.country);
    } else {
      states = await FrontEndOperations.frontEndStates(req.query.country);
    }
    res.status(200).json(states);
  } catch (e) {
    res.status(400).json([
      {
        key: "",
        value: "No State found",
      },
    ]);
  }
});

app.get("/frontEndCities", async (req, res, next) => {
  try {
    let cities;
    if (!!req.query.home) {
      cities = await FrontEndOperations.frontEndCitiesHome(
        req.query.state,
        req.query.country
      );
    } else {
      cities = await FrontEndOperations.frontEndCities(
        req.query.state,
        req.query.country
      );
    }

    res.status(200).json(cities);
  } catch (e) {
    res.status(400).json([
      {
        key: "",
        value: "No City found",
      },
    ]);
  }
});

app.get("/", csrfProtection, home_auth, async (req, res, next) => {
  try {
    const banners = await FrontEndOperations.getBanners();
    // const premiumCompanies = await frontEndPremiumCompanies();
    const premiumCompanies = [];
    const countries = await FrontEndOperations.frontEndCountriesHome();
    return res.render("frontend_new/index_banners", {
      owner: req.owner,
      banners: banners,
      csrfToken: req.csrfToken(),
      premiumCompanies,
      production: process.env.PRODUCTION !== "false",
      countries,
    });
  } catch (error) {
    next(error);
  }
});

app.get("/:url/post", csrfProtection, home_auth, async (req, res, next) => {
  try {
    const company = await FrontEndOperations.companyByUrl(req.params.url);
    company.owner = req.owner;
    if (!!company.user_id && company.user_id == req.user_id) {
      company.owner_this = true;
      company.csrfToken = req.csrfToken();
    } else {
      company.owner_this = false;
      company.csrfToken = req.csrfToken();
    }
    company.production = process.env.PRODUCTION !== "false";
    res.render("profile/full_profile", {
      company,
    });
  } catch (error) {
    next(error);
  }
});

app.get(
  "/country/:country",
  csrfProtection,
  home_auth,
  async (req, res, next) => {
    try {
      const country_name_short = req.params.country;
      const states = await FrontEndOperations.frontEndStatesHome(
        country_name_short
      );
      return res.render("frontend_new/states", {
        owner: req.owner,
        csrfToken: req.csrfToken(),
        production: process.env.PRODUCTION !== "false",
        states,
        country_name_short,
      });
    } catch (error) {
      next(error);
    }
  }
);

app.get(
  "/:country/:state",
  csrfProtection,
  home_auth,
  async (req, res, next) => {
    try {
      const country_name_short = req.params.country;
      const state_name_short = req.params.state;
      const cities = await FrontEndOperations.frontEndCitiesHomePage(
        state_name_short,
        country_name_short
      );
      if (cities.length <= 0) {
        throw createError(400);
      }
      return res.render("frontend_new/cities", {
        owner: req.owner,
        csrfToken: req.csrfToken(),
        production: process.env.PRODUCTION !== "false",
        cities,
        country_name_short,
        state_name_short,
      });
    } catch (error) {
      next(error);
    }
  }
);

app.get(
  "/premium_profiles",
  csrfProtection,
  home_auth,
  async (req, res, next) => {
    try {
      const premiumCompanies =
        await FrontEndOperations.frontEndPremiumCompanies();
      return res.render("frontend_new/premium_show_case", {
        owner: req.owner,
        csrfToken: req.csrfToken(),
        premiumCompanies,
      });
    } catch (error) {
      next(error);
    }
  }
);

app.get("/getSearchCompanies", async function (req, res, next) {
  try {
    const {
      type,
      location,
      term,
      start,
      length,
      draw,
      order = [
        {
          dir: "asc",
        },
      ],
    } = req.query;
    let [{ dir }] = order;
    const { companies, count } =
      await FrontEndOperations.frontEndCompanyByLocationList(
        type,
        term,
        location,
        length,
        start,
        dir
      );

    res.send({
      draw,
      recordsTotal: count,
      recordsFiltered: count,
      data: companies,
    });
  } catch (error) {
    next(error);
  }
});

app.get("/getSearchCategories", async function (req, res, next) {
  try {
    const {
      type,
      location,
      term,
      start,
      length,
      draw,
      order = [
        {
          dir: "asc",
        },
      ],
    } = req.query;
    let [{ dir }] = order;

    const { categories, count } =
      await FrontEndOperations.frontEndCategoriesList(
        term,
        type,
        location,
        length,
        start,
        dir
      );

    res.send({
      draw,
      recordsTotal: count,
      recordsFiltered: count,
      data: categories,
    });
  } catch (error) {
    next(error);
  }
});

app.get("/getSearchCompaniesBycategory", async function (req, res, next) {
  try {
    const {
      category,
      location,
      type,
      start,
      length,
      draw,
      order = [
        {
          dir: "asc",
        },
      ],
    } = req.query;
    let [{ dir }] = order;
    const { companies, count } =
      await FrontEndOperations.frontEndCompanyListByCategory(
        category,
        location,
        type,
        start,
        length,
        dir
      );
    res.send({
      draw,
      recordsTotal: count,
      recordsFiltered: count,
      data: companies,
    });
  } catch (error) {
    next(error);
  }
});

app.get(
  "/search_results",
  csrfProtection,
  home_auth,
  async function (req, res, next) {
    try {
      if (req.query.filter_type == "company") {
        res.render("frontend_new/user_search_datatable", {
          location_type: req.query.type,
          type: "company",
          location_name: req.query.location_name,
          keyword: req.query.keyword,
          location_key: req.query.location_key,
          owner: req.owner,
          csrfToken: req.csrfToken(),
        });
      } else if (req.query.filter_type == "services") {
        res.render("frontend_new/user_search_datatable", {
          location_type: req.query.type,
          type: "services",
          location_name: req.query.location_name,
          keyword: req.query.keyword,
          location_key: req.query.location_key,
          owner: req.owner,
          csrfToken: req.csrfToken(),
        });
      } else if (req.query.filter_type == "brands") {
        const brands = await FrontEndOperations.frontEndBrandsList(
          req.query.keyword,
          req.query.type,
          req.query.location_key
        );

        res.render("frontend_new/user_search_node", {
          location_type: req.query.type,
          type: "brands",
          brands,
          location_name: req.query.location_name,
          keyword: req.query.keyword,
          location_key: req.query.location_key,
          owner: req.owner,
          csrfToken: req.csrfToken(),
        });
      }
    } catch (error) {
      next(error);
    }
  }
);

app.get(
  "/company-browse/:country/:keyword/page-:page",
  csrfProtection,
  home_auth,
  async function (req, res, next) {
    try {
      const {
        type = "country",
        country,
        keyword,
        length = 100,
        draw,
        order = [
          {
            dir: "asc",
          },
        ],
        page,
      } = req.params;
      let [{ dir }] = order;

      const start = (page - 1) * 100;

      let location = await FrontEndOperations.countryByName(country);

      if (location.length <= 0) {
        throw createError(400);
      }

      let locationName = location[0].country_name;

      location = location[0].country_id;

      const { companies, count } =
        await FrontEndOperations.frontEndCompanyByLocationList(
          type,
          keyword,
          location,
          length,
          start,
          dir
        );

      res.render("frontend_new/user_search_datatable_browse_company", {
        type: "company",
        page,
        country,
        state: "",
        city: "",
        location_name: locationName,
        keyword: req.params.keyword,
        owner: req.owner,
        csrfToken: req.csrfToken(),
        draw,
        location_name: locationName,
        recordsTotal: count,
        recordsFiltered: count,
        data: companies,
        production: process.env.PRODUCTION !== "false",
      });
    } catch (error) {
      next(error);
    }
  }
);

app.get(
  "/company-browse/:country/:state/:keyword/page-:page",
  csrfProtection,
  home_auth,
  async function (req, res, next) {
    try {
      const {
        type = "state",
        country,
        state,
        keyword,
        length = 100,
        draw,
        order = [
          {
            dir: "asc",
          },
        ],
        page,
      } = req.params;
      let [{ dir }] = order;

      const start = (page - 1) * 100;

      let location = await FrontEndOperations.stateByName(state, country);

      if (location.length <= 0) {
        throw createError(400);
      }

      let locationName = location[0].state_name;

      location = location[0].state_id;

      const { companies, count } =
        await FrontEndOperations.frontEndCompanyByLocationList(
          type,
          keyword,
          location,
          length,
          start,
          dir
        );

      res.render("frontend_new/user_search_datatable_browse_company", {
        type: "company",
        page: req.query.page,
        country,
        state,
        city: "",
        location_name: locationName,
        keyword: req.params.keyword,
        owner: req.owner,
        csrfToken: req.csrfToken(),
        draw,
        location_name: locationName,
        recordsTotal: count,
        recordsFiltered: count,
        data: companies,
        production: process.env.PRODUCTION !== "false",
      });
    } catch (error) {
      next(error);
    }
  }
);

app.get(
  "/company-browse/:country/:state/:city/:keyword/page-:page",
  csrfProtection,
  home_auth,
  async function (req, res, next) {
    try {
      const {
        type = "city",
        country,
        state,
        city,
        keyword,
        length = 100,
        draw,
        order = [
          {
            dir: "asc",
          },
        ],
        page,
      } = req.params;
      let [{ dir }] = order;

      const start = (page - 1) * 100;

      let location = await FrontEndOperations.cityByName(city, state, country);

      if (location.length <= 0) {
        throw createError(400);
      }

      let locationName = location[0].city_name;

      location = location[0].city_id;

      const { companies, count } =
        await FrontEndOperations.frontEndCompanyByLocationList(
          type,
          keyword,
          location,
          length,
          start,
          dir
        );

      res.render("frontend_new/user_search_datatable_browse_company", {
        type: "company",
        page,
        country,
        state,
        city,
        location_name: locationName,
        keyword: req.params.keyword,
        owner: req.owner,
        csrfToken: req.csrfToken(),
        draw,
        location_name: locationName,
        recordsTotal: count,
        recordsFiltered: count,
        data: companies,
        production: process.env.PRODUCTION !== "false",
      });
    } catch (error) {
      next(error);
    }
  }
);

app.get(
  "/ps/:country/:category/page-:page",
  csrfProtection,
  home_auth,
  async function (req, res, next) {
    try {
      const {
        category,
        country,
        type = "country",
        length = 100,
        draw,
        order = [
          {
            dir: "asc",
          },
        ],
        page,
      } = req.params;

      const start = (page - 1) * 100;

      let location = await FrontEndOperations.countryByName(country);

      if (location.length <= 0) {
        throw createError(400);
      }

      let locationName = location[0].country_name;

      let { city_id = null, state_id = null, country_id } = location[0];

      location = location[0].country_id;

      let cat = await FrontEndOperations.categoryByName(category);

      if (cat.length <= 0) {
        throw createError(400);
      }

      let category_name = cat[0].category_name;

      cat = cat[0].category_id;

      let production = process.env.PRODUCTION !== "false";

      if (cat == "category-1119") {
        production = false;
      }

      const subcats = await FrontEndOperations.frontEndSubcategoriesByCategory(
        cat
      );

      const nonDissubcats =
        await FrontEndOperations.frontEndNonDisSubcategoriesByCategory(cat);

      const allSubcat = [...subcats, ...nonDissubcats];

      const subcategories = allSubcat.map((e) => e.subcategory_name);

      let [{ dir }] = order;

      const { companies, count, relatedCompanies, relatedCategories } =
        await FrontEndOperations.frontEndCompanyListByCategory(
          cat,
          location,
          type,
          start,
          length,
          dir,
          city_id,
          state_id,
          country_id
        );

      res.render("frontend_new/user_search_datatable_browse_category_company", {
        type: "categories_results",
        draw,
        category,
        subcategories,
        page,
        category_name,
        location_name: locationName,
        recordsTotal: count,
        country,
        state: "",
        city: "",
        owner: req.owner,
        csrfToken: req.csrfToken(),
        recordsFiltered: count,
        data: companies,
        production,
        relatedCompanies,
        relatedCategories,
      });
    } catch (error) {
      next(error);
    }
  }
);

app.get(
  "/ps/:country/:state/:category/page-:page",
  csrfProtection,
  home_auth,
  async function (req, res, next) {
    try {
      const {
        category,
        country,
        state,
        type = "state",
        length = 100,
        draw,
        order = [
          {
            dir: "asc",
          },
        ],
        page,
      } = req.params;

      const start = (page - 1) * 100;

      let location = await FrontEndOperations.stateByName(state, country);

      if (location.length <= 0) {
        throw createError(400);
      }

      let { city_id = null, state_id, country_id } = location[0];

      let locationName = location[0].state_name;

      location = location[0].state_id;

      let cat = await FrontEndOperations.categoryByName(category);

      if (cat.length <= 0) {
        throw createError(400);
      }

      let category_name = cat[0].category_name;

      cat = cat[0].category_id;

      let production = process.env.PRODUCTION !== "false";

      if (cat == "category-1119") {
        production = false;
      }

      const subcats = await FrontEndOperations.frontEndSubcategoriesByCategory(
        cat
      );

      const nonDissubcats =
        await FrontEndOperations.frontEndNonDisSubcategoriesByCategory(cat);

      const allSubcat = [...subcats, ...nonDissubcats];

      const subcategories = allSubcat.map((e) => e.subcategory_name);

      let [{ dir }] = order;

      const { companies, count, relatedCompanies, relatedCategories } =
        await FrontEndOperations.frontEndCompanyListByCategory(
          cat,
          location,
          type,
          start,
          length,
          dir,
          city_id,
          state_id,
          country_id
        );

      res.render("frontend_new/user_search_datatable_browse_category_company", {
        type: "categories_results",
        draw,
        category,
        subcategories,
        page,
        category_name,
        location_name: locationName,
        recordsTotal: count,
        country,
        state,
        city: "",
        owner: req.owner,
        csrfToken: req.csrfToken(),
        recordsFiltered: count,
        data: companies,
        production,
        relatedCompanies,
        relatedCategories,
      });
    } catch (error) {
      next(error);
    }
  }
);

app.get(
  "/ps/:country/:state/:city/:category/page-:page",
  csrfProtection,
  home_auth,
  async function (req, res, next) {
    try {
      const {
        category,
        country,
        state,
        city,
        type = "city",
        length = 100,
        draw,
        order = [
          {
            dir: "asc",
          },
        ],
        page,
      } = req.params;

      const start = (page - 1) * 100;

      let location = await FrontEndOperations.cityByName(city, state, country);

      if (location.length <= 0) {
        throw createError(400);
      }

      let { country_id, state_id, city_id } = location[0];

      let locationName = location[0].city_name;

      location = location[0].city_id;

      let cat = await FrontEndOperations.categoryByName(category);

      if (cat.length <= 0) {
        throw createError(400);
      }

      let category_name = cat[0].category_name;

      cat = cat[0].category_id;

      let production = process.env.PRODUCTION !== "false";

      if (cat == "category-1119") {
        production = false;
      }

      const subcats = await FrontEndOperations.frontEndSubcategoriesByCategory(
        cat
      );

      const nonDissubcats =
        await FrontEndOperations.frontEndNonDisSubcategoriesByCategory(cat);

      const allSubcat = [...subcats, ...nonDissubcats];

      const subcategories = allSubcat.map((e) => e.subcategory_name);

      let [{ dir }] = order;

      const { companies, count, relatedCompanies, relatedCategories } =
        await FrontEndOperations.frontEndCompanyListByCategory(
          cat,
          location,
          type,
          start,
          length,
          dir,
          city_id,
          state_id,
          country_id
        );

      res.render("frontend_new/user_search_datatable_browse_category_company", {
        type: "categories_results",
        draw,
        category,
        subcategories,
        page,
        category_name,
        location_name: locationName,
        recordsTotal: count,
        country,
        state,
        city,
        owner: req.owner,
        csrfToken: req.csrfToken(),
        recordsFiltered: count,
        data: companies,
        production,
        relatedCompanies,
        relatedCategories,
      });
    } catch (e) {
      next(e);
    }
  }
);

app.get(
  "/ps-browse/:country/:keyword/page-:page",
  csrfProtection,
  home_auth,
  async function (req, res, next) {
    try {
      const {
        country,
        keyword,
        length = 100,
        draw = 1,
        order = [
          {
            dir: "asc",
          },
        ],
        page,
      } = req.params;

      const start = (page - 1) * 100;
      let [{ dir }] = order;

      const { categories, count, location_name } =
        await FrontEndOperations.frontEndCategoriesList(
          keyword,
          (city = null),
          (state = null),
          country,
          length,
          start,
          dir
        );

      res.render("frontend_new/user_search_datatable_browse_category", {
        type: "services",
        page,
        country,
        location_name,
        keyword: req.params.keyword,
        owner: req.owner,
        csrfToken: req.csrfToken(),
        data: categories,
        draw,
        recordsTotal: count,
        recordsFiltered: count,
        production: process.env.PRODUCTION !== "false",
      });
    } catch (error) {
      next(error);
    }
  }
);

app.get(
  "/ps-browse/:country/:state/:keyword/:page-:page",
  csrfProtection,
  home_auth,
  async function (req, res, next) {
    try {
      const {
        country,
        state,
        keyword,
        length = 100,
        draw = 1,
        page,
        order = [
          {
            dir: "asc",
          },
        ],
      } = req.params;

      const start = (page - 1) * 100;
      let [{ dir }] = order;

      const { categories, count, location_name } =
        await FrontEndOperations.frontEndCategoriesList(
          keyword,
          (city = null),
          state,
          country,
          length,
          start,
          dir
        );

      res.render("frontend_new/user_search_datatable_browse_category", {
        type: "services",
        page,
        city,
        state,
        country,
        location_name,
        keyword: req.params.keyword,
        owner: req.owner,
        csrfToken: req.csrfToken(),
        data: categories,
        draw,
        recordsTotal: count,
        recordsFiltered: count,
        production: process.env.PRODUCTION !== "false",
      });
    } catch (error) {
      next(error);
    }
  }
);

app.get(
  "/ps-browse/:country/:state/:city/:keyword/page-:page",
  csrfProtection,
  home_auth,
  async function (req, res, next) {
    try {
      const {
        country,
        state,
        city,
        keyword,
        length = 100,
        draw = 1,
        page,
        order = [
          {
            dir: "asc",
          },
        ],
      } = req.params;

      const start = (page - 1) * 100;
      let [{ dir }] = order;

      const { categories, count, location_name } =
        await FrontEndOperations.frontEndCategoriesList(
          keyword,
          city,
          state,
          country,
          length,
          start,
          dir
        );

      res.render("frontend_new/user_search_datatable_browse_category", {
        type: "services",
        page,
        city,
        state,
        country,
        location_name,
        keyword: req.params.keyword,
        owner: req.owner,
        csrfToken: req.csrfToken(),
        data: categories,
        draw,
        recordsTotal: count,
        recordsFiltered: count,
        production: process.env.PRODUCTION !== "false",
      });
    } catch (error) {
      next(error);
    }
  }
);

app.get(
  "/:country/:state/:city/page-:page",
  csrfProtection,
  home_auth,
  async function (req, res, next) {
    try {
      const {
        country,
        state,
        city,
        length = 100,
        draw = 1,
        page,
        order = [
          {
            dir: "asc",
          },
        ],
      } = req.params;

      const start = (page - 1) * 100;
      let [{ dir }] = order;

      const { categories, count, location_name } =
        await FrontEndOperations.frontEndCategoriesListWithOutKeyWord(
          city,
          state,
          country,
          length,
          start,
          dir
        );

      res.render("frontend_new/user_search_datatable_browse_category_wo_key", {
        type: "services",
        page,
        city,
        state,
        country,
        location_name,
        owner: req.owner,
        csrfToken: req.csrfToken(),
        data: categories,
        draw,
        recordsTotal: count,
        recordsFiltered: count,
        production: process.env.PRODUCTION !== "false",
      });
    } catch (error) {
      next(error);
    }
  }
);

app.get("/brands_results", home_auth, async (req, res, next) => {
  try {
    const companyList = await FrontEndOperations.frontEndCompanyListByBrand(
      req.query.brand,
      req.query.location_key,
      req.query.type
    );

    return res.status(200).render("frontend/user_search_node", {
      type: "brands_results",
      companies: companyList,
      owner: req.owner,
    });
  } catch (error) {
    next(error);
  }
});

app.get(
  "/categories_results",
  csrfProtection,
  home_auth,
  async (req, res, next) => {
    try {
      const cat = await FrontEndOperations.categoryById(req.query.category);

      if (cat.length <= 0) {
        throw createError(400);
      }

      const subcats = await FrontEndOperations.frontEndSubcategoriesByCategory(
        req.query.category
      );

      let subcategories = subcats.map((e) => e.subcategory_name);

      return res.status(200).render("frontend_new/user_search_datatable", {
        type: "categories_results",
        location_type: req.query.type,
        category: cat[0].category_name,
        category_id: req.query.category,
        subcategories,
        location_key: req.query.location_key,
        owner: req.owner,
        keyword: "",
        csrfToken: req.csrfToken(),
      });
    } catch (error) {
      next(error);
    }
  }
);

app.get("/categories_results_select", async (req, res, next) => {
  try {
    const category = await FrontEndOperations.categoryById(req.query.category);

    const subcategories =
      await FrontEndOperations.frontEndSubcategoriesByCategory(
        req.query.category
      );

    const non_dis_subcategories =
      await FrontEndOperations.frontEndNonDisplaySubcategoriesByCategory(
        req.query.category
      );

    res.json({
      category,
      subcategories,
      non_dis_subcategories,
    });
  } catch (error) {
    next(error);
  }
});

app.get("/companiesName", async (req, res, next) => {
  try {
    const r = await FrontEndOperations.frontEndCompanyByLocation(
      req.query.type,
      req.query.term,
      req.query.location
    );
    if (r.length <= 0) {
      throw new Error("No companies found");
    }
    res.status(200).json(r);
  } catch (e) {
    res.status(404).json([
      {
        key: "",
        value: "",
        label: "No companies found",
      },
    ]);
  }
});

app.get("/onBoardingCompaniesName", async (req, res, next) => {
  try {
    const names = await FrontEndOperations.getCompaniesName(req.query.term);
    res.send(names);
  } catch (error) {
    res.send({
      label: "no results found",
    });
  }
});

app.get("/brandName", async (req, res, next) => {
  try {
    const r = await FrontEndOperations.frontEndBrands(req.query.term);
    if (r.length <= 0) {
      throw new Error("No brand found");
    }
    res.status(200).json(r);
  } catch (e) {
    res.status(404).json([
      {
        key: "",
        value: "",
        label: "No brand found",
      },
    ]);
  }
});

app.get("/locationsform", async (req, res, next) => {
  try {
    const r = await FrontEndOperations.formLocations(req.query.term);
    if (r.length <= 0) {
      throw new Error(`No location found`);
    }
    r.forEach((rl) => {
      let x = rl.value.split("#$%");
      if (x[0] == x[1] && x[1] == x[2]) {
        rl.value = x[0];
      } else if (x[1] == x[2]) {
        rl.value = x[0] + ", " + x[1];
      } else if (x[0] == x[1]) {
        rl.value = x[1] + ", " + x[2];
      } else {
        rl.value = `${x[0]}, ${x[1]}, ${x[2]}`;
      }
    });
    res.status(200).send(r);
  } catch (e) {
    res.status(404).send([
      {
        key: "",
        value: "",
        label: `No location found`,
      },
    ]);
  }
});

app.get("/categoryName", async (req, res, next) => {
  try {
    let r;
    if (!!req.query.home) {
      r = await FrontEndOperations.frontEndCategoriesStartsHome(req.query.term);
    } else {
      r = await FrontEndOperations.frontEndCategoriesStarts(req.query.term);
    }
    if (r.length <= 0) {
      throw new Error(`No category found`);
    }
    res.status(200).json(r);
  } catch (e) {
    res.status(404).json([
      {
        key: "",
        value: "",
        label: `No category found`,
      },
    ]);
  }
});

app.get("/browse", (req, res, next) => {
  res.render("frontend/browse", {
    flag: req.query.action,
  });
});

app.get("/check_url", async (req, res, next) => {
  try {
    const r = await FrontEndOperations.checkUrl(
      req.query.company_url,
      req.query.company_key
    );

    if (r.length > 0) {
      res.send('""');
    } else {
      res.send('"true"');
    }
  } catch (e) {
    res.send('""');
  }
});

app.get("/check_url_button", async (req, res, next) => {
  try {
    const r = await FrontEndOperations.checkUrl(
      req.query.company_url,
      req.query.company_key
    );

    if (r.length > 0) {
      res.status(400).send("This url is already taken");
    } else {
      res.status(200).send("This url is not taken");
    }
  } catch (e) {
    res.status(400).send("This url is already taken");
  }
});

app.get("/check_email", async (req, res, next) => {
  try {
    const r = await FrontEndOperations.checkEmail(req.query.company_email);

    if (r.length > 0) {
      res.send('""');
    } else {
      res.send('"true"');
    }
  } catch (e) {
    res.send('""');
  }
});

app.get("/check_name", async (req, res, next) => {
  try {
    const r = await FrontEndOperations.checkName(
      req.query.company_business_name
    );

    if (r.length > 0) {
      res.send('""');
    } else {
      res.send('"true"');
    }
  } catch (e) {
    res.send('""');
  }
});

app.get("/feedback", (req, res, next) => {
  res.render("frontend/feedback", {
    csrfToken: req.csrfToken(),
  });
});

app.post("/feedback", csrfProtection, (req, res, next) => {
  let {
    nm_cust = "Unknown",
    tx_cust_email,
    tx_message,
    contact_form_type,
  } = req.body;

  let mailOptions = {
    from: '"Orapages " <info@orapages.com>',
    to: "info@orapages.com",
    replyTo: tx_cust_email,
    subject: contact_form_type,
    bcc: "boby.sharma.vaibhav@gmail.com",
    html: `<div style="FONT:10pt tahoma">
                <div style="background:#f5f5f5;padding:20px" >
                    <div><b>Form Type: </b>${contact_form_type}</div>
                    <div><b>Name: </b>${nm_cust}</div>
                    <div><b>Email: </b>${tx_cust_email}</div>
                </div>
                <div>&nbsp;</div>
                <div style="padding:20px">
                ${tx_message.replace(/\n/g, "<br>")}
                </div>
            </div>`,
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      res.send(error);
    } else {
      res.send("Your feedback is submitted");
    }
  });
});

app.get(
  "/about-orapages",
  csrfProtection,
  home_auth,
  async (req, res, next) => {
    res.render("frontend_new/about-us", {
      owner: req.owner,
      csrfToken: req.csrfToken(),
      production: process.env.PRODUCTION !== "false",
    });
  }
);

app.get("/why-orapages", csrfProtection, home_auth, async (req, res, next) => {
  res.render("frontend_new/why-ora", {
    owner: req.owner,
    csrfToken: req.csrfToken(),
    production: process.env.PRODUCTION !== "false",
  });
});

app.get(
  "/terms-conditions",
  csrfProtection,
  home_auth,
  async (req, res, next) => {
    res.render("frontend_new/t-c", {
      owner: req.owner,
      csrfToken: req.csrfToken(),
      production: process.env.PRODUCTION !== "false",
    });
  }
);

app.get(
  "/privacy-policy",
  csrfProtection,
  home_auth,
  async (req, res, next) => {
    res.render("frontend_new/privacy-policy", {
      owner: req.owner,
      csrfToken: req.csrfToken(),
      production: process.env.PRODUCTION !== "false",
    });
  }
);

app.get("/disclaimer", csrfProtection, home_auth, async (req, res, next) => {
  res.render("frontend_new/disclaimer", {
    owner: req.owner,
    csrfToken: req.csrfToken(),
    production: process.env.PRODUCTION !== "false",
  });
});

app.get(
  "/tags/:tag/page-:page",
  csrfProtection,
  home_auth,
  async function (req, res, next) {
    try {
      const {
        tag,
        length = 100,
        draw,
        order = [
          {
            dir: "asc",
          },
        ],
        page = 1,
      } = req.params;

      const start = (page - 1) * 100;

      let cat = await FrontEndOperations.tagByName(tag);

      if (cat.length <= 0) {
        throw createError(400);
      }

      let tag_name = cat[0].tag;

      cat = cat[0].tag_id;

      let [{ dir }] = order;

      const { companies, count } =
        await FrontEndOperations.frontEndCompanyListByTag(
          cat,
          start,
          length,
          dir
        );

      res.render("frontend_new/user_search_datatable_browse_tag_company", {
        type: "tags_results",
        draw,
        tag,
        page,
        tag_name,
        recordsTotal: count,
        owner: req.owner,
        csrfToken: req.csrfToken(),
        recordsFiltered: count,
        data: companies,
        production: process.env.PRODUCTION !== "false",
      });
    } catch (error) {
      next(error);
    }
  }
);

app.get("/:url?", csrfProtection, home_auth, async (req, res, next) => {
  try {
    const company = await FrontEndOperations.companyByUrl(req.params.url);
    company.owner = req.owner;
    if (!!company.user_id && company.user_id == req.user_id) {
      company.owner_this = true;
      company.csrfToken = req.csrfToken();
    } else {
      company.owner_this = false;
      company.csrfToken = req.csrfToken();
    }
    company.production = process.env.PRODUCTION !== "false";
    for (const category of company.categories) {
      if (category.category_id == "category-1119") {
        company.production = false;
        break;
      }
    }
    res.render("profile/full_profile", {
      company,
    });
  } catch (e) {
    if (e.message == "no_company_found") {
      return next();
    }
    next(new Error(e.message));
  }
});

app.use(function (req, res, next) {
  let company = {};
  company.owner = req.owner;
  company.csrfToken = req.csrfToken();
  company.production = process.env.PRODUCTION !== "false";
  res.status(404).render("frontend_new/profile_not_found", {
    company,
  });
});

app.use(function (err, req, res, next) {
  if (err.status == 400) {
    let company = {};
    company.owner = req.owner;
    company.csrfToken = req.csrfToken();
    company.production = process.env.PRODUCTION !== "false";
    return res.status(400).render("frontend_new/profile_not_found", {
      company,
    });
  }

  // let mailOptions = {
  //   from: '"Orapages " <info@orapages.com>',
  //   to: "boby.sharma.vaibhav@gmail.com, bms1608@gmail.com",
  //   subject: `[${new Date()}] Orapages Error`,
  //   html: `<div>
  //           <h1 style="color:red">${err.message}</h1>
  //           <pre>${err.stack}</pre>
  //         </div>`,
  // };

  fse.appendFileSync(
    "website_errors.log",
    `--------------------------------------------------------
    ${req.url}\n ${err.message} \n ${err.stack} \n\n
    ---------------------------------------------------------`
  );

  // transporter.sendMail(mailOptions, function (error, info) {
  //   if (error) {
  //     res.send(error);
  //   }
  // });

  if (process.env.production !== "true") {
    console.log(err);
  }

  res.status(err.status || 500).send(err.message);
});

app.listen(port, () => console.log(`Orapages started listning on ${port}`));
