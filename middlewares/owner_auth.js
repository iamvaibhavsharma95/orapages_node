const jwt = require('jsonwebtoken');
const {
    selectPromise,
    queryPromise
} = require('./../database/database');

const owner_auth = async (req, res, next) => {
    try {
        const decoded = jwt.verify(req.session.user_token, 'orpagesjsonwebtoken');
        const user = await selectPromise(`select user_id,token from user_token where token = '${req.session.user_token}' and user_id = '${decoded.user_id}' limit 1`);
        if (user.length >= 0) {
            req.user_id = decoded.user_id;
            req.owner = true;
            next();
        } else {
            throw new Error('Unable to login');
        }
    } catch (e) {
        req.session.destroy();
        req.owner = false;
        res.redirect('/');
    }
}

const owner_auth_ajax = async (req, res, next) => {
    try {
        const decoded = jwt.verify(req.session.user_token, 'orpagesjsonwebtoken');
        const user = await selectPromise(`select user_id,token from user_token where token = '${req.session.user_token}' and user_id = '${decoded.user_id}' limit 1`);
        if (user.length >= 0) {
            req.user_id = decoded.user_id;
            req.owner = true;
            next();
        }
    } catch (e) {
        req.session.destroy();
        req.owner = false;
        res.status(500).send("There is some problem");
    }
}


const owner_auth_ajax_post = async (req, res, next) => {
    try {
        const decoded = jwt.verify(req.session.user_token, 'orpagesjsonwebtoken');
        const user = await selectPromise(`select user_id,token from user_token where token = '${req.session.user_token}' and user_id = '${decoded.user_id}' limit 1`);
        if (user.length >= 0) {
            req.user_id = decoded.user_id;
            req.owner = true;
            next();
        }
    } catch (e) {
        req.session.destroy();
        req.owner = false;
        req.user_id = '';
        next();
    }
}

const home_auth = async (req, res, next) => {
    try {
        const decoded = jwt.verify(req.session.user_token, 'orpagesjsonwebtoken');
        const user = await selectPromise(`select user_id,token from user_token where token = '${req.session.user_token}' and user_id = '${decoded.user_id}' limit 1`);
        if (user.length <= 0) {
            req.owner = false;
            next();
        } else {
            req.owner = true;
            req.user_id = decoded.user_id;
            next();
        }
    } catch (e) {
        req.owner = false;
        next();
    }
}

const owner_log_auth = async (req,res,next)=>{
    try {
        const decoded = jwt.verify(req.session.user_token, 'orpagesjsonwebtoken');
        const user = await selectPromise(`select user_id,token from user_token where token = '${req.session.user_token}' and user_id = '${decoded.user_id}' limit 1`);
        if (user.length <= 0) {
            req.owner = false;
            next();
        } else {
            req.owner = true;
            if(req.path == '/login'){
                res.send('You are already logged in');
            }else{
                res.redirect('/owner_panel');
            }
        }
    } catch (e) {
        req.owner = false;
        next();
    }
}


module.exports = {owner_auth,owner_log_auth,home_auth,owner_auth_ajax,owner_auth_ajax_post};