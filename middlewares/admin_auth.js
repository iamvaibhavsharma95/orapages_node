const jwt = require('jsonwebtoken');
const {
    selectPromise,
    queryPromise
} = require('./../database/database');




const admin_auth = async (req, res, next) => {
    try {
        const decoded = jwt.verify(req.session.admin_token, 'orpagesjsonwebtoken');
        const admin = await selectPromise(`select admin_id,token from admin_token where token = '${req.session.admin_token}' and admin_id = '${decoded.admin_id}' limit 1`);
        if (admin.length >= 0) {
            req.admin_id = decoded.admin_id;
            req.admin = true;
            next();
        } else {
            throw new Error('Unable to login');
        }
    } catch (e) {
        req.session.destroy();
        req.admin = false;
        res.redirect('/admin');
    }
}


const admin_auth_ajax = async (req, res, next) => {
    try {
        const decoded = jwt.verify(req.session.admin_token, 'orpagesjsonwebtoken');
        const admin = await selectPromise(`select admin_id,token from admin_token where token = '${req.session.admin_token}' and admin_id = '${decoded.admin_id}' limit 1`);
        if (admin.length >= 0) {
            req.admin_id = decoded.admin_id;
            req.admin = true;
            next();
        } else {
            throw new Error('Unable to login');
        }
    } catch (e) {
        req.session.destroy();
        req.admin = false;
        res.status(400).send("You are not an admin try to login again");
    }
}



const admin_log_auth = async (req,res,next)=>{
    try {
        const decoded = jwt.verify(req.session.admin_token, 'orpagesjsonwebtoken');
        
        const admin = await selectPromise(`select admin_id,token from admin_token where token = '${req.session.admin_token}' and admin_id = '${decoded.admin_id}' limit 1`);
        
        if (admin.length <= 0) {
            
            req.admin = false;
            next();

        } else {
            req.admin = true;
            res.redirect('/admin_panel');
        }
    } catch (e) {
        req.admin = false;
        next();
    }
}



module.exports = {admin_auth,admin_log_auth,admin_auth_ajax};