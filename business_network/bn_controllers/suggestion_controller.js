const Friend = require("../bn_models/friend");
const FriendRequest = require("../bn_models/friend_request");
const Suggestions = require("./../bn_models/suggestions");

module.exports = {
  suggestions: async (req, res, next) => {
    try {
      let { user_id } = req;
      let suggestions = await Suggestions.generate_suggestion(user_id);

      let suggestion_list = {
        suggestions,
      };

      res.render("../bn_views/orapages_components/modals/modal_suggestions", {
        suggestion_list,
      });
    } catch (error) {
      next(error);
    }
  },

  initialSuggestions: async (req, res, next) => {
    try {
      let { user_id } = req;

      const count = await Friend.getFriendCount(user_id);
      const request = await FriendRequest.getFriendRequestsSendCount(user_id);

      if (count > 0 || request > 0) {
        return res.status(500).send("This user is already got suggestions");
      }

      let suggestions = await Suggestions.generate_suggestion(user_id);

      let suggestion_list = {
        suggestions,
      };

      res.render(
        "../bn_views/orapages_components/modals/initial_modal_suggestions",
        { suggestion_list }
      );
    } catch (error) {
      next(error);
    }
  },
};
