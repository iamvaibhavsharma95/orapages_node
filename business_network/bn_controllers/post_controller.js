const createHttpError = require("http-errors");
const FrontEndOperations = require("../../database/front_end");
const Post = require("../bn_models/post");

module.exports = {
  create: async function (req, res, next) {
    try {
      let { post_content = "" } = req.body;
      const post = new Post({});
      let images = [];
      let videos = [];
      if (!!req.files.images) {
        images = req.files.images.map((file) => file.path);
      }
      if (!!req.files.video) {
        videos = req.files.video.map((file) => file.path);
      }
      const id = await post.create(req.user_id, post_content, images, videos);
      res.redirect(`/posts/${id}`);
    } catch (error) {
      next(error);
    }
  },

  delete: async function (req, res, next) {
    try {
      let { post_id } = req.query;
      let { user_id } = req;
      const post = new Post({});
      const postById = await post.getPostById(post_id);
      if (postById.length <= 0) {
        throw createHttpError(404, "No post found");
      }
      if (postById[0].user_id == user_id) {
        await post.deletePost(post_id);
        return res.status(200).send(`${post_id} has been deleted`);
      }
    } catch (error) {
      next(error);
    }
  },

  update: async function (req, res, next) {
    try {
      let { post_id, post_content } = req.body;
      const post = new Post({});
      let { user_id } = req;
      const postById = await post.getPostById(post_id);
      if (postById.length <= 0) {
        throw createHttpError(404, "No post found");
      }
      if (postById[0].user_id == user_id) {
        const r = await post.updatePost(post_id, post_content);
      }
      res.redirect(`/posts/${post_id}`);
    } catch (error) {
      next(error);
    }
  },

  getAll: async function (req, res, next) {
    const post = new Post({});
    let { user_id } = req.params;

    try {
      const r = await post.getRelatedPosts(user_id);
      const posts = r;
      posts.owner = req.owner;
      posts.user_id = req.user_id;
      return res.render("post_box/post_views", {
        posts,
      });
    } catch (error) {
      next(error);
    }
  },

  get: async function (req, res, next) {
    try {
      const post = new Post({});
      let content = await post.getPostByIdForView(req.params.id);
      const company = await FrontEndOperations.companyByUrl(
        content.company_url
      );
      company.owner = req.owner;

      if (!!company.user_id && company.user_id == req.user_id) {
        company.owner_this = true;
        company.csrfToken = req.csrfToken();
      } else {
        company.owner_this = false;
        company.csrfToken = req.csrfToken();
      }
      company.production = process.env.PRODUCTION !== "false";
      company.posts = [content];
      company.posts.owner = req.owner;
      company.posts.user_id = req.user_id;
      res.render("profile/full_profile_single", {
        company,
      });
    } catch (e) {
      next(e);
    }
  },
};
