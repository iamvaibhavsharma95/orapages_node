const Friend = require("../bn_models/friend");
const UserOnBoadingController = require("../bn_models/user_onboarding");
module.exports = {
  post: async function (req, res, next) {
    try {
      const company = await UserOnBoadingController.post(req.user_id);
      res.render("../../views/frontend_new/onboarding_post", {
        company,
        csrfToken: req.csrfToken(),
      });
    } catch (error) {
      next(error);
    }
  },

  suggestions: async function (req, res, next) {
    try {
      const friends = await Friend.getFriends(req.user_id);
      await res.render("../../views/frontend_new/onboarding_suggestions", {
        friends,
        csrfToken: req.csrfToken(),
        next_button: req.query.next_button || false,
      });
    } catch (error) {
      next(error);
    }
  },

  populateSuggestions: async function (req, res, next) {
    const {
      category,
      location,
      start,
      length,
      draw,
      order = [
        {
          dir: "desc",
        },
      ],
    } = req.query;
    let [{ dir }] = order;
    let city_id = location.split(",")[0];
    let state_id = location.split(",")[1];
    let country_id = location.split(",")[2];

    try {
      const { companies, count } =
        await UserOnBoadingController.userSuggestions(
          req.user_id,
          city_id,
          state_id,
          country_id,
          category,
          length,
          start,
          dir
        );
      res.send({
        draw,
        recordTotal: count,
        recordsFiltered: count,
        data: companies,
      });
    } catch (error) {
      next(error);
    }
  },

  populateSuggestionCompany: async function (req, res, next) {
    const {
      company_id,
      start,
      length,
      draw,
      order = [
        {
          dir: "desc",
        },
      ],
    } = req.query;
    let [{ dir }] = order;

    try {
      const { companies, count } =
        await UserOnBoadingController.userSuggestionCompany(
          req.user_id,
          company_id,
          length,
          start,
          dir
        );
      res.send({
        draw,
        recordTotal: count,
        recordsFiltered: count,
        data: companies,
      });
    } catch (error) {
      next(error);
    }
  },

  makeSuggestionsFriends: async function (req, res, next) {
    const { category, location, length = 500 } = req.query;
    let city_id = location.split(",")[0];
    let state_id = location.split(",")[1];
    let country_id = location.split(",")[2];

    try {
      const response = await UserOnBoadingController.userSuggestionsMakeFriends(
        req.user_id,
        city_id,
        state_id,
        country_id,
        category,
        length
      );
      res.send(response);
    } catch (error) {
      next(error);
    }
  },

  populateSuggestionsCompany: async function (req, res, next) {
    const {
      term,
      location,
      start,
      length,
      draw,
      order = [
        {
          dir: "asc",
        },
      ],
    } = req.query;
    let [{ dir }] = order;
    let city_id = location.split(",")[0];
    let state_id = location.split(",")[1];
    let country_id = location.split(",")[2];
    try {
      const { companies, count } =
        await UserOnBoadingController.userSuggestionsCompany(
          term,
          city_id,
          state_id,
          country_id,
          length,
          start,
          dir
        );
      res.send({
        draw,
        recordTotal: count,
        recordsFiltered: count,
        data: companies,
      });
    } catch (error) {
      next(error);
    }
  },

  getCategories: async function (req, res, next) {
    try {
      let offset = parseInt(req.query.offset);
      const categories = await UserOnBoadingController.categoriesCompanies(
        req.user_id,
        offset
      );

      res.send(categories);
    } catch (error) {
      next(error);
    }
  },

  getCategoriesByTerm: async function (req, res, next) {
    try {
      let offset = parseInt(req.query.offset);
      const categories =
        await UserOnBoadingController.searchCompaneisbyCategory(
          req.user_id,
          offset,
          req.query.term
        );

      res.send(categories);
    } catch (error) {
      next(error);
    }
  },

  addCategory: async function (req, res, next) {
    try {
      await UserOnBoadingController.addCategory(
        req.user_id,
        req.query.category_id
      );

      res.send("added");
    } catch (error) {
      next(error);
    }
  },

  removeCategory: async function (req, res, next) {
    await UserOnBoadingController.removeCategory(
      req.user_id,
      req.query.category_id
    );

    res.send("deleted");
  },
};
