const FriendRequest = require("../bn_models/friend_request");

module.exports = {
  sendRequest: async (req, res, next) => {
    let { requestee_id } = req.body;

    try {
      const r = await FriendRequest.sendFriendRequest(
        req.user_id,
        requestee_id
      );
      res.send(`<strong>${r}</strong>`);
    } catch (error) {
      next(error);
    }
  },

  getRequests: async (req, res, next) => {
    try {
      let { user_id } = req;
      const requests = await FriendRequest.getFriendRequests(user_id);
      friend_requests = {
        requests,
        request_count: requests.length,
      };
      res.render(
        "../bn_views/orapages_components/modals/modal_friend_request",
        friend_requests
      );
    } catch (error) {
      next(error);
    }
  },

  getRequestCount: async (req, res, next) => {
    try {
      let { user_id } = req;
      let count = await FriendRequest.getFriendRequestsCount(user_id);
      if (count.length <= 0) {
        return res.send(count);
      }
      count = count[0];
      return res.send(count);
    } catch (error) {
      next(error);
    }
  },

  acceptRequest: async (req, res, next) => {
    let { sender, receiver = req.user_id } = req.query;

    try {
      const r = await FriendRequest.acceptFriendRequest(sender, receiver);
      res.send(`<strong>${r}</strong>`);
    } catch (error) {
      next(error);
    }
  },

  deleteRequest: async (req, res, next) => {
    try {
      let { sender, receiver = req.user_id } = req.query;
      await FriendRequest.deleteFriendRequest(sender, receiver);
      res.send(`<strong>${r}</strong>`);
    } catch (error) {
      next(error);
    }
  },

  checkRequest: async (req, res, next) => {
    try {
      let { user_id } = req;
      let { requestee_id } = req.query;
      let r = await FriendRequest.checkRequestSent(user_id, requestee_id);
      res.send(`<strong>${r}</strong>`);
    } catch (error) {
      next(error);
    }
  },
};
