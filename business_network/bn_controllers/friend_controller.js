const Company = require("../../database/company");
const FrontEndOperations = require("../../database/front_end");
const Friend = require("../bn_models/friend");

module.exports = {
  getFriends: async (req, res, next) => {
    try {
      let { user_id, del_button = true } = req;
      if (!user_id) {
        user_id = req.params.user_id;
        del_button = false;
      }
      const friends = await Friend.getFriends(user_id);
      let friend_list = {
        friends,
        friend_count: friends.length,
        del_button,
      };
      res.render("../bn_views/orapages_components/modals/modal_friends", {
        friend_list,
      });
    } catch (error) {
      next(error);
    }
  },

  getFriendsData: async (req, res, next) => {
    try {
      let {
        start,
        length,
        draw,
        order = [
          {
            dir: "asc",
            column: 0,
          },
        ],
      } = req.query;
      let [{ dir, column }] = order;
      let { user_id } = req;
      const friends = await Friend.getFriends(
        user_id,
        start,
        length,
        dir,
        column
      );
      if (!start) {
        res.send(friends);
      } else {
        const friendCount = await Friend.getFriendCount(user_id);
        let data = [];
        for (const friend of friends) {
          let company = await FrontEndOperations.companyByUserId(
            friend.user2_id
          );
          company = new Company(company);
          await company.getFirstLocationsDB();
          company.user1_id = friend.user1_id;
          company.user2_id = friend.user2_id;
          company.company_url = friend.company_url;
          company.name = friend.name;
          company.date = friend.date;
          data.push(company);
        }
        res.json({
          draw,
          recordTotal: friendCount.count,
          recordsFiltered: friendCount.count,
          data,
        });
      }
    } catch (error) {
      next(error);
    }
  },

  getFriendsCount: async (req, res, next) => {
    try {
      let { user_id } = req;
      if (!user_id) {
        user_id = req.params.user_id;
      }
      const friendCount = await Friend.getFriendCount(user_id);
      res.send(friendCount);
    } catch (error) {
      next(error);
    }
  },

  deleteFriend: async (req, res, next) => {
    try {
      let { user1_id, user2_id } = req.body;
      await Friend.deleteFriend(user1_id, user2_id);
      res.send("Friend has been deleted");
    } catch (error) {
      next(error);
    }
  },
};
