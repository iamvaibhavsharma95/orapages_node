const createHttpError = require("http-errors");
const FrontEndOperations = require("../../database/front_end");
const Post = require("../bn_models/post");

module.exports = {
  about: async function (req, res, next) {
    try {
      const company = await FrontEndOperations.companyByUrl(req.params.url);
      company.owner = req.owner;

      if (!!company.user_id && company.user_id == req.user_id) {
        company.owner_this = true;
        company.csrfToken = req.csrfToken();
      } else {
        company.owner_this = false;
        company.csrfToken = req.csrfToken();
      }
      company.production = process.env.PRODUCTION !== "false";
      res.render("timeline/company_about", {
        company,
      });
    } catch (e) {
      next(createHttpError(400, e.message));
    }
  },

  posts: async function (req, res, next) {
    const company = await FrontEndOperations.companyByUrl(req.params.url);
    company.owner = req.owner;

    if (!!company.user_id && company.user_id == req.user_id) {
      company.owner_this = true;
      company.csrfToken = req.csrfToken();
    } else {
      company.owner_this = false;
      company.csrfToken = req.csrfToken();
    }
    company.production = process.env.PRODUCTION !== "false";
    if (!req.query.layout) {
      return res.render("timeline/company_posts", {
        company,
      });
    }
    company.path = req.query.location;
    return res.render("timeline/company_posts_registration", {
      company,
    });
  },

  notifications: async function (req, res, next) {
    const company = await FrontEndOperations.companyByUrl(req.params.url);
    company.owner = req.owner;

    if (!!company.user_id && company.user_id == req.user_id) {
      company.owner_this = true;
      company.csrfToken = req.csrfToken();
    } else {
      company.owner_this = false;
      company.csrfToken = req.csrfToken();
    }
    company.production = process.env.PRODUCTION !== "false";
    if (!req.query.layout) {
      return res.render("timeline/company_notifications", {
        company,
      });
    }
    company.path = req.query.location;
    return res.render("timeline/company_notifications_registration", {
      company,
    });
  },

  post_box: async (req, res, next) => {
    let company;
    if (!req.user_id) {
      return res.render("orapages_components/modals/modal_login_to_post");
    }

    company = await FrontEndOperations.companyCheckByUserId(req.user_id);
    company.owner = req.owner;
    company.csrfToken = req.csrfToken();

    if (!!company.user_id && company.user_id == req.user_id) {
      res.render("post_box/modal_post", { company });
    } else if (!!company.user_id && company.user_id !== req.user_id) {
      res.render("post_box/modal_post", { company });
    } else if (!!company.owner) {
      res.render("orapages_components/modals/modal_login_to_post", { company });
    }
  },

  intial_post_box: async (req, res, next) => {
    let company;
    if (!req.user_id) {
      return res.render("orapages_components/modals/modal_login_to_post");
    }

    const post = new Post({});

    const count = await post.getPostByUserCount(req.user_id);

    if (count > 0) {
      return res.status(500).send("Already posted");
    }

    company = await FrontEndOperations.companyCheckByUserId(req.user_id);
    company.owner = req.owner;
    company.csrfToken = req.csrfToken();

    if (!!company.user_id && company.user_id == req.user_id) {
      res.render("post_box/initial_modal_post", { company });
    } else if (!!company.user_id && company.user_id !== req.user_id) {
      res.render("post_box/initial_modal_post", { company });
    } else if (!!company.owner) {
      res.render("orapages_components/modals/modal_login_to_post", { company });
    }
  },

  post_box_edit: async (req, res, next) => {
    let company;
    let { post_id } = req.query;
    if (!req.user_id) {
      return res.render("orapages_components/modals/modal_login_to_post");
    }

    company = await FrontEndOperations.companyCheckByUserId(req.user_id);
    company.owner = req.owner;
    company.csrfToken = req.csrfToken();

    if (!!company.user_id && company.user_id == req.user_id) {
      let post = new Post({});
      post = await post.getPostById(post_id);
      post[0].csrfToken = req.csrfToken();
      res.render("post_box/modal_post_edit", { post: post[0] });
    } else if (!!company.user_id && company.user_id !== req.user_id) {
      res.render("post_box/modal_post_edit", { post });
    } else if (!!company.owner) {
      res.render("orapages_components/modals/modal_login_to_post", { company });
    }
  },

  report_box: async (req, res, next) => {
    let { post_id } = req.query;
    res.render("orapages_components/modals/modal_report_post", {
      post_id,
      user_id: req.user_id,
      csrfToken: req.csrfToken(),
    });
  },
};
