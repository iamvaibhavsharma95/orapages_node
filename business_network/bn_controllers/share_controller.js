const createHttpError = require("http-errors");
const Share = require("../bn_models/share");

module.exports = {
  share: async (req, res, next) => {
    const { user_id } = req;
    const { post_id } = req.query;
    try {
      await Share.sharePost(post_id, user_id);
      res.send("Post Has been shared");
    } catch (error) {
      next(createHttpError(500, "Post is not deleted"));
    }
  },

  getShares: async (req, res, next) => {
    const { post_id } = req.query;
    try {
      const shares = await Share.getShares(post_id);
      const shares_count = await Share.getSharesCount(post_id);
      const shares_list = { shares, shares_count };
      res.render("../bn_views/orapages_components/modals/modal_shares.ejs", {
        shares_list,
      });
    } catch (error) {
      next(error);
    }
  },
};
