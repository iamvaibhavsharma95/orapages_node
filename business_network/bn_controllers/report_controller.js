const Report = require("../bn_models/report");

module.exports = {
  reportPost: async (req, res, next) => {
    let { user_id, post_id, report_content } = req.body;
    try {
      const report = await Report.reportPost(post_id, user_id, report_content);
      res.send(report);
    } catch (error) {
      next(error);
    }
  },
};
