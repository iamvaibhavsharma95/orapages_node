const Comment = require("../bn_models/comment");

module.exports = {
  addComment: async (req, res, next) => {
    try {
      let { user_id } = req;
      let { comment, post_id } = req.query;
      const comments = await Comment.addComment(comment, user_id, post_id);
      comments.user_id = user_id;
      comments.owner = req.owner;
      res.render("post_box/post_comments", { comments });
    } catch (error) {
      next(error);
    }
  },

  deleteComment: async (req, res, next) => {
    try {
      let { user_id } = req;
      let { comment_id, post_id } = req.query;
      const comments = await Comment.deleteComment(
        post_id,
        comment_id,
        user_id
      );
      comments.owner = req.owner;
      comments.user_id = req.user_id;
      res.render("post_box/post_comments", { comments });
    } catch (error) {
      next(error);
    }
  },

  getAllComments: async (req, res, next) => {
    try {
      let { post_id } = req.query;
      const comments = await Comment.getAllComments(post_id);
      comments.owner = req.owner;
      comments.user_id = req.user_id;
      res.render("post_box/post_comments", { comments });
    } catch (error) {
      next(error);
    }
  },

  editComment: async (req, res, next) => {
    let { user_id } = req;
    let { comment_id, post_id, comment } = req.query;
    try {
      const comments = await Comment.editComment(
        post_id,
        comment,
        comment_id,
        user_id
      );
      comments.user_id = user_id;
      comments.owner = req.owner;
      res.render("post_box/post_comments", { comments });
    } catch (error) {
      next(error);
    }
  },
};
