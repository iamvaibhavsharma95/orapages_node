const { NText } = require("mssql");
const Like = require("../bn_models/like");

module.exports = {
  addLike: async (req, res, next) => {
    const { user_id } = req;
    const { post_id } = req.query;
    try {
      const likes_count = await Like.addLike(post_id, user_id);
      res.send({ likes_count });
    } catch (error) {
      next(error);
    }
  },

  removeLike: async (req, res, next) => {
    const { user_id } = req;
    const { post_id } = req.query;
    try {
      const likes_count = await Like.removeLike(post_id, user_id);
      res.send({ likes_count });
    } catch (error) {
      next(error);
    }
  },

  getLikes: async (req, res, next) => {
    const { post_id } = req.query;
    try {
      const likes = await Like.getLikes(post_id);
      const likes_count = await Like.getLikesCount(post_id);
      const likes_list = { likes, likes_count };
      res.render("../bn_views/orapages_components/modals/modal_likes", {
        likes_list,
      });
    } catch (error) {
      next(error);
    }
  },

  getLikesCount: async (req, res, next) => {
    const { post_id } = req.query;
    try {
      const likes_count = await Likes.getLikesCount(post_id);
      res.send({ likes_count });
    } catch (error) {
      next(error);
    }
  },
};
