const Notification = require("../bn_models/notification");

module.exports = {
  getAll: async function (req, res, next) {
    const notification = new Notification({});
    let { user_id } = req.params;

    try {
      const r = await notification.getNotifications(user_id);
      const notifications = r;
      notifications.owner = req.owner;
      notifications.user_id = req.user_id;
      return res.render("post_box/notification_views", {
        notifications,
      });
    } catch (error) {
      next(error);
    }
  },
};
