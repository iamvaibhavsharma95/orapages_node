const createHttpError = require("http-errors");
const Follow = require("../bn_models/follower");

module.exports = {
  follow: async (req, res, next) => {
    let { following } = req.body;

    try {
      const r = await Follow.follow(req.user_id, following);
      res.send(`<strong>${r}</strong>`);
    } catch (error) {
      next(error);
    }
  },

  getFollowers: async (req, res, next) => {
    try {
      let { user_id, del_button = true } = req;
      if (!user_id) {
        user_id = req.params.user_id;
        del_button = false;
      }

      const followers = await Follow.getFollowers(user_id);

      followers_list = {
        followers,
        followers_count: followers.length,
        del_button,
      };

      return res.render(
        "../bn_views/orapages_components/modals/modal_followers",
        followers_list
      );
    } catch (error) {
      next(error);
    }
  },

  getFollowings: async (req, res, next) => {
    let { user_id } = req;
    let followings_list = {};

    try {
      const followings = await Follow.getFollowings(user_id);

      followings_list = {
        followings,
        followings_count: followings.length,
      };

      res.render("../bn_views/orapages_components/modals/modal_followings", {
        followings_list,
      });
    } catch (error) {
      next(error);
    }
  },

  getFollowersCount: async (req, res, next) => {
    try {
      let { user_id } = req;
      let count = await Follow.getFollowersCount(user_id);
      if (count.length > 0) {
        count = count[0];
        res.send(count);
      } else {
        throw createHttpError(404, "No follwers found");
      }
    } catch (error) {
      next(error);
    }
  },

  unfollow: async (req, res, next) => {
    try {
      let { following, follower = req.user_id } = req.body;
      await Follow.unfollow(follower, following);
      res.send(`<strong>Unfollowed</strong>`);
    } catch (error) {
      next(error);
    }
  },

  removeFollow: async (req, res, next) => {
    try {
      let { following = req.user_id, follower } = req.body;
      await Follow.unfollow(follower, following);
      res.send(`<strong>Removed</strong>`);
    } catch (error) {
      next(error);
    }
  },

  checkFollow: async (req, res, next) => {
    try {
      let { user_id } = req;
      let { following } = req.query;
      let r = await Follow.checkFollower(user_id, following);
      res.send(`<strong>${r}</strong>`);
    } catch (error) {
      next(error);
    }
  },
};
