const createHttpError = require("http-errors");
const { queryPromise } = require("../../database/database");
const Friend = require("./friend");

module.exports = {
  async sendFriendRequest(requester_id, requestee_id) {
    const checkFriend = `select * from bn_partner
    where (partner1_id = ? and partner2_id = ?)
    or (partner2_id = ? and partner1_id = ?)`;
    const friends = await queryPromise(checkFriend, [
      requester_id,
      requestee_id,
      requester_id,
      requestee_id,
    ]);

    const count = await Friend.getFriendCount(requester_id);

    if (count.count >= 500) {
      throw createHttpError(
        404,
        `You have too many Friends Please wait for some time we will
        let you know more about it`
      );
    }

    if (friends.length > 0) {
      await Friend.deleteFriend(requester_id, requestee_id);
      return "Add Friend";
    } else {
      await Friend.addFriend(requestee_id, requester_id);
      return "Friends";
    }
  },

  async getFriendRequests(user_id) {
    const query = `select company.company_business_name as 'name',
    company.company_url as 'company_url',
    bn_partner_request.requester_id as 'sender',
    bn_partner_request.requestee_id as 'receiver'
    from bn_partner_request
    inner join
    company_user on bn_partner_request.requester_id = company_user.user_id
    inner join
    company on company.company_id = company_user.company_id
    and requestee_id = ?`;
    return await queryPromise(query, [user_id]);
  },

  async getFriendRequestsCount(user_id) {
    const query = `select count(requestee_id) as 'count' from bn_partner_request
    where requestee_id = ?`;
    return await queryPromise(query, [user_id]);
  },

  async getFriendRequestsSendCount(user_id) {
    const query = `select count(requester_id) as 'count' from bn_partner_request
    where requester_id = ?`;
    let count = await queryPromise(query, [user_id]);
    return count[0].count;
  },

  async acceptFriendRequest(requester_id, requestee_id) {
    return await Friend.addFriend(requester_id, requestee_id);
  },

  async deleteFriendRequest(requester_id, requestee_id) {
    const query = `BEGIN;
    delete from bn_partner_request where (requestee_id = '${requester_id}'
    and requester_id = '${requestee_id}')
    or (requestee_id = '${requestee_id}' and requester_id = '${requester_id}');
    commit;`;
    await queryPromise(query);
    return "Add Friend";
  },

  async checkRequestSent(requester_id, requestee_id) {
    const friend = `select * from bn_partner where
    (partner1_id = ? and partner2_id = ?)
    or (partner1_id = ? and partner2_id = ?)`;
    const friends = await queryPromise(friend, [
      requester_id,
      requestee_id,
      requestee_id,
      requester_id,
    ]);
    if (friends.length > 0) {
      return "Friends";
    } else {
      return "Add Friend";
    }
  },
};
