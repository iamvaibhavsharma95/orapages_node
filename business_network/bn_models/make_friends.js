const { queryPromise } = require("./../../database/database");
const Suggestions = require("./suggestions");
const Friends = require("./friend");

module.exports = {
  makeFriends: async function (offset) {
    try {
      const query = `select user_id from users limit 200 offset ?;`;
      const users = await queryPromise(query, [offset * 200 + 1]);
      for (const user of users) {
        const friend_query = `select * from bn_partner where partner1_id = ?`;
        const fr = await queryPromise(friend_query, [user.user_id]);
        if (fr.length < 20) {
          const suggested_users = await Suggestions.generate_suggestion_user(
            user.user_id
          );
          for (const su of suggested_users) {
            await Friends.addFriendbackEnd(user.user_id, su.user_id);
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  },

  deleteFriends: async function (offset) {
    try {
      const query = `select distinct(partner1_id) from bn_partner where date
      like '2021-10-07%' or date like '2021-10-08%' limit 200 offset ?`;
      let users = await queryPromise(query, [offset]);
      let usersArray = [];
      users.forEach((user) => {
        usersArray.push(user.partner1_id);
      });
      const deletequery = `delete from bn_partner where partner1_id in (?)`;
      await queryPromise(deletequery, [usersArray]);
    } catch (error) {
      console.log(error);
    }
  },
};
