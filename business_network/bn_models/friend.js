const { queryPromise } = require("../../database/database");
const Notification = require("./notification");

module.exports = {
  async getFriends(
    user_id,
    start = 0,
    length = 500,
    order = "asc",
    column = 0
  ) {
    let cols = ["company.company_business_name", "", "date"];
    const query = `select bn_partner.partner2_id as user2_id, ? as user1_id, date,
    company.company_business_name as 'name',company.company_url as 'company_url'
    from bn_partner
    inner join
    company_user on company_user.user_id = bn_partner.partner2_id
    inner join
    company on company.company_id = company_user.company_id where
    bn_partner.partner1_id = ? order by ${cols[column]} ${order}
    limit ${length} offset ${start}`;
    return await queryPromise(query, [user_id, user_id]);
  },

  async getFriendIds(user_id) {
    const query = `select bn_partner.partner2_id as user_id from bn_partner
    where bn_partner.partner1_id = ?`;
    const friendsIds = await queryPromise(query, [user_id]);
    let ids = [];
    friendsIds.forEach((id) => {
      ids.push(id.user_id);
    });
    return ids;
  },

  async getFOF(user_id) {
    const friends = await this.getFriendIds(user_id);

    if (friends.length > 0) {
      const query = `select partner2_id as user_id from bn_partner
      where partner1_id in (?) and partner2_id != ?`;
      fofs = await queryPromise(query, [friends, user_id]);
      fofs.forEach((id) => {
        friends.push(id.user_id);
      });
    }

    return friends;
  },

  async getFriendCount(user_id) {
    const query = `select count(partner1_id) as 'count' from
    bn_partner where partner1_id = ?`;
    const friends = await queryPromise(query, [user_id]);
    return friends[0];
  },

  async addFriend(requester_id, requestee_id) {
    const count = await this.getFriendCount(requester_id);

    if (count.count >= 500) {
      throw createHttpError(
        404,
        `You have too many Friends Please wait for some time we will
        let you know more about it`
      );
    }

    const query = `BEGIN;
    insert into bn_partner(partner1_id,partner2_id)
    values('${requester_id}','${requestee_id}');
    insert into bn_partner(partner2_id,partner1_id)
    values('${requester_id}','${requestee_id}');
    delete from bn_partner_request where
    (requestee_id = '${requester_id}' and requester_id = '${requestee_id}')
    or (requestee_id = '${requestee_id}' and requester_id = '${requester_id}');
    commit;`;
    const company = await queryPromise(
      `select * from company
      inner join
      company_user on company.company_id = company_user.company_id
      and user_id = ?`,
      [requester_id]
    );
    const company2 = await queryPromise(
      `select * from company
        inner join
        company_user on company.company_id = company_user.company_id and user_id = ?`,
      [requestee_id]
    );
    let notification = new Notification({});
    await notification.create(
      requester_id,
      `<b><a href="/${company[0].company_url}">${company[0].company_business_name}</a>
         are now friend with <a href="/${company2[0].company_url}">${company2[0].company_business_name}</a></b>`
    );
    await notification.create(
      requestee_id,
      `<b><a href="/${company2[0].company_url}">${company2[0].company_business_name}</a>
         are now friend with <a href="/${company[0].company_url}">${company[0].company_business_name}</a></b>`
    );
    await queryPromise(query);
    return "Accepted";
  },

  async addFriendbackEnd(requester_id, requestee_id) {
    const check = `select * from bn_partner where partner1_id = ?
    and partner2_id = ?`;
    const checkResult = await queryPromise(check, [requester_id, requestee_id]);
    if (checkResult.length <= 0) {
      const query = `BEGIN;
      insert into bn_partner(partner1_id,partner2_id)
      values('${requester_id}','${requestee_id}');
      insert into bn_partner(partner2_id,partner1_id)
      values('${requester_id}','${requestee_id}');
      delete from bn_partner_request where
      (requestee_id = '${requester_id}' and requester_id = '${requestee_id}')
      or (requestee_id = '${requestee_id}' and requester_id = '${requester_id}');
      commit;`;
      return await queryPromise(query);
    }
  },

  async deleteFriend(requester_id, requestee_id) {
    const query = `delete from bn_partner where (partner1_id = '${requester_id}'
    and partner2_id = '${requestee_id}')
    or (partner1_id = '${requestee_id}' and partner2_id = '${requester_id}');`;
    await queryPromise(query);
    return "Add Friend";
  },
};
