const { queryPromise, selectPromise } = require("../../database/database");

const random = require("randomstring");

module.exports = {
  async addLike(post_id, user_id) {
    try {
      const likedByUser = await this.likedByUser(post_id, user_id);
      const likes_count = {};
      if (!!likedByUser) {
        likes_count.likes_count = await this.removeLike(post_id, user_id);
        likes_count.liked = false;
        return likes_count;
      }
      const query = `insert into bn_post_like(post_id,user_id) values(?,?)`;
      await queryPromise(query, [post_id, user_id]);
      likes_count.likes_count = await this.getLikesCount(post_id, user_id);
      likes_count.liked = true;
      return likes_count;
    } catch (error) {
      console.log(error);
    }
  },

  async getLikes(post_id) {
    const query = `select company_business_name, company_url from bn_post
                    inner join bn_post_user on bn_post_user.post_id = bn_post.post_id
                    inner join bn_post_like on bn_post.post_id = bn_post_like.post_id
                    inner join company_user on company_user.user_id = bn_post_like.user_id
                    inner join company on company.company_id = company_user.company_id
                    and bn_post.post_id = ?`;
    return await queryPromise(query, [post_id]);
  },

  async getLikesCount(post_id) {
    const query = `select count(user_id) as likes_count from bn_post_like where post_id = ?`;
    let count = await queryPromise(query, [post_id]);
    count = count[0].likes_count;
    return count;
  },

  async removeLike(post_id, user_id) {
    const query = `delete from bn_post_like where post_id = ? and user_id = ?`;
    await queryPromise(query, [post_id, user_id]);
    return this.getLikesCount(post_id);
  },

  async likedByUser(post_id, user_id) {
    if (!user_id) {
      return false;
    }
    const query = `select * from bn_post_like where post_id= ? and user_id = ?`;
    const likedByUser = await queryPromise(query, [post_id, user_id]);
    if (likedByUser.length > 0) {
      return true;
    } else {
      return false;
    }
  },
};
