const { queryPromise } = require("./../../database/database");

module.exports = {
  reportPost: async (user_id, post_id, message) => {
    const query = `insert into bn_post_report(post_id,user_id,message) values (?,?,?)`;
    await queryPromise(query, [user_id, post_id, message]);
    return "Post reported";
  },

  reportList: async () => {
    const query = `select * from bn_post_report
      inner join company_user on bn_post_report.user_id = company_user.user_id
      inner join company on company.company_id = company_user.user_id`;
    await queryPromise(query);
  },
};
