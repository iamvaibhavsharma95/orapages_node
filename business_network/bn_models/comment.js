const { queryPromise, selectPromise } = require("../../database/database");

const random = require("randomstring");

module.exports = {
  getCommentId() {
    const id = random.generate({
      length: 30,
      charset: "hex",
    });
    const comment_id = new Date().getTime() + id;
    return comment_id;
  },

  async addComment(comment, user_id, post_id) {
    const query = `insert into bn_post_comment(comment_id, post_id,user_id,comment) values (?,?,?,?)`;
    await queryPromise(query, [this.getCommentId(), post_id, user_id, comment]);
    return await this.getTopFiveComments(post_id);
  },

  async getTopFiveComments(post_id) {
    const commentQuery = `select bn_post.post_id as 'post_id', bn_post_comment.comment_id as 'comment_id', company_url,
        comment, company_business_name,bn_post_comment.user_id as 'commented_by',bn_post_user.user_id as 'posted_by'
        from bn_post inner join bn_post_comment on bn_post.post_id = bn_post_comment.post_id
        inner join company_user on company_user.user_id = bn_post_comment.user_id
        inner join company on company.company_id = company_user.company_id
        inner join bn_post_user on bn_post_user.post_id = bn_post.post_id
        and bn_post.post_id = ? order by bn_post_comment.date desc limit 3 `;
    const comments = await queryPromise(commentQuery, [post_id]);
    return comments;
  },

  async getAllComments(post_id) {
    const commentQuery = `select bn_post.post_id as 'post_id', bn_post_comment.comment_id as 'comment_id', bn_post_comment.user_id as 'commented_by'
        ,bn_post_user.user_id as 'posted_by', company_url, comment, company_business_name from bn_post inner join bn_post_comment on bn_post.post_id = bn_post_comment.post_id
        inner join bn_post_user on bn_post_user.post_id = bn_post.post_id
        inner join company_user on company_user.user_id = bn_post_comment.user_id inner join company on company.company_id = company_user.company_id
        and bn_post.post_id = ? order by bn_post_comment.date desc`;
    const comments = await queryPromise(commentQuery, [post_id]);
    return comments;
  },

  async getComment(post_id) {
    const commentQuery = `select bn_post.post_id as 'post_id', bn_post_comment.comment_id as 'comment_id', company_url, comment, company_business_name, bn_post_comment.user_id as 'commented_by', bn_post_user.user_id as 'posted_by' from bn_post
        inner join bn_post_user on bn_post_user.post_id = bn_post.post_id
        inner join bn_post_comment on bn_post.post_id = bn_post_comment.post_id
        inner join company_user on company_user.user_id = bn_post_comment.user_id
        inner join company on company.company_id = company_user.company_id
        and bn_post.post_id = ?`;
    const comment = await queryPromise(commentQuery, [post_id]);
    return comment[0];
  },

  async deleteComment(post_id, comment_id, user_id) {
    const comment = await this.getComment(post_id);
    if (comment.commented_by == user_id || comment.posted_by == user_id) {
      const deleteComment = `delete from bn_post_comment where comment_id = ?`;
      await queryPromise(deleteComment, [comment_id]);
      return await this.getTopFiveComments(post_id);
    } else {
      throw new Error("Comment is not deleted");
    }
  },

  async editComment(post_id, comment, comment_id, user_id) {
    const commentt = await this.getComment(post_id);
    if (commentt.commented_by == user_id) {
      const query = `update bn_post_comment set comment = ? where comment_id = ?`;
      await queryPromise(query, [comment, comment_id]);
      return await this.getTopFiveComments(post_id);
    } else {
      throw new Error("Comment is not updated");
    }
  },
};
