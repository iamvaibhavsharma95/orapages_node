const { queryPromise, selectPromise } = require("../../database/database");

const Company = require("../../database/company");

module.exports = {
  generate_suggestion: async (user_id) => {
    let user = `select company.company_id as 'company_id',
    company.company_url as 'company_url' from company
    inner join
    company_user on company_user.company_id = company.company_id
    inner join
    users on users.user_id = company_user.user_id
    where users.user_id = '${user_id}'`;

    user = await queryPromise(user);
    if (user.length <= 0) {
      throw new Error("No Company found with this company_id");
    }

    let company_id = user[0].company_id;

    let company = new Company({
      company_id,
    });

    await company.getCategoriesDB();
    await company.getFirstLocationsDB();

    // let queryRequest = `and company_user.user_id Not in
    // (Select company_user.user_id as 'user_id' from company
    // inner join company_user on company_user.company_id = company.company_id
    // inner join bn_partner_request on bn_partner_request.requester_id = company_user.user_id
    // and company_user.user_id = '${user_id}')`;

    let queryRequest = `and company_user.user_id not in
        (Select requestee_id from bn_partner_request
        where bn_partner_request.requester_id = '${user_id}')
        and company_user.user_id not in
        (select requester_id from bn_partner_request where requestee_id = '${user_id}')
        and company_user.user_id not in
        (select partner2_id from bn_partner where partner1_id = '${user_id}')`;

    let categories = company.categories.map((category) => category.category_id);
    let query = `SELECT company.company_business_name as 'name',
    company_user.user_id as 'user_id', company.company_url as 'company_url'
    from company
    INNER JOIN
    company_location on company_location.company_id = company.company_id
    INNER JOIN
    company_category ON company_category.company_id = company.company_id
    INNER JOIN
    company_user on company_user.company_id = company.company_id
    AND company_location.city_id = '${company.locations[0].city_id}'
    AND company_category.category_id in (?)
    AND company.company_id != '${company_id}'
    ${queryRequest} limit 500`;

    let companies_city = await queryPromise(query, [categories]);

    if (companies_city.length >= 0 && companies_city.length < 20) {
      let urls = companies_city.map((url) => url.company_url);
      urls.push(company.company_url);
      let queryState;
      if (urls.length > 0) {
        queryState = `SELECT company.company_business_name as 'name',
        company_user.user_id as 'user_id', company.company_url as 'company_url'
        from company
        INNER JOIN
        company_location on company_location.company_id = company.company_id
        INNER JOIN
        company_category ON company_category.company_id = company.company_id
        inner join
        company_user on company_user.company_id = company.company_id
        AND company_location.state_id = ?
        AND company_category.category_id in (?)
        AND company.company_url not in (?)
        ${queryRequest} LIMIT ${20 - companies_city.length}`;
      } else {
        queryState = `SELECT company.company_business_name as 'name',
        company_user.user_id as 'user_id',
        company.company_url as 'company_url' from company
        INNER JOIN
        company_location on company_location.company_id = company.company_id
        INNER JOIN
        company_category ON company_category.company_id = company.company_id
        INNER JOIN
        company_user on company_user.company_id = company.company_id
        AND company_location.state_id = ?
        AND company_category.category_id in (?)
        ${queryRequest} LIMIT ${20 - companies_city.length}`;
      }
      let companies_state = await queryPromise(queryState, [
        company.locations[0].state_id,
        categories,
        urls,
      ]);
      companies_city.push(...companies_state);
    } else if (companies_city.length >= 0 && companies_city.length < 20) {
      let urls = companies_city.map((url) => url.company_url);
      urls.push(company.company_url);
      let queryCountry;
      if (urls.length > 0) {
        queryCountry = `SELECT company.company_business_name as 'name',
        company_user.user_id as 'user_id', company.company_url as 'company_url'
        from company
        INNER JOIN
        company_location on company_location.company_id = company.company_id
        INNER JOIN
        company_category ON company_category.company_id = company.company_id
        INNER JOIN company_user on company_user.company_id = company.company_id
        AND company_location.country_id = ?
        AND company_category.category_id in (?)
        AND company.company_url not in (?)
        ${queryRequest} LIMIT ${20 - companies_city.length}`;
      } else {
        queryCountry = `SELECT company.company_business_name as 'name',
        company_user.user_id as 'user_id',
        company.company_url as 'company_url' from company
        INNER JOIN
        company_location ON company_location.company_id = company.company_id
        INNER JOIN
        company_category ON company_category.company_id = company.company_id
        INNER JOIN company_user on company_user.company_id = company.company_id
        AND company_location.country_id = ?
        AND company_category.category_id in (?)
        ${queryRequest} LIMIT ${20 - companies_city.length}`;
      }
      let companies_country = await queryPromise(queryCountry, [
        company.locations[0].country_id,
        categories,
        urls,
      ]);
      companies_city.push(...companies_country);
    } else if (companies_city.length >= 0 && companies_city.length < 20) {
      let urls = companies_city.map((url) => url.company_url);
      urls.push(company.company_url);
      let queryCountry;
      if (urls.length > 0) {
        queryCountry = `SELECT company.company_business_name as 'name',
        company_user.user_id as 'user_id',
        company.company_url as 'company_url' from company
        INNER JOIN
        company_category ON company_category.company_id = company.company_id
        INNER JOIN company_user on
        company_user.company_id = company.company_id
        AND company_category.category_id = ? and company.company_url not in (?)
        ${queryRequest} LIMIT ${20 - companies_city.length}`;
      } else {
        queryCountry = `SELECT company.company_business_name as 'name',
        company_user.user_id as 'user_id',
        company.company_url as 'company_url' from company
        INNER JOIN
        company_category ON company_category.company_id = company.company_id
        INNER JOIN company_user on company_user.company_id = company.company_id
        AND company_category.category_id = ?
        ${queryRequest} LIMIT ${20 - companies_city.length}`;
      }

      let companies_country = await queryPromise(queryCountry, [
        company.locations[0].country_id,
        company.categories[0].category_id,
        urls,
      ]);
      companies_city.push(...companies_country);
    }

    return companies_city;
  },

  generate_suggestion_category_location: async (
    user_id,
    location,
    type,
    category_id,
    limit = 500,
    offset = 0,
    dir = "desc"
  ) => {
    let categoryQuery = "";
    let categoryInnerJoin = "";
    if (!!category_id) {
      categoryInnerJoin = `company_category ON company_category.company_id = company.company_id
    INNER JOIN`;
      categoryQuery = `AND company_category.category_id = '${category_id}'`;
    }

    let queryRequest = `and company_user.user_id not in
        (select partner2_id from bn_partner where partner1_id = '${user_id}')`;

    let query = `SELECT company.company_business_name as 'name', company.company_id,
    company_user.user_id as 'user_id', company.company_url as 'company_url'
    from company
    INNER JOIN
    company_location on company_location.company_id = company.company_id
    INNER JOIN
    ${categoryInnerJoin}
    company_user on company_user.company_id = company.company_id
    AND company_location.${type}_id = '${location}'
    ${categoryQuery}
    AND company_user.user_id != '${user_id}'
    ${queryRequest} order by name ${dir} limit ${limit} offset ${offset}`;

    let companies_city = await queryPromise(query);

    let queryCount = `SELECT count(company.company_id) as count
    from company
    INNER JOIN
    company_location on company_location.company_id = company.company_id
    INNER JOIN
    ${categoryInnerJoin}
    company_user on company_user.company_id = company.company_id
    AND company_location.${type}_id = '${location}'
    ${categoryQuery}
    AND company_user.user_id != '${user_id}'
    ${queryRequest}`;

    let count = await queryPromise(queryCount);
    count = count[0].count;

    let companies = [];
    for (const company of companies_city) {
      let comp = new Company({ company_id: company.company_id });
      await comp.getFirstLocationsDB();
      location = `${comp.locations[0].city_name}, ${comp.locations[0].state_name}, ${comp.locations[0].country_name}`;
      company.location = location;
      companies.push(company);
    }
    return { companies: companies, count };
  },

  generate_suggestion_company: async (
    user_id,
    company_id,
    limit = 500,
    offset = 0,
    dir = "desc"
  ) => {
    let user = `select company.company_id as 'company_id',
    company.company_url as 'company_url' from company
    inner join
    company_user on company_user.company_id = company.company_id
    inner join
    users on users.user_id = company_user.user_id
    where users.user_id = '${user_id}'`;

    user = await queryPromise(user);
    if (user.length <= 0) {
      throw new Error("No Company found with this company_id");
    }

    let company_id1 = user[0].company_id;

    let queryRequest = `and company_user.user_id not in
        (select partner2_id from bn_partner where partner1_id = '${user_id}')`;

    let query = `SELECT company.company_business_name as 'name', company.company_id,
    company_user.user_id as 'user_id', company.company_url as 'company_url'
    from company
    INNER JOIN
    company_user on company_user.company_id = company.company_id
    AND company.company_id != '${company_id1}'
    AND company.company_id = '${company_id}'
    ${queryRequest} order by name ${dir} limit ${limit} offset ${offset}`;

    let companies_city = await queryPromise(query);

    let queryCount = `SELECT count(company.company_id) as count
    from company
    INNER JOIN
    company_user on company_user.company_id = company.company_id
    AND company.company_id != '${company_id1}'
    AND company.company_id = '${company_id}'
    ${queryRequest}`;

    let count = await queryPromise(queryCount);
    count = count[0].count;

    let companies = [];
    for (const company of companies_city) {
      let comp = new Company({ company_id: company.company_id });
      await comp.getFirstLocationsDB();
      location = `${comp.locations[0].city_name}, ${comp.locations[0].state_name}, ${comp.locations[0].country_name}`;
      company.location = location;
      companies.push(company);
    }
    return { companies: companies, count };
  },

  add_friends_suggestion_category_location: async (
    user_id,
    city_id,
    state_id,
    country_id,
    category_id,
    limit = 500
  ) => {
    let user = `select company.company_id as 'company_id',
    company.company_url as 'company_url' from company
    inner join
    company_user on company_user.company_id = company.company_id
    inner join
    users on users.user_id = company_user.user_id
    where users.user_id = '${user_id}'`;

    user = await queryPromise(user);
    if (user.length <= 0) {
      throw new Error("No Company found with this company_id");
    }

    let company_id = user[0].company_id;

    let company = new Company({
      company_id,
    });

    await company.getCategoriesDB();
    await company.getFirstLocationsDB();

    let queryRequest = `and company_user.user_id not in
        (Select requestee_id from bn_partner_request
        where bn_partner_request.requester_id = '${user_id}')
        and company_user.user_id not in
        (select requester_id from bn_partner_request where requestee_id = '${user_id}')
        and company_user.user_id not in
        (select partner2_id from bn_partner where partner1_id = '${user_id}')`;

    let query = `SELECT company.company_business_name as 'name', company.company_id,
    company_user.user_id as 'user_id', company.company_url as 'company_url'
    from company
    INNER JOIN
    company_location on company_location.company_id = company.company_id
    INNER JOIN
    company_category ON company_category.company_id = company.company_id
    INNER JOIN
    company_user on company_user.company_id = company.company_id
    AND company_location.city_id = '${city_id}'
    AND company_category.category_id = '${category_id}'
    AND company.company_id != '${company_id}'
    ${queryRequest} order by name limit ${limit}`;

    let companies_city = await queryPromise(query);

    if (companies_city.length >= 0 && companies_city.length < 100) {
      let queryState = `SELECT company.company_business_name as 'name', company.company_id,
        company_user.user_id as 'user_id', company.company_url as 'company_url'
        from company
        INNER JOIN
        company_location on company_location.company_id = company.company_id
        INNER JOIN
        company_category ON company_category.company_id = company.company_id
        inner join
        company_user on company_user.company_id = company.company_id
        AND company_location.state_id = ?
        AND company_category.category_id = ?
        ${queryRequest} order by name limit ${limit}`;
      companies_city = await queryPromise(queryState, [state_id, category_id]);
    }

    if (companies_city.length >= 0 && companies_city.length < 100) {
      let queryCountry = `SELECT company.company_business_name as 'name', company.company_id,
        company_user.user_id as 'user_id', company.company_url as 'company_url'
        from company
        INNER JOIN
        company_location on company_location.company_id = company.company_id
        INNER JOIN
        company_category ON company_category.company_id = company.company_id
        INNER JOIN company_user on company_user.company_id = company.company_id
        AND company_location.country_id = ?
        AND company_category.category_id = ?
        ${queryRequest} order by name limit ${limit}`;

      companies_city = await queryPromise(queryCountry, [
        country_id,
        category_id,
      ]);
    }
    return { companies: companies_city };
  },

  generate_suggestion_companies_description: async (user_id) => {
    let user = `select company.company_id as 'company_id',
    company.company_url as 'company_url' from company
    inner join
    company_user on company_user.company_id = company.company_id
    inner join
    users on users.user_id = company_user.user_id
    where users.user_id = '${user_id}'`;

    user = await queryPromise(user);
    if (user.length <= 0) {
      throw new Error("No Company found with this company_id");
    }

    let company_id = user[0].company_id;

    let company = new Company({
      company_id,
    });

    await company.getCategoriesDB();
    await company.getFirstLocationsDB();

    // let queryRequest = `and company_user.user_id Not in
    // (Select company_user.user_id as 'user_id' from company
    // inner join company_user on company_user.company_id = company.company_id
    // inner join bn_partner_request on bn_partner_request.requester_id = company_user.user_id
    // and company_user.user_id = '${user_id}')`;

    let queryRequest = `and company_user.user_id not in
        (Select requestee_id from bn_partner_request
        where bn_partner_request.requester_id = '${user_id}')
        and company_user.user_id not in
        (select requester_id from bn_partner_request where requestee_id = '${user_id}')
        and company_user.user_id not in
        (select partner2_id from bn_partner where partner1_id = '${user_id}')
        and company_user.user_id not in
        (select following from bn_follower where follower = '${user_id}')`;

    let categories = company.categories.map((category) => category.category_id);
    let query = `SELECT *
    from company
    INNER JOIN
    company_category ON company_category.company_id = company.company_id
    INNER JOIN
    company_user on company_user.company_id = company.company_id
    AND company_category.category_id in (?)
    AND company.company_id != '${company_id}'
    ${queryRequest} limit 250`;

    let companies = await queryPromise(query, [categories]);

    return companies;
  },

  generate_suggestion_user: async (user_id) => {
    let user = `select company.company_id as 'company_id',
    company.company_url as 'company_url' from company
    inner join
    company_user on company_user.company_id = company.company_id
    inner join
    users on users.user_id = company_user.user_id
    where users.user_id = '${user_id}'`;

    user = await queryPromise(user);
    if (user.length <= 0) {
      throw new Error("No Company found with this company_id");
    }

    let company_id = user[0].company_id;

    let company = new Company({
      company_id,
    });
    await company.getCategoriesDB();
    await company.getFirstLocationsDB();

    // let queryRequest = `and company_user.user_id Not in
    // (Select company_user.user_id as 'user_id' from company
    // inner join company_user on company_user.company_id = company.company_id
    // inner join bn_partner_request on bn_partner_request.requester_id = company_user.user_id
    // and company_user.user_id = '${user_id}')`;

    let queryRequest = `and company_user.user_id not in
        (Select requestee_id from bn_partner_request
        where bn_partner_request.requester_id = '${user_id}')
        and company_user.user_id not in
        (select requester_id from bn_partner_request where requestee_id = '${user_id}')
        and company_user.user_id not in
        (select partner2_id from bn_partner where partner1_id = '${user_id}')
        and company_user.user_id not in
        (select following from bn_follower where follower = '${user_id}')`;

    let categories = company.categories.map((category) => category.category_id);
    let query = `SELECT *
    from company
    INNER JOIN
    company_category ON company_category.company_id = company.company_id
    INNER JOIN
    company_user on company_user.company_id = company.company_id
    AND company_category.category_id in (?)
    AND company.company_id != '${company_id}'
    ${queryRequest} limit 20`;

    let companies = await queryPromise(query, [categories]);

    return companies;
  },

  generate_suggestion_companies_description_by_user_selected_categories: async (
    user_id
  ) => {
    let selectedCategories = `select category_id from user_selected_category where user_id = ?`;

    let categories = await queryPromise(selectedCategories, [user_id]);

    categories = categories.map((category) => {
      return category.category_id;
    });

    if (categories.length <= 0) {
      return [];
    }

    let queryRequest = `and company_user.user_id not in
        (Select requestee_id from bn_partner_request
        where bn_partner_request.requester_id = '${user_id}')
        and company_user.user_id not in
        (select requester_id from bn_partner_request where requestee_id = '${user_id}')
        and company_user.user_id not in
        (select partner2_id from bn_partner where partner1_id = '${user_id}')
        and company_user.user_id not in
        (select following from bn_follower where follower = '${user_id}')`;

    let query = `SELECT *
    from company
    INNER JOIN
    company_category ON company_category.company_id = company.company_id
    INNER JOIN
    company_user on company_user.company_id = company.company_id
    AND company_category.category_id in (?)
    AND company_user.user_id != '${user_id}'
    ${queryRequest} limit 250`;

    let companies = await queryPromise(query, [categories]);

    return companies;
  },
};
