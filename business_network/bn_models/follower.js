const { queryPromise } = require("../../database/database");

module.exports = {
  async follow(follower, following) {
    const check = await this.checkFollowerBool(follower, following);
    if (!!check) {
      await this.unfollow(follower, following);
      return "Follow";
    }
    const query = `insert into bn_follower(follower,following) values(?,?)`;
    await queryPromise(query, [follower, following]);
    return "Following";
  },

  async unfollow(follower, following) {
    const query = `delete from bn_follower where follower = ? and following = ?`;
    await queryPromise(query, [follower, following]);
  },

  async getFollowers(user_id) {
    const query = `select company.company_business_name as 'name',
    company.company_url as 'company_url',
    bn_follower.follower as 'follower',
    bn_follower.following as 'following'
    from bn_follower
    inner join
    company_user on bn_follower.follower = company_user.user_id
    inner join
    company on company.company_id = company_user.company_id
    and following = ?`;
    return await queryPromise(query, [user_id]);
  },

  async getFollowingsId(user_id) {
    const query = `select following from bn_follower where follower = ?;`;
    const followings = await queryPromise(query, [user_id]);
    const result = [];
    followings.forEach((following) => {
      result.push(following.following);
    });
    return result;
  },

  async getFollowings(user_id) {
    const query = `select company.company_business_name as 'name',
    company.company_url as 'company_url',
    bn_follower.follower as 'follower',
    bn_follower.following as 'following'
    from bn_follower
    inner join
    company_user on bn_follower.following = company_user.user_id
    inner join
    company on company.company_id = company_user.company_id
    and follower = ?`;
    return await queryPromise(query, [user_id]);
  },

  async getFollowersCount(user_id) {
    const query = `select count(follower) as 'count' from bn_follower
    where following = ?`;
    return await queryPromise(query, [user_id]);
  },

  async checkFollowerBool(follower, following) {
    const query = `select * from bn_follower where follower = ? and following = ?`;
    const count = await queryPromise(query, [follower, following]);
    if (count.length > 0) {
      return true;
    }
    return false;
  },

  async checkFollower(follower, following) {
    const query = `select * from bn_follower where follower = ? and following = ?`;
    const count = await queryPromise(query, [follower, following]);

    if (count.length > 0) {
      return "Following";
    }
    return "Follow";
  },
};
