const { queryPromise, selectPromise } = require("../../database/database");
const random = require("randomstring");
const Comments = require("./comment");
const Likes = require("./like");
const Follow = require("./follower");

class Post {
  constructor({
    post_id,
    post_content,
    post_date,
    share_id,
    user_id,
    images,
    videos,
  }) {
    this.post_id = post_id;
    this.post_content = post_content;
    this.post_date = post_date;
    this.share_id = share_id;
    this.user_id = user_id;
    this.images = images;
    this.videos = videos;
  }

  async getRelatedPosts(user_id, number_last = 0) {
    const Friend = require("./friend");
    let fofs = await Friend.getFOF(user_id);
    const followings = await Follow.getFollowingsId(user_id);
    fofs.push(...followings);

    fofs = [...new Set(fofs)];

    fofs.push(user_id);

    const query = `select bn_post.post_id as 'post_id', bn_post.share_id as
				'share_id', bn_post_user.user_id as 'created_by',
				company.company_id as 'company_id', company_url, company_business_name,
				post_date,post_content from bn_post inner join bn_post_user on
				bn_post.post_id = bn_post_user.post_id
        inner join company_user on bn_post_user.user_id = company_user.user_id
        inner join company on company.company_id = company_user.company_id
        and bn_post_user.user_id in (?) order by post_date desc`;

    const post_ids = await queryPromise(query, [fofs]);
    const posts = [];
    for (const post of post_ids) {
      let { post_id, share_id, created_by } = post;
      let fofsPost = await Friend.getFOF(created_by);
      let fofPostCount = fofsPost.length;
      if (!!share_id) {
        const image_query = `select * from bn_post inner join bn_post_image on
				bn_post.post_id = bn_post_image.post_id and bn_post.post_id = ?`;
        const images = await queryPromise(image_query, [share_id]);
        const video_query = `select * from bn_post inner join bn_post_video
				on bn_post.post_id = bn_post_video.post_id and bn_post.post_id = ?`;
        const videos = await queryPromise(video_query, [share_id]);
        post.shared_post = {};
        const post_content = `select bn_post.post_id as 'post_id',
				bn_post_user.user_id as 'user_id', company.company_id as 'company_id',
				company_url, company_business_name, post_date, post_content from bn_post
				inner join bn_post_user on bn_post.post_id = bn_post_user.post_id
				inner join company_user on bn_post_user.user_id = company_user.user_id
				inner join company on company.company_id = company_user.company_id
				and bn_post.post_id = ?`;
        const shared_post = await queryPromise(post_content, [share_id]);
        if (shared_post.length > 0) {
          post.shared_post = shared_post[0];
          post.shared_post.images = images;
          post.shared_post.video = videos;
        }
      } else {
        const image_query = `select * from bn_post inner join bn_post_image
				on bn_post.post_id = bn_post_image.post_id and bn_post.post_id = ?`;
        const images = await queryPromise(image_query, [post_id]);
        post.images = images;
        const video_query = `select * from bn_post inner join bn_post_video
				on bn_post.post_id = bn_post_video.post_id and bn_post.post_id = ?`;
        const videos = await queryPromise(video_query, [post_id]);
        post.video = videos;
      }
      const likes_count = await Likes.getLikesCount(post_id);
      post.likes_count = likes_count;
      const liked_by_user = await Likes.likedByUser(post_id, user_id);
      post.liked_by_user = liked_by_user;
      const share_query = `select count(*) as 'share_count' from bn_post
			inner join bn_post_share on bn_post.post_id = bn_post_share.post_id
			and bn_post.post_id = ?`;
      const shares = await queryPromise(share_query, [post_id]);
      post.share_count = shares[0].share_count;
      const comment_count_query = `select count(*) as 'comment_count'
			from bn_post inner join bn_post_comment
			on bn_post.post_id = bn_post_comment.post_id and bn_post.post_id = ?`;
      const comment_count = await queryPromise(comment_count_query, [post_id]);
      post.comment_count = comment_count[0].comment_count;
      const comments = await Comments.getTopFiveComments(post_id);
      post.comments = comments;
      post.reach = fofPostCount;
      posts.push(post);
    }
    return posts;
  }

  async getPostByUserCount(user_id) {
    const query = `select count(bn_post.post_id) as count from bn_post
		inner join bn_post_user on bn_post.post_id  = bn_post_user.post_id
		and bn_post_user.user_id = ?`;
    const result = await queryPromise(query, [user_id]);
    return result[0].count;
  }

  async getPosts(user_id) {
    const query = `select bn_post.post_id as 'post_id', bn_post.share_id as
		 		'share_id', bn_post_user.user_id as 'created_by',
				company.company_id as 'company_id',
        company_url, company_business_name, post_date, post_content from bn_post
        inner join bn_post_user on bn_post.post_id = bn_post_user.post_id
        inner join company_user on bn_post_user.user_id = company_user.user_id
        inner join company on company.company_id = company_user.company_id
        order by post_date desc`;

    const post_ids = await selectPromise(query);
    const posts = [];
    for (const post of post_ids) {
      let { post_id, share_id } = post;
      if (!!share_id) {
        const image_query = `select * from bn_post inner join bn_post_image
				on bn_post.post_id = bn_post_image.post_id and bn_post.post_id = ?`;
        const images = await queryPromise(image_query, [share_id]);
        const video_query = `select * from bn_post inner join bn_post_video
				on bn_post.post_id = bn_post_video.post_id and bn_post.post_id = ?`;
        const videos = await queryPromise(video_query, [share_id]);
        post.shared_post = {};
        const post_content = `select bn_post.post_id as 'post_id',
				bn_post_user.user_id as 'user_id', company.company_id as 'company_id',
				company_url, company_business_name, post_date, post_content from bn_post
				inner join bn_post_user on bn_post.post_id = bn_post_user.post_id
				inner join company_user on bn_post_user.user_id = company_user.user_id
				inner join company on company.company_id = company_user.company_id
				and bn_post.post_id = ?`;
        const shared_post = await queryPromise(post_content, [share_id]);
        if (shared_post.length > 0) {
          post.shared_post = shared_post[0];
          post.shared_post.images = images;
          post.shared_post.video = videos;
        }
      } else {
        const image_query = `select * from bn_post inner join bn_post_image
				on bn_post.post_id = bn_post_image.post_id and bn_post.post_id = ?`;
        const images = await queryPromise(image_query, [post_id]);
        post.images = images;
        const video_query = `select * from bn_post inner join bn_post_video
				on bn_post.post_id = bn_post_video.post_id and bn_post.post_id = ?`;
        const videos = await queryPromise(video_query, [post_id]);
        post.video = videos;
      }
      const likes_count = await Likes.getLikesCount(post_id);
      post.likes_count = likes_count;
      const liked_by_user = await Likes.likedByUser(post_id, user_id);
      post.liked_by_user = liked_by_user;
      const share_query = `select count(*) as 'share_count' from bn_post
			inner join bn_post_share on
			bn_post.post_id = bn_post_share.post_id and bn_post.post_id = ?`;
      const shares = await queryPromise(share_query, [post_id]);
      post.share_count = shares[0].share_count;
      const comment_count_query = `select count(*) as 'comment_count' from
			bn_post inner join bn_post_comment on
			bn_post.post_id = bn_post_comment.post_id and bn_post.post_id = ?`;
      const comment_count = await queryPromise(comment_count_query, [post_id]);
      post.comment_count = comment_count[0].comment_count;
      const comments = await Comments.getTopFiveComments(post_id);
      post.comments = comments;
      posts.push(post);
    }
    return posts;
  }

  async getPostById(post_id) {
    const query = `select bn_post.post_id as 'post_id', bn_post.share_id as
		'share_id', bn_post_user.user_id as 'user_id',
		company.company_id as 'company_id',
		company_url, company_business_name, post_date, post_content from bn_post
		inner join bn_post_user on bn_post.post_id = bn_post_user.post_id
		inner join company_user on bn_post_user.user_id = company_user.user_id
		inner join company on company.company_id = company_user.company_id
		where bn_post.post_id = ?`;
    return await queryPromise(query, [post_id]);
  }

  async getPostByIdForView(postId, user_id = "") {
    const query = `select bn_post.post_id as 'post_id', bn_post.share_id
		as 'share_id', bn_post_user.user_id as 'created_by',
		company.company_id as 'company_id',
		company_url, company_business_name, post_date, post_content from bn_post
		inner join bn_post_user on bn_post.post_id = bn_post_user.post_id
		inner join company_user on bn_post_user.user_id = company_user.user_id
		inner join company on company.company_id = company_user.company_id
		 where bn_post.post_id = ?`;
    let post = await queryPromise(query, [postId]);
    if (post.length <= 0) {
      return {
        message: "Post not found",
      };
    }

    post = post[0];

    let { post_id, share_id } = post;
    if (!!share_id) {
      const image_query = `select * from bn_post inner join bn_post_image
			on bn_post.post_id = bn_post_image.post_id and bn_post.post_id = ?`;
      const images = await queryPromise(image_query, [share_id]);
      post.shared_post = {};
      const post_content = `select bn_post.post_id as 'post_id',
			bn_post_user.user_id as 'user_id', company.company_id as 'company_id',
			company_url, company_business_name, post_date, post_content from bn_post
			inner join bn_post_user on bn_post.post_id = bn_post_user.post_id
			inner join company_user on bn_post_user.user_id = company_user.user_id
			inner join company on company.company_id = company_user.company_id
			and bn_post.post_id = ?`;
      const shared_post = await queryPromise(post_content, [share_id]);
      if (shared_post.length > 0) {
        post.shared_post = shared_post[0];
        post.shared_post.images = images;
      }
    } else {
      const image_query = `select * from bn_post inner join bn_post_image
			on bn_post.post_id = bn_post_image.post_id and bn_post.post_id = ?`;
      const images = await queryPromise(image_query, [post_id]);
      post.images = images;
    }
    const likes_count = await Likes.getLikesCount(post_id);
    post.likes_count = likes_count;
    const liked_by_user = await Likes.likedByUser(post_id, user_id);
    post.liked_by_user = liked_by_user;
    const share_query = `select count(*) as 'share_count' from bn_post
		inner join bn_post_share on bn_post.post_id = bn_post_share.post_id
		and bn_post.post_id = ?`;
    const shares = await queryPromise(share_query, [post_id]);
    post.share_count = shares[0].share_count;
    const comment_count_query = `select count(*) as 'comment_count' from bn_post
		inner join bn_post_comment on bn_post.post_id = bn_post_comment.post_id
		and bn_post.post_id = ?`;
    const comment_count = await queryPromise(comment_count_query, [post_id]);
    post.comment_count = comment_count[0].comment_count;
    const comments = await Comments.getTopFiveComments(post_id);
    post.comments = comments;
    return post;
  }

  async create(user_id, post_content, images = [], videos = []) {
    const post_id = this.getPostId();
    const query = `insert into bn_post(post_id,post_content)
		values('${post_id}','${post_content}');
		insert into bn_post_user(post_id,user_id)
		values('${post_id}','${user_id}');`;

    try {
      await queryPromise(query);

      for (const image of images) {
        const image_query = `insert into bn_post_image(post_id, post_image)
				value (?,?)`;
        await queryPromise(image_query, [post_id, `/${image}`]);
      }

      for (const video of videos) {
        const video_query = `insert into bn_post_video(post_id, post_video)
				value (?,?)`;
        await queryPromise(video_query, [post_id, `${video}`]);
      }

      return post_id;
    } catch (error) {
      console.log(error);
    }
  }

  async deleteSharedPost(post_id, share_id) {
    let query = `delete from bn_post_user where post_id = ?;
            delete from bn_post_like where post_id = ?;
            delete from bn_post_comment where post_id = ?;
            delete from bn_post where post_id = ?;
            delete from bn_post_share where post_id = ?;`;
    return await queryPromise(query, [
      post_id,
      post_id,
      post_id,
      post_id,
      share_id,
    ]);
  }

  async deletePost(post_id) {
    const queryShare = `select * from bn_post where share_id is not null
		and post_content is null and post_id = ?`;
    let shared = await queryPromise(queryShare, [post_id]);
    if (shared.length > 0) {
      return this.deleteSharedPost(post_id, shared[0].share_id);
    }

    const shared_by_post = `select * from bn_post where share_id = ?`;
    const shared_by_post_result = await queryPromise(shared_by_post, [post_id]);

    shared_by_post_result.forEach(async (post) => {
      await this.deleteSharedPost(post.post_id, post.share_id);
    });

    const query = `delete from bn_post_image where post_id = ?;
            delete from bn_post_video where post_id = ?;
            delete from bn_post_user where post_id = ?;
            delete from bn_post_like where post_id = ?;
            delete from bn_post_comment where post_id = ?;
            delete from bn_post_report where post_id = ?;
            delete from bn_post where post_id = ?;`;

    return await queryPromise(query, [
      post_id,
      post_id,
      post_id,
      post_id,
      post_id,
      post_id,
      post_id,
    ]);
  }

  async deleteReport(post_id, report_id) {
    const query = `delete from bn_post_report where post_id = ? and report_id = ?`;
    return await queryPromise(query, [post_id, report_id]);
  }

  async updatePost(post_id, post_content, images = [], videos = []) {
    const query = `update bn_post set post_content = ? where post_id = ?`;

    try {
      const r = await this.getPostById(post_id);
      await queryPromise(query, [post_content, post_id]);
      return r;
    } catch (error) {
      console.log(error);
    }
  }

  getPostId() {
    const id = random.generate({
      length: 30,
      charset: "hex",
    });
    const post_id = new Date().getTime() + id;
    return post_id;
  }
}

module.exports = Post;
