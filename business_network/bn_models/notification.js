const { queryPromise, selectPromise } = require("../../database/database");
const random = require("randomstring");

class Notification {
  constructor({ notification_id, notification_content, notification_date }) {
    this.notification_id = notification_id;
    this.notification_content = notification_content;
    this.notification_date = notification_date;
  }

  async getNotifications(user_id) {
    const query = `select bn_notification.notification_id as 'notification_id',
        bn_notification_user.user_id as 'created_by',
				company.company_id as 'company_id', company_url, company_business_name,
				notification_date,notification_content from bn_notification inner join bn_notification_user on
				bn_notification.notification_id = bn_notification_user.notification_id
        inner join company_user on bn_notification_user.user_id = company_user.user_id
        inner join company on company.company_id = company_user.company_id
        and bn_notification_user.user_id = ? order by notification_date desc`;
    let notifications = await queryPromise(query, [user_id]);
    return notifications;
  }

  async getNotificationByUserCount(user_id) {
    const query = `select count(bn_notification.notification_id) as count from bn_notification
		inner join bn_notification_user on bn_notification.notification_id  = bn_notification_user.notification_id
		and bn_notification_user.user_id = ?`;
    const result = await queryPromise(query, [user_id]);
    return result[0].count;
  }

  async create(user_id, notification_content) {
    const notification_id = this.getNotificationId();
    const query = `insert into bn_notification(notification_id,notification_content)
		values('${notification_id}','${notification_content}');
		insert into bn_notification_user(notification_id,user_id)
		values('${notification_id}','${user_id}');`;

    try {
      await queryPromise(query);
      return notification_id;
    } catch (error) {
      return 0;
    }
  }

  async deleteNotification(notification_id) {
    const query = `delete from bn_notification_user where notification_id = ?;
            delete from bn_notification where notification_id = ?;`;

    return await queryPromise(query, [notification_id, notification_id]);
  }

  getNotificationId() {
    const id = random.generate({
      length: 30,
      charset: "hex",
    });
    const notification_id = new Date().getTime() + id;
    return notification_id;
  }
}

module.exports = Notification;
