const Company = require("../../database/company");
const { queryPromise } = require("../../database/database");
const Friend = require("./friend");
const Suggestions = require("./suggestions");

const rewrite = (name) =>
  name
    .replace(/[^a-z0-9A-z]/g, "-")
    .replace(/-+$/, "")
    .replace(/-+/g, "-")
    .toLowerCase();

module.exports = {
  async post(user_id) {
    const query = `select * from company
    inner join
    company_user on company.company_id = company_user.company_id
    and company_user.user_id = ?`;
    let company = await queryPromise(query, [user_id]);
    if (company.length <= 0) {
      throw new Error("No Company found with this company_id");
    }
    company = company[0];
    return company;
  },

  async suggestions(user_id) {
    return await Suggestions.generate_suggestion_companies_description(user_id);
  },

  async userSuggestions(
    user_id,
    city_id = "",
    state_id = "",
    country_id = "",
    category_id = "",
    limit = 500,
    offset = 0,
    dir = "desc"
  ) {
    let location = "";
    if (city_id !== "null") {
      type = "city";
      location = city_id;
    } else if (state_id != "null") {
      type = "state";
      location = state_id;
    } else if (!!country_id) {
      type = "country";
      location = country_id;
    }
    const { companies, count } =
      await Suggestions.generate_suggestion_category_location(
        user_id,
        location,
        type,
        category_id,
        limit,
        offset,
        dir
      );
    return { companies, count };
  },

  async userSuggestionCompany(
    user_id,
    company_id,
    limit = 500,
    offset = 0,
    dir = "desc"
  ) {
    const { companies, count } = await Suggestions.generate_suggestion_company(
      user_id,
      company_id,
      limit,
      offset,
      dir
    );
    return { companies, count };
  },

  async userSuggestionsMakeFriends(
    user_id,
    city_id = "",
    state_id = "",
    country_id = "",
    category_id = "",
    limit = 500,
    offset = 0,
    dir = "desc"
  ) {
    let location = "";
    if (city_id !== "null") {
      type = "city";
      location = city_id;
    } else if (state_id != "null") {
      type = "state";
      location = state_id;
    } else if (!!country_id) {
      type = "country";
      location = country_id;
    }
    const count = await Friend.getFriendCount(user_id);
    if (count.count > 500) {
      throw new Error("Can not add more than 500 Friends");
    }
    const { companies } =
      await Suggestions.generate_suggestion_category_location(
        user_id,
        location,
        type,
        category_id,
        limit - count.count,
        offset,
        dir
      );
    for (const company of companies) {
      Friend.addFriend(user_id, company.user_id);
    }
    return { companies };
  },

  async userSuggestionsCompany(
    term = "a",
    city_id = "",
    state_id = "",
    country_id = "",
    limit = 500,
    offset = 0,
    dir = "desc"
  ) {
    let queryCount = "";
    let query = "";
    let type = "";
    let location = "";
    if (!!city_id && !!state_id && !!country_id) {
      type = "city";
      location = city_id;
    } else if (!!state_id && !!country_id) {
      type = "state";
      location = state_id;
    } else if (!!country_id) {
      type = "country";
      location = country_id;
    }
    if (term == "0-9") {
      queryCount = `select count(company.company_id) as 'total' from company_location
    inner join company on company_location.company_id = company.company_id and
    company_business_name regexp '^[${term}]' and company_location.${type}_id =
    '${location}';`;

      query = `select * from(
    (select 1 as sort_order, company.company_id, company.company_business_name,
    company.company_url from company_location inner join company on
    company_location.company_id = company.company_id and
    company_business_name like '^[${term}]' and
    company_location.${type}_id = '${location}'
    and (company.premium_expiry_date - ${new Date().getTime()}) >= 0
    order by company.company_business_name)
    union
    (select 2 as sort_order, company.company_id, company.company_business_name,
    company.company_url from company_location
    inner join company on company_location.company_id = company.company_id and
    company_business_name like '^[${term}]' and
    company_location.${type}_id = '${location}'
    and (${new Date().getTime()} - company.premium_expiry_date) >= 0
    order by company.company_business_name
    )) as result order by sort_order, company_business_name ${dir}
    limit ${limit} offset ${offset}`;
    } else {
      queryCount = `select count(company.company_id) as 'total' from
    company_location inner join
    company on company_location.company_id = company.company_id and
    company_business_name like '${term}%' and
    company_location.${type}_id = '${location}';`;

      query = `select * from(
    (select 1 as sort_order, company.company_id, company.company_business_name ,
    company.company_url from company_location
    inner join company on company_location.company_id = company.company_id and
    company_business_name like '${term}%' and
    company_location.${type}_id = '${location}'
    and (company.premium_expiry_date - ${new Date().getTime()}) >= 0
    order by company.company_business_name)
    union
    (select 2, company.company_id, company.company_business_name ,
    company.company_url from company_location
    inner join company on company_location.company_id = company.company_id and
    company_business_name like '${term}%' and
    company_location.${type}_id = '${location}'
    and (${new Date().getTime()} - company.premium_expiry_date) >= 0
    order by company.company_business_name
    )) as result order by sort_order, company_business_name ${dir}
    limit ${limit} offset ${offset}`;
    }

    const r = await queryPromise(query);
    let count = await queryPromise(queryCount);
    count = count[0].total;

    let companies = [];

    for (const s of r) {
      let comp = new Company(s);
      await comp.getFirstLocationsDB();
      await comp.getUserDB();
      location = `${comp.locations[0].city_name}, ${comp.locations[0].state_name}, ${comp.locations[0].country_name}`;
      s.location = location;
      s.user_id = comp.user_id;
      companies.push(s);
    }

    return { companies, count };
  },

  async selectedCategories(user_id) {
    const query = `select category.category_name as name, category.category_id
    as value from category
    inner join user_selected_category on
    user_selected_category.category_id = category.category_id and
    user_selected_category.user_id = ?`;
    return await queryPromise(query, [user_id]);
  },

  async categoriesCompanies(user_id, offset) {
    const query = `select category.category_name as name, category.category_id as value from
    category where category.category_id
    NOT IN (select category.category_id from category
    inner join user_selected_category on
    user_selected_category.category_id = category.category_id and
    user_selected_category.user_id = ?) order by name asc limit 50 offset ?`;
    return await queryPromise(query, [user_id, 50 * offset]);
  },

  async searchCompaneisbyCategory(user_id, offset, term) {
    const query = `select category_name as name, category_id as value from
    category where category_id
    NOT IN (select category_id from company
    INNER JOIN
    company_category ON company_category.company_id = company.company_id
    INNER JOIN
    company_user ON company_user.company_id = company.company_id
    WHERE company_user.user_id = ?) and REPLACE(category_name_short,"-","") like
    '${rewrite(term).replace("-", "")}%'
    order by name asc limit 50 offset ?`;
    return await queryPromise(query, [user_id, 50 * offset]);
  },

  async addCategory(user_id, category_id) {
    const query = `insert into user_selected_category(user_id, category_id) values(?,?)`;
    return await queryPromise(query, [user_id, category_id]);
  },

  async removeCategory(user_id, category_id) {
    const query = `delete from user_selected_category where user_id = ? and category_id = ?`;
    return await queryPromise(query, [user_id, category_id]);
  },
};
