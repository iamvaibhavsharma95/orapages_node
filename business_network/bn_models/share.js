const { queryPromise, selectPromise } = require("../../database/database");

const random = require("randomstring");

module.exports = {
  sharePost: async (post_id, user_id) => {
    const id = random.generate({
      length: 30,
      charset: "hex",
    });
    const share_id = new Date().getTime() + id;

    const post = `select * from bn_post where post_id = ? and share_id is not null`;
    const post_shared_check = await queryPromise(post, [post_id]);

    let query = `insert into bn_post(post_id,share_id) values('${share_id}','${post_id}');
                    insert into bn_post_user(post_id,user_id) values('${share_id}','${user_id}');
                    insert into bn_post_share(post_id,user_id) values('${post_id}','${user_id}');`;

    if (post_shared_check.length > 0) {
      query = `insert into bn_post(post_id,share_id) values('${share_id}','${post_shared_check[0].share_id}');
            insert into bn_post_user(post_id,user_id) values('${share_id}','${user_id}');
            insert into bn_post_share(post_id,user_id) values('${post_shared_check[0].share_id}','${user_id}');`;
    }

    try {
      await queryPromise(query);
      return post_id;
    } catch (error) {
      return "";
    }
  },

  getShares: async (post_id) => {
    const query = `select company_business_name, company_url from bn_post
                    inner join bn_post_user on bn_post_user.post_id = bn_post.post_id
                    inner join bn_post_share on bn_post.post_id = bn_post_share.post_id
                    inner join company_user on company_user.user_id = bn_post_share.user_id
                    inner join company on company.company_id = company_user.company_id
                    and bn_post.post_id = ?`;
    return await queryPromise(query, [post_id]);
  },

  getSharesCount: async (post_id) => {
    const query = `select count(user_id) as shares_count from bn_post_share where post_id = ?`;
    let count = await queryPromise(query, [post_id]);
    count = count[0].shares_count;
    return count;
  },
};
