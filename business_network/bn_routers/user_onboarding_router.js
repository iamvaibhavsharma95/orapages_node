const csurf = require("csurf");
const express = require("express");
const router = express.Router();
const OnboardingController = require("../bn_controllers/user_onboarding_controller");
const csrfProtection = csurf({
  cookie: true,
});

const {
  owner_auth,
  owner_auth_ajax,
  owner_auth_ajax_post,
} = require("../../middlewares/owner_auth");

router.use(csrfProtection);

router.get("/", owner_auth, OnboardingController.suggestions);

router.get("/post", owner_auth, OnboardingController.post);

router.get("/categories", owner_auth_ajax, OnboardingController.getCategories);

router.get(
  "/search_category",
  owner_auth_ajax,
  OnboardingController.getCategoriesByTerm
);

router.get("/add_category", owner_auth_ajax, OnboardingController.addCategory);

router.get(
  "/populate_suggestions",
  owner_auth_ajax,
  OnboardingController.populateSuggestions
);

router.get(
  "/populate_suggestions_location_category",
  owner_auth_ajax,
  OnboardingController.populateSuggestions
);

router.get(
  "/populate_suggestion_company",
  owner_auth_ajax,
  OnboardingController.populateSuggestionsCompany
);

router.get(
  "/add_suggestions_category",
  owner_auth_ajax,
  OnboardingController.makeSuggestionsFriends
);

router.get(
  "/populate_suggestions_company_name",
  owner_auth_ajax,
  OnboardingController.populateSuggestionCompany
);

router.get(
  "/remove_category",
  owner_auth_ajax,
  OnboardingController.removeCategory
);

module.exports = router;
