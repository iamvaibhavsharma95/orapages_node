const csurf = require('csurf');
const express = require('express');
const router = express.Router();
const ShareController = require('../bn_controllers/share_controller');
const csrfProtection = csurf({
    cookie: true
});

const {owner_auth_ajax,owner_auth_ajax_post} = require('../../middlewares/owner_auth');

router.use(csrfProtection);

router.get('/',owner_auth_ajax, ShareController.share);
router.get('/get_shares',owner_auth_ajax_post, ShareController.getShares);

module.exports = router;