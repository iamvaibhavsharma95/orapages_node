const csurf = require("csurf");
const express = require("express");
const router = express.Router();
const FollowController = require("../bn_controllers/follow_controller");
const csrfProtection = csurf({
  cookie: true,
});

const { owner_auth_ajax } = require("../../middlewares/owner_auth");

router.use(csrfProtection);

router.post("/", owner_auth_ajax, FollowController.follow);
router.get("/", owner_auth_ajax, FollowController.getFollowers);
router.get("/followings", owner_auth_ajax, FollowController.getFollowings);
router.post("/unfollow", owner_auth_ajax, FollowController.unfollow);
router.post("/remove_follow", owner_auth_ajax, FollowController.removeFollow);
router.get("/check", owner_auth_ajax, FollowController.checkFollow);
router.get("/:user_id", FollowController.getFollowers);

module.exports = router;
