const csurf = require('csurf');
const express = require('express');
const router = express.Router();
const LikeController = require('../bn_controllers/like_controller');
const csrfProtection = csurf({
    cookie: true
});

const {
    owner_auth_ajax, owner_auth_ajax_post
} = require('../../middlewares/owner_auth');


router.use(csrfProtection);


router.get("/add_likes", owner_auth_ajax, LikeController.addLike);
router.get("/get_all_likes",owner_auth_ajax_post, LikeController.getLikes);
router.get("/get_like_count", owner_auth_ajax, LikeController.getLikesCount);
router.get("/remove_like", owner_auth_ajax, LikeController.removeLike);

module.exports = router;