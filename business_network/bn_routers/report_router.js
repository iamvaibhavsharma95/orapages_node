const Report = require("../bn_controllers/report_controller");

const csurf = require("csurf");
const express = require("express");
const router = express.Router();
const csrfProtection = csurf({
  cookie: true,
});

const { owner_auth_ajax } = require("../../middlewares/owner_auth");

router.use(csrfProtection);

router.post("/", csrfProtection, owner_auth_ajax, Report.reportPost);

module.exports = router;
