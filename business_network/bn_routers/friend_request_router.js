const csurf = require("csurf");
const express = require("express");
const router = express.Router();
const FriendRequestController = require("../bn_controllers/friend_request_controller");
const csrfProtection = csurf({
  cookie: true,
});

const { owner_auth_ajax } = require("../../middlewares/owner_auth");

router.use(csrfProtection);

router.post("/", owner_auth_ajax, FriendRequestController.sendRequest);
router.get("/", owner_auth_ajax, FriendRequestController.getRequests);
router.get(
  "/requestCount",
  owner_auth_ajax,
  FriendRequestController.getRequestCount
);
router.get("/accept", owner_auth_ajax, FriendRequestController.acceptRequest);
router.get("/decline", owner_auth_ajax, FriendRequestController.deleteRequest);
router.get("/check", owner_auth_ajax, FriendRequestController.checkRequest);

module.exports = router;
