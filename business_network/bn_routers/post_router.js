const csurf = require("csurf");
const express = require("express");
const router = express.Router();
const PostController = require("../bn_controllers/post_controller");
const csrfProtection = csurf({
  cookie: true,
});

const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    if (file.mimetype !== "video/mp4") {
      cb(null, `post_images/`);
    } else {
      cb(null, `post_videos/`);
    }
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`;
    if (file.mimetype !== "video/mp4") {
      cb(
        null,
        file.fieldname +
          "-" +
          uniqueSuffix +
          "." +
          file.originalname.split(".")[1]
      );
    } else {
      cb(
        null,
        file.fieldname +
          "-" +
          uniqueSuffix +
          "." +
          file.mimetype.replace("video/", "")
      );
    }
  },
});

const upload = multer({
  storage: storage,
});

const {
  owner_auth_ajax,
  owner_auth_ajax_post,
} = require("../../middlewares/owner_auth");

router.use(csrfProtection);

router.post(
  "/",
  owner_auth_ajax,
  upload.fields([
    {
      name: "images",
      maxCount: 20,
    },
    {
      name: "video",
      maxCount: 1,
    },
  ]),
  PostController.create
);
router.post("/edit_post", owner_auth_ajax, PostController.update);
router.get("/delete_post", owner_auth_ajax, PostController.delete);
router.get("/user_id/:user_id", owner_auth_ajax_post, PostController.getAll);
router.get("/:id", owner_auth_ajax_post, PostController.get);

module.exports = router;
