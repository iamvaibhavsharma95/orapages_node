const csurf = require('csurf');
const express = require('express');
const router = express.Router();
const CommentController = require('../bn_controllers/comment_controller');
const csrfProtection = csurf({
    cookie: true
});

const {
    owner_auth_ajax, owner_auth_ajax_post
} = require('../../middlewares/owner_auth');


router.use(csrfProtection);


router.get("/add_comment", owner_auth_ajax, CommentController.addComment);
router.get("/get_all_comments",owner_auth_ajax_post, CommentController.getAllComments);
router.get("/delete_comment", owner_auth_ajax, CommentController.deleteComment);
router.get("/edit_comment", owner_auth_ajax, CommentController.editComment);

module.exports = router;