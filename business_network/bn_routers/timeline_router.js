const Timeline = require("../bn_controllers/timeline");

const csurf = require("csurf");
const express = require("express");
const router = express.Router();
const csrfProtection = csurf({
  cookie: true,
});

const { home_auth } = require("../../middlewares/owner_auth");

router.use(csrfProtection);

router.get("/:url/about", csrfProtection, home_auth, Timeline.about);
router.get("/:url/posts", csrfProtection, home_auth, Timeline.posts);
router.get(
  "/:url/notifications",
  csrfProtection,
  home_auth,
  Timeline.notifications
);
router.get("/post_box", csrfProtection, home_auth, Timeline.post_box);
router.get("/post_box_edit", csrfProtection, home_auth, Timeline.post_box_edit);
router.get("/report_box", csrfProtection, home_auth, Timeline.report_box);
router.get(
  "/initial_post",
  csrfProtection,
  home_auth,
  Timeline.intial_post_box
);

module.exports = router;
