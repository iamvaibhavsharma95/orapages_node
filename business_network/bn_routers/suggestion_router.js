const Suggestions = require('../bn_controllers/suggestion_controller');


const csurf = require('csurf');
const express = require('express');
const router = express.Router();
const csrfProtection = csurf({
    cookie: true
});

const {home_auth,owner_auth_ajax_post} = require('../../middlewares/owner_auth');

router.use(csrfProtection);

router.get('/',owner_auth_ajax_post, Suggestions.suggestions);
router.get('/suggestion_count',owner_auth_ajax_post, Suggestions.suggestions);
router.get('/initial_suggestions',owner_auth_ajax_post,Suggestions.initialSuggestions);

module.exports = router;