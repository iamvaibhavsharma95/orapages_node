const csurf = require("csurf");
const express = require("express");
const router = express.Router();
const NotificationController = require("../bn_controllers/notification_controller");
const csrfProtection = csurf({
  cookie: true,
});

const {
  owner_auth_ajax,
  owner_auth_ajax_post,
} = require("../../middlewares/owner_auth");

router.use(csrfProtection);

router.get(
  "/user_id/:user_id",
  owner_auth_ajax_post,
  NotificationController.getAll
);

module.exports = router;
