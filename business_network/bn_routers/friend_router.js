const csurf = require("csurf");
const express = require("express");
const router = express.Router();
const FriendController = require("../bn_controllers/friend_controller");
const csrfProtection = csurf({
  cookie: true,
});

const { owner_auth_ajax } = require("../../middlewares/owner_auth");

router.use(csrfProtection);

router.get("/", owner_auth_ajax, FriendController.getFriends);
router.get("/getFriends", owner_auth_ajax, FriendController.getFriendsData);
router.get("/friend_count", owner_auth_ajax, FriendController.getFriendsCount);
router.get("/:user_id", FriendController.getFriends);
router.post("/delete_friend", owner_auth_ajax, FriendController.deleteFriend);
router.get("/friend_count/:user_id", FriendController.getFriendsCount);

module.exports = router;
