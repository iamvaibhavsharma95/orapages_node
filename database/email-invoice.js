const mailer = require("nodemailer");
const hbs = require("nodemailer-express-handlebars");

//attach the plugin to the nodemailer transporter
const transporter = mailer.createTransport({
  pool: true,
  host: "mail.orapages.com",
  port: 465,
  secure: true,
  requireTLS: true,
  auth: {
    user: "sales@orapages.com",
    pass: "sales@orapages",
  },
});

transporter.use(
  "compile",
  hbs({
    viewEngine: {
      extName: ".hbs",
      partialsDir: "../emailer",
      layoutsDir: "../emailer",
      defaultLayout: null,
    },
    viewPath: __dirname.replace("database", "") + "/emailer/",
  })
);

let invoiceEmail = async (name, email, date, payment_id) => {
  const mail = {
    from: '"Orapages" <sales@orapages.com>',
    to: email,
    bcc: "bms1608@gmail.com ,iamvaibhavsharma95@gmail.com, yhimanshu86@gmail.com",
    subject: `Congratulations ${name}, Your order has been placed successfully`,
    template: "/invoice",
    context: {
      name,
      email,
      date,
      payment_id,
    },
  };
  await transporter.sendMail(mail);
};

module.exports = invoiceEmail;
