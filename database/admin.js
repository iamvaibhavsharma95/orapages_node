const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { selectPromise, queryPromise } = require("./database");
const random = require("randomstring");

const setAdmin = async (email, password, name = null) => {
  let hashed_password = await bcrypt.hash(password, 8);
  let admin_id = random.generate({
    length: 30,
    charset: "hex",
  });

  const token = jwt.sign({ admin_id }, "orpagesjsonwebtoken", {
    expiresIn: "2 days",
  });

  await queryPromise(`insert into admins(admin_id,admin_name,admin_email,password)
    values('${admin_id}','${name}','${email}', '${hashed_password}')`);

  await queryPromise(`insert into admin_token(admin_id,token)
    values('${admin_id}','${token}')`);

  return token;
};

const getAdmin = async (admin_email, admin_password) => {
  let admin = await selectPromise(
    `select admin_id, admin_email, password from admins
    where admin_email ='${admin_email}' limit 1`
  );

  if (admin.length <= 0) {
    throw new Error(`No admin records found`);
  }

  let { admin_id, password } = admin[0];

  let result = await bcrypt.compare(admin_password, password);

  if (result) {
    let token = jwt.sign({ admin_id }, "orpagesjsonwebtoken", {
      expiresIn: "1 days",
    });
    await queryPromise(
      `insert into admin_token(admin_id,token) values('${admin_id}','${token}')`
    );
    return token;
  } else {
    throw new Error(`Unable to login`);
  }
};

const logoutAdmin = async (token) => {
  const decoded = jwt.verify(token, "orpagesjsonwebtoken");

  await queryPromise(
    `delete from admin_token where admin_id = '${decoded.admin_id}'`
  );
};

module.exports = {
  setAdmin,
  getAdmin,
  logoutAdmin,
};
