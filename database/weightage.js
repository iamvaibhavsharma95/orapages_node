const { selectPromise, queryPromise } = require("./database");

const Company = require(`./company`);

const insertWeightage = async (
  business_id,
  company_name_weight,
  company_address_weight,
  company_phone_weight,
  company_fax_weight,
  company_email_weight,
  company_website_weight,
  company_description_weight,
  company_image_weight,
  company_keywords_weight,
  company_category_weight,
  company_full_weight,
  company_logo_weight,
  company_video_weight
) => {
  const query = `INSERT INTO
  company_weight(company_id,
    company_name_weight,
    company_address_weight,
    company_phone_weight,
    company_fax_weight,
    company_email_weight,
    company_website_weight,
    company_description_weight,
    company_image_weight,
    company_keywords_weight,
    company_category_weight,
    company_logo_weight,
    company_video_weight,
    company_full_weight)
     VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
  await queryPromise(query, [
    business_id,
    company_name_weight,
    company_address_weight,
    company_phone_weight,
    company_fax_weight,
    company_email_weight,
    company_website_weight,
    company_description_weight,
    company_image_weight,
    company_keywords_weight,
    company_category_weight,
    company_logo_weight,
    company_video_weight,
    company_full_weight,
  ]);
};

const updateWeightage = async (
  business_id,
  company_name_weight,
  company_address_weight,
  company_phone_weight,
  company_fax_weight,
  company_email_weight,
  company_website_weight,
  company_description_weight,
  company_image_weight,
  company_keywords_weight,
  company_category_weight,
  company_full_weight,
  company_logo_weight,
  company_video_weight
) => {
  const query = `update company_weight set
    company_name_weight = ?,
    company_address_weight=?,
    company_phone_weight=?,
    company_fax_weight=?,
    company_email_weight=?,
    company_website_weight=?,
    company_description_weight=?,
    company_image_weight=?,
    company_keywords_weight=?,
    company_category_weight=?,
    company_logo_weight=?,
    company_video_weight=?,
    company_full_weight=? where company_id = ?`;
  await queryPromise(query, [
    company_name_weight,
    company_address_weight,
    company_phone_weight,
    company_fax_weight,
    company_email_weight,
    company_website_weight,
    company_description_weight,
    company_image_weight,
    company_keywords_weight,
    company_category_weight,
    company_logo_weight,
    company_video_weight,
    company_full_weight,
    business_id,
  ]);
};

const calcCategory = (categories) => {
  let total = 0;
  for (let index = 0; index < categories.length && index < 10; index++) {
    const category = categories[index];
    const { category_desc } = category;
    total += 5;
    if (category.category_desc.length > 200) {
      total += 5;
    }
  }
  return total;
};

const calcImage = (images) => {
  if (images.length > 0) {
    let total = 0;
    for (let index = 0; index < 10 && index < images.length; index++) {
      const image = images[index];
      total += 2;
      if (image.image_des.length > 0) {
        total += 3;
      }
    }
    return total;
  }

  return 0;
};

const calcTag = (tags) => {
  let count = tags.length;
  switch (count) {
    case 0:
      return 0;
    case 1:
      return 1;
    case 2:
      return 2;
    case 3:
      return 3;
    case 4:
      return 4;
    case 5:
      return 5;
    case 6:
      return 6;
    case 7:
      return 7;
    case 8:
      return 8;
    case 9:
      return 9;
    default:
      return 10;
  }
};

const calcName = (name) => {
  if (!name) {
    return 0;
  }
  let count = name.trim().split(" ").length;
  if (count >= 4) {
    return 10;
  } else if (count >= 3) {
    return 8;
  } else if (count >= 2) {
    return 6;
  } else {
    return 0;
  }
};

const calcAddress = (address) => {
  if (!address) {
    return 0;
  }
  let count = address.trim().split(" ").length;
  switch (count) {
    case 1:
      return 1;
    case 2:
      return 2;
    case 3:
      return 3;
    case 4:
      return 4;
    case 5:
      return 5;
    case 6:
      return 6;
    case 7:
      return 7;
    case 8:
      return 8;
    case 9:
      return 9;
    default:
      return 10;
  }
};

const calcPhone = (phone) => {
  if (!!phone) {
    return 10;
  } else {
    return 0;
  }
};

const calcFax = (fax) => {
  if (!!fax) {
    return 10;
  } else {
    return 0;
  }
};

const calcEmail = (email) => {
  if (!!email) {
    return 10;
  } else {
    return 0;
  }
};

const calcWebsite = (website) => {
  if (!!website) {
    return 10;
  } else {
    return 0;
  }
};

const calcDescription = (desc) => {
  let count = desc.split(" ").length;
  switch (true) {
    case count < 50:
      return 0;
      break;
    case count > 50 && count < 100:
      return 2;
      break;
    case count > 100 && count < 200:
      return 5;
      break;
    case count > 200 && count < 300:
      return 10;
      break;
    case count > 300 && count < 400:
      return 20;
      break;
    case count > 400 && count < 500:
      return 30;
      break;
    default:
      return 40;
      break;
  }
};

const calcVideo = (video) => {
  if (video.length > 0) {
    return 20;
  } else {
    return 0;
  }
};

const calcLogo = (logo) => {
  if (!logo) {
    return 0;
  } else {
    return 10;
  }
};

const calculateWeightage = async () => {
  try {
    const query = `SELECT * FROM company
    WHERE company.company_id
    not in (SELECT company_id FROM company_weight)`;
    const companies = await queryPromise(query);

    for (const company of companies) {
      let r = new Company(company);
      await r.getCategoriesDB();
      await r.getLogoDB();
      await r.getLocationsDB();
      await r.getVideoDB();
      await r.getTagsDB();
      await r.getImagesDB();

      try {
        const wCat = calcCategory(r.categories);
        const wImage = calcImage(r.images);
        const wlogo = calcLogo(r.logo);
        const wName = calcName(r.company_business_name);
        const wAddress = calcAddress(r.locations[0].complete_address);
        const wDetails = calcDescription(r.company_business_details);
        const wEmail = calcEmail(r.company_email);
        const wFax = calcFax(r.locations[0].fax);
        const wWebsite = calcWebsite(r.company_website);
        const wPhone = calcPhone(r.locations[0].mobile);
        const wVideo = calcVideo(r.videos);
        const wTags = calcTag(r.tags);

        const company_full_weight =
          wCat +
          wImage +
          wlogo +
          wName +
          wAddress +
          wDetails +
          wEmail +
          wFax +
          wWebsite +
          wPhone +
          wTags +
          wVideo;

        await insertWeightage(
          r.company_id,
          wName,
          wAddress,
          wPhone,
          wFax,
          wEmail,
          wWebsite,
          wDetails,
          wImage,
          wTags,
          wCat,
          company_full_weight,
          wlogo,
          wVideo
        );
      } catch (error) {
        await insertWeightage(
          r.company_id,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0
        );
      }
    }
  } catch (error) {
    console.log(error);
  }
};

const calculateWeightageSingle = async (company_id) => {
  try {
    const queryWeigth = `select * from company_weight where company_id = '${company_id}'`;
    const weight = await queryPromise(queryWeigth);
    const query = `select * from company where company_id = '${company_id}'`;
    const company = await queryPromise(query);

    let r = new Company(company[0]);
    await r.getCategoriesDB();
    await r.getLogoDB();
    await r.getLocationsDB();
    await r.getVideoDB();
    await r.getTagsDB();
    await r.getImagesDB();

    const wCat = calcCategory(r.categories);
    const wImage = calcImage(r.images);
    const wlogo = calcLogo(r.logo);
    const wName = calcName(r.company_business_name);
    const wAddress = calcAddress(r.locations[0].complete_address);
    const wDetails = calcDescription(r.company_business_details);
    const wEmail = calcEmail(r.company_email);
    const wFax = calcFax(r.locations[0].fax);
    const wWebsite = calcWebsite(r.company_website);
    const wPhone = calcPhone(r.locations[0].mobile);
    const wVideo = calcVideo(r.videos);
    const wTags = calcTag(r.tags);

    const company_full_weight =
      wCat +
      wImage +
      wlogo +
      wName +
      wAddress +
      wDetails +
      wEmail +
      wFax +
      wWebsite +
      wPhone +
      wTags +
      wVideo;

    if (weight.length > 0) {
      await updateWeightage(
        r.company_id,
        wName,
        wAddress,
        wPhone,
        wFax,
        wEmail,
        wWebsite,
        wDetails,
        wImage,
        wTags,
        wCat,
        company_full_weight,
        wlogo,
        wVideo
      );
    } else {
      await insertWeightage(
        r.company_id,
        wName,
        wAddress,
        wPhone,
        wFax,
        wEmail,
        wWebsite,
        wDetails,
        wImage,
        wTags,
        wCat,
        company_full_weight,
        wlogo,
        wVideo
      );
    }
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  calculateWeightage,
  calculateWeightageSingle,
};
