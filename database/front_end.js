const strip = require("string-strip-html");

const Company = require(`./company`);
const { selectPromise, queryPromise } = require("./database");

const rewrite = (name) =>
  name
    .replace(/[^a-z0-9A-z]/g, "-")
    .replace(/-+$/, "")
    .replace(/-+/g, "-")
    .toLowerCase();

const relatedCompaniesByCompanyPs = async (
  city_id,
  state_id,
  country_id,
  category_id
) => {
  let companies_city = [];
  if (!!city_id) {
    let query = `SELECT * from company
    INNER JOIN
    company_location on company_location.company_id = company.company_id
    INNER JOIN
    company_category ON company_category.company_id = company.company_id
    AND company_location.city_id = ?
    AND company_category.category_id = ? LIMIT 6`;
    companies_city = await queryPromise(query, [city_id, category_id]);
  } else if (
    companies_city.length >= 0 &&
    companies_city.length < 6 &&
    !!state_id
  ) {
    let urls = companies_city.map((url) => url.company_url);
    let queryState;
    if (urls.length > 0) {
      queryState = `SELECT * from company
      INNER JOIN
      company_location on company_location.company_id = company.company_id
      INNER JOIN
      company_category ON company_category.company_id = company.company_id
      AND company_location.state_id = ?
      AND company_category.category_id = ?
      AND company.company_url not in (?)
      LIMIT ${6 - companies_city.length}`;
    } else {
      queryState = `SELECT * from company
      INNER JOIN
      company_location on company_location.company_id = company.company_id
      INNER JOIN
      company_category ON company_category.company_id = company.company_id
      AND company_location.state_id = ?
      AND company_category.category_id = ?
      LIMIT ${6 - companies_city.length}`;
    }
    let companies_state = await queryPromise(queryState, [
      state_id,
      category_id,
      urls,
    ]);
    companies_city.push(...companies_state);
  } else if (
    companies_city.length >= 0 &&
    companies_city.length < 6 &&
    !!country_id
  ) {
    let urls = companies_city.map((url) => url.company_url);
    let queryCountry;
    if (urls.length > 0) {
      queryCountry = `SELECT * from company
      INNER JOIN
      company_location on company_location.company_id = company.company_id
      INNER JOIN
      company_category ON company_category.company_id = company.company_id
      AND company_location.country_id = ?
      AND company_category.category_id = ?
      AND company.company_url not in (?)
      LIMIT ${6 - companies_city.length}`;
    } else {
      queryCountry = `SELECT * from company
      INNER JOIN
      company_location on company_location.company_id = company.company_id
      INNER JOIN
      company_category ON company_category.company_id = company.company_id
      AND company_location.country_id = ?
      AND company_category.category_id = ?
      LIMIT ${6 - companies_city.length}`;
    }
    let companies_country = await queryPromise(queryCountry, [
      country_id,
      category_id,
      urls,
    ]);
    companies_city.push(...companies_country);
  } else if (companies_city.length >= 0 && companies_city.length < 6) {
    let urls = companies_city.map((url) => url.company_url);
    urls.push(company.company_url);
    let queryCountry;
    if (urls.length > 0) {
      queryCountry = `SELECT * from company
      INNER JOIN
      company_category ON company_category.company_id = company.company_id
      AND company_category.category_id = ?
      AND company.company_url not in (?)
      LIMIT ${6 - companies_city.length}`;
    } else {
      queryCountry = `SELECT * from company
      INNER JOIN
      company_category ON company_category.company_id = company.company_id
      AND company_category.category_id = ?
      LIMIT ${6 - companies_city.length}`;
    }
    let companies_country = await queryPromise(queryCountry, [
      category_id,
      urls,
    ]);
    companies_city.push(...companies_country);
  }

  let r = [];
  for (const company of companies_city) {
    let { result } = strip(company.company_business_details.substring(0, 300));
    company.company_business_details = result;
  }

  return companies_city;
};

const peopleAlsoSeeCategory = async (city_id, state_id, country_id) => {
  let related_categories = [];
  if (!!city_id) {
    let query = `select 'city', category.category_name_short,
    category.category_name, company_category.category_id,
    count(company.company_id) as 'count'
    from company_category
    INNER JOIN
    company_location on company_location.company_id = company_category.company_id
    INNER JOIN
    company on company_category.company_id = company.company_id
    INNER JOIN
    category ON category.category_id = company_category.category_id
    AND company_location.city_id = '${city_id}'
    group by category_id order by count DESC limit 20`;
    related_categories = await selectPromise(query);
  } else if (
    !!state_id &&
    related_categories.length >= 0 &&
    related_categories.length < 30
  ) {
    let query = `select 'state', category.category_name_short,
    category.category_name, company_category.category_id,
    count(company.company_id) as 'count'
    from company_category
    INNER JOIN
    company_location on company_location.company_id = company_category.company_id
    INNER JOIN
    company on company_category.company_id = company.company_id
    INNER JOIN
    category ON category.category_id = company_category.category_id
    AND company_location.state_id = '${state_id}'
    group by category_id order by count DESC
    limit ${20 - related_categories.length}`;
    let related = await selectPromise(query);
    related_categories.push(...related);
  } else if (
    !!country_id &&
    related_categories.length >= 0 &&
    related_categories.length < 30
  ) {
    let query = `select 'country', category.category_name_short,
    category.category_name, company_category.category_id,
    count(company.company_id) as 'count'
    from company_category
    INNER JOIN
    company_location on company_location.company_id = company_category.company_id
    INNER JOIN
    company on company_category.company_id = company.company_id
    INNER JOIN
    category ON category.category_id = company_category.category_id
    AND company_location.country_id = '${country_id}'
    GROUP BY category_id order by count DESC
    LIMIT ${20 - related_categories.length}`;
    let related = await selectPromise(query);
    related_categories.push(...related);
  } else if (related_categories.length >= 0 && related_categories.length < 30) {
    let query = `select 'world', category.category_name_short,
    category.category_name, company_category.category_id,
    count(company.company_id) as 'count'
    from company_category
    INNER JOIN
    company on company_category.company_id = company.company_id
    INNER JOIN
    category ON category.category_id = company_category.category_id
    group by category_id order by count DESC
    limit ${20 - related_categories.length}`;
    let related = await selectPromise(query);
    related_categories.push(...related);
  }

  return related_categories;
};

const setBanner = async (id) => {
  const query = `update banner set active=1 where bn_id=${id}`;
  await queryPromise(query);
};

const getBanner = async () => {
  const query = `select bn_id,url from banner where active = 1 and flag = 'on'`;
  return await queryPromise(query);
};

const frontEndBrandCount = async (type, brand, location) => {
  const query = `select count(company_brand.company_id) as count from
  company_brand inner join company_location on
  company_brand.company_id = company_location.company_id
  and brand_id = '${brand}' and company_location.${type}_id = '${location}'`;

  return await queryPromise(query);
};

const frontEndCategoryCount = async (type, category, location) => {
  const query = `select count(company_category.company_id) as count from
  company_category
  inner join
  company_location on company_category.company_id = company_location.company_id
  and category_id = '${category}' and
  company_location.${type}_id = '${location}'`;
  return await queryPromise(query);
};

const relatedCompaniesByCompany = async (company) => {
  let categories = company.categories.map((category) => category.category_id);
  let query = `SELECT * from company
  INNER JOIN
  company_location on company_location.company_id = company.company_id
  INNER JOIN
  company_category ON company_category.company_id = company.company_id
  AND company_location.city_id = ?
  AND company_category.category_id in (?) and company.company_url != ? LIMIT 6`;
  let companies_city = await queryPromise(query, [
    company.locations[0].city_id,
    categories,
    company.company_url,
  ]);
  if (companies_city.length >= 0 && companies_city.length < 6) {
    let urls = companies_city.map((url) => url.company_url);
    urls.push(company.company_url);
    let queryState;
    if (urls.length > 0) {
      queryState = `SELECT * from company
      INNER JOIN
      company_location on company_location.company_id = company.company_id
      INNER JOIN
      company_category ON company_category.company_id = company.company_id
      AND company_location.state_id = ?
      AND company_category.category_id in (?)
      AND company.company_url not in (?) LIMIT ${6 - companies_city.length}`;
    } else {
      queryState = `SELECT * from company
      INNER JOIN
      company_location on company_location.company_id = company.company_id
      INNER JOIN
      company_category ON company_category.company_id = company.company_id
      AND company_location.state_id = ?
      AND company_category.category_id in (?)
      LIMIT ${6 - companies_city.length}`;
    }
    let companies_state = await queryPromise(queryState, [
      company.locations[0].state_id,
      categories,
      urls,
    ]);
    companies_city.push(...companies_state);
  } else if (companies_city.length >= 0 && companies_city.length < 6) {
    let urls = companies_city.map((url) => url.company_url);
    urls.push(company.company_url);
    let queryCountry;
    if (urls.length > 0) {
      queryCountry = `SELECT * from company
      INNER JOIN
      company_location on company_location.company_id = company.company_id
      INNER JOIN
      company_category ON
      company_category.company_id = company.company_id
      AND company_location.country_id = ?
      AND company_category.category_id in (?)
      AND company.company_url not in (?)
      LIMIT ${6 - companies_city.length}`;
    } else {
      queryCountry = `SELECT * from company
      INNER JOIN
      company_location on company_location.company_id = company.company_id
      INNER JOIN
      company_category ON company_category.company_id = company.company_id
      AND company_location.country_id = ?
      AND company_category.category_id in (?)
      LIMIT ${6 - companies_city.length}`;
    }
    let companies_country = await queryPromise(queryCountry, [
      company.locations[0].country_id,
      categories,
      urls,
    ]);
    companies_city.push(...companies_country);
  } else if (companies_city.length >= 0 && companies_city.length < 6) {
    let urls = companies_city.map((url) => url.company_url);
    urls.push(company.company_url);
    let queryCountry;
    if (urls.length > 0) {
      queryCountry = `SELECT * from company
      INNER JOIN
      company_category ON company_category.company_id = company.company_id
      AND company_category.category_id = ?
      AND company.company_url not in (?)
      LIMIT ${6 - companies_city.length}`;
    } else {
      queryCountry = `SELECT * from company
      INNER JOIN
      company_category ON company_category.company_id = company.company_id
      AND company_category.category_id = ?
      LIMIT ${6 - companies_city.length}`;
    }

    let companies_country = await queryPromise(queryCountry, [
      company.locations[0].country_id,
      company.categories[0].category_id,
      urls,
    ]);
    companies_city.push(...companies_country);
  }

  let r = [];
  for (const company of companies_city) {
    let { result } = strip(company.company_business_details.substring(0, 300));
    company.company_business_details = result;
  }

  return companies_city;
};

module.exports = {
  findNextAndSetBanner: async () => {
    try {
      const current = await getBanner();

      const next = `select bn_id from banner where bn_id > ${current[0].bn_id}
    and flag = 'on'`;

      const r = await queryPromise(next);

      if (!r[0]) {
        const first = `select bn_id from banner where flag = 'on'`;
        const s = await queryPromise(first);
        const restDisable = `update banner set active = 0
        where bn_id != ${s[0].bn_id}`;
        await queryPromise(restDisable);
        await setBanner(s[0].bn_id);
      } else {
        const restDisable = `update banner set active = 0
       where bn_id != ${r[0].bn_id}`;
        await queryPromise(restDisable);
        await setBanner(r[0].bn_id);
      }
    } catch (e) {
      const queryOnBanner = `select bn_id from banner where flag = 'on' limit 1`;
      const banner = await queryPromise(queryOnBanner);
      const bn_id = banner[0].bn_id;
      const query = `update banner set active = 1 where bn_id = ${bn_id}`;
      await queryPromise(query);
    }
  },

  getBanners: async () => {
    const query = `select bn_id,url from banner where flag = 'on'
   order by banner_order asc`;
    return await queryPromise(query);
  },

  frontEndLocations: async (term) => {
    const r =
      await queryPromise(`select country_id as 'key', country_name as 'value',
    countries.type from countries where country_name_short
    like '${rewrite(term)}%' union select state_id,
    concat(countries.country_name,'#$%', state_name), states.type
    from states inner join countries on states.country_id =
    countries.country_id and state_name_short like '${rewrite(term)}%'
    union select city_id, CONCAT(countries.country_name, '#$%',
    states.state_name, '#$%', city_name), cities.type from cities
    inner join states on cities.state_id = states.state_id
    inner join countries on cities.country_id = countries.country_id
     and city_name_short like '${rewrite(term)}%' order by value asc`);
    return r;
  },

  countryByName: async (country) => {
    const r = await selectPromise(
      `select * from countries where country_name_short = '${country}'`
    );
    return r;
  },

  stateByName: async (state, country) => {
    const r = await selectPromise(
      `select * from states inner join countries on
    countries.country_id = states.country_id and
    states.state_name_short = '${state}' and
    countries.country_name_short = '${country}'`
    );
    return r;
  },

  cityByName: async (city, state, country) => {
    const r = await selectPromise(`select * from ((cities INNER JOIN states ON
    states.state_name_short = '${state}' and cities.city_name_short =
    '${city}' and states.state_id = cities.state_id)
    inner JOIN countries on states.country_id =
    countries.country_id and countries.country_name_short = '${country}')`);
    return r;
  },

  categoryByName: async (category) => {
    const r = await selectPromise(
      `select * from category where category_name_short = '${category}'`
    );
    return r;
  },

  frontEndCountries: async () => {
    const r = await selectPromise(
      `select country_name as 'value', country_id as 'key' from countries
     order by value asc`
    );
    return r;
  },

  frontEndCountriesHome: async () => {
    const r = await selectPromise(
      `select country_name as 'value', country_name_short as 'key'
    from countries order by value asc`
    );
    return r;
  },

  frontEndStates: async (country_id) => {
    const r =
      await selectPromise(`select states.state_name as 'value', states.state_id
    as 'key' from states inner join countries on states.country_id =
    countries.country_id and countries.country_id = '${country_id}'
    order by value asc`);
    return r;
  },

  frontEndStatesHome: async (country_id) => {
    const r = await selectPromise(`select countries.country_name as 'country',
    states.state_name as 'value', states.state_name_short as 'key' from states
    inner join countries on states.country_id = countries.country_id and
    countries.country_id = (select country_id from countries WHERE
    country_name_short='${country_id}') order by value asc`);
    return r;
  },

  frontEndCities: async (state_id, country_id = null) => {
    if (state_id !== "all_states") {
      const r =
        await selectPromise(`select cities.city_name as 'value', cities.city_id
        as 'key' from cities inner join states on states.state_id =
        cities.state_id and states.state_id = '${state_id}' order by value asc`);
      return r;
    } else {
      const r =
        await selectPromise(`select cities.city_name as 'value', cities.city_id as
       'key' from cities inner join countries on countries.country_id =
        cities.country_id and countries.country_id = '${country_id}'
        order by value asc`);
      return r;
    }
  },

  frontEndCitiesHome: async (state_id, country_id = null) => {
    if (state_id !== "all_states") {
      const r = await selectPromise(`select cities.city_name as value,
        concat(cities.city_name_short,",",states.state_name_short,",",
        countries.country_name_short) as 'key' from
        ((cities INNER JOIN states ON states.state_name_short = '${state_id}'
        and states.state_id = cities.state_id) inner JOIN countries on
        states.country_id = countries.country_id and
        countries.country_name_short = '${country_id}') order
        by cities.city_name asc`);
      return r;
    } else {
      const r = await selectPromise(`select cities.city_name as value,
        concat(cities.city_name_short,",",states.state_name_short,",",
        countries.country_name_short) as 'key' from
        ((cities INNER JOIN states ON states.state_id = cities.state_id)
        inner JOIN countries on states.country_id = countries.country_id and
        countries.country_name_short = '${country_id}')
        order by cities.city_name asc`);
      return r;
    }
  },

  frontEndCitiesHomePage: async (state_id, country_id = null) => {
    if (state_id !== "all_states") {
      const r = await selectPromise(`select states.state_name as 'state',
        countries.country_name as 'country', cities.city_name as value,
        concat(countries.country_name_short,"/",states.state_name_short,"/",
        cities.city_name_short) as 'key' from ((cities INNER JOIN states ON
        states.state_name_short = '${state_id}' and
        states.state_id = cities.state_id) inner JOIN countries on
        states.country_id = countries.country_id and
        countries.country_name_short = '${country_id}')
        order by cities.city_name asc`);
      return r;
    } else {
      const r = await selectPromise(`select states.state as 'state',
        cities.city_name as value, countries.country_name as 'country',
        concat(countries.country_name_short,"/",states.state_name_short,"/",
        cities.city_name_short) as 'key' from ((cities INNER JOIN states ON
        states.state_id = cities.state_id) inner JOIN countries on
        states.country_id = countries.country_id and
        countries.country_name_short = '${country_id}')
        order by cities.city_name asc`);
      return r;
    }
  },

  frontEndCompanyByLocation: async (type, term, location) => {
    const query = `select company.company_business_name as 'value',
    company.company_url as 'key' from company_location
    inner join company on company_location.company_id = company.company_id and
    company_business_name like '${term}%' and
    company_location.${type}_id = '${location}' order by value asc;`;
    const r = await queryPromise(query);
    return r;
  },

  frontEndCompanyByLocationList: async (
    type,
    term,
    location,
    limit,
    offset,
    dir = "asc"
  ) => {
    let query = "";
    let queryCount = "";
    if (term == "0-9") {
      queryCount = `select count(company.company_id) as 'total' from company_location
    inner join company on company_location.company_id = company.company_id and
    company_business_name regexp '^[${term}]' and company_location.${type}_id =
    '${location}';`;

      query = `select * from(
    (select 1 as sort_order, company.company_id, company.company_business_name,
    company.company_url from company_location inner join company on
    company_location.company_id = company.company_id and
    company_business_name like '^[${term}]' and
    company_location.${type}_id = '${location}'
    and (company.premium_expiry_date - ${new Date().getTime()}) >= 0
    order by company.company_business_name)
    union
    (select 2 as sort_order, company.company_id, company.company_business_name,
    company.company_url from company_location
    inner join company on company_location.company_id = company.company_id and
    company_business_name like '^[${term}]' and
    company_location.${type}_id = '${location}'
    and (${new Date().getTime()} - company.premium_expiry_date) >= 0
    order by company.company_business_name
    )) as result order by sort_order, company_business_name ${dir}
    limit ${limit} offset ${offset}`;
    } else {
      queryCount = `select count(company.company_id) as 'total' from
    company_location inner join
    company on company_location.company_id = company.company_id and
    company_business_name like '${term}%' and
    company_location.${type}_id = '${location}';`;

      query = `select * from(
    (select 1 as sort_order, company.company_id, company.company_business_name ,
    company.company_url from company_location
    inner join company on company_location.company_id = company.company_id and
    company_business_name like '${term}%' and
    company_location.${type}_id = '${location}'
    and (company.premium_expiry_date - ${new Date().getTime()}) >= 0
    order by company.company_business_name)
    union
    (select 2, company.company_id, company.company_business_name ,
    company.company_url from company_location
    inner join company on company_location.company_id = company.company_id and
    company_business_name like '${term}%' and
    company_location.${type}_id = '${location}'
    and (${new Date().getTime()} - company.premium_expiry_date) >= 0
    order by company.company_business_name
    )) as result order by sort_order, company_business_name ${dir}
    limit ${limit} offset ${offset}`;
    }

    const r = await queryPromise(query);
    let count = await queryPromise(queryCount);
    count = count[0].total;

    let companies = [];

    for (const s of r) {
      let company = new Company(s);
      await company.getCategoriesDB();
      company.sort_order = s.sort_order;
      companies.push(company);
    }

    return {
      companies,
      count,
    };
  },

  frontEndPremiumCompanies: async (dir = "asc") => {
    const query = `select company.company_id, company.company_business_name,
  company.company_url, company.company_business_details from
  company where ${new Date().getTime()} - company.premium_expiry_date <= -1
  order by company.company_business_name ${dir} limit 20`;

    const r = await queryPromise(query);
    let companies = [];

    for (const s of r) {
      let company = new Company(s);
      let { result } = strip(
        company.company_business_details.substring(0, 300),
        {
          stripTogetherWithTheirContents: [
            "script", // default
            "style", // default
            "xml", // default,
            "img",
            "a",
          ],
        }
      );
      company.company_business_details = result;
      await company.getLogoDB();
      companies.push(company);
    }

    return companies;
  },

  frontEndCategories: async (term) => {
    const query = `select category_id as 'key',category_name as 'value'
  from category where category_name_short like '${rewrite(term)}%' union
  select category.category_id, category.category_name from category_subcategory
  inner join subcategory on subcategory.subcategory_id =
  category_subcategory.subcat_id
  and subcategory.subcategory_name_short like '${rewrite(term)}%'
  inner join category on category.category_id =
  category_subcategory.category_id order by value asc;`;

    const r = await queryPromise(query);
    return r;
  },

  frontEndCategoriesStarts: async (term) => {
    const query = `select category_id as 'key',category_name as 'value' from
  category where REPLACE(category_name_short,"-","") like
  '${rewrite(term).replace(/-/g, "")}%'
  union
  select category.category_id, subcategory.subcategory_name from
  category_subcategory
  inner join
  subcategory on subcategory.subcategory_id = category_subcategory.subcat_id
  and REPLACE(subcategory.subcategory_name_short,"-","") like
  '${rewrite(term).replace(/-/g, "")}%'
  inner join
  category on category.category_id = category_subcategory.category_id
  order by value`;
    const r = await queryPromise(query);
    return r;
  },

  frontEndCategoriesStartsHome: async (term) => {
    const query = `select category_name_short as 'key',category_name as 'value'
  from category where REPLACE(category_name_short,"-","") like
  '${rewrite(term).replace("-", "")}%'
  union
  select category.category_name_short, subcategory.subcategory_name from
  category_subcategory inner join subcategory on
  subcategory.subcategory_id = category_subcategory.subcat_id
  and REPLACE(subcategory.subcategory_name_short,"-","")
  like '${rewrite(term).replace("-", "")}%'
  inner join
  category on category.category_id = category_subcategory.category_id
  order by value`;
    const r = await queryPromise(query);
    return r;
  },

  frontEndBrands: async (term) => {
    const query = `select brand_name as 'value', brand_id as 'key' from brands
  where brand_name_short like '%${rewrite(term)}%' order by value;`;

    const r = queryPromise(query);
    return r;
  },

  frontEndBrandsList: async (term, type, location) => {
    let query = "";

    if (term == "0-9") {
      query = `select brand_name, brand_id from brands where brand_name_short
    regexp '^[${term}]' order by brand_name;`;
    } else {
      query = `select brand_name, brand_id from brands where brand_name_short
    like '${rewrite(term)}%' order by brand_name;`;
    }

    let brands = await queryPromise(query);

    for (const brand of brands) {
      let r = await frontEndBrandCount(type, brand.brand_id, location);
      brand.count = r[0].count;
    }

    return brands;
  },

  frontEndCategoriesList: async (
    term,
    city,
    state,
    country,
    limit,
    offset,
    dir = "asc"
  ) => {
    let query = "";
    let queryCount = "";
    if (term == "0-9") {
      queryCount = `select count(*) as 'count' from
    (select category_id, category_name from category where category_name_short
    regexp '^[${term}]'
    union
    select category.category_id, subcategory.subcategory_name from
    category_subcategory
    inner join
    subcategory on subcategory.subcategory_id = category_subcategory.subcat_id
    and subcategory.subcategory_name_short regexp '^[${term}]'
    inner join
    category on category.category_id = category_subcategory.category_id) x`;

      query = `select category_id,category_name,category_name_short from
    category where category_name_short regexp '^[${term}]'
    union
    select category.category_id, subcategory.subcategory_name,
    category.category_name_short from category_subcategory
    inner join
    subcategory on subcategory.subcategory_id = category_subcategory.subcat_id
    and subcategory.subcategory_name_short regexp '^[${term}]'
    inner join
    category on category.category_id = category_subcategory.category_id
    order by category_name ${dir} limit ${limit} offset ${offset};`;
    } else {
      queryCount = `select count(*) as 'count' from
    (select category_id, category_name from category where
    category_name_short like '${rewrite(term)}%'
    union
    select category.category_id, subcategory.subcategory_name from
    category_subcategory
    inner join
    subcategory on subcategory.subcategory_id = category_subcategory.subcat_id
    and subcategory.subcategory_name_short like
    '${rewrite(term)}%'
    inner join
    category on category.category_id = category_subcategory.category_id) x`;

      query = `select category_id, category_name, category_name_short from
    category where category_name_short like '${rewrite(term)}%'
    union
    select category.category_id, subcategory.subcategory_name,
    category.category_name_short from category_subcategory
    inner join
    subcategory on subcategory.subcategory_id = category_subcategory.subcat_id
    and subcategory.subcategory_name_short like '${rewrite(term)}%'
    inner join
    category on category.category_id = category_subcategory.category_id
    order by category_name ${dir} limit ${limit} offset ${offset};`;
    }

    let queryLocation;
    let type;
    if (!!city && !!state && !!country) {
      type = "city";
      queryLocation = `select cities.city_name,cities.city_id as id from
    ((cities INNER JOIN
    states ON states.state_name_short = '${state}'
    and cities.city_name_short = '${city}'
    and states.state_id = cities.state_id)
    inner JOIN
    countries on states.country_id = countries.country_id
    and countries.country_name_short = '${country}')`;
    } else if (!!state && !!country) {
      type = "state";
      queryLocation = `SELECT states.state_name as city_name,
    states.state_id as id from states
    INNER JOIN
    countries on states.country_id = countries.country_id
    WHERE countries.country_name_short = '${country}'
    AND states.state_name_short="${state}"`;
    } else {
      type = "country";
      queryLocation = `select countries.country_name as city_name,
    countries.country_id as id from countries
    where country_name_short = '${country}'`;
    }

    let cityName = await queryPromise(queryLocation);

    let cityId = cityName[0].id;

    cityName = cityName[0].city_name;

    let categories = await queryPromise(query);

    let count = await queryPromise(queryCount);
    count = count[0].count;

    for (const category of categories) {
      let r = await frontEndCategoryCount(type, category.category_id, cityId);
      category.count = r[0].count;
    }

    return {
      categories,
      count,
      location_name: cityName,
    };
  },

  frontEndCategoriesListWithOutKeyWord: async (
    city,
    state,
    country,
    limit,
    offset,
    dir = "asc"
  ) => {
    let query = "";
    let queryCount = "";

    queryCount = `select count(*) as 'count' from
  (select category_id, category_name from category
  union
  select category.category_id, subcategory.subcategory_name
  from category_subcategory
  inner join
  subcategory on subcategory.subcategory_id = category_subcategory.subcat_id
  inner join
  category on category.category_id = category_subcategory.category_id) x`;

    query = `select category_id, category_name, category_name_short from category
  union
  select category.category_id, subcategory.subcategory_name,
  category.category_name_short from category_subcategory
  inner join
  subcategory on subcategory.subcategory_id = category_subcategory.subcat_id
  inner join
  category on category.category_id = category_subcategory.category_id
  order by category_name ${dir} limit ${limit} offset ${offset};`;

    let queryLocation;
    let type;
    if (!!city && !!state && !!country) {
      type = "city";
      queryLocation = `select cities.city_name, cities.city_id as id from
    ((cities
    INNER JOIN
    states ON states.state_name_short = '${state}'
    AND cities.city_name_short = '${city}'
    AND states.state_id = cities.state_id)
    INNER JOIN
    countries on states.country_id = countries.country_id and
    countries.country_name_short = '${country}')`;
    } else if (!!state && !!country) {
      type = "state";
      queryLocation = `SELECT states.state_name as city_name,
    states.state_id as id from states
    INNER JOIN
    countries on states.country_id = countries.country_id
    WHERE countries.country_name_short = '${country}'
    AND states.state_name_short="${state}"`;
    } else {
      type = "country";
      queryLocation = `select countries.country_name as city_name,
    countries.country_id as id from countries
    WHERE country_name_short = '${country}'`;
    }

    let cityName = await queryPromise(queryLocation);

    let cityId = cityName[0].id;

    cityName = cityName[0].city_name;

    let categories = await queryPromise(query);

    let count = await queryPromise(queryCount);
    count = count[0].count;

    for (const category of categories) {
      let r = await frontEndCategoryCount(type, category.category_id, cityId);
      category.count = r[0].count;
    }

    return {
      categories,
      count,
      location_name: cityName,
    };
  },

  formLocations: async (term) => {
    const query = `select CONCAT(city_id,',',states.state_id,',',
  countries.country_id) AS 'key', CONCAT( city_name, '#$%', states.state_name,
  '#$%',countries.country_name) as 'value',
  cities.type from cities inner join states on cities.state_id = states.state_id
  inner join countries on cities.country_id = countries.country_id
  and city_name_short like '${rewrite(term)}%' ORDER by value asc`;

    const r = queryPromise(query);
    return r;
  },

  frontEndCompanyCountBrand: async (brand, location, type) => {
    const query = `select company_brand.company_id from company_brand
  inner join company_location on company_brand.company_id =
  company_location.company_id
  and brand_id = 'brand-1034' and company_location.country_id = 'country-98';`;

    const r = await queryPromise(query);

    return r;
  },

  frontEndCompanyListByBrand: async (brand, location, type) => {
    const query = ` select company.company_id, company.company_business_name,
  company.company_url from company_brand
  inner join
  company_location on company_brand.company_id = company_location.company_id
  inner join company on company_brand.company_id = company.company_id and
  brand_id = '${brand}' and company_location.${type}_id = '${location}'
  order by company.company_business_name asc`;

    const companyListBrand = await queryPromise(query);

    const companyList = [];

    for (const company of companyListBrand) {
      let c = new Company(company);
      await c.getLocationDBId(type, location);
      await c.getBrandDBId(brand);
      companyList.push(c);
    }

    return companyList;
  },

  frontEndCompanyListByCategory: async (
    category,
    location,
    type,
    start,
    limit,
    dir = "asc",
    city_id,
    state_id,
    country_id
  ) => {
    const query = `select * from
  ((select 1 as sort_order, company.company_id, company.company_business_name,
  company.company_url
  from
  company_category
  inner join
  company_location on company_location.company_id = company_category.company_id
  inner join
  company on company_category.company_id = company.company_id
  and category_id = '${category}' and company_location.${type}_id = '${location}'
  and (company.premium_expiry_date - ${new Date().getTime()}) >= 0
  order by company.company_business_name)

  union

  (select 2, company.company_id, company.company_business_name,
  company.company_url
  from
  company_category
  inner join
  company_location on company_location.company_id = company_category.company_id
  inner join
  company on company_category.company_id = company.company_id
  and category_id = '${category}' and company_location.${type}_id = '${location}'
  and (${new Date().getTime()} - company.premium_expiry_date) >= 0
  order by company.company_business_name ))
  as resulttable order by sort_order,
  company_business_name ${dir}
  limit ${limit} offset ${start}`;

    const queryCount = `select count(company.company_id) as 'count'
  from company_category
  inner join
  company_location on company_location.company_id = company_category.company_id
  inner join
  company on company_category.company_id = company.company_id and
  category_id = '${category}' and company_location.${type}_id = '${location}'
  and ${new Date().getTime()} - company.premium_expiry_date >= 0
  order by company.company_business_name ${dir}`;

    let counts = await queryPromise(queryCount);

    let { count } = counts[0];

    const companyListCategory = await queryPromise(query);

    const companies = [];

    let relatedCompanies;
    let relatedCategories;

    // if (companyListCategory.length <= 0) {
    relatedCompanies = await relatedCompaniesByCompanyPs(
      city_id,
      state_id,
      country_id,
      category
    );

    relatedCategories = await peopleAlsoSeeCategory(
      city_id,
      state_id,
      country_id
    );
    // }

    for (const company of companyListCategory) {
      let c = new Company(company);
      await c.getLocationDBId(type, location);
      await c.getCategoryDBId(category);
      await c.getLogoDB();
      c.sort_order = company.sort_order;
      companies.push(c);
    }

    return {
      companies,
      count,
      relatedCompanies,
      relatedCategories,
    };
  },

  frontEndQueryFormCategory: async (category, location, type) => {
    const query = `select * from
  ((select 1 as sort_order, company.company_id, company.company_business_name,
  company.company_url,company.company_email,company.company_website
  from
  company_category
  inner join
  company_location on company_location.company_id = company_category.company_id
  inner join
  company on company_category.company_id = company.company_id
  and category_id = '${category}' and company_location.${type}_id = '${location}'
  and
  (company.premium_expiry_date - ${new Date().getTime()}) >= 0
  order by company.company_business_name)

  union

  (select 2, company.company_id, company.company_business_name,
  company.company_url,company.company_email,company.company_website
  from
  company_category
  inner join
  company_location on company_location.company_id = company_category.company_id
  inner join
  company on company_category.company_id = company.company_id
  and category_id = '${category}' and company_location.${type}_id = '${location}'
  and (${new Date().getTime()} - company.premium_expiry_date) >= 0
  order by company.company_business_name)) as resulttable
  order by sort_order, company_business_name `;

    const companyListCategory = await queryPromise(query);

    const companies = [];
    let comps = "";

    for (const company of companyListCategory) {
      let c = new Company(company);
      await c.getLocationsDB();
      companies.push(c.company_email);
      comps +=
        `"${c.company_business_name}","${c.company_email}",` +
        `"https://www.orapages.com/${c.company_url}","${c.locations[0].mobile}"\n`;
    }

    return {
      companies,
      comps,
      // count,
    };
  },

  frontEndSubcategoriesByCategory: async (category) => {
    const query = `select subcategory.subcategory_name from subcategory
  inner join
  category_subcategory on
  category_subcategory.subcat_id = subcategory.subcategory_id and
  category_subcategory.category_id = '${category}'
  order by subcategory.subcategory_name;`;
    return await queryPromise(query);
  },

  frontEndNonDisSubcategoriesByCategory: async (category) => {
    const query = `select non_dis_subcategory.non_dis_subcategory_name as
  'subcategory_name' from non_dis_subcategory
  inner join
  non_dis_category_subcategory on
  non_dis_category_subcategory.non_dis_subcat_id = non_dis_subcategory.non_dis_subcategory_id
  and non_dis_category_subcategory.category_id = '${category}'
  order by non_dis_subcategory.non_dis_subcategory_name;`;
    return await queryPromise(query);
  },

  frontEndNonDisplaySubcategoriesByCategory: async (category) => {
    const query = `SELECT non_dis_subcategory.non_dis_subcategory_name
  as 'subcategory_name' FROM non_dis_subcategory
  INNER JOIN
  non_dis_category_subcategory on
  non_dis_category_subcategory.non_dis_subcat_id = non_dis_subcategory.non_dis_subcategory_id
  AND non_dis_category_subcategory.category_id = '${category}'
  ORDER BY non_dis_subcategory.non_dis_subcategory_name asc`;
    return await queryPromise(query);
  },

  categoryById: async (category) => {
    const query = `select category_name from category where
  category_id = '${category}'`;
    return await queryPromise(query);
  },

  brandById: async (brand) => {
    const query = `select brand_name from brands where brand_id = '${brand}'`;
    return await queryPromise(query);
  },

  checkUrl: async (term, key = undefined) => {
    if (!!key) {
      const query = `select company_url from company where
    company_url = '${rewrite(term)}'
    and company_id != '${key}'`;
      const r = queryPromise(query);
      return r;
    }

    const query = `select company_url from company where
  company_url = '${rewrite(term)}'`;

    const r = queryPromise(query);
    return r;
  },

  checkEmail: async (term, key = undefined) => {
    if (!!key) {
      const query = `select user_email from users where
    user_email = '${term}' and user_id != '${key}'`;
      const r = await queryPromise(query);
      return r;
    }

    const query = `select user_email from users where user_email = '${term}'`;

    const r = await queryPromise(query);
    return r;
  },

  checkName: async (term, key = undefined) => {
    if (!key) {
      const query = `select company_business_name from company
    where company_business_name = ?`;
      const r = await queryPromise(query, [term]);
      return r;
    }

    const rl = `select company_user.company_id as 'id' from users
  inner join
  company_user on company_user.user_id = users.user_id
  and users.user_id = '${key}'`;

    const rls = await queryPromise(rl);

    const query = `select company_business_name from company where
  company_business_name = ? and company_id != ?`;

    const r = await queryPromise(query, [term, rls[0].id]);

    return r;
  },

  companyCheckByUserId: async (user_id) => {
    const query = `select * from company_user
  inner join
  company on company_user.company_id = company.company_id and
  company_user.user_id = '${user_id}';`;

    const r = await queryPromise(query);

    if (r.length <= 0) {
      throw new Error(`You have no company`);
    }

    const company = new Company(r[0]);

    return company;
  },

  companyByUserId: async (user_id) => {
    const query = `select * from company_user
  inner join
  company on company_user.company_id = company.company_id and
  company_user.user_id = '${user_id}';`;

    const r = await queryPromise(query);

    if (r.length <= 0) {
      throw new Error(`no_company_found`);
    }

    const company = new Company(r[0]);

    await company.getCategoriesDB();

    await company.getTagsDB();

    await company.getImagesDB();

    await company.getLogoDB();

    await company.getLocationsDB();

    await company.getBrandsDB();

    await company.getUserDB();

    await company.getVideoDB();

    await company.getOtherEmail();

    await company.getOtherWebsite();

    return company;
  },

  companyByUrl: async (url) => {
    const query = `select * from company where company_url = '${url}' limit 1 ;`;

    const r = await queryPromise(query);

    if (r.length < 1) {
      throw new Error(`no_company_found`);
    }

    const company = new Company(r[0]);

    await company.getLogoDB();

    await company.getCategoriesDB();

    await company.getTagsDB();

    await company.getImagesDB();

    await company.getVideoDB();

    await company.getLocationsDB();

    await company.getBrandsDB();

    await company.getUserDB();

    await company.getOtherEmail();

    await company.getOtherWebsite();

    let companies = await relatedCompaniesByCompany(company);

    company.companies = companies;

    return company;
  },

  companyBytemp: async (url) => {
    const query = `select * from company where company_url = '${url}' limit 1 ;`;

    const r = await queryPromise(query);

    if (r.length <= 0) {
      throw new Error(`No companies found`);
    }

    const company = new Company(r[0]);

    await company.getLogoDB();

    await company.getCategoriesDB();

    await company.getImagesDB();

    await company.getLocationsDB();

    await company.getBrandsDB();

    await company.getUserDB();

    return company;
  },

  insertTag: async (tag) => {
    const searchQuery = `select * from user_tags where tag_short =
  '${rewrite(tag)}'`;

    const e = await selectPromise(searchQuery);
    if (e.length <= 0) {
      const query = `INSERT INTO user_tags(tag,tag_short)
    VALUES ('${tag}','${rewrite(tag)}')`;

      const r = await queryPromise(query);
      return r;
    }
    return e;
  },

  searchTags: async (tag) => {
    const query = `select tag as id, tag as text from user_tags where tag_short
  like '%${rewrite(tag)}%'`;

    const r = await queryPromise(query);
    return r;
  },

  tagByName: async (tag) => {
    const r = await selectPromise(
      `select * from user_tags where tag_short = '${tag}'`
    );
    return r;
  },

  frontEndCompanyListByTag: async (tag, start, limit, dir = "asc") => {
    const query = `select company.company_id, company.company_business_name,
  company.company_url, company.company_business_details from
  company_tag
  inner join
  user_tags on user_tags.tag_id = company_tag.tag_id
  inner join
  company on company_tag.company_id = company.company_id
  and user_tags.tag_id = '${tag}'
  order by company.company_business_name ${dir}
  limit ${limit} offset ${start}`;

    const queryCount = `select count(company.company_id) as 'count'
  from company_tag
  inner join user_tags on user_tags.tag_id = company_tag.tag_id
  inner join company on company_tag.company_id = company.company_id
  and user_tags.tag_id = '${tag}'
  order by company.company_business_name ${dir}`;

    let counts = await queryPromise(queryCount);

    let { count } = counts[0];

    const companyListTag = await queryPromise(query);

    const companies = [];

    for (const company of companyListTag) {
      let c = new Company(company);
      await c.getLogoDB();
      await c.getLocationsDB();
      let { result } = strip(
        company.company_business_details.substring(0, 2500),
        {
          stripTogetherWithTheirContents: [
            "script", // default
            "style", // default
            "xml", // default,
            "img",
            "a",
          ],
        }
      );
      c.company_business_details = result.substring(0, 5000);
      companies.push(c);
    }

    return {
      companies,
      count,
    };
  },

  insertUserQuery: async (name, mobile, message, email) => {
    const query = `insert into user_query(name,mobile,message,email)
  values('${name}','${mobile}',${JSON.stringify(message)},'${email}')`;

    const r = await queryPromise(query);
    return r;
  },

  selectUserQueryOnce: async (email) => {
    const query = `select * from user_query where email = '${email}' and
  Date(query_date) = CURDATE()`;

    const r = await queryPromise(query);
    return r;
  },

  getCompaniesName: async (term) => {
    const query = `select company_business_name as 'value', company_id
  as 'key' from company where company_business_name
    like "${term}%" order by value asc limit 200`;
    const names = await queryPromise(query);
    if (names.length <= 0) {
      throw new Error("No results found");
    }
    return names;
  },
};
