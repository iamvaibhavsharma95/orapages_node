const { selectPromise, queryPromise } = require("./database");

class PremiumOrder {
  constructor({
    premium_request_id,
    name,
    designation = null,
    email,
    company_name,
    pics_website = null,
    pics_enclosed = null,
    pics_later = 0,
    details = null,
    premium_url = null,
    payment_id = null,
    payment_status = null,
    payment_create_time = null,
    payment_update_time = null,
    ordered_at = new Date().getTime(),
    completed_at = 0,
  }) {
    this.premium_request_id = premium_request_id;
    this.name = name;
    this.designation = designation;
    this.email = email;
    this.company_name = company_name;
    this.pics_website = pics_website;
    this.pics_enclosed = pics_enclosed;
    this.pics_later = pics_later;
    this.details = details;
    this.premium_url = premium_url;
    this.payment_id = payment_id;
    this.payment_status = payment_status;
    this.ordered_at = ordered_at;
    this.completed_at = completed_at;
    this.payment_id = payment_id;
    this.payment_status = payment_status;
    this.payment_create_time = payment_create_time;
    this.payment_update_time = payment_update_time;
  }

  async setOrder() {
    let premiumQuery = `INSERT INTO premium_orders(
        name,
        designation,
        email,
        company_name,
        pics_website,
        pics_enclosed,
        pics_later,
        details,
        payment_id,
        payment_status,
        payment_create_time,
        payment_update_time,
        ordered_at,
        completed_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;

    await queryPromise(premiumQuery, [
      this.name,
      this.designation,
      this.email,
      this.company_name,
      this.pics_website,
      this.pics_enclosed,
      this.pics_later,
      this.details,
      this.payment_id,
      this.payment_status,
      this.payment_create_time,
      this.payment_update_time,
      this.ordered_at,
      this.completed_at,
    ]);
  }

  async updateOrder(payment_status, payment_update_time, payment_id) {
    let updatePremiumQuery = `update premium_orders set payment_status = ?,
    payment_update_time = ? where payment_id = ?`;

    await queryPromise(updatePremiumQuery, [
      payment_status,
      payment_update_time,
      payment_id,
    ]);
  }

  async updateOrderCompleted(premium_url, payment_id) {
    let updatePremiumCompletedQuery = `update premium_orders set
    premium_url = ?, completed_at = ? where payment_id = ?`;

    await queryPromise(updatePremiumCompletedQuery, [
      premium_url,
      new Date().getTime(),
      payment_id,
    ]);

    this.premium_url = premium_url;
  }
}

module.exports = PremiumOrder;
