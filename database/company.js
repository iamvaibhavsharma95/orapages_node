const { selectPromise, queryPromise } = require("./database");
const random = require("randomstring");
const fs = require("fs");

class Company {
  constructor({
    company_id = null,
    company_name,
    company_business_name,
    company_email,
    company_name_short,
    company_url,
    company_theme = "default",
    company_website,
    created_at,
    last_update,
    uploaded_by,
    uploaded_ip,
    last_update_ip,
    images = [],
    categories = [],
    tags = [],
    weightage = null,
    brands = [],
    locations = [],
    company_business_details,
    logo = null,
    user_id = null,
    premium_expiry_date = 0,
    youtube = null,
    other_emails = [],
    other_websites = [],
  }) {
    if (company_id == null) {
      this.company_id = random.generate({
        length: 30,
        charset: "hex",
      });
    } else {
      this.company_id = company_id;
    }

    this.company_name = company_name;
    this.company_business_name = company_business_name;
    this.company_email = company_email;
    this.company_name_short = company_name_short;
    this.company_url = company_url;
    this.company_website = company_website;
    this.created_at = created_at;
    this.last_update = last_update;
    this.uploaded_by = uploaded_by;
    this.uploaded_ip = uploaded_ip;
    this.last_update_ip = last_update_ip;
    this.images = images;
    this.categories = categories;
    this.tags = tags;
    this.brands = brands;
    this.locations = locations;
    this.company_business_details = company_business_details;
    this.logo = logo;
    this.user_id = user_id;
    this.company_theme = company_theme;
    this.premium_expiry_date = premium_expiry_date;
    this.weightage = weightage;
    this.other_emails = other_emails;
    this.other_websites = other_websites;
    this.youtube = youtube;
  }

  async setCompany() {
    let companyQuery = `INSERT INTO company(company_id,
      company_business_name,
      company_name,
      company_name_short,
      company_url,
      company_email,
      company_website,
      created_at,
      last_update,
      uploaded_by,
      uploaded_ip,
      company_business_details,
      last_update_ip,
      company_theme,
      youtube)
      VALUES ("${this.company_id}",
    "${this.company_business_name}",
    "${this.company_name}",
    "${this.company_name_short}",
    "${this.company_url}",
    "${this.company_email}",
    "${this.company_website}",
    ${new Date(this.created_at).getTime()},
    ${new Date(this.last_update).getTime()},
    "${this.uploaded_by}",
    "${this.uploaded_ip}",
    ${JSON.stringify(this.company_business_details)},
    "${this.last_update_ip}",
    "${this.company_theme}",
    "${this.youtube}")`;

    this.locations.forEach(async (location) => {
      await this.setLocation(location);
    });

    this.categories.forEach(
      async (category) => await this.setCategory(category)
    );

    this.brands.forEach(async (brand) => await this.setbrand(brand));

    this.tags.forEach(async (tag) => await this.setTag(tag));

    this.other_emails.forEach(async (email) => await this.setOtherEmail(email));

    this.other_websites.forEach(
      async (website) => await this.setOtherWebsite(website)
    );

    await queryPromise(companyQuery);

    if (!!this.premium_expiry_date) {
      await this.setPremiumProfileRenew();
    }
  }

  async updateCompany() {
    let uploadedCategory = [];
    for (const category of this.categories) {
      const s = await this.updateCategory(category);
      uploadedCategory.push(s);
    }

    let uploadedBrand = [];
    for (const brand of this.brands) {
      const s = await this.updateBrand(brand);
      uploadedBrand.push(s);
    }

    let uploadedLocation = [];
    for (const location of this.locations) {
      const s = await this.updateLocation(location);
      uploadedLocation.push(s);
    }

    let uploadedTag = [];
    for (const tag of this.tags) {
      const s = await this.updateTag(tag);
      uploadedTag.push(s);
    }

    // let uploadedImage = [];
    // for (const image of this.images) {
    //     const s= await this.updateImage(image);
    //     uploadedImage.push(s);
    // }

    let uploadedEmail = [];
    for (const email of this.other_emails) {
      const s = await this.updateOtherEmail(email);
      if (!!email.email) {
        uploadedEmail.push(s);
      }
    }

    let uploadedWebsite = [];
    for (const website of this.other_websites) {
      const s = await this.updateOtherWebsite(website);
      if (!!website.website) {
        uploadedWebsite.push(s);
      }
    }

    // let uploadedWebsites = [];
    // for(const website of this.other_websites){
    //     const s = await this.uploadedOtherWebsites(website);
    //     uploadedWebsites.push(s);
    // }

    await this.deteteCategoryExtra(uploadedCategory);
    await this.deteteBrandExtra(uploadedBrand);
    await this.deteteLocationExtra(uploadedLocation);
    await this.deteteTagExtra(uploadedTag);
    await this.deleteEmailExtra(uploadedEmail);
    await this.deleteWebsiteExtra(uploadedWebsite);
    // await this.deleteImageExtra(uploadedImage)

    let companyQuery = `update company set company_business_name =?,
    company_name=?,
    company_name_short=?,
    company_url=?,
    company_email=?,
    company_website=?,
    created_at=?,
    last_update=?,
    uploaded_by=?,
    uploaded_ip=?,
    company_business_details=?,
    last_update_ip=?,
    company_theme=?,
    youtube = ?
    where company_id = ?`;

    await queryPromise(companyQuery, [
      this.company_business_name,
      this.company_name,
      this.company_name_short,
      this.company_url,
      this.company_email,
      this.company_website,
      this.created_at,
      new Date(this.last_update).getTime(),
      this.uploaded_by,
      this.uploaded_ip,
      this.company_business_details,
      this.last_update_ip,
      this.company_theme,
      this.youtube,
      this.company_id,
    ]);
  }

  async deleteCompany() {
    const deleteCompany = `delete from company where company_id = '${this.company_id}'`;
    // const deleteCompanyCategory = `delete from company_category where company_id = '${this.company_id}'`;
    // const deleteCompanyBrand = `delete from company_brand where company_id = '${this.company_id}'`;
    // const deleteCompanyImage = `delete from company_image where company_id = '${this.company_id}'`;
    // const deleteCompanyLogo = `delete from company_logo where company_id = '${this.company_id}'`;
    // const deleteCompanyUser = `delete from company_user where company_id = '${this.company_id}'`;
    // const deleteCompanyTag = `delete from company_tag where company_id = '${this.company_id}'`;
    // const deleteWeightage = `delete from company_weight where company_id = '${this.company_id}'`;
    // const deleteVideo = `delete from company_video where company_id = '${this.company_id}'`;
    // const deleteEmail = `delete from other_emails where company_id = '${this.company_id}'`;
    // const deleteWebsite = `delete from other_websites where company_id = '${this.company_id}'`;

    const getCompanyUserQuery = `select user_id from company_user where company_id = '${this.company_id}'`;
    let user_id = await queryPromise(getCompanyUserQuery);
    // const bnFollower = `delete from bn_follower where follower = '${user_id[0].user_id}' or following = '${user_id[0].user_id}'`;

    await queryPromise(deleteCompany);
    // await queryPromise(deleteCompanyCategory);
    // await queryPromise(deleteCompanyBrand);
    // await queryPromise(deleteCompanyImage);
    // await queryPromise(deleteCompanyLogo);
    if (!!user_id[0]) {
      const usersPartner = `delete from bn_partner where partner1_id = '${user_id[0].user_id}' or partner2_id = '${user_id[0].user_id}'`;
      const usersPartnerRequest = `delete from bn_partner_request where requester_id = '${user_id[0].user_id}' or requestee_id = '${user_id[0].user_id}'`;
      const usersDelete = `delete from users where user_id = '${user_id[0].user_id}'`;
      const userTokenDelete = `delete from user_token where user_id = '${user_id[0].user_id}'`;
      const bnFollower = `delete from bn_follower where follower = '${user_id[0].user_id}' or following = '${user_id[0].user_id}'`;
      const bnPostUser = `delete from bn_post_user where user_id = '${user_id[0].user_id}'`;
      const bnNotification = `delete from bn_notification_user where user_id = '${user_id[0].user_id}'`;
      const bnPostShare = `delete from bn_post_share where user_id = '${user_id[0].user_id}'`;
      const bnPostReport = `delete from bn_post_report where user_id = '${user_id[0].user_id}'`;
      const bnPostLike = `delete from bn_post_like where user_id = '${user_id[0].user_id}'`;
      await queryPromise(usersPartner);
      await queryPromise(usersPartnerRequest);
      await queryPromise(bnFollower);
      await queryPromise(bnPostUser);
      await queryPromise(bnNotification);
      await queryPromise(bnPostShare);
      await queryPromise(bnPostReport);
      await queryPromise(bnPostLike);
      await queryPromise(userTokenDelete);
      await queryPromise(usersDelete);
    }
    // await queryPromise(deleteCompanyTag);
    // await queryPromise(deleteCompanyUser);
    // await queryPromise(deleteWeightage);
    // await queryPromise(deleteVideo);
    // await queryPromise(deleteEmail);
    // await queryPromise(deleteWebsite);
  }

  async setLocation(location) {
    let {
      city_id,
      state_id,
      country_id,
      complete_address,
      mobile = "",
      fax = "",
      email = "",
      website = "",
    } = location;
    let companyLocationQuery = `INSERT INTO company_location(company_id,
      city_id,
      state_id,
      country_id,
      complete_address,
      phone,
      fax,
      email,
      website) VALUES (
    "${this.company_id}",
    "${city_id}",
    "${state_id}",
    "${country_id}",
    ${JSON.stringify(complete_address)},
    ${JSON.stringify(mobile)},
    ${JSON.stringify(fax)},
    "${email}",
    "${website}")`;
    return await queryPromise(companyLocationQuery);
  }

  async getLocationsDB() {
    const queryLocation = `select company_location.cl_id as 'location_id',
    countries.country_name, states.state_name, cities.city_name,
    countries.country_id, states.state_id,cities.city_id,
    company_location.phone as 'mobile', company_location.complete_address,
    company_location.email, company_location.website,company_location.fax
    from company_location
    inner join
    countries on countries.country_id = company_location.country_id
    inner join
    states on states.state_id = company_location.state_id
    inner join
    cities on cities.city_id = company_location.city_id
    and company_location.company_id = '${this.company_id}' order by cl_id asc`;

    const company_location = await queryPromise(queryLocation);

    this.locations = company_location;
  }

  async getFirstLocationsDB() {
    const queryLocation = `select company_location.cl_id as 'location_id',
    countries.country_name, states.state_name, cities.city_name,
    countries.country_id, states.state_id,cities.city_id,
    company_location.phone as 'mobile', company_location.complete_address,
    company_location.email, company_location.website,company_location.fax from
    company_location
    inner join
    cities on cities.city_id = company_location.city_id and
    company_location.company_id = '${this.company_id}'
    inner join
    states on cities.state_id = states.state_id
    inner join
    countries on cities.country_id = countries.country_id limit 1`;

    const company_location = await queryPromise(queryLocation);

    this.locations = company_location;
  }

  async getLocationDBId(type, location) {
    const queryLocation = `select company_location.cl_id as 'location_id',
    countries.country_name, states.state_name, cities.city_name,
    countries.country_id, states.state_id,cities.city_id,
    company_location.phone as 'mobile', company_location.complete_address,
    company_location.email, company_location.website,company_location.fax
    from company_location
    inner join
    countries on countries.country_id = company_location.country_id
    inner join
    states on states.state_id = company_location.state_id inner join
    cities on cities.city_id = company_location.city_id
    and company_location.company_id = '${this.company_id}'
    and company_location.${type}_id = '${location}' order by cl_id asc`;

    const company_location = await queryPromise(queryLocation);

    this.locations = company_location;
  }

  async updateLocation(location) {
    let {
      location_id = null,
      city_id,
      state_id,
      country_id,
      complete_address,
      mobile = "",
      fax = "",
      email = "",
      website = "",
    } = location;

    const getlocation = `select * from company_location where
    company_id= ? and cl_id = ?`;

    const result = await queryPromise(getlocation, [
      this.company_id,
      location_id,
    ]);

    if (result.length <= 0) {
      const r = await this.setLocation(location);
      return r.insertId;
    } else {
      let companyLocationQuery = `update company_location set complete_address=?,
      phone=?, fax=?, email=?,website=? where company_id =? and cl_id = ?`;

      await queryPromise(companyLocationQuery, [
        complete_address,
        mobile,
        fax,
        email,
        website,
        this.company_id,
        location_id,
      ]);
      return parseInt(location_id);
    }
  }

  async deteteLocationExtra(locations) {
    if (locations.length > 0) {
      const companylocationQuery = `delete from company_location where
      company_id =? and cl_id not in (?)`;
      await queryPromise(companylocationQuery, [this.company_id, locations]);
    }
  }

  async deleteLocation(location) {
    let { location_id } = location;

    let companyLocationQuery = `delete from company_location
    where comapny_id = ? and cl_id = ?`;

    await queryPromise(companyLocationQuery, [this.company_id, location_id]);
  }

  async setCategory(category) {
    const { category_id, category_des } = category;

    const companyCategoryQuery = `INSERT INTO
    company_category(company_id, category_id, category_desc)
    VALUES ("${this.company_id}","${category_id}",
    ${JSON.stringify(category_des)})`;

    await queryPromise(companyCategoryQuery);
  }

  async updateCategory(category) {
    const { category_id, category_des } = category;

    const getCategory = `select * from company_category where
    company_id = ? and category_id = ?`;

    const result = await queryPromise(getCategory, [
      this.company_id,
      category_id,
    ]);

    if (result.length <= 0) {
      await this.setCategory(category);
      return category_id;
    } else {
      const companyCategoryQuery = `update company_category set
      category_desc = ? where company_id=? and category_id=?`;

      await queryPromise(companyCategoryQuery, [
        category_des,
        this.company_id,
        category_id,
      ]);
      return category_id;
    }
  }

  async deleteCategory(category) {
    const { category_id } = category;

    const companyCategoryQuery = `delete from company_category
    where company_id=? and category_id=?`;

    await queryPromise(companyCategoryQuery, [this.company_id, category_id]);
  }

  async deteteCategoryExtra(categories) {
    if (categories.length > 0) {
      const companyCategoryQuery = `delete from company_category
      where company_id =? and category_id not in (?)`;

      await queryPromise(companyCategoryQuery, [this.company_id, categories]);
    }
  }

  async getCategoriesDB() {
    const queryCategory = `select company_category.category_id,
    company_category.category_desc,
    category.category_name from company_category
    inner join
    category on company_category.category_id = category.category_id
    and company_category.company_id = '${this.company_id}'
    order by category.category_name`;

    const company_category = await queryPromise(queryCategory);

    this.categories = company_category;
  }

  async getCategoryDBId(category) {
    const queryCategory = `select company_category.category_id,
    company_category.category_desc,
    category.category_name from company_category
    inner join
    category on company_category.category_id = category.category_id
    and company_category.company_id = '${this.company_id}'
    and company_category.category_id = '${category}'
    order by category.category_name limit 1`;

    const company_category = await queryPromise(queryCategory);

    this.categories = company_category;
  }

  async setbrand(brand) {
    const { brand_id, brand_des } = brand;

    let companybrandQuery = `INSERT INTO company_brand(company_id, brand_id, brand_desc)
    VALUES ("${this.company_id}","${brand_id}",${JSON.stringify(brand_des)})`;

    await queryPromise(companybrandQuery);
  }

  async updateBrand(brand) {
    const { brand_id, brand_des } = brand;

    const getbrand = `select * from company_brand where company_id= ? and brand_id = ?`;

    const result = await queryPromise(getbrand, [this.company_id, brand_id]);

    if (result.length <= 0) {
      await this.setbrand(brand);
      return brand_id;
    } else {
      const companybrandQuery = `update company_brand set  brand_desc = ?
      where company_id=? and brand_id=?`;

      await queryPromise(companybrandQuery, [
        brand_des,
        this.company_id,
        brand_id,
      ]);
      return brand_id;
    }
  }

  async getBrandsDB() {
    const queryBrand = `select company_brand.brand_id,
    company_brand.brand_desc,
    brands.brand_name from company_brand
    inner join
    brands on company_brand.brand_id = brands.brand_id
    and company_brand.company_id = '${this.company_id}'`;

    const company_brand = await queryPromise(queryBrand);

    this.brands = company_brand;
  }

  async getBrandDBId(brand) {
    const queryBrand = `select company_brand.brand_id, company_brand.brand_desc,
    brands.brand_name from company_brand
    inner join
    brands on company_brand.brand_id = brands.brand_id
    and company_brand.company_id = '${this.company_id}'
    and company_brand.brand_id = '${brand}' limit 1`;

    const company_brand = await queryPromise(queryBrand);

    this.brands = company_brand;
  }

  async deleteBrand(brand) {
    const { brand_id } = brand;
    let companybrandQuery = `delete from company_brand where company_id = ?
    and brand_id = ?`;
    await queryPromise(companybrandQuery, [this.company_id, brand_id]);
  }

  async deteteBrandExtra(brands) {
    if (brands.length > 0) {
      const companybrandQuery = `delete from company_brand where
      company_id =? and brand_id not in (?)`;

      await queryPromise(companybrandQuery, [this.company_id, brands]);
    } else {
      const companybrandQuery = `delete from company_brand where company_id =?`;
      await queryPromise(companybrandQuery, [this.company_id]);
    }
  }

  async setImage(image) {
    const { file64, file_name, contentType, description, name, order } = image;

    const image_id = random.generate({
      length: 30,
      charset: "hex",
    });

    const companyimageQuery = `INSERT INTO company_image(company_id, image_id,
    image_url, image_original_name, image_des, image_order,
    image_stored_name) VALUES ("${this.company_id}",
    "${image_id}",
    "company_images/${this.company_id}/${file_name}",
    "${name}",${JSON.stringify(description)},${order},
    "${file_name}")`;

    await queryPromise(companyimageQuery);

    return image_id;
  }

  async updateImage(image) {
    const { image_id, description, order } = image;

    const companyimageQuery = `update company_image set image_des = ?,
    image_order = ? where company_id =? and image_id =?`;

    await queryPromise(companyimageQuery, [
      description,
      order,
      this.company_id,
      image_id,
    ]);
  }

  async getImagesDB() {
    const queryImage = `select image_id,image_url,
    image_original_name,image_des,image_order from company_image
    where company_id = '${this.company_id}' order by image_order asc`;

    const company_image = await queryPromise(queryImage);

    this.images = company_image;
  }

  async deleteImage(image) {
    const { image_id } = image;

    const companyimageQuery = `delete from company_image where company_id = ?
    and image_id =?`;

    await queryPromise(companyimageQuery, [this.company_id, image_id]);
  }

  async deleteImageExtra(images) {
    if (images.length > 0) {
      const companyimageQuery = `delete from company_image where company_id = ?
      and image_id not in (?)`;

      await queryPromise(companyimageQuery, [this.company_id, images]);
    } else {
      const companyimageQuery = `delete from company_image where company_id = ?`;

      await queryPromise(companyimageQuery, [this.company_id]);
    }
  }

  async setVideo(video) {
    const { originalname, filename, path, video_description } = video;

    const video_id = random.generate({
      length: 30,
      charset: "hex",
    });

    await this.deleteExtraVideo();

    const companyVideoQuery = `INSERT INTO company_video(company_id, video_id,
    video_url, video_original_name, video_des, video_stored_name) VALUES (
    "${this.company_id}",
    "${video_id}","${path.replace("\\", "/")}",
    "${originalname}",
    ${JSON.stringify(video_description)},
    "${filename}")`;

    await queryPromise(companyVideoQuery);

    return video_id;
  }

  async updateVideo(video) {
    const { video_id, video_description } = video;

    const companyvideoQuery = `update company_video set video_des = ?
    where company_id = ? and video_id = ?`;

    await queryPromise(companyvideoQuery, [
      video_description,
      this.company_id,
      video_id,
    ]);
  }

  async getVideoDB() {
    const queryVideo = `select video_id,
    video_url,video_original_name,video_des from company_video
    where company_id = '${this.company_id}'`;

    const company_video = await queryPromise(queryVideo);

    this.videos = company_video;
  }

  async deleteExtraVideo() {
    const companyVideoSelect = `select video_url from company_video
    where company_id =?`;

    const r = await queryPromise(companyVideoSelect, [this.company_id]);

    if (r.length > 0) {
      fs.unlinkSync(r[0].video_url);
    }

    const companyVideoQuery = `delete from company_video where company_id =?`;
    await queryPromise(companyVideoQuery, [this.company_id]);
  }

  async setLogo(logo) {
    const {
      file64,
      file_name,
      contentType,
      logo_name,
      logo_caption,
      uploaded = true,
    } = logo;

    const image_id = random.generate({
      length: 30,
      charset: "hex",
    });

    const companyLogoQuery = `INSERT INTO company_logo(company_id, logo_id,
    logo_url, logo_original_name, logo_des, logo_stored_name) VALUES
    ("${this.company_id}",
    "${image_id}",
    "company_images/${this.company_id}/logo/${file_name}",
    "${logo_name}",
    "${logo_caption}",
    "${file_name}")`;

    const deleteQueryLogo = `delete from company_logo where
    company_id='${this.company_id}' and logo_id != '${image_id}'`;

    await queryPromise(companyLogoQuery);

    await queryPromise(deleteQueryLogo);
  }

  async getLogoDB() {
    const queryLogo = `select logo_id,logo_url,logo_original_name,logo_des
    from company_logo where company_id = '${this.company_id}' limit 1`;

    let company_logo = await queryPromise(queryLogo);

    if (company_logo.length > 0) {
      company_logo = company_logo[0];
    } else {
      company_logo = null;
    }

    this.logo = company_logo;
  }

  async updateLogo(logo) {
    const { logo_id, caption } = logo;

    const companyLogoQuery = `update company_logo set logo_des=?
    where company_id =? and logo_id = ?`;

    await queryPromise(companyLogoQuery, [caption, this.company_id, logo_id]);
  }

  async deleteLogoExtra() {
    const logoDelete = `delete from company_logo
    where company_id = '${this.company_id}'`;

    await queryPromise(logoDelete);
  }

  async getUserDB() {
    const queryUser = `select user_id from company_user
    where company_id = '${this.company_id}'`;

    const company_user = await queryPromise(queryUser);

    if (company_user.length > 0) {
      this.user_id = company_user[0]["user_id"];
    }
  }

  async setPremiumProfileRenew() {
    const queryPremimum = `insert into premium_renew (company_id, start_date, end_date)
    values (?,?,?)`;

    const queryPremimumProfile = `update company set premium_expiry_date = ?
    where company_id = ?`;

    const expiry_date = new Date().getTime() + 31536000 * 1000;

    await queryPromise(queryPremimum, [
      this.company_id,
      new Date().getTime(),
      expiry_date,
    ]);

    await queryPromise(queryPremimumProfile, [expiry_date, this.company_id]);
  }

  async setTag(tag) {
    const companyTagQuery = `INSERT INTO company_tag(company_id, tag_id)
    (select "${this.company_id}", tag_id from user_tags where tag = "${tag}")`;
    await queryPromise(companyTagQuery);
  }

  async updateTag(tag) {
    const getTag = `select * from company_tag where company_id= ?
    and tag_id = (select tag_id from user_tags where tag = ?)`;
    const result = await queryPromise(getTag, [this.company_id, tag]);

    if (result.length <= 0) {
      await this.setTag(tag);
      return tag;
    } else {
      return tag;
    }
  }

  async deteteTagExtra(tags) {
    if (tags.length > 0) {
      const companyTagQuery = `DELETE from company_tag WHERE company_id = ? \
      and tag_id not in
      (SELECT tag_id FROM user_tags WHERE tag in (?))`;
      await queryPromise(companyTagQuery, [this.company_id, tags]);
    }
  }

  async getTagsDB() {
    const queryTags = `select company_tag.tag_id,user_tags.tag from company_tag
    inner join user_tags on company_tag.tag_id = user_tags.tag_id
    and company_tag.company_id = '${this.company_id}'
    order by user_tags.tag`;

    const company_tags = await queryPromise(queryTags);

    this.tags = company_tags;
  }

  async getWeightage() {
    const queryWeight = `select * from company_weight
    where company_id = '${this.company_id}'`;

    const company_weight = await queryPromise(queryWeight);

    this.weightage = company_weight[0];
  }

  async deleteWeightage() {
    const query = `delete from company_weigth
    where comany_id = '${this.company_id}'`;

    await queryPromise(query);
  }

  async getOtherEmail() {
    const query = `select * from other_emails
    where company_id = '${this.company_id}'`;

    this.other_emails = await queryPromise(query);
  }

  async setOtherEmail(emails) {
    let { email } = emails;

    const query = `insert into other_emails(company_id,email) values(?,?)`;

    await queryPromise(query, [this.company_id, email]);
  }

  async updateOtherEmail(e_mail) {
    let { email_id = "", email } = e_mail;
    if (!email_id) {
      await this.setOtherEmail(e_mail);
      return;
    }
    const query = `update other_emails set email= ? where oe_id = ?`;
    await queryPromise(query, [email, email_id]);
  }

  async deleteEmailExtra(emails) {
    if (emails.length > 0) {
      const companyEmailQuery = `DELETE from other_emails
      WHERE company_id = ? and oe_id not in (?)`;

      await queryPromise(companyEmailQuery, [this.company_id, emails]);
    } else {
      const query = `Delete from other_emails where company_id = ?`;

      await queryPromise(query, [this.company_id]);
    }
  }

  async getOtherWebsite() {
    const query = `select * from other_websites
    where company_id = '${this.company_id}'`;

    this.other_websites = await queryPromise(query);
  }

  async setOtherWebsite(websites) {
    const { website } = websites;
    const query = `insert into other_websites(company_id,website) values(?,?)`;
    await queryPromise(query, [this.company_id, website]);
  }

  async updateOtherWebsite(web) {
    let { website_id = "", website } = web;
    if (!website_id) {
      await this.setOtherWebsite(web);
      return;
    }
    const query = `update other_websites set website= ? where ow_id = ?`;
    await queryPromise(query, [website, website_id]);
  }

  async deleteWebsiteExtra(websites) {
    if (websites.length > 0) {
      const companywebsiteQuery = `DELETE from other_websites
      WHERE company_id = ? and ow_id not in (?)`;
      await queryPromise(companywebsiteQuery, [this.company_id, websites]);
    } else {
      const query = `Delete from other_websites where company_id = ?`;
      await queryPromise(query, [this.company_id]);
    }
  }
}

module.exports = Company;
