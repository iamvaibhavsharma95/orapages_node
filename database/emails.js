const mailer = require("nodemailer");
const hbs = require("nodemailer-express-handlebars");

//attach the plugin to the nodemailer transporter
const transporter = mailer.createTransport({
  pool: true,
  host: "mail.orapages.com",
  port: 465,
  secure: true,
  requireTLS: true,
  auth: {
    user: "info@orapages.com",
    pass: "info@orapages",
  },
});

transporter.use(
  "compile",
  hbs({
    viewEngine: {
      extName: ".hbs",
      partialsDir: "../emailer",
      layoutsDir: "../emailer",
      defaultLayout: null,
    },
    viewPath: __dirname.replace("database", "") + "/emailer/",
  })
);

const friends_request = async (name, email) => {
  const mail = {
    from: '"Orapages" <network@orapages.com>',
    to: email,
    subject: `One more friend request, ${name}`,
    template: "/friend_request",
    context: {
      name,
    },
  };
  await transporter.sendMail(mail);
};

const friends_add = async (name, email) => {
  const mail = {
    from: '"Orapages" <network@orapages.com>',
    to: email,
    subject: `${name} has accepted your friend request`,
    template: "/friend_add",
    context: {
      name,
    },
  };
  await transporter.sendMail(mail);
};

const company_add = async (company_business_name, company_url, email) => {
  const mail = {
    from: '"Orapages" <register@orapages.com>',
    to: email,
    subject: `Your Company has been inserted into Orapages`,
    template: "/registration",
    context: {
      company_business_name,
      company_url,
    },
  };
  return await transporter.sendMail(mail);
};

module.exports = {
  friends_request,
  friends_add,
  company_add,
};
