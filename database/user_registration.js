const { queryPromise } = require("./database");

const { setUser } = require("./owner");

const getUnlistedUsersCompany = async () => {
  const query = `select * from company where company_id not in
  (select company_id from company_user);`;

  const result = await queryPromise(query);

  for (const r of result) {
    try {
      const { company_email, company_id, company_name } = r;
      await setUser(
        company_email,
        company_id.substring(0, 8),
        company_id,
        company_name
      );
    } catch (error) {
      console.log(error);
    }
  }
};

// const getUnregistered = async()=>{
//     const query = `select company_id from company where company_id not in (select company_id from company_user)`;
//     const r = await queryPromise(query);
//     for (const s of r) {
//         try {
//             const {company_id} = s;
//             const
//         } catch (e) {
//             console.log(e);
//         }
//     }
// }

module.exports = { getUnlistedUsersCompany };
