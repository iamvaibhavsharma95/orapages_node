const mailer = require("nodemailer");

let getCompanyEmail = (
  company_business_name,
  company_email,
  company_url,
  password = "Your entered password"
) => {
  return `<div style="FONT-SIZE:12pt;FONT-FAMILY:'Calibri';COLOR:#000000">
    </div>
    <div
        style="FONT-SIZE:small;TEXT-DECORATION:none;FONT-FAMILY:Calibri;FONT-WEIGHT:normal;COLOR:#000000;FONT-STYLE:normal;DISPLAY:inline">
        <div style="FONT:10pt tahoma">
            <div style="BACKGROUND:#f5f5f5">
                <div><b>From:</b> <a title="info@orapages.com">OraPages</a> </div>
                <div><b>Sent:</b> ${new Date().toGMTString()}</div>
                <div><b>To:</b> <a title="${company_email}">${company_email}</a>
                </div>
                <div><b>Subject:</b> ${company_business_name} - Welcome to
                    OraPages</div>
            </div>
        </div>
        <div>&nbsp;</div>
    </div>
    <div
        style="FONT-SIZE:small;TEXT-DECORATION:none;FONT-FAMILY:Calibri;FONT-WEIGHT:normal;COLOR:#000000;FONT-STYLE:normal;DISPLAY:inline">
        <table style="COLOR:#000000">
            <tbody>
                <tr>
                    <td style="WIDTH:95%">Dear ${company_business_name} <br><br>Welcome to
                        <strong><a style="COLOR:#df0000" href="https://www.orapages.com" target="_blank">OraPages</a></strong>!
                        Your business
                        listing for <strong><a style="COLOR:#df0000" href="https://www.orapages.com/${company_url}"
                                target="_blank">${company_business_name}</a></strong> is now live under <strong>your own unique OraPages
                            URL!</strong> <br><br>To view your listing on <strong>OraPages</strong>,
                        please visit <strong><a style="COLOR:#df0000" href="https://www.orapages.com/${company_url}"
                                target="_blank">https://www.orapages.com/<wbr>${company_url}</a></strong>.
                        You will also be pleased to know that <strong>OraPages</strong> allows you
                        to <strong>edit your entire business listing by yourself</strong> from
                        this point onwards! For example, you can: <br><br>
                        <table style="COLOR:#000000">
                            <tbody>
                                <tr>
                                    <td
                                        style="WIDTH:700px;PADDING-BOTTOM:0px;PADDING-TOP:5px;PADDING-LEFT:5px;PADDING-RIGHT:5px;BACKGROUND-COLOR:#e3e3e3">
                                        <ul>
                                            <li>Change your business description or add more content to it
                                            </li>
                                            <li>Upload new photos of your products and services
                                            </li>
                                            <li>List your business under new categories and brands for better
                                                exposure on search engines
                                            </li>
                                            <li>Embed YouTube videos in your listing
                                            </li>
                                            <li>And so much more... </li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table><br>Having
                        <strong>FULL CONTROL</strong> over your own business listing is what makes
                        <strong>OraPages</strong> different from other online business
                        directories. Furthermore, it is totally FREE and will always remain FREE.
                        <br><br>
                        <table style="COLOR:#000000">
                            <tbody>
                                <tr>
                                    <td
                                        style="FONT-SIZE:16px;WIDTH:700px;PADDING-BOTTOM:5px;PADDING-TOP:5px;PADDING-LEFT:5px;PADDING-RIGHT:5px;BACKGROUND-COLOR:#e3e3e3">
                                        <strong>TO
                                            EDIT YOUR BUSINESS LISTING, FOLLOW THE STEPS BELOW:</strong>
                                        <ul>
                                            <li>Please visit <strong><a style="COLOR:#df0000"
                                                        href="https://www.orapages.com" target="_blank">https://www.orapages.com</a></strong>

                                            </li>
                                            <li>Click '<strong> Login</strong>' at the top right
                                                hand side
                                            </li>
                                            <li>Enter the following login details:
                                            </li>
                                            <li>Email address: <strong><a href="mailto:${company_email}"
                                                        target="_blank">${company_email}</a></strong>
                                            </li>
                                            <li>Password: <strong>${password}</strong>
                                            </li>
                                            <li>Click '<strong>Sign In</strong>'
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table><br>As a measure of respect for your
                        privacy, you will also be able to delete your business listing if you ever
                        wish to do so. To delete your listing, simply click the '<strong>Delete
                            Listing</strong>' button at the buttom of the listing form.<br><br>We are
                        certain that you will realise the benefits of listing on
                        <strong>OraPages</strong> over the coming weeks and months. We have many
                        plans for enhancing this unique business directory to make it truly
                        rewarding to all registered businesses.<br><br>Should you have any
                        difficulty using OraPages, please do not hesitate to contact us either by
                        replying to this email or emailing <a style="COLOR:#df0000">info@orapages.com</a>. <br><br>Kind
                        Regards<br>OraPages Team <br><br><img
                            style="PADDING-BOTTOM:5px;PADDING-TOP:5px;PADDING-LEFT:5px;MARGIN-LEFT:10px;PADDING-RIGHT:5px"
                            src="https://orapagesdir.web.app/orapages/images/logo.jpg">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>`;
};

const transporter = mailer.createTransport({
  pool: true,
  host: "mail.orapages.com",
  port: 465,
  secure: true,
  requireTLS: true,
  auth: {
    user: "info@orapages.com",
    pass: "info@orapages",
  },
});

let mailOptions = (
  company_email,
  company_business_name,
  company_url,
  password = null
) => {
  return {
    from: '"Orapages " <info@orapages.com>',
    to: company_email,
    subject: company_business_name + " has been registered to orapges",
    html: getCompanyEmail(
      company_business_name,
      company_email,
      company_url,
      password
    ),
  };
};

module.exports = { mailOptions, transporter };
