const bcrypt = require("bcryptjs");
const csvtojson = require("csvtojson");
const random = require("randomstring");
const request = require("request");
const fse = require("fs-extra");
require("events").EventEmitter.prototype._maxListeners = 0;

const { selectPromise, queryPromise } = require("./database");
const { updateUserEmail } = require("./owner");
const { transporter } = require("./../database/email");
const Company = require(`./company`);
const PremiumOrder = require(`./premium_order`);

const rewrite = (name) =>
  name
    .replace(/[^a-z0-9A-z]/g, "-")
    .replace(/-+$/, "")
    .replace(/-+/g, "-")
    .toLowerCase();

const getCategoryById = async (category) => {
  const query = `select category_id, category_name from
    category where category_id = '${category}'`;
  const category1 = await queryPromise(query);
  return category1[0];
};

let LARGE_BULK_DONE = true;

const saveFile = (path, file) => {
  return new Promise((resolve, reject) => {
    fse.outputFile(path, Buffer.from(file, "base64"), async function (err) {
      if (!err) {
        resolve(path);
      } else {
        reject(err);
      }
    });
  });
};

const doRequest = (url) => {
  return new Promise(function (resolve, reject) {
    request.get(
      {
        url: url,
        headers: {
          "User-Agent": "request",
        },
        timeout: 15000,
        jar: true,
      },
      function (error, res, body) {
        if (!error && res.statusCode == 200) {
          resolve(body);
        } else {
          reject(error);
        }
      }
    );
  });
};

const deleteCSV = async (csv_id, type = "admin") => {
  let csv_type;
  if (type == "admin") {
    csv_type = "csv";
  } else {
    csv_type = "auto_csv";
  }
  const query = `delete from ${csv_type}_data where csv_id = '${csv_id}'`;
  const query1 = `delete from ${csv_type}s where csv_id = '${csv_id}'`;
  await queryPromise(query);
  await queryPromise(query1);
};

const getLocation = async (location) => {
  const cityQuery = `select city_name from cities where
      city_id='${location.split(",")[0]}'`;
  const stateQuery = `select state_name from states where
      state_id='${location.split(",")[1]}'`;
  const countryQuery = `select country_name from countries where
     country_id='${location.split(",")[2]}'`;
  const city = await selectPromise(cityQuery);
  const state = await selectPromise(stateQuery);
  const country = await selectPromise(countryQuery);
  const full_name = [
    city[0].city_name,
    state[0].state_name,
    country[0].country_name,
  ].join(", ");
  return full_name;
};

const insertCompanies = async (companies, ip, uploaded_by = "admin") => {
  let r = uploaded_by;
  for (const company in companies) {
    if (companies.hasOwnProperty(company)) {
      const {
        categories = [],
        company_business_name,
        company_business_details = "\n",
        company_email,
        company_name,
        company_name_short,
        company_url,
        company_website,
        locations = [],
        created_at = new Date().toUTCString(),
        last_update = new Date().toUTCString(),
        last_update_ip = ip,
        uploaded_by = r,
        uploaded_ip = ip,
      } = companies[company];

      const companyCheck = `select * from company where
         company_name_short = '${company_name_short}'`;

      const companyCheckResult = await queryPromise(companyCheck);

      if (companyCheckResult.length <= 0) {
        let companyQuery = `INSERT INTO company(company_id,company_business_name
              ,company_name,company_name_short,company_url, company_email,
              company_website,created_at, last_update, uploaded_by, uploaded_ip,
              company_business_details,last_update_ip) VALUES
              ("${company}","${company_business_name}","${company_name}",
              "${company_name_short}","${company_url}","${company_email}",
              "${company_website}",${new Date(created_at).getTime()},${new Date(
          last_update
        ).getTime()},"${uploaded_by}","${uploaded_ip}",${JSON.stringify(
          company_business_details
        )},"${last_update_ip}")`;

        for (const location in locations) {
          if (locations.hasOwnProperty(location)) {
            const {
              complete_address,
              email = "",
              fax = "",
              location_id,
              mobile = "",
              website = "",
            } = locations[location];
            const { city_id, state_id, country_id } = location_id;
            let companyLocationQuery = `INSERT INTO company_location(company_id,
                  city_id, state_id, country_id, complete_address, phone, fax,
                  email,website) VALUES ("${company}","${city_id}","${state_id}",
                  "${country_id}",${JSON.stringify(
              complete_address
            )},${JSON.stringify(mobile)},${JSON.stringify(
              fax
            )},"${email}","${website}")`;

            await queryPromise(companyLocationQuery);
          }
        }

        for (const category in categories) {
          if (categories.hasOwnProperty(category)) {
            const categoryr = categories[category];
            let { category_id, category_des } = categoryr;
            let companyCategoryQuery = `INSERT INTO company_category(company_id,
                  category_id, category_desc) VALUES ("${company}",
                  "${category_id}",${JSON.stringify(category_des)})`;

            await queryPromise(companyCategoryQuery);
          }
        }

        let password = random.generate({
          length: 7,
          charset: "hex",
        });
        let user = random.generate({
          length: 30,
          charset: "hex",
        });

        await setUser(user, company_email, password, company, company_name);

        await queryPromise(companyQuery);

      }

      // transporter.sendMail(mailOptions(company_email,company_business_name
      //   ,company_url,password), function (error, info) {
      //     if (error) {
      //         console.log(error);
      //     } else {
      //         console.log('Email sent: ' + info.response);
      //     }
      // });
    }
  }
};

const setUser = async (user_id, email, password, company_id, name = null) => {
  let hashed_password = await bcrypt.hash(password, 8);

  await queryPromise(
    `insert into users(user_id,user_name,user_email,password)

      values('${user_id}',${JSON.stringify(
      name
    )},'${email}', '${hashed_password}')`
  );
  await queryPromise(
    `insert into company_user(user_id,company_id)
       values('${user_id}','${company_id}')`
  );
};

module.exports = {
  getCountryCompanyCount: async () => {
    const query = `select countries.country_name as name,
      count(distinct company_location.company_id) as count from company_location
      inner join countries on countries.country_id = company_location.country_id
      inner join company on company.company_id = company_location.company_id
      GROUP by countries.country_name`;
    return await queryPromise(query);
  },

  catSubCatNonCatTagTotal: async () => {
    const query = `select count(category_id) as total from category`;
    const query2 = `select count(subcategory_id) as total from subcategory`;
    const query3 = `select count(non_dis_subcategory_id) as total
                  from non_dis_subcategory`;
    const query4 = `select count(tag_id) as total from user_tags`;
    const count = await queryPromise(query);
    const count2 = await queryPromise(query2);
    const count3 = await queryPromise(query3);
    const count4 = await queryPromise(query4);
    let r = [];
    r[0] = count[0].total;
    r[1] = count2[0].total;
    r[2] = count3[0].total;
    r[3] = count4[0].total;
    return r;
  },

  getCountries: async () => {
    const query = `select country_id,country_name from countries
      order by country_name_short asc`;
    return await queryPromise(query);
  },

  getCompanies: async (limit, offset = 0) => {
    const query = `select * from company order by created_at desc
    limit ${limit} offset ${offset} `;
    const companies = await queryPromise(query);
    let coms = [];
    for (const company of companies) {
      let r = new Company(company);
      await r.getFirstLocationsDB();
      coms.push(r);
    }
    return coms;
  },

  getTotalCompanies: async () => {
    const query = `select count(company_id) as company_count from company`;
    let count = await queryPromise(query);
    return count[0];
  },

  getCompaniesName: async (term) => {
    const query = `select company_business_name as 'value', company_id
    as 'key' from company where company_business_name
      like "${term}%" order by value asc limit 200`;
    const names = await queryPromise(query);
    if (names.length <= 0) {
      throw new Error("No results found");
    }
    return names;
  },

  getCompanyEmail: async (term) => {
    const query = `select company.company_id as 'key',users.user_email as 'value'
      from company_user inner join company on company_user.company_id
      = company.company_id inner join users on company_user.user_id
      = users.user_id and users.user_email like '${term}%'
      order by value limit 200;`;
    const email = await queryPromise(query);
    if (email.length <= 0) {
      throw new Error("No results found");
    }
    return email;
  },

  getCompaniesByBrand: async (brand) => {
    const query = `select * from company inner join company_brand on
     company_brand.company_id = company.company_id
     and company_brand.brand_id = '${brand}';`;
    const companies = await queryPromise(query);
    if (companies <= 0) {
      throw new Error("No results found");
    }
    let coms = [];
    for (const company of companies) {
      let r = new Company(company);
      await r.getFirstLocationsDB();
      coms.push(r);
    }
    return coms;
  },

  getCompaniesByCategory: async (category) => {
    const query = `select * from company inner join company_category on
      company_category.company_id = company.company_id and
      company_category.category_id = '${category}';`;
    const companies = await queryPromise(query);
    if (companies <= 0) {
      throw new Error("No results found");
    }
    let coms = [];
    for (const company of companies) {
      let r = new Company(company);
      await r.getFirstLocationsDB();
      coms.push(r);
    }
    return coms;
  },

  getCompanyById: async (company_id) => {
    const query = `select * from company where company_id = '${company_id}'`;
    const company = await queryPromise(query);
    if (company <= 0) {
      throw new Error("No result found");
    }
    let com = new Company(company[0]);
    await com.getFirstLocationsDB();
    return com;
  },

  getCompanyByIdFull: async (company_id) => {
    const query = `select * from company where company_id = '${company_id}';`;

    const r = await queryPromise(query);

    if (r.length <= 0) {
      throw new Error(`You have no company`);
    }

    const company = new Company(r[0]);

    await company.getCategoriesDB();

    await company.getImagesDB();

    await company.getLogoDB();

    await company.getLocationsDB();

    await company.getBrandsDB();

    await company.getUserDB();

    await company.getTagsDB();

    await company.getVideoDB();

    await company.getOtherEmail();

    await company.getOtherWebsite();

    return company;
  },

  deleteCompany: async (company_id) => {
    let company = new Company({
      company_id,
    });
    await company.deleteCompany();
  },

  updateAdminCompany: async (company) => {
    let c = new Company(company);
    if (!!c.logo) {
      if (c.logo.uploaded === "false") {
        await saveFile(
          `company_images/${c.company_id}/logo/${c.logo.file_name}`,
          c.logo.file64
        );
        const s = await c.setLogo(c.logo);
      } else {
        await c.updateLogo(c.logo);
      }
    } else {
      await c.deleteLogoExtra();
    }

    let uploadedImage = [];
    for (const image of c.images) {
      if (!!image.uploaded) {
        uploadedImage.push(image.image_id);
        await c.updateImage(image);
      } else {
        await saveFile(
          `company_images/${c.company_id}/${image.file_name}`,
          image.file64
        );
        const s = await c.setImage(image);
        uploadedImage.push(s);
      }
    }

    await c.getUserDB();

    await c.deleteImageExtra(uploadedImage);

    await updateUserEmail(c.user_id, c.company_email);

    await c.updateCompany();

    return c;
  },

  insertCompanyPremium: async (company) => {
    const {
      categories = [],
      images = [],
      brands = [],
      company_business_name,
      company_business_details = "\n",
      company_email,
      company_password,
      company_name,
      company_name_short,
      company_url,
      company_website,
      locations = [],
      created_at = null,
      last_update = null,
      last_update_ip = "0.0.0.0",
      uploaded_by,
      uploaded_ip,
      logo,
      company_theme,
      premium_expiry_date = 0,
    } = company;
    let c = new Company({
      company_id: null,
      company_name,
      company_business_name,
      company_email,
      company_name_short,
      company_url,
      company_website,
      created_at,
      last_update,
      uploaded_by,
      uploaded_ip,
      last_update_ip,
      images,
      categories,
      brands,
      locations,
      company_business_details,
      logo,
      user_id: null,
      company_theme,
      premium_expiry_date,
    });

    if (!!c.logo) {
      await saveFile(
        `company_images/${c.company_id}/logo/${c.logo.file_name}`,
        c.logo.file64
      );
      const s = await c.setLogo(c.logo);
    }

    c.images.forEach(async function (image) {
      await saveFile(
        `company_images/${c.company_id}/${image.file_name}`,
        image.file64
      );
      await c.setImage(image);
    });

    await c.setCompany();
    await setUser(
      c.company_email,
      company_password,
      c.company_id,
      c.company_name
    );
    return c;
  },

  checkEmail: async (term, key) => {
    const company = new Company({
      company_id: key,
    });
    await company.getUserDB();
    const query = `select user_email from users where
      user_email = '${term}' and user_id != '${company.user_id}'`;
    const r = await queryPromise(query);
    return r;
  },

  checkName: async (term, key) => {
    const company = new Company({
      company_id: key,
    });
    const query = `select company_business_name from company
     where company_business_name = ? and company_id != ?`;
    const r = await queryPromise(query, [term, company.company_id]);
    return r;
  },

  getCountriesByterm: async (term) => {
    const query = `select country_name as 'value' ,country_id as 'key'
     from countries where country_name_short like '${rewrite(
       term
     )}%' order by country_name asc`;
    return await queryPromise(query);
  },

  addCountry: async (country) => {
    const id = random.generate({
      length: 30,
      charset: "hex",
    });
    const countryQuery = `select * from countries where
    country_name_short = '${rewrite(country)}'`;
    const countries = await queryPromise(countryQuery);
    if (countries.length > 0) {
      throw new Error("This country already exists");
    }
    const query = `insert into countries
    (country_id,country_name,country_name_short) values (?,?,?)`;
    await queryPromise(query, [id, country, rewrite(country)]);
  },

  updateCountry: async (country_name, country_id) => {
    const query = `update countries set country_name = ?,
     country_name_short = ? where country_id = ?`;
    const countryQuery = `select * from countries where
     country_name_short = '${rewrite(
       country_name
     )}' and country_id != '${country_id}'`;
    const countries = await queryPromise(countryQuery);
    if (countries.length > 0) {
      throw new Error("This country already exists");
    }
    await queryPromise(query, [
      country_name,
      rewrite(country_name),
      country_id,
    ]);
  },

  deleteCountry: async (country_id) => {
    const query = `select state_name from states where country_id = '${country_id}'`;
    const states = await queryPromise(query);
    if (states.length > 0) {
      let r = "";
      let x = 1;
      states.forEach((state) => {
        r += `${x}: ${state.state_name}\n`;
        x++;
      });
      throw new Error(`This country has states\n${r}`);
    }
    const deleteCountryQuery = `delete from countries where country_id= '${country_id}'`;
    await queryPromise(deleteCountryQuery);
  },

  getStatesByterm: async (country, term) => {
    const query = `select state_name as 'value', state_id as 'key'
     from states where state_name_short like '${rewrite(
       term
     )}%' and country_id = '${country}' order by value asc`;
    return await queryPromise(query);
  },

  addState: async (state, country_id) => {
    const id = random.generate({
      length: 30,
      charset: "hex",
    });
    const query = `select * from states where country_id = '${country_id}'
     and state_name_short = '${rewrite(state)}'`;
    const states = await queryPromise(query);
    if (states.length > 0) {
      throw new Error("This state is already exists in this country");
    }
    const stateQuery = `insert into states(country_id,state_id,state_name,
      state_name_short) values(?,?,?,?)`;

    await queryPromise(stateQuery, [country_id, id, state, rewrite(state)]);
  },

  updateState: async (state, state_id, country_id) => {
    const query = `select * from states where country_id = '${country_id}'
     and state_name_short = '${rewrite(state)}' and state_id != '${state_id}'`;
    const states = await queryPromise(query);
    if (states.length > 0) {
      throw new Error("This state is already exists in this country");
    }
    const stateUpdate = `update states set state_name = ?,
     state_name_short= ? where country_id = ? and state_id = ?`;
    await queryPromise(stateUpdate, [
      state,
      rewrite(state),
      country_id,
      state_id,
    ]);
  },

  deleteState: async (state_id, country_id) => {
    const query = `select city_name from cities where state_id = '${state_id}'`;
    const cities = await queryPromise(query);
    if (cities.length > 0) {
      let r = "";
      let x = 1;
      cities.forEach((city) => {
        r += `${x}: ${city.city_name}\n`;
        x++;
      });
      throw new Error(`This state has cities\n${r}`);
    }
    const queryState = `delete from states where state_id = '${state_id}'
     and country_id = '${country_id}'`;
    await queryPromise(queryState);
  },

  getCitiesByterm: async (state, country, term) => {
    const query = `select city_name as 'value', city_id as 'key'
      from cities where city_name_short like '${rewrite(term)}%'
      and country_id = '${country}' and state_id = '${state}' order by value asc`;
    return await queryPromise(query);
  },

  addCity: async (city, state_id, country_id) => {
    const id = random.generate({
      length: 30,
      charset: "hex",
    });
    const query = `select * from cities where country_id = '${country_id}'
     and state_id ='${state_id}' and city_name_short = '${rewrite(city)}'`;
    const cities = await queryPromise(query);
    if (cities.length > 0) {
      throw new Error("This city is already exists in this state");
    }
    const cityQuery = `insert into cities(country_id,state_id,city_id
      ,city_name,city_name_short) values(?,?,?,?,?)`;

    await queryPromise(cityQuery, [
      country_id,
      state_id,
      id,
      city,
      rewrite(city),
    ]);
  },

  updateCity: async (city, city_id, state_id, country_id) => {
    const query = `select * from cities where country_id = '${country_id}'
     and state_id = '${state_id}' and city_name_short = '${rewrite(city)}'
      and city_id != '${city_id}'`;
    const cities = await queryPromise(query);
    if (cities.length > 0) {
      throw new Error("This city is already exists in this state");
    }
    const cityUpdate = `update cities set city_name = ?, city_name_short= ?
     where country_id = ? and state_id = ? and city_id = ?`;
    await queryPromise(cityUpdate, [
      city,
      rewrite(city),
      country_id,
      state_id,
      city_id,
    ]);
  },

  deleteCity: async (city_id, state_id, country_id) => {
    const query = `select company.company_business_name from cities INNER join
      company_location on company_location.city_id = cities.city_id and
      cities.city_id = '${city_id}' INNER JOIN company on
      company_location.company_id = company.company_id`;
    const cities = await queryPromise(query);
    if (cities.length > 0) {
      let r = "";
      let x = 1;
      cities.forEach((city) => {
        r += `${x}: ${city.company_business_name}\n`;
        x++;
      });
      throw new Error(`This city has companies \n ${r}`);
    }
    const queryCity = `delete from cities where city_id = '${city_id}'
     and country_id = '${country_id}' and state_id = '${state_id}'`;
    await queryPromise(queryCity);
  },

  getTranferCityCompanies: async (city_id) => {
    const query = `select company.company_business_name from cities INNER join
      company_location on company_location.city_id = cities.city_id and
      cities.city_id = '${city_id}' INNER JOIN company on
      company_location.company_id = company.company_id`;
    const cities = await queryPromise(query);
    let r = "";
    if (cities.length > 0) {
      cities.forEach((city) => {
        r += `<li>${city.company_business_name}</li>`;
      });
    }
    return r;
  },

  getTranferCategoryCompanies: async (category_id) => {
    const query = `select company.company_business_name from category
      INNER join company_category on company_category.category_id
      = category.category_id and category.category_id = '${category_id}'
      INNER JOIN company on company_category.company_id = company.company_id`;
    const companies = await queryPromise(query);
    let r = "";
    if (companies.length > 0) {
      companies.forEach((company) => {
        r += `<li>${company.company_business_name}</li>`;
      });
    }
    return r;
  },

  transferCompaniesByCity: async (first, second) => {
    const [f_city, f_state, f_country] = first.split(",");
    const [s_city, s_state, s_country] = second.split(",");
    const query = `update company_location set city_id=?,state_id=?,country_id=?
    where city_id=? and state_id=? and country_id=?`;
    await queryPromise(query, [
      s_city,
      s_state,
      s_country,
      f_city,
      f_state,
      f_country,
    ]);
  },

  transferCompaniesByCategory: async (first, second) => {
    const firstCategory = first;
    const secondCategory = second;
    const query = `update company_category set category_id = ? where category_id= ?`;
    const queryDelete = `DELETE t1 FROM company_category t1
                          INNER JOIN company_category t2
                          WHERE
                              t1.cc_id < t2.cc_id AND
                              t1.company_id = t2.company_id AND
                              t1.category_id = t2.category_id AND
                              t1.category_id = '${secondCategory}'`;
    await queryPromise(query, [secondCategory, firstCategory]);
    await queryPromise(queryDelete);
  },

  getCategoriesByTerm: async (term) => {
    const query = `select category_name as 'value', category_id as 'key' from
      category where category_name_short like
      '%${rewrite(term)}%' order by value asc`;
    return await queryPromise(query);
  },

  addCategory: async (category) => {
    const id = random.generate({
      length: 30,
      charset: "hex",
    });
    const categoryQuery = `select * from category where
    category_name_short = '${rewrite(category)}'`;
    const categories = await queryPromise(categoryQuery);
    if (categories.length > 0) {
      throw new Error("This category is already exists");
    }
    const query = `insert into category (category_id,category_name,
      category_name_short) values (?,?,?)`;
    await queryPromise(query, [id, category, rewrite(category)]);
  },

  updateCategory: async (category_id, category_name) => {
    const categoryQuery = `select * from category where
    category_name_short = '${rewrite(category_name)}'`;
    const categories = await queryPromise(categoryQuery);
    if (categories.length > 0) {
      throw new Error("This category is already exists");
    }
    const query = `update category set category_name = ?,
     category_name_short = ? where category_id = ?`;
    await queryPromise(query, [
      category_name,
      rewrite(category_name),
      category_id,
    ]);
  },

  deleteCategory: async (category_id) => {
    const query = `select subcategory.subcategory_name from category_subcategory
     inner join subcategory on subcategory.subcategory_id =
     category_subcategory.subcat_id and category_subcategory.category_id
     = '${category_id}' inner join category on category.category_id
     = category_subcategory.category_id`;
    const query2 = `select non_dis_subcategory.non_dis_subcategory_name
      from non_dis_category_subcategory inner join non_dis_subcategory on
      non_dis_subcategory.non_dis_subcategory_id =
      non_dis_category_subcategory.non_dis_subcat_id
      and non_dis_category_subcategory.category_id = '${category_id}'
      inner join category on category.category_id =
      non_dis_category_subcategory.category_id;`;
    const non_dis_subcategories = await queryPromise(query2);
    const subcategories = await queryPromise(query);
    const companyQuery = `select company.company_business_name from
    company_category INNER JOIN company on company.company_id =
    company_category.company_id and company_category.category_id
    = '${category_id}'`;
    const companies = await queryPromise(companyQuery);
    if (subcategories.length > 0) {
      let r = "";
      let x = 1;
      subcategories.forEach((subcategory) => {
        r += `${x}: ${subcategory.subcategory_name}\n`;
        x++;
      });
      throw new Error(`This category has subcategories \n ${r}`);
    }
    if (non_dis_subcategories.length > 0) {
      let r = "";
      let x = 1;
      non_dis_subcategories.forEach((subcategory) => {
        r += `${x}: ${subcategory.non_dis_subcategory_name}\n`;
        x++;
      });
      throw new Error(`This category has Non Display Subcategory \n ${r}`);
    }
    if (companies.length > 0) {
      let r = "";
      let x = 1;
      companies.forEach((company) => {
        r += `${x}: ${company.company_business_name}\n`;
        x++;
      });
      throw new Error(`This category has companies \n ${r}`);
    }
    const deleteCategoryQuery = `delete from category where
     category_id= '${category_id}'`;
    await queryPromise(deleteCategoryQuery);
  },

  getSubcategoriesByTerm: async (term) => {
    const query = `select subcategory_name as 'value', subcategory_id as
     'key' from subcategory where subcategory_name_short
     like '%${rewrite(term)}%' order by value asc`;
    return await queryPromise(query);
  },

  getNonDisSubcategoriesByTerm: async (term) => {
    const query = `select non_dis_subcategory_name as 'value',
      non_dis_subcategory_id as 'key' from non_dis_subcategory
      where non_dis_subcategory_name_short
      like '%${rewrite(term)}%' order by value asc`;
    return await queryPromise(query);
  },

  getCategoryById: async (category) => {
    const query = `select category_id, category_name from
    category where category_id = '${category}'`;
    const category1 = await queryPromise(query);
    return category1[0];
  },

  getSubcatCategories: async (subcategory_id) => {
    const query = `select category.category_id as 'key', category.category_name
      as 'value' from category_subcategory inner join category
      on category.category_id = category_subcategory.category_id where
      category_subcategory.subcat_id = '${subcategory_id}' order by 'value';`;
    return await queryPromise(query);
  },

  getNonDisSubcatCategories: async (subcategory_id) => {
    const query = `select category.category_id as 'key', category.category_name
      as 'value' from non_dis_category_subcategory inner join category
      on category.category_id = non_dis_category_subcategory.category_id
      where non_dis_category_subcategory.non_dis_subcat_id =
      '${subcategory_id}' order by 'value';`;
    return await queryPromise(query);
  },

  addSubcategory: async (
    category_id = null,
    subcategory_id = "",
    subcategory
  ) => {
    let query = "";
    let id = "";
    if (!!subcategory_id) {
      id = subcategory_id;
      query = `select * from subcategory where
      subcategory_name_short = '${rewrite(subcategory)}' and
      subcategory_id != '${id}'`;
    } else {
      query = `select * from subcategory where
       subcategory_name_short = '${rewrite(subcategory)}'`;
      id = random.generate({
        length: 30,
        charset: "hex",
      });
    }

    const subcategories = await queryPromise(query);

    if (subcategories.length > 0) {
      throw new Error("This subcategory is already exists");
    }

    if (!subcategory_id) {
      const subcategoryQuery = `insert into subcategory(subcategory_id,
          subcategory_name,subcategory_name_short) values (?,?,?)`;
      await queryPromise(subcategoryQuery, [
        id,
        subcategory,
        rewrite(subcategory),
      ]);
    }

    if (!!category_id) {
      const categorySubcategory = `insert into category_subcategory(subcat_id,
          category_id) values ('${id}','${category_id}')`;
      await queryPromise(categorySubcategory);
    }

    return id;
  },

  addNonDisSubcategory: async (
    category_id = null,
    subcategory_id = "",
    subcategory
  ) => {
    let query = "";
    let id = "";
    if (!!subcategory_id) {
      id = subcategory_id;
      query = `select * from non_dis_subcategory where
      non_dis_subcategory_name_short =
      '${rewrite(subcategory)}' and non_dis_subcategory_id != '${id}'`;
    } else {
      query = `select * from non_dis_subcategory where
      non_dis_subcategory_name_short = '${rewrite(subcategory)}'`;
      id = random.generate({
        length: 30,
        charset: "hex",
      });
    }

    const subcategories = await queryPromise(query);

    if (subcategories.length > 0) {
      throw new Error("This non displayable subcategory is already exists");
    }

    if (!subcategory_id) {
      const subcategoryQuery = `insert into non_dis_subcategory
      (non_dis_subcategory_id,non_dis_subcategory_name,
      non_dis_subcategory_name_short) values (?,?,?)`;
      await queryPromise(subcategoryQuery, [
        id,
        subcategory,
        rewrite(subcategory),
      ]);
    }

    if (!!category_id) {
      const categorySubcategory = `insert into non_dis_category_subcategory
      (non_dis_subcat_id,category_id) values ('${id}','${category_id}')`;
      await queryPromise(categorySubcategory);
    }

    return id;
  },

  updateSubcategory: async (category_arr, subcategory_id, subcategory) => {
    const query = `select * from subcategory where subcategory_name_short
    = '${rewrite(subcategory)}' and subcategory_id != '${subcategory_id}'`;
    const subcategories = await queryPromise(query);

    if (subcategories.length > 0) {
      throw new Error("This subcategory is already exists");
    }

    const updateQuery = `update subcategory set subcategory_name = ?,
     subcategory_name_short = ? where subcategory_id = ?`;
    await queryPromise(updateQuery, [
      subcategory,
      rewrite(subcategory),
      subcategory_id,
    ]);

    let uploadedCategory = [];
    for (const category of category_arr) {
      const query1 = `select * from category_subcategory where
      subcat_id = '${subcategory_id}' and category_id = '${category}'`;
      const categories = await queryPromise(query1);
      uploadedCategory.push(category);
      if (categories.length <= 0) {
        const addCategory = `insert into category_subcategory
        (subcat_id,category_id) values ('${subcategory_id}','${category}')`;
        await queryPromise(addCategory);
      }
    }

    const deleteExtra = `delete from category_subcategory where
     category_id not in (?) and subcat_id = ?`;
    await queryPromise(deleteExtra, [uploadedCategory, subcategory_id]);
  },

  updateNonDisSubcategory: async (
    category_arr,
    subcategory_id,
    subcategory
  ) => {
    const query = `select * from non_dis_subcategory where
     non_dis_subcategory_name_short
     = '${rewrite(subcategory)}'
     and non_dis_subcategory_id != '${subcategory_id}'`;
    const subcategories = await queryPromise(query);

    if (subcategories.length > 0) {
      throw new Error("This Non Displayable subcategory is already exists");
    }

    const updateQuery = `update non_dis_subcategory set non_dis_subcategory_name
    = ?, non_dis_subcategory_name_short = ? where non_dis_subcategory_id = ?`;
    await queryPromise(updateQuery, [
      subcategory,
      rewrite(subcategory),
      subcategory_id,
    ]);

    let uploadedCategory = [];
    for (const category of category_arr) {
      const query1 = `select * from non_dis_category_subcategory where
      non_dis_subcat_id = '${subcategory_id}' and category_id = '${category}'`;
      const categories = await queryPromise(query1);
      uploadedCategory.push(category);
      if (categories.length <= 0) {
        const addCategory = `insert into non_dis_category_subcategory
        (non_dis_subcat_id,category_id) values ('${subcategory_id}','${category}')`;
        await queryPromise(addCategory);
      }
    }

    const deleteExtra = `delete from non_dis_category_subcategory
    where category_id not in (?) and non_dis_subcat_id = ?`;
    await queryPromise(deleteExtra, [uploadedCategory, subcategory_id]);
  },

  deleteSubcatCat: async (category_id, subcategory_id) => {
    const deleteQuery = `delete from category_subcategory where
      subcat_id = '${subcategory_id}' and category_id = '${category_id}'`;
    await queryPromise(deleteQuery);
  },

  deleteNonDisSubcatCat: async (category_id, subcategory_id) => {
    const deleteQuery = `delete from non_dis_category_subcategory where
    non_dis_subcat_id = '${subcategory_id}' and category_id = '${category_id}'`;
    await queryPromise(deleteQuery);
  },

  deleteSubcategory: async (subcategory_id) => {
    const deleteQuery = `delete from subcategory where subcategory_id
    = '${subcategory_id}'`;
    const deleteQuery2 = `delete from category_subcategory where
     subcat_id = '${subcategory_id}'`;
    await queryPromise(deleteQuery);
    await queryPromise(deleteQuery2);
  },

  deleteNonDisSubcategory: async (subcategory_id) => {
    const deleteQuery = `delete from non_dis_subcategory where
    non_dis_subcategory_id = '${subcategory_id}'`;
    const deleteQuery2 = `delete from non_dis_category_subcategory
    where non_dis_subcat_id = '${subcategory_id}'`;
    await queryPromise(deleteQuery);
    await queryPromise(deleteQuery2);
  },

  addBrand: async (brand) => {
    const id = random.generate({
      length: 30,
      charset: "hex",
    });
    const query = `insert into brands (brand_id,brand_name,brand_name_short)
      values (?,?,?)`;
    await queryPromise(query, [id, brand, rewrite(brand)]);
  },

  updateBrand: async (brand_id, brand_name) => {
    const query = `update brands set brand_name = ?, country_short_name = ?
     where brand_id = ?`;
    await queryPromise(query, [brand_name, rewrite(brand_name)], brand_id);
  },

  deleteBrand: async (brand_id) => {
    const companyQuery = `select * from company_brand where
    brand_id = '${brand_id}'`;
    const companies = await queryPromise(companyQuery);
    if (companies.length > 0) {
      throw new Error("This brand has companies");
    }
    const deleteBrandQuery = `delete from brands where brand_id= '${brand_id}'`;
    await queryPromise(deleteBrandQuery);
  },

  uploadBanner: async (banner) => {
    const bannerOrder = `select max(banner_order) as 'order' from banner`;
    const order = await queryPromise(bannerOrder);
    let maxOrder = 1;
    if (order.length >= 1) {
      maxOrder = order[0]["order"] + 1;
    }
    const { file, name, status } = banner;
    const id = random.generate({
      length: 30,
      charset: "hex",
    });
    const url = `banners/${id}.${file.image_type}`;
    saveFile(url, file.image);
    const bannerQuery = `INSERT INTO banner(banner_id, name, flag, url,
      banner_order, active) VALUES ('${id}','${name}','${status}',
      '${url}',${maxOrder},0)`;
    await queryPromise(bannerQuery);
  },

  updateBanner: async (banner) => {
    const { banner_id, name, banner_order, flag } = banner;

    const query = `update banner set name = ?, banner_order = ?,
      flag = ? where banner_id = ?`;
    await queryPromise(query, [name, banner_order, flag, banner_id]);
  },

  deleteBanner: async (banner_id) => {
    const query = `delete from banner where banner_id = '${banner_id}'`;
    await queryPromise(query);
  },

  getBanners: async () => {
    const query = `select * from banner order by banner_order`;
    const banners = await queryPromise(query);
    if (banners.length <= 0) {
      throw new Error(`No banners found please upload them`);
    }

    return banners;
  },

  uploadLargeCSV: async (csv, ip) => {
    let fileURL = csv.path;
    let created_at = new Date().toUTCString();
    let last_update = new Date().toUTCString();
    let uploaded_by = "large_bulk";
    if (!LARGE_BULK_DONE) {
      return "Process is Running Please try after some time";
    }
    let id = random.generate({
      length: 30,
      charset: "hex",
    });

    LARGE_BULK_DONE = false;

    try {
      await fse.writeFileSync(
        `Large Bulk/${id}.csv`,
        "Name, Address, City, Phone, Fax, Email, URL, Profile, Category\n"
      );
    } catch (error) {
      console.log(error);
    }

    await csvtojson()
      .fromFile(fileURL)
      .subscribe((json) => {
        return new Promise(async function (resolve, reject) {
          try {
            let company_id = random.generate({
              length: 30,
              charset: "hex",
            });

            let companyCheck = `select * from company where company_name_short = ?
             or company_url = ?`;

            let duplicateCompany = await queryPromise(companyCheck, [
              rewrite(json.Name || ""),
              rewrite(json.Name || ""),
            ]);

            //   Checking the category
            if (!json.Category) {
              throw new Error("No Categories in this profile");
            }

            //   Checking the city
            if (!json.City) {
              throw new Error("No cities in this profile");
            }

            //   Checking the duplicate profile
            if (duplicateCompany.length > 0) {
              throw new Error("Duplicate profile");
            }

            if (!json.Email) {
              throw new Error("No Email us found");
            }

            // Checking the Url is empty or not
            let body;
            if (!!json.URL) {
              body = await doRequest(json.URL);
              if (body.includes("Domain ") && !!body) {
                fse.appendFileSync(
                  "Large Bulk/eliminated.csv",
                  `"${json.Name}","${json.Address}","${json.City}","${json.Phone}",
                  "${json.Fax}","${json.Email}","${json.URL}","${json.Profile}",
                  "${json.Category}"\n`
                );
                throw new Error("This is invalid profile");
              }
            }

            if (duplicateCompany.length <= 0) {
              for (const category of json.Category.split(",")) {
                if (!!category) {
                  let categoryCheck = `select * from category where
                     category_id = ?`;
                  let category_check_result = await queryPromise(
                    categoryCheck,
                    [category]
                  );

                  if (category_check_result.length > 0) {
                    let companyCategoryQuery = `INSERT INTO company_category
                      (company_id, category_id, category_desc) VALUES (?,?,?)`;
                    await queryPromise(companyCategoryQuery, [
                      company_id,
                      category,
                      json.Profile,
                    ]);
                  } else {
                    fse.appendFileSync(
                      `Large Bulk/category-${id}.csv`,
                      `"${json.Name}","${json.Address}","${json.City}"
                        ,"${json.Phone}","${json.Fax}","${json.Email}"
                        ,"${json.URL}","${json.Profile}","${json.Category}"\n`
                    );
                    throw new Error("Category not found");
                  }
                }
              }

              let companyQuery = `INSERT INTO company(company_id,
                  company_business_name,company_name,company_name_short,company_url,
                  company_email, company_website,created_at, last_update,
                  uploaded_by, uploaded_ip, company_business_details,last_update_ip)
                  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)`;

              await queryPromise(companyQuery, [
                company_id,
                json.Name.trim(),
                json.Name.trim(),
                rewrite(json.Name.trim() || ""),
                rewrite(json.Name.trim() || ""),
                json.Email,
                json.URL,
                new Date(created_at).getTime(),
                new Date(last_update).getTime(),
                uploaded_by,
                ip,
                json.Profile,
                ip,
              ]);

              let companyLocationQuery = `INSERT INTO company_location
                  (company_id, city_id, state_id, country_id,
                  complete_address, phone, fax, email,website)
                  VALUES (?,?,?,?,?,?,?,?,?)`;

              let cityCheck = `select * from cities where city_id = ?
                 and state_id = ? and country_id = ?`;
              let city_check_result = await queryPromise(cityCheck, [
                json.City.split(",")[0],
                json.City.split(",")[1],
                json.City.split(",")[2],
              ]);

              if (city_check_result.length > 0) {
                await queryPromise(companyLocationQuery, [
                  company_id,
                  json.City.split(",")[0],
                  json.City.split(",")[1],
                  json.City.split(",")[2],
                  json.Address,
                  json.Phone,
                  json.Fax,
                  json.Email,
                  json.URL,
                ]);
              } else {
                fse.appendFileSync(
                  `Large Bulk/city-${id}.csv`,
                  `"${json.Name}","${json.Address}","${json.City}",
                    "${json.Phone}","${json.Fax}","${json.Email}","${json.URL}",
                    "${json.Profile}","${json.Category}"\n`
                );
                throw new Error("No city in this profile");
              }

              let password = random.generate({
                length: 7,
                charset: "hex",
              });

              let user = random.generate({
                length: 30,
                charset: "hex",
              });

              await setUser(user, json.Email, password, company_id, json.Name);

            } else {
              fse.appendFileSync(
                `Large Bulk/${id}.csv`,
                `"${json.Name}","${json.Address}","${json.City}","${json.Phone}"
                  ,"${json.Fax}","${json.Email}","${json.URL}","${json.Profile}",
                  "${json.Category}"\n`
              );
              throw new Error("Duplicate company");
            }
          } catch (error) {
            fse.appendFileSync("error.log", error + "\n");
          }
          resolve();
        });
      });

    LARGE_BULK_DONE = true;

    const mail = {
      from: '"Orapages Admin" <info@orapages.com>',
      to: "bms1608@gmail.com",
      subject: `CSV is Uploaded Successfully`,
      text: "Your uploaded CSV has been completed successfully",
    };

    transporter.sendMail(mail, function (error, message) {
      if (error) {
        console.log(error);
      } else {
        console.log(message);
      }
    });
    return "Uploaded Successfully";
  },

  uploadCSV: async (csv) => {
    const csv_id = random.generate({
      length: 30,
      charset: "hex",
    });
    const query = `insert into csvs(csv_id,csv_name,csv_url,csv_uploaded_date)
    values ('${csv_id}','${csv.originalname}',
    '${csv.path.replace("\\", "/")}',${new Date().getTime()})`;
    let fileURL = csv.path;
    let companies = {};
    await csvtojson()
      .fromFile(fileURL)
      .subscribe((json) => {
        return new Promise(async function (resolve, reject) {
          let locationKey = random.generate({
            length: 30,
            charset: "hex",
          });
          let company_id = random.generate({
            length: 30,
            charset: "hex",
          });

          let company = {
            company_name: json.Name.trim(),
            company_name_short: rewrite(json.Name.trim() || ""),
            company_business_details: json.Profile,
            company_business_name: json.Name.trim(),
            company_email: json.Email,
            locations: {
              [locationKey]: {
                email: json.Email,
                website: json.URL,
                complete_address: json.Address,
                mobile: json.Phone,
                fax: json.Fax,
                address: "",
              },
            },
          };

          companies[company_id] = company;

          resolve();
        });
      });

    const setCompany = `insert into csv_data(csv_id,company_data) values (?,?)`;

    await queryPromise(setCompany, [csv_id, JSON.stringify(companies)]);
    await queryPromise(query);
    csv.csv_id = csv_id;
    return csv;
  },

  uploadAutoCSV: async (csv) => {
    const csv_id = random.generate({
      length: 30,
      charset: "hex",
    });
    const query = `insert into auto_csvs(csv_id,csv_name,csv_url,
      csv_uploaded_date) values ('${csv_id}','${
      csv.originalname
    }','${csv.path.replace("\\", "/")}',${new Date().getTime()})`;
    let fileURL = csv.path;
    let companies = {};
    await csvtojson()
      .fromFile(fileURL)
      .subscribe((json) => {
        return new Promise(async function (resolve, reject) {
          let locationKey = random.generate({
            length: 30,
            charset: "hex",
          });
          let company_id = random.generate({
            length: 30,
            charset: "hex",
          });

          let categories = {};

          for (const category of json.Category.split(",")) {
            let category_name = await getCategoryById(category);
            category_name = category_name.category_name;
            categories[category] = {
              category_id: category,
              category_des: json.Profile,
              category_name,
            };
          }

          let company = {
            company_name: json.Name.trim(),
            company_name_short: rewrite(json.Name.trim() || ""),
            company_url: rewrite(json.Name.trim() || ""),
            company_business_details: json.Profile,
            company_business_name: json.Name.trim(),
            company_email: json.Email,
            company_website: json.URL,
            locations: {
              [locationKey]: {
                email: json.Email,
                website: json.URL,
                complete_address: json.Address,
                mobile: json.Phone,
                fax: json.Fax,
                address: await getLocation(json.City),
                location_id: {
                  city_id: json.City.split(",")[0],
                  state_id: json.City.split(",")[1],
                  country_id: json.City.split(",")[2],
                },
              },
            },
            categories,
          };

          companies[company_id] = company;

          resolve();
        });
      });

    // return JSON.stringify(companies);
    const setCompany = `insert into auto_csv_data(csv_id,company_data)
    values (?,?)`;
    await queryPromise(setCompany, [csv_id, JSON.stringify(companies)]);
    await queryPromise(query);
    csv.csv_id = csv_id;
    return csv;
  },

  getCSVList: async (type = "admin") => {
    let query;
    if (type == "admin") {
      query = `select csvs.csv_id,csvs.csv_name,csvs.csv_url,csvs.csv_url,
      csvs.csv_uploaded_date,csvs.csv_updated_date,
      JSON_LENGTH(JSON_KEYS(csv_data.company_data))
       as company_count from csvs INNER JOIN csv_data WHERE
       csv_data.csv_id = csvs.csv_id order by csv_uploaded_date desc`;
    } else {
      query = `select auto_csvs.csv_id,auto_csvs.csv_name,auto_csvs.csv_url,
      auto_csvs.csv_url,auto_csvs.csv_uploaded_date,auto_csvs.csv_updated_date,
      JSON_LENGTH(JSON_KEYS(auto_csv_data.company_data)) as company_count
      from auto_csvs INNER JOIN auto_csv_data
      WHERE auto_csv_data.csv_id = auto_csvs.csv_id
      order by csv_uploaded_date desc`;
    }
    const csvs = await queryPromise(query);
    return csvs;
  },

  deleteCSV: async (csv_id, type = "admin") => {
    let csv_type;
    if (type == "admin") {
      csv_type = "csv";
    } else {
      csv_type = "auto_csv";
    }
    const query = `delete from ${csv_type}_data where csv_id = '${csv_id}'`;
    const query1 = `delete from ${csv_type}s where csv_id = '${csv_id}'`;
    await queryPromise(query);
    await queryPromise(query1);
  },

  updateCSV: async (csv_id, company_data) => {
    const query = `update csv_data set company_data = ? where csv_id = ?`;
    await queryPromise(query, [JSON.stringify(company_data), csv_id]);
    const queryCSV = `update csvs set csv_updated_date = '${new Date().getTime()}'
     where csv_id = '${csv_id}'`;
    await queryPromise(queryCSV);
  },

  transferCSVToDatabase: async (csv_id, ip, uploaded_by = "admin") => {
    let query;

    if (uploaded_by == "admin") {
      query = `select company_data from csv_data where csv_id = '${csv_id}'`;
    } else {
      query = `select company_data from auto_csv_data where csv_id = '${csv_id}'`;
    }

    const csv_data = await queryPromise(query);

    if (csv_data <= 0) {
      throw new Error("CSV has no companies");
    }

    await insertCompanies(
      JSON.parse(csv_data[0].company_data),
      ip,
      uploaded_by
    );
    await deleteCSV(csv_id, uploaded_by);
  },

  getTempCompanies: async (csv_id, type = "admin") => {
    let query;
    if (type === "admin") {
      query = `select * from csv_data where csv_id = '${csv_id}'`;
    } else {
      query = `select * from auto_csv_data where csv_id = '${csv_id}'`;
    }
    return await queryPromise(query);
  },

  checkNameUrl: async (url) => {
    const query = `select * from company where company_url = '${url}'
      or company_name_short = '${url}'`;
    const result = await queryPromise(query);
    if (result.length > 0) {
      return {
        status: true,
        company_id: result[0].company_id,
        company_url: result[0].company_url,
      };
    } else {
      return {
        status: false,
        company_id: "invalid",
        company_url: "invalid",
      };
    }
  },

  getPremiumOrders: async () => {
    const query = `select * from premium_orders where payment_status is not null
    and payment_status = 'approved' order by ordered_at desc`;
    const orders = await queryPromise(query);
    let pr_orders = [];
    for (const order of orders) {
      let r = new PremiumOrder(order);
      pr_orders.push(r);
    }
    return pr_orders;
  },

  updatePremiumOrderCompleted: async (premium_url, payment_id) => {
    const urlQuery = `select * from company where company_url = '${premium_url}'`;
    const url = await queryPromise(urlQuery);
    if (url.length > 0) {
      throw new Error("Url is already exists");
    }
    const query = `select * from premium_orders where payment_id = '${payment_id}'`;
    const order = await queryPromise(query);
    let r = new PremiumOrder(order[0]);
    await r.updateOrderCompleted(premium_url, payment_id);
    return r;
  },

  getReportsData: async (
    start_timestamp,
    end_timestamp,
    start_date,
    end_date
  ) => {
    const postQuery = ` select count(post_id) as post_count from bn_post where Date(post_date) >= ?
    and Date(post_date) <= ? and post_content not like '<b><a href=%'`;
    const postsCount = await queryPromise(postQuery, [start_date, end_date]);
    const friendsQuery = `select COUNT(DISTINCT(partner1_id)) as friend_count from bn_partner
    where Date(date) >= ?
    and Date(date) <= ?`;
    const friendsCount = await queryPromise(friendsQuery, [
      start_date,
      end_date,
    ]);
    const companyQuery = `select count(company_id) as company_count, uploaded_by
    from company where created_at >= ? and created_at <= ?
    group by uploaded_by`;
    const companyCount = await queryPromise(companyQuery, [
      start_timestamp,
      end_timestamp,
    ]);
    return { postsCount, friendsCount, companyCount };
  },

  allPosts: async (limit, offset, dir = "desc") => {
    let query = `select * from bn_post where post_content not like '<b><a href="%'
    order by post_date ${dir} limit ${limit} offset ${offset}`;
    let queryCount = `select count(post_id) as total from bn_post
    where post_content not like '<b><a href="%'`;
    const posts = await queryPromise(query);
    let count = await queryPromise(queryCount);
    count = count[0].total;
    return { count, posts };
  },

  reportedPosts: async (limit, offset, dir = "desc") => {
    let query = `select * from bn_post_report
    inner join
    bn_post on bn_post.post_id = bn_post_report.post_id
    inner join
    company_user on company_user.user_id = bn_post_report.user_id
    inner join
    company on company.company_id = company_user.company_id
    order by bn_post_report.date ${dir} limit ${limit} offset ${offset}`;
    let queryCount = `select count(post_id) as total from bn_post_report`;
    const posts = await queryPromise(query);
    let count = await queryPromise(queryCount);
    count = count[0].total;
    return { count, posts };
  },
};

// module.exports = {
//   getCountryCompanyCount,
//   getCountries,
//   getCompanies,
//   getTotalCompanies,
//   getCompaniesName,
//   getCompanyEmail,
//   getCompaniesByCategory,
//   getCompaniesByBrand,
//   getCompanyById,
//   getCompanyByIdFull,
//   updateAdminCompany,
//   addCountry,
//   updateCountry,
//   deleteCountry,
//   getCountriesByterm,
//   getStatesByterm,
//   addState,
//   updateState,
//   deleteState,
//   getCitiesByterm,
//   addCity,
//   updateCity,
//   deleteCity,
//   getCategoriesByTerm,
//   addCategory,
//   addNonDisSubcategory,
//   updateCategory,
//   deleteCategory,
//   addSubcategory,
//   getSubcategoriesByTerm,
//   getNonDisSubcategoriesByTerm,
//   getSubcatCategories,
//   getNonDisSubcatCategories,
//   deleteSubcategory,
//   deleteNonDisSubcategory,
//   deleteSubcatCat,
//   deleteNonDisSubcatCat,
//   updateSubcategory,
//   updateNonDisSubcategory,
//   deleteCompany,
//   checkEmail,
//   checkName,
//   uploadBanner,
//   getBanners,
//   updateBanner,
//   deleteBanner,
//   uploadCSV,
//   uploadAutoCSV,
//   uploadLargeCSV,
//   getTempCompanies,
//   updateCSV,
//   getCategoryById,
//   getCSVList,
//   transferCSVToDatabase,
//   deleteCSV,
//   checkNameUrl,
//   getTranferCityCompanies,
//   transferCompaniesByCity,
//   getTranferCategoryCompanies,
//   transferCompaniesByCategory,
//   getPremiumOrders,
//   insertCompanyPremium,
//   updatePremiumOrderCompleted,
//   catSubCatNonCatTagTotal,
//   getReportsData,
//   allPosts,
//   reportedPosts,
// };
