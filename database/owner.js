const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const random = require("randomstring");

const { selectPromise, queryPromise } = require("./database");

const setUser = async (email, password, company_id, name = null) => {
  let hashed_password = await bcrypt.hash(password, 8);
  let user_id = random.generate({
    length: 30,
    charset: "hex",
  });
  const token = jwt.sign({ user_id }, "orpagesjsonwebtoken", {
    expiresIn: "2 days",
  });

  await queryPromise(`insert into users(user_id,user_name,user_email,password)
    values('${user_id}',${JSON.stringify(
    name
  )},'${email}', '${hashed_password}')`);

  await queryPromise(`insert into user_token(user_id,token)
    values('${user_id}','${token}')`);

  await queryPromise(`insert into company_user(user_id,company_id)
    values('${user_id}','${company_id}')`);

  return token;
};

const updateUserEmail = async (user_id, user_email) => {
  await queryPromise(`update users set user_email = '${user_email}'
    where user_id = '${user_id}'`);
};

const updateUserPassword = async (user_id, password) => {
  let hashed_password = await bcrypt.hash(password, 8);

  await queryPromise(`update users set password = '${hashed_password}'
    where user_id = '${user_id}'`);

  await queryPromise(`delete from reset_links where user_id = '${user_id}'`);
};

const getUser = async (user_email, user_password) => {
  let user = await selectPromise(`select user_id, user_email, password from
    users where user_email ='${user_email}' limit 1`);

  if (user.length <= 0) {
    throw new Error(`No user records found`);
  }

  let { user_id, password } = user[0];

  let result = await bcrypt.compare(user_password, password);

  if (result) {
    let token = jwt.sign({ user_id }, "orpagesjsonwebtoken", {
      expiresIn: "2 days",
    });

    await queryPromise(`insert into user_token(user_id,token)
        values('${user_id}','${token}')`);

    return token;
  } else {
    throw new Error(`Unable to login`);
  }
};

const logoutUser = async (token) => {
  const decoded = jwt.verify(token, "orpagesjsonwebtoken");

  await queryPromise(`delete from user_token
    where user_id = '${decoded.user_id}' and token = '${token}'`);
};

const setResetUser = async (user_email) => {
  let userQuery = `select user_id from users where user_email = '${user_email}'`;
  let user = await queryPromise(userQuery);

  if (user.length <= 0) {
    throw new Error(`No record found of this email`);
  }

  let { user_id } = user[0];

  let token = jwt.sign({ user_id }, "orapagesjsonwebtoken", {
    expiresIn: "30 mins",
  });

  await queryPromise(`insert into reset_links(user_id,token)
    values ('${user_id}','${token}')`);

  return token;
};

const getResetUser = async (token) => {
  const decoded = jwt.verify(token, "orapagesjsonwebtoken");

  const resetUser = await queryPromise(`select user_id from reset_links
    where token = '${token}'`);

  if (resetUser.length <= 0) {
    throw new Error("This link has been expired");
  }

  return resetUser[0].user_id;
};

module.exports = {
  setUser,
  getUser,
  logoutUser,
  getResetUser,
  setResetUser,
  updateUserPassword,
  updateUserEmail,
};
