const mysql = require("mysql2");
require("dotenv").config();

let con;
const createPool = async () => {
  con = await mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    multipleStatements: true,
  });
};

(async function () {
  try {
    await createPool();
  } catch (error) {
    console.log(error);
  }
})();

let queryPromise = (query, values = []) => {
  return new Promise((resolve, reject) => {
    if (values.length <= 0) {
      con.query(query, function (err, result) {
        if (!err) {
          resolve(result);
        } else {
          reject(err);
        }
      });
    } else {
      con.query(query, values, function (err, result) {
        if (!err) {
          resolve(result);
        } else {
          reject(err);
        }
      });
    }
  });
};

let selectPromise = (query) => {
  return new Promise((resolve, reject) => {
    con.query(query, function (err, result, fields) {
      if (!err) {
        resolve(result);
      } else {
        reject(err);
      }
    });
  });
};

module.exports = {
  selectPromise,
  queryPromise,
};
