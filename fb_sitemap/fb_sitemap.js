require("dotenv").config();

const passport = require("passport");
const Strategy = require("passport-facebook");
const request = require("request-promise");
const fse = require("fs-extra");
const strip = require("string-strip-html");

const { queryPromise } = require("../database/database");
const FrontEndOperations = require("../database/front_end");

const optionss = {
  clientID: process.env["FACEBOOK_CLIENT_ID"],
  clientSecret: process.env["FACEBOOK_CLIENT_SECRET"],
  callbackURL: "http://localhost:7070/facebook_auth",
};

passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (obj, done) {
  done(null, obj);
});

const callback = async function (accessToken, refreshToken, profile, cb) {
  const options = {
    method: "GET",
    uri: "https://graph.facebook.com/v8.0/oauth/access_token?",
    qs: {
      grant_type: "fb_exchange_token",
      client_id: process.env["FACEBOOK_CLIENT_ID"],
      client_secret: process.env["FACEBOOK_CLIENT_SECRET"],
      fb_exchange_token: accessToken,
    },
  };
  const fbRes = await request(options);
  const parsedRes = JSON.parse(fbRes);
  const longLivedOptions = {
    method: "get",
    uri: "https://graph.facebook.com/108990650951136?",
    qs: {
      fields: "access_token",
      access_token: parsedRes.access_token,
    },
  };
  const fbResLong = await request(longLivedOptions);
  const parsedFbResLong = JSON.parse(fbResLong);
  const query = `UPDATE social_media_token SET token_value=? WHERE token_name=?`;
  await queryPromise(query, [
    parsedFbResLong.access_token,
    "facebook_page_token",
  ]);
  process.env.FB_PAGE_TOKEN = parsedFbResLong.access_token;
  return cb(null, accessToken);
};

passport.use(new Strategy(optionss, callback));

const insertSocialMedia = async (company_id, message, link, categories) => {
  let access_token = "";
  if (!!process.env.FB_PAGE_TOKEN) {
    access_token = process.env.FB_PAGE_TOKEN;
  } else {
    access_token = await queryPromise(
      `select token_value from social_media_token where token_name = 'facebook_page_token'`
    );
    access_token = access_token[0].token_value;
  }
  const options = {
    method: "POST",
    uri: `https://graph.facebook.com/v8.0/108990650951136/feed?fields=permalink_url`,
    qs: {
      message: `#${categories.join(", #")}\n${message}`,
      link,
      access_token,
    },
  };
  const postRes = await request(options);
  const dataPostRes = JSON.parse(postRes);
  const query = `INSERT INTO facebook_posts(company_id,post_id, post_url) VALUES (?,?,?)`;
  await queryPromise(query, [
    company_id,
    dataPostRes.id,
    dataPostRes.permalink_url,
  ]);
  return dataPostRes;
};

const insertSocialMediaUrl = async () => {
  const query = `select * from company where company_id not in (select company_id from facebook_posts) limit 20`;
  const queryResult = await queryPromise(query);
  for (const r of queryResult) {
    const company = await FrontEndOperations.companyByUrl(r.company_url);
    let { result } = strip(
      company.company_business_details.substring(0, 2500),
      {
        stripTogetherWithTheirContents: [
          "script", // default
          "style", // default
          "xml", // default,
          "img",
          "a",
        ],
      }
    );
    company.company_business_details =
      result.length >= 2499
        ? `${result}.... [Visit www.orapages.com/${company.company_url} to Read More]`
        : result;
    const categoryy = company.categories.map((category) =>
      category.category_name
        .replace(/[^a-z0-9A-z]/g, "_")
        .replace(/_+$/, "")
        .replace(/_+/g, "_")
    );
    let message = `Company: ${company.company_business_name}
                    \nMobile: ${company.locations[0].mobile}
                    \nLocation: ${company.locations[0].complete_address}
                    \n${company.company_business_details}`;
    try {
      await insertSocialMedia(
        company.company_id,
        message,
        "www.orapages.com/" + company.company_url,
        categoryy
      );
      // console.log(company.company_id,company.company_url);
    } catch (error) {
      const query = `INSERT INTO facebook_posts(company_id) VALUES (?)`;
      await queryPromise(query, [company.company_id]);
      fse.writeFileSync("hi.txt", error + "\n\n\n\n");
    }
    // console.log(categoryy, message);
  }
};

module.exports = {
  passport,
  insertSocialMedia,
  insertSocialMediaUrl,
};
