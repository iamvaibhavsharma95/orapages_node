const fs = require('fs'),
    convert = require('xml-js'),
    hostBlogBaseURL = 'https://www.orapages.com',
    random = require('randomstring'),
    moment = require('moment'),
    {
        selectPromise,
        queryPromise
    } = require('../database/database'),
    options = {
        compact: true,
        ignoreComment: true,
        spaces: 4
    };

let untrackedUrlsList = [];
/* 
    Method to Fetch dynamic List of URLs from Rest API/DB 
*/
const fetchDataBaseUrl = async () => {

    const query = `SELECT company_id,company_url from company WHERE company_id not in (select company_id from sitemap_urls) limit 5000`;
    const urls = await queryPromise(query);
    
    if (urls.length > 0) {
        urls.forEach(url => {
            const modifiedURL = url.company_url;
            untrackedUrlsList.push({
                url: `${hostBlogBaseURL}/${modifiedURL}`,
                company_id: url.company_id
            });
        });
    }else{
        return;
    }

    const sitemapQuery = `select * from sitemaps order by date desc limit 1`;
    const lastSiteMap = await queryPromise(sitemapQuery);
    if (lastSiteMap.length > 0) {
        const lenthQuery = `select * from sitemap_urls where sitemap_id = '${lastSiteMap[0].id}'`;
        const lengthSiteMap = await queryPromise(lenthQuery);
        if (lengthSiteMap.length < 5000) {
            await filterUniqueURLs(lastSiteMap[0].id, lastSiteMap[0].name);
        } else {
            await filterUniqueURLs(lastSiteMap[0].id+1);
        }
    } else {
        await filterUniqueURLs(1);
    }
}

/* 
    Method to Filter/Unique already existing URLs and new urls we fetched from DB
*/
const filterUniqueURLs = async (last_file,sitemap_file_name = `${random.generate({length: 30,charset: 'hex'})}.xml`) => {
    fs.readFile('sitemaps/'+sitemap_file_name, async (err, data) => {
        let r;
        if (!!data) {
            r = data;
            if (!!data.toString()) {
                r = data.toString();
            }
        }
        if (!!r) {
            const existingSitemapList = JSON.parse(convert.xml2json(data, options));
            let existingSitemapURLStringList = [];
            if (existingSitemapList.urlset && existingSitemapList.urlset.url && existingSitemapList.urlset.url.length) {
                existingSitemapURLStringList = existingSitemapList.urlset.url.map(ele => ele.loc._text);
            }
            let untrackedDBUrlsList = [];
            untrackedUrlsList.forEach(ele => {
                if (existingSitemapURLStringList.indexOf(ele.url) == -1) {
                    untrackedDBUrlsList.push([ele.company_id, ele.url, last_file,ele.url, 'monthly', '0.5', moment(new Date()).format('YYYY-MM-DD')]);
                    existingSitemapList.urlset.url.push({
                        loc: {
                            _text: ele.url,
                        },
                        changefreq: {
                            _text: 'monthly'
                        },
                        priority: {
                            _text: 0.5
                        },
                        lastmod: {
                            _text: moment(new Date()).format('YYYY-MM-DD')
                        }
                    });
                }
            });
            await createSitemapFile(sitemap_file_name, untrackedDBUrlsList, existingSitemapList);
        } else {
            const query = `INSERT INTO sitemaps(id,name) VALUES (?,?)`;
            await queryPromise(query, [last_file, sitemap_file_name]);
            const existingSitemapList = {};
            existingSitemapList._declaration = {};
            existingSitemapList._declaration._attributes = {
                "version":"1.0",
                "encoding":"UTF-8"
            }
            existingSitemapList.urlset = {};
            existingSitemapList.urlset._attributes = {
                "xmlns":"http://www.sitemaps.org/schemas/sitemap/0.9",
                "xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance",
                "xsi:schemaLocation":`http://www.sitemaps.org/schemas/sitemap/0.9
                http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd`
            };
            existingSitemapList.urlset.url = [];
            let untrackedDBUrlsList = [];
            untrackedUrlsList.forEach(ele => {
                untrackedDBUrlsList.push([ele.company_id, ele.url, last_file, ele.url,'monthly', '0.5', moment(new Date()).format('YYYY-MM-DD')]);
                existingSitemapList.urlset.url.push({
                    loc: {
                        _text: ele.url,
                    },
                    changefreq: {
                        _text: 'monthly'
                    },
                    priority: {
                        _text: 0.5
                    },
                    lastmod: {
                        _text: moment(new Date()).format('YYYY-MM-DD')
                    }
                });

            });
            await createSitemapFile(sitemap_file_name, untrackedDBUrlsList, existingSitemapList);
        }
    });
}

/* 
    Method to convert JSON format data into XML format
*/
const createSitemapFile = async (sitemap_file_name, untrackedDBUrlsList, list, ) => {
    const finalXML = convert.json2xml(list, options); // to convert json text to xml text
    await saveNewSitemap(sitemap_file_name, untrackedDBUrlsList, finalXML);
}

/* 
    Method to Update sitemap.xml file content
*/
const saveNewSitemap = async (sitemap_file_name, untrackedDBUrlsList, xmltext) => {
    fs.writeFile("sitemaps/"+sitemap_file_name, xmltext, async (err) => {
        if (err) {
            return console.log(err);
        }
        untrackedUrlsList = [];
        const query = `insert into sitemap_urls (company_id, company_url, sitemap_id, loc, changefreq, priority, lastmod) values ?`;
        await queryPromise(query, [untrackedDBUrlsList]);
    });
}

module.exports = {
    fetchDataBaseUrl
};