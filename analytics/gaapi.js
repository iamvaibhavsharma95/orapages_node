const { google } = require("googleapis");
const scopes = "https://www.googleapis.com/auth/analytics.readonly";
const gaAuth = require("./../auth.json");

const jwt = new google.auth.JWT(
  gaAuth.client_email,
  null,
  gaAuth.private_key,
  scopes
);

const view_id = "214217217";

module.exports = {
  gaPageViews: async (company, startDate = "7", endDate = "yesterday") => {
    const response = await jwt.authorize();
    let config;
    if (parseInt(startDate) > 365) {
      config = {
        auth: jwt,
        ids: "ga:" + view_id,
        "start-date": `${startDate}daysAgo`,
        "end-date": endDate,
        dimensions: "ga:year",
        metrics: "ga:pageviews",
        filters: `ga:pagePath==/${company}`,
      };
    } else if (parseInt(startDate) > 90) {
      config = {
        auth: jwt,
        ids: "ga:" + view_id,
        "start-date": `${startDate}daysAgo`,
        "end-date": endDate,
        dimensions: "ga:yearMonth",
        metrics: "ga:pageviews",
        filters: `ga:pagePath==/${company}`,
      };
    } else {
      config = {
        auth: jwt,
        ids: "ga:" + view_id,
        "start-date": `${startDate}daysAgo`,
        "end-date": endDate,
        dimensions: "ga:date",
        metrics: "ga:pageviews",
        filters: `ga:pagePath==/${company}`,
      };
    }
    const result = await google.analytics("v3").data.ga.get(config);
    return result.data;
  },

  gaSessions: async (company, startDate = "7", endDate = "yesterday") => {
    const response = await jwt.authorize();
    let config;
    if (parseInt(startDate) > 365) {
      config = {
        auth: jwt,
        ids: "ga:" + view_id,
        "start-date": `${startDate}daysAgo`,
        "end-date": endDate,
        dimensions: "ga:year",
        metrics: "ga:sessions",
        filters: `ga:pagePath==/${company}`,
      };
    } else if (parseInt(startDate) > 90) {
      config = {
        auth: jwt,
        ids: "ga:" + view_id,
        "start-date": `${startDate}daysAgo`,
        "end-date": endDate,
        dimensions: "ga:yearMonth",
        metrics: "ga:sessions",
        filters: `ga:pagePath==/${company}`,
      };
    } else {
      config = {
        auth: jwt,
        ids: "ga:" + view_id,
        "start-date": `${startDate}daysAgo`,
        "end-date": endDate,
        dimensions: "ga:date",
        metrics: "ga:sessions",
        filters: `ga:pagePath==/${company}`,
      };
    }
    const result = await google.analytics("v3").data.ga.get(config);

    return result.data;
  },

  gaBounceRate: async (company, startDate = "7", endDate = "yesterday") => {
    const response = await jwt.authorize();
    const result = await google.analytics("v3").data.ga.get({
      auth: jwt,
      ids: "ga:" + view_id,
      "start-date": `${startDate}daysAgo`,
      "end-date": endDate,
      dimensions: "ga:date",
      metrics: "ga:bounceRate",
      filters: `ga:pagePath==/${company}`,
    });

    return result.data;
  },

  gaUsers: async (company, startDate = "7", endDate = "yesterday") => {
    const response = await jwt.authorize();
    let config;
    if (parseInt(startDate) > 365) {
      config = {
        auth: jwt,
        ids: "ga:" + view_id,
        "start-date": `${startDate}daysAgo`,
        "end-date": endDate,
        dimensions: "ga:year",
        metrics: "ga:users",
        filters: `ga:pagePath==/${company}`,
      };
    } else if (parseInt(startDate) > 90) {
      config = {
        auth: jwt,
        ids: "ga:" + view_id,
        "start-date": `${startDate}daysAgo`,
        "end-date": endDate,
        dimensions: "ga:yearMonth",
        metrics: "ga:users",
        filters: `ga:pagePath==/${company}`,
      };
    } else {
      config = {
        auth: jwt,
        ids: "ga:" + view_id,
        "start-date": `${startDate}daysAgo`,
        "end-date": endDate,
        dimensions: "ga:date",
        metrics: "ga:users",
        filters: `ga:pagePath==/${company}`,
      };
    }
    const result = await google.analytics("v3").data.ga.get(config);

    return result.data;
  },

  gaCity: async (company, startDate = "7", endDate = "yesterday") => {
    const response = await jwt.authorize();
    const result = await google.analytics("v3").data.ga.get({
      auth: jwt,
      ids: "ga:" + view_id,
      "start-date": `${startDate}daysAgo`,
      "end-date": endDate,
      dimensions: "ga:country,ga:city",
      metrics: "ga:pageviews",
      sort: "-ga:pageviews",
      filters: `ga:pagePath==/${company}`,
    });

    return result.data;
  },

  gaCountry: async (company, startDate = "7", endDate = "yesterday") => {
    const response = await jwt.authorize();
    const result = await google.analytics("v3").data.ga.get({
      auth: jwt,
      ids: "ga:" + view_id,
      "start-date": `${startDate}daysAgo`,
      "end-date": endDate,
      dimensions: "ga:country",
      metrics: "ga:pageviews",
      sort: "-ga:pageviews",
      filters: `ga:pagePath==/${company}`,
    });

    return result.data;
  },

  convertDate: (dateString) => {
    const monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];
    if (dateString.length == 4) {
      return dateString;
    }
    if (dateString.length == 6) {
      let year = dateString.substring(0, 4);
      let month = dateString.substring(4, 6);
      return `${monthNames[parseInt(month) - 1]} ${year}`;
    }
    let year = dateString.substring(0, 4);
    let month = dateString.substring(4, 6);
    let day = dateString.substring(6, 8);

    return `${day} ${monthNames[parseInt(month) - 1]}`;
  },

  getRandomColor: () => {
    let letters = "0123456789ABCDEF";
    let color = "#";
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  },
};
// module.exports = {gaCity,gaPageViews,gaSessions,gaBounceRate,gaUsers,gaCountry,convertDate,getRandomColor};
